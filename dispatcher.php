<?php

class Dispatcher
{

    private $request;

    public function dispatch()
    {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);

        $controller = $this->loadController();
		$controllerclass = '';

	    if (!empty($controller)) 
		{
			if(isset($this->request->action)) // -- no action dispatch normal
			{
			if(method_exists($controller,$this->request->action))
			{
				call_user_func_array([$controller, $this->request->action], $this->request->params);
			}
			else{$this->defaultdispatch();	}
			}
			else{$this->defaultdispatch();	}
		}
		else
		{
			// -- Default to login page.
			//$controllerclass = 'login';
			//$controller = $this->loadControllerCustom($controllerclass);
			//call_user_func_array([$controller, 'index'], $this->request->params);
			$this->defaultdispatch();			
		}
	}
	
	public function defaultdispatch()
	{
		// -- Default to login page.
			$controllerclass = 'login';
			$controller = $this->loadControllerCustom($controllerclass);
			call_user_func_array([$controller, 'index'], $this->request->params);
	}

    public function loadController()
    {
        $name = $this->request->controller . "Controller";
        $file = ROOT . 'Controllers/' . $name . '.php';
		$controller = null;
		if(file_exists($file))
		{require($file);
			$controller = new $name();
		}
        return $controller;
    }

	public function loadControllerCustom($controllerclass)
    {
        $name = $controllerclass . "Controller";
        $file = ROOT . 'Controllers/' . $name . '.php';
		$controller = null;
			
		if(file_exists($file))
		{
			require($file);
			        $controller = new $name();

		}
	
        return $controller;
    }
}
?>