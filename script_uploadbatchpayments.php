<?php
header("Content-Type: text/html; charset=utf-8");
error_reporting( E_ALL );
ini_set('display_errors', 1);
		
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
	
//////////////////////////////////////////////////////////////////////// 
if(!function_exists('extractcsvfile'))
{
	function extractcsvfile($keydata)
	{
			$filerootpath = filerootpath();//str_replace('content','',filerootpath());
			require  $filerootpath.'/config/db.php';			
			require  $filerootpath.'/core/Model.php';
			require  $filerootpath.'/models/payment.php';
			require  $filerootpath.'/models/invoice.php';
			
			// -- Object Payment.
			$payment  = new payment();
			$invoice  = new invoice();
			
			$keydataJson = array();
			$message 	 = '';
			$notificationsList = null;
			$anySuccess  = '';
			
			// -- Upload payments for invoices
			$INVNumber = 0;
			$Status    = '';
			$Paid      = 0;
			$notes	   = '';
			
			try{
							$csvdata = $keydata;
							// -- UTF-8
							$keydata = json_encode($keydata,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
							if(empty($keydata))
							{
								// -- ANSI
								$keydata = $csvdata;//$csvdata[$x];
							}
							else
							{
								$keydataJson = json_decode($keydata,true);
							}
							
							$TotalColmn = 17;
							$index = 0;
							
							// -- Is to avoid unicharacters e.g. \ff
							if(!empty($keydataJson))
							{
							$keydata = array();
								foreach($keydataJson as $item => $v) 
								{ 
								//print_r($v);
								//print('<br/>');
								// -- Trim trailing & leading spaces.
									$v = trim($v);
									//$v = mysql_real_escape_string($v);
									if($index == 1){$keydata['invoice_id'] = $v;}
									if($index == 9){$keydata['payment_amount'] = $v;}
									if($index == 10){$keydata['payment_date'] = $v;}
									if($index == 11){$keydata['payment_notes'] = $v;}
									if($index == 6){$keydata['status'] = $v;}
									$index = $index  + 1;
								}
							}	
							// -- if empty payment date, default today.
							if(isset($keydata['payment_date']))
							{
								if(empty($keydata['payment_date']))
								{
									 $keydata['payment_date'] = date('Y-m-d');
								}
							}
							else
							{
								 $keydata['payment_date'] = date('Y-m-d');								
							}
							// -- EFT 							
							// -- NAEDO 
							// -- UTF-8
							//print_r($keydata);
							$message = addpayment($keydata,$payment,$invoice); 
						// -- Add Message to begin of an array.	
						//array_unshift($keydata,$message);

			}
			catch (Exception $e) 
			{
				$message = $e->getMessage();
				if($message == "Could not execute: /var/qmail/bin/sendmail\n")
				{ 
					$message = 'ERROR:Could not sent an email.';
				}
			}
							return $message;
	}
}
// -- Scan the File Convert all record to UTF-8
if(!function_exists('scanfile_to_UTF8'))	
{	
   function scanfile_to_UTF8($data)
   {
	   	$data = iconv('UTF-8', 'ASCII//IGNORE', $data);
		$data = trim($data);
	    return $data;
   }
}
// -- Add Payment.
if(!function_exists('addpayment'))
{
	function addpayment($data,$payment,$invoice)
	{
		$idPayment = array();
		if($data["payment_amount"] != '0.00')
		{
			$idPayment = $payment->create($data["invoice_id"], $data["payment_date"],$data["payment_amount"],$data["payment_notes"]);
		}
		$idPayment['feedback'] = 'success';
		// -- success.
		if (isset($idPayment['id']) > 0)
		{
			$idPayment['feedback'] = 'success';
			$status = '0';
			$statusTxt = strtolower($data["status"]);
			switch ($statusTxt) 
					{
					case "draft":
						$status   = '0';
						break;
					case "closed":
						$status  = '1';
						break;
					case "opened":
						$status  = '2';
						break;
					case "overdue":
						$status = '3';
						break;
					default:
						$status = '2';
					}
			$invoice->updateStatus($data["invoice_id"],$status);					
		}
		else
		{
			$idPayment['feedback'] = 'ERROR';
		}
		
		if($data["payment_amount"] == '0.00')
		{
			$idPayment['feedback'] = 'no payment';	
		}
		return $idPayment['feedback'];
	}
}
// -- POST VALUES validations & assignment.
// -- Check if Charges Exist.
if(!function_exists('getpost'))
{
	function getpost($name)
	{
		$value = '';
		if(isset($_POST[$name]))
		{
			$value = $_POST[$name];
		}

		return $value;
	}
}
////////////////////////////////////////////////////////////////////////
// -- https://www.php.net/manual/en/function.iconv.php
if(!function_exists('getCsvDelimiter'))	
{	
   function getCsvDelimiter($filePath)
   {
	  $checkLines = 1;
      $delimiters =[',', ';', '\t'];

      $default =',';

       $fileObject = new \SplFileObject($filePath);
       $results = [];
       $counter = 0;
       while ($fileObject->valid() && $counter <= $checkLines) {
           $line = $fileObject->fgets();
           foreach ($delimiters as $delimiter) {
               $fields = explode($delimiter, $line);
               $totalFields = count($fields);
               if ($totalFields > 1) {
                   if (!empty($results[$delimiter])) {
                       $results[$delimiter] += $totalFields;
                   } else {
                       $results[$delimiter] = $totalFields;
                   }
               }
           }
           $counter++;
       }
       if (!empty($results)) {
           $results = array_keys($results, max($results));

           return $results[0];
       }
return $default;
}
}

// -- Move Uploaded File -- //
if(!function_exists('moveuploadedfile'))
{
	function moveuploadedfile($name,$tmp_name )
	{
			$today = date('Y-m-d H-i-s');
			$userid = getpost('userid');
			$path = "Files/Payments/".$userid."_".$today."_"; // Upload directory 
			$count = 0;
		 // No error found! Move uploaded files 
		 // Filename add datetime - extension.
			//$name = date('d-m-Y-H-i-s').$name;
			//$ext = pathinfo($name, PATHINFO_EXTENSION);
			// -- Encrypt The filename.
			//$name = md5($name).'.'.$ext;
			
		 // -- Update File Name Directory	
	            if(move_uploaded_file($tmp_name , $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }
			$newfilename = $path.$name;
			return 	$count.$newfilename ;

	}
}

//If you need a function which converts a string array into a utf8 encoded string array then this function might be useful for you:
if(!function_exists('utf8_string_array_encode'))	
{	

function utf8_string_array_encode($array){
    $func = function(&$value,&$key){
        if(is_string($value)){
            $value = utf8_encode($value);
        }
        if(is_string($key)){
            $key = utf8_encode($key);
        }
        if(is_array($value)){
            utf8_string_array_encode($value);
        }
    };
    array_walk($array,$func);
    return $array;
}
}
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}

if(!function_exists('uploadcsvfile'))
{
	function uploadcsvfile()
	{
		$output = null;
		$column = null;
		$row_data = null;
		$message = '';
		$tmp_name    = '';
		$separator = '';
		try{
		$tmp_name = $_FILES['csv_file']['tmp_name'];
		$cachedData = '';//etmasterdata();	
		if(!empty($tmp_name))
		{
		// -- Backuploaded file for history & tracking
		 $name = $_FILES['csv_file']['name'];	
		 
		 $separator = getCsvDelimiter($tmp_name); 
		 $file_data = fopen($tmp_name, 'r');
		 $column = fgetcsv($file_data,0,$separator);	 		 
		 array_unshift($column,'Feedback');	
 		 $colCount    = count($column) - 1; 
		 $colCountAft = count($column); 
		 array_unshift($column,'Count');	
		 $rowCount = 1;
		 while($row = fgetcsv($file_data,0,$separator))
		 {
			// -- Scan the File Convert all record to UTF-8
			for($x=0;$x<$colCount;$x++ )
			{
				 $row[$x] = scanfile_to_UTF8($row[$x]);
			}	
			$message = extractcsvfile($row);
			array_unshift($row,$message);	
			array_unshift($row,$rowCount);	
			
			/* -- Re-Scan the File Convert all record to UTF-8
			for($x=0;$x<$colCountAft;$x++ )
			{
				 $row[$x] = scanfile_to_UTF8($row[$x]);
			}*/	
			$rowCount = $rowCount + 1;
			$row_data[] = $row;
		 }
		 
		 $output = array('column'  => $column,'row_data'  => $row_data);
		 $filenameEncryp = moveuploadedfile($name,$tmp_name);		
		 $output = json_encode($output);
		}
		}
		catch (Exception $e) 
			{
				$message = $e->getMessage();
				$row_data[] = $message;
				$output = array('column'  => $column,'row_data'  => $row_data);
			}
		
		return $output;//json_encode(fgetcsv($file_data,0,";"));//$output ;
	}
}

ob_start ();
$output = uploadcsvfile();
//$UserCode	 = $_FILES['csv_file']['name'];//getpost('UserCode');
ob_end_clean();
if($output == "SQLSTATE[HY000] [2002] Only one usage of each socket address (protocol/network address/port) is normally permitted.\r\n")
{
	$column[] = 'Feedback';
	$row_data[] = $output; 
	$output = array('column'  => $column,'row_data'  => $row_data);
	$output = json_encode($output);
}	
echo $output;
?>