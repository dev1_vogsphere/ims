function displayEvent(value)
{
	alert(value);
}
// -- Add Taxes.
function add_taxes()
{
	var table=document.getElementById("invoice-taxes");
	var table_len=(table.rows.length);
var row ="<tr>"
		+"<td>"
		+"<a href='#' class='btn' onclick='delete_tax("+table_len+")'><span class='glyphicon glyphicon-trash'></span></a>"
		+"</td>                                                                                              "
		+"<td><div class='form-group'>                                                                       "
		+"<label class='control-label' for='taxes_name'>Name</label>                                         "
		+"<input type='text' id='taxes_name_"+table_len+"' name='taxes_name_"+table_len+"' class='form-control' />                   "
		+"</div>                                                                                             "
		+"</td>                                                                                              "
		+"<td>                                                                                               "
		+"<div class='form-group'>                                                                           "
		+"<label class='control-label' for='taxes_name_value'>Value</label>                                  "
		+"<input type='text' id='taxes_value_"+table_len+"' name='taxes_value_"+table_len+"'  class='form-control' />                "
		+"</div>                                                                                             "
		+"</td>                                                                                              "
		+"<td>                                                                                               "
		+"<div class='form-group'>                                                                           "
		+"<div class='checkbox'>                                                                             "
		+"<label>                                                                                            "
		+"<input type='checkbox' id='taxes_active_"+table_len+"' name='taxes_active_"+table_len+"' value='1' />Active</label>        "
		+"</div>                                                                                             "
		+"</div>                                                                                             "
		+"</td>                                                                                              "
		+"<td>                                                                                               "
		+"<div class='form-group'>"
		+"<div class='checkbox'><label><input type='checkbox' id='taxes_is_default_"+table_len+"' name='taxes_is_default_"+table_len+"' value='1' /> Is default</label></div>"
		+"</div>"
		+"</td> "
		+"<td>"
		+"<div class='form-group'>                                                                           "
		+"<label class='control-label' for='taxes_id_"+table_len+"'>#id</label>                                  "
		+"<input type='text' id='taxes_id_"+table_len+"' name='taxes_id_"+table_len+"'  class='form-control' />                "
		+"</div>                                                                                             "
		+"</td>"
		+"</tr> ";
		LineCount = parseInt(document.getElementById("taxCount").value) + 1;
		document.getElementById("taxCount").value = LineCount;
		return row;
}

// -- Add Series.
function add_series()
{
	var table=document.getElementById("invoice-series");
	var table_len=(table.rows.length);
 var row = "<tr>                                                                                                                            "
	 	+"<td>                                                                                                                            "
		+"<a href='#' class='btn' onclick='delete_series("+table_len+")'><span class='glyphicon glyphicon-trash'></span></a>                          "
		+"</td>                                                                                                                           "
		+"<td>	                                                                                                                          "
		+"<div class='form-group'>                                                                                                        "
		+"<label class='control-label required' for='series_name_"+table_len+"'>Name</label>                                                          "
		+"<input type='text' id='series_name_"+table_len+"' name='series_name_"+table_len+"' required='required' class='form-control' />          "
		+"</div>                                                                                                                          "
		+"</td>                                                                                                                           "
		+"<td>	                                                                                                                          "
		+"<div class='form-group'><label class='control-label required' for='series_value_"+table_len+"'>Value</label>                                "
		+"<input type='text' id='series_value_"+table_len+"' name='series_value_"+table_len+"' required='required' class='form-control' />           "
		+"</div>                                                                                                                          "
		+"</td>	                                                                                                                          "
		+"<td>	                                                                                                                          "
		+"<div class='form-group'><label class='control-label required' for='series_first_number_"+table_len+"'>First number</label>                  "
		+"<input type='text' id='series_first_number_"+table_len+"' name='series_first_number_"+table_len+"' required='required' class='form-control' />"
		+"</div>                                                                                                                          "
		+"</td>	                                                                                                                          "
		+"<td>	                                                                                                                          "
		+"<div class='form-group'>                                                                                                        "
		+"<div class='checkbox'>                                                                                                          "
		+"<label class='required'>                                                                                                        "
		+"<input type='checkbox' id='series_enabled_"+table_len+"' name='series_enabled_"+table_len+"' required='required' /> Enabled</label>"
		+"</div>"
	    +"</div>"
		+"</td>	"
		+"<td>"
		+"<div class='form-group'>                                                                           "
		+"<label class='control-label' for='series_id_"+table_len+"'>#id</label>                                  "
		+"<input type='text' id='series_id_"+table_len+"' name='series_id_"+table_len+"'  class='form-control' />                "
		+"</div>                                                                                             "
		+"</td>"
	    +"</tr> ";
		
		LineCount = parseInt(document.getElementById("seriesCount").value) + 1;
		document.getElementById("seriesCount").value = LineCount;
		return row;
}

function add_row()
{
 //var new_name=document.getElementById("new_name").value;
 //var new_country=document.getElementById("new_country").value;
 //var new_age=document.getElementById("new_age").value;
 //alert("HERE");
 //+document.getElementById("invoice_items_0_taxes").innerHTML
 var currency = 'R';	
 var table=document.getElementById("invoice-like-items");
 var table_len=(table.rows.length)-1;
 var rowSize = (table.rows.length);

 var taxhtml = '<option value="0.00" seleted>Choose</option><option data-taxid="1" value="15">SARS</option';
 if((document.getElementById("invoice_items_0_taxes")))
 {
 	taxhtml = document.getElementById("invoice_items_0_taxes").innerHTML;
 }
	
 var row ="<tr class='edit-item-row' id='row"+table_len+"'>"
	  +"<td class='btn-group-xs'>"
	  +"<button class='btn btn-default btn-xs' type='button' onclick='delete_row("+table_len+")'><span class='glyphicon glyphicon-trash'></span></button>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<input type='text' id='invoice_items_"+table_len+"_product' name='invoice_items_"+table_len+"_product' class='product-autocomplete-name form-control ui-autocomplete-input' autocomplete='off'/>"
      +"</td>"
	  +"<td>"
      +"<input type='text' id='invoice_items_"+table_len+"_description' name='invoice_items_"+table_len+"_description' required='required' class='product-autocomplete-description form-control' />"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<input type='text' id='invoice_items_"+table_len+"_quantity' name='invoice_items_"+table_len+"_quantity' required='required' class='form-control' value='1' onchange='line_total("+table_len+")'/>"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'>"
	  //+"<span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='text' id='invoice_items_"+table_len+"_unitary_cost' name='invoice_items_"+table_len+"_unitary_cost' required='required' class='form-control' onchange='line_total("+table_len+")'/></div>"  
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'><input type='text' id='invoice_items_"+table_len+"_discount_percent' name='invoice_items_"+table_len+"_discount_percent' required='required' class='form-control' value='0.00' onchange='line_total("+table_len+")'/>"
	  +"</div>" 
	  +"</td>"
	  +"<td class='taxes form-inline'>"
	  +"<select id='invoice_items_"+table_len+"_taxes' name='invoice_items_"+table_len+"_taxes' class='form-control' size='1' onChange='line_total("+table_len+")'>"
	  +taxhtml
	  +"</select>"
	  +"</td>"
	  +"<td class='cell-align-right item-gross-amount'>"
	  +"<div class='input-group'><span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='text' id='invoice_items_"+table_len+"_totals' name='invoice_items_"+table_len+"_totals' required='required' class='form-control' value='0.00'"
	  +"readonly />    </div>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<div class='input-group'><input type='text' id='invoice_items_"+table_len+"_tax_id' name='invoice_items_"+table_len+"_tax_id' class='product-autocomplete-name form-control' value='' readonly />"
	  +"</div></td>"
	  +"</tr>";
	  	LineCount = parseInt(document.getElementById("invoiceItemsCount").value) + 1;
document.getElementById("invoiceItemsCount").value = LineCount;
	  //alert(document.getElementById("invoice_items_0_taxes").innerHTML);
	  
 //document.getElementById("new_name").value="";
 //document.getElementById("new_country").value="";
 //document.getElementById("new_age").value="";
 
//     $("#invoice-like-items > tbody").append(row);      // Append the new elements 
	 
	 return row;
}

function delete_row(no)
{
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value) - 1;
	document.getElementById("invoiceItemsCount").value = LineCount;
	line_total(no);
	// -- Disable Item Remover for now
	// -- Need to rethink the logic.
	//document.getElementById("row"+no+"").outerHTML="";
}

function delete_tax(no)
{
	//LineCount = parseInt(document.getElementById("invoiceItemsCount").value) - 1;
	//document.getElementById("invoiceItemsCount").value = LineCount;
	//line_total(no);
	// -- Disable Item Remover for now
	// -- Need to rethink the logic.
	//document.getElementById("row"+no+"").outerHTML="";
}

function delete_series(no)
{
	//LineCount = parseInt(document.getElementById("invoiceItemsCount").value) - 1;
	//document.getElementById("invoiceItemsCount").value = LineCount;
	//line_total(no);
	// -- Disable Item Remover for now
	// -- Need to rethink the logic.
	//document.getElementById("row"+no+"").outerHTML="";
}

function line_total(no)
{
	// - Declarations
	var Linetotal 			= 0.00;
	var LineCount			= 0;
	var subtotal 		    = 0.00;
	var Total				= 0.00;
	var tax 			    = 0.00;
	var Totaltax 			= 0.00;
	var LinetotalIter 		= 0.00;		

	/*Add Qty multiply price */
	//alert("Debug 1");
	var qty 	  		    = parseInt(document.getElementById("invoice_items_"+no+"_quantity").value);
	if(isNaN(qty)){qty = 1;}
	//alert("Debug qty");
	var unitprice 			= parseFloat(document.getElementById("invoice_items_"+no+"_unitary_cost").value);
	if(isNaN(unitprice)){unitprice = parseFloat(0.00).toFixed(2);}else{unitprice = parseFloat(unitprice).toFixed(2);}

	//alert("Debug unitprice");
	var discountPercentage  = parseFloat(document.getElementById("invoice_items_"+no+"_discount_percent").value);
	if(isNaN(discountPercentage)){discountPercentage = parseFloat(0.00).toFixed(2);}else{discountPercentage = parseFloat(discountPercentage).toFixed(2);}

	// Tax
	//tax = parseFloat(document.getElementById("invoice_items_"+no+"_taxes").value);
	
	//if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}
	//alert('tax : '+tax);	

	Linetotal = (qty * unitprice);

	// -- discount
	Linetotal = Linetotal - (discountPercentage / 100) * Linetotal;
	
	// -- Add tax on discounted amount.
	//	Linetotal = Linetotal - (tax / 100) * Linetotal;

			//alert('Linetotal = '+Linetotal+'+'+tax+'/ 100'+'*'+ Linetotal);

	if(isNaN(Linetotal)){Linetotal = parseFloat(0.00).toFixed(2);}else{Linetotal = parseFloat(Linetotal).toFixed(2);}
	
	document.getElementById("invoice_items_"+no+"_totals").value = Linetotal;
	
	// -- How many Item on the added add the subtotal for their line items.
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value);
//	alert("Debug LineCount" + LineCount);

	if(LineCount > 0)
	{var iter = 0;
//	alert(LineCount);

		for (iter = 0; iter < LineCount; iter++)
		{
			// Tax
			tax = parseFloat(document.getElementById("invoice_items_"+iter+"_taxes").value);
			if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}

			// -- Linetotal
			LinetotalIter = parseFloat(document.getElementById("invoice_items_"+iter+"_totals").value);
			if(isNaN(LinetotalIter)){LinetotalIter = parseFloat(0.00).toFixed(2);}else{LinetotalIter = parseFloat(LinetotalIter).toFixed(2);}

			// -- Subtotal
			subtotal = parseFloat(subtotal) + parseFloat(LinetotalIter);
			if(isNaN(subtotal)){subtotal = parseFloat(0.00).toFixed(2);}else{subtotal = parseFloat(subtotal).toFixed(2);}
				//alert('subtotal : '+subtotal);
	//		alert('Totaltax = '+Totaltax+"+"+tax+"/ 100 * "+LinetotalIter);	

			// -- Total Tax
			Totaltax = parseFloat(Totaltax) + parseFloat((tax / 100)) * parseFloat(LinetotalIter);
			
			if(isNaN(Totaltax)){Totaltax = parseFloat(0.00).toFixed(2);}else{Totaltax = parseFloat(Totaltax).toFixed(2);}

			// -- Total inclusive of Tax
					Total = parseFloat(subtotal) + parseFloat(Totaltax);
					Total = parseFloat(Total).toFixed(2);
					
			// -- Get the Tax ID					
		}
		
		//if (isNaN(subtotal)) subtotal = 0.00;
		document.getElementById("Subtotal").value = subtotal;
		
		// -- Taxes
		//alert(tax);
		document.getElementById("taxamount").value = Totaltax;

		// -- Total inclusive of Tax
		document.getElementById("grossamount").value = Total;

jQuery("#invoice_items_"+no+"_taxes").on("change",function()
{
    var dataid = $("#invoice_items_"+no+"_taxes option:selected").attr('data-taxid');
	var element_tax_item = document.getElementById("invoice_items_"+no+"_tax_id");//tax_id
				//alert(dataid+' '+element_tax_item.toString());

	if (typeof(element_tax_item) != 'undefined' && element_tax_item != null)
	{

		element_tax_item.value = dataid;
	}
    
});

	}
}

/* Begin of - Database BackedUp Scripts */
function BackUpDatabase() 
{ 
	path = document.getElementById("path").value;
	email = document.getElementById("email").value;
btnEmailBackup = document.getElementById("btnEmailBackup").value;
btnRunBackup = document.getElementById("btnRunBackup").value;

	jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"/ims/backup/databasebackup.php",
					data:{path:path,email:email,btnRunBackup:btnRunBackup,btnEmailBackup:btnEmailBackup},
					success: function(data)
					{
					 var Message = "<p>Database BackedUp Succesfully - "+data+"!</p>";
					 jQuery("#divSuccess").html(Message);
					 		move();
				    }
				});
			});
}

jQuery(function() {
    //----- OPEN
    jQuery('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
function arraryofindexes(invoiceItemsCount)
  {
			indexes = new Array();
			count 			= 0;
			totalIndex 		= 0;
			
			// While havent found all indexes.
			while(totalIndex != invoiceItemsCount)
			{
				value = "invoice_items_"+count+"_description";
				if(document.getElementById(value))
				{
					// -- count number of items
				   indexes[totalIndex] = count;
				   totalIndex = totalIndex + 1;
				}
				count = count + 1;
			}
	   return indexes;
  }
  
  function add_row2()
{
 //var new_name=document.getElementById("new_name").value;
 //var new_country=document.getElementById("new_country").value;
 //var new_age=document.getElementById("new_age").value;

 var currency = 'R';	
 var table=document.getElementById("invoice-like-items");
 var table_len=(table.rows.length)-1;
 if(table_len < 0)
 {
	 table_len = 0;
 }
 // alert(table_len);
  var rowSize = (table.rows.length);
  var taxhtml = '<option value="0.00" selected>Choose</option><option data-taxid="1" value="15">SARS</option';
//alert(typeof document.getElementById("invoice_items_0_taxes").value);

	if((document.getElementById("invoice_items_0_taxes")))
	{
		//alert(typeof document.getElementById("invoice_items_0_taxes"));
		taxhtml = document.getElementById("invoice_items_0_taxes").innerHTML;
	}

 var row ="<tr class='edit-item-row' id='row"+table_len+"'>"
	  +"<td class='btn-group-xs'>"
	  +"<button class='btn btn-default btn-xs' type='button' onclick='delete_row2("+table_len+")'><span class='glyphicon glyphicon-trash'></span></button>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<input type='text' id='invoice_items_"+table_len+"_product' name='invoice_items_"+table_len+"_product' class='product-autocomplete-name form-control ui-autocomplete-input' autocomplete='off'/>"  
      +"</td>"
	  +"<td>"
      +"<input type='text' id='invoice_items_"+table_len+"_description' name='invoice_items_"+table_len+"_description' required='required' class='product-autocomplete-description form-control' />"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<input type='number' id='invoice_items_"+table_len+"_quantity' name='invoice_items_"+table_len+"_quantity' required='required' class='form-control' value='1' onchange='line_total("+table_len+")'/>"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'>"
	  //+"<span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='number' step='0.01' id='invoice_items_"+table_len+"_unitary_cost' name='invoice_items_"+table_len+"_unitary_cost' required='required' class='form-control' step='0.01' value='0.00' onchange='line_total("+table_len+")'/></div>"  
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'><input step='0.01' type='number' id='invoice_items_"+table_len+"_discount_percent' name='invoice_items_"+table_len+"_discount_percent' required='required' class='form-control' value='0.00' onchange='line_total("+table_len+")'/>"
	  +"</div>" 
	  +"</td>"
	  +"<td class='taxes form-inline'>"
	  +"<select id='invoice_items_"+table_len+"_taxes' name='invoice_items_"+table_len+"_taxes' class='form-control' size='1' onChange='line_total("+table_len+")'>"
	  +taxhtml
	  +"</select>"
	  +"</td>"
	  +"<td class='cell-align-right item-gross-amount'>"
	  +"<div class='input-group'><span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='text' id='invoice_items_"+table_len+"_totals' name='invoice_items_"+table_len+"_totals' required='required' class='form-control' value='0.00'"
	  +"readonly />    </div>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<div class='input-group'><input type='text' id='invoice_items_"+table_len+"_tax_id' name='invoice_items_"+table_len+"_tax_id' class='product-autocomplete-name form-control' value='' readonly />"
	  +"</div></td>"
	  +"</tr>";
	  LineCount = parseInt(document.getElementById("invoiceItemsCount").value) + 1;
	  document.getElementById("invoiceItemsCount").value = LineCount;
	  rowCount = parseInt(document.getElementById("rowscount").value) + 1;
	  document.getElementById("rowscount").value = rowCount;
	 return row;
}

function delete_row2(no)
{
	//alert(no);
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value) - 1;
	document.getElementById("invoiceItemsCount").value = LineCount;
	//line_total(no);
	// -- Disable Item Remover for now
	// -- Need to rethink the logic.
	document.getElementById("row"+no+"").outerHTML="";
	rowCount = parseInt(document.getElementById("rowscount").value) - 1;
	if(rowCount < 0)
	{
		rowCount = 0;
	}
	document.getElementById("rowscount").value = rowCount;
	
	// -- Determined index if there deletione e.t.c sequence would have changed.
			totalIndex = 0;
			count      = 0;
			
			indexes = arraryofindexes(rowCount);
			
			// -- Loop through all the item & recalculate
			//alert(indexes);
			// -- Reinitialise Totals
			totalIndex = indexes.length;
			
			document.getElementById('Subtotal').value = '0.00';
			document.getElementById('taxamount').value = '0.00';
			document.getElementById('grossamount').value = '0.00';
			for(i=0;i<totalIndex;i++)
			{
				//alert(arraryofindexes[i]);
					line_total2(indexes[i],indexes);
			}
}

function line_total2(no,arraryofindexes)
{
	// - Declarations
	var Linetotal 			= 0.00;
	var LineCount			= 0;
	var subtotal 		    = 0.00;
	var Total				= 0.00;
	var tax 			    = 0.00;
	var Totaltax 			= 0.00;
	var LinetotalIter 		= 0.00;		

	/*Add Qty multiply price */
	//alert("Debug 1");
	var qty 	  		    = parseInt(document.getElementById("invoice_items_"+no+"_quantity").value);
	if(isNaN(qty)){qty = 1;}
	//alert("Debug qty");
	var unitprice 			= parseFloat(document.getElementById("invoice_items_"+no+"_unitary_cost").value);
	if(isNaN(unitprice)){unitprice = parseFloat(0.00).toFixed(2);}else{unitprice = parseFloat(unitprice).toFixed(2);}

	//alert("Debug unitprice");
	var discountPercentage  = parseFloat(document.getElementById("invoice_items_"+no+"_discount_percent").value);
	if(isNaN(discountPercentage)){discountPercentage = parseFloat(0.00).toFixed(2);}else{discountPercentage = parseFloat(discountPercentage).toFixed(2);}

	// Tax
	//tax = parseFloat(document.getElementById("invoice_items_"+no+"_taxes").value);
	
	//if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}
	//alert('tax : '+tax);	

	Linetotal = (qty * unitprice);

	// -- discount
	Linetotal = Linetotal - (discountPercentage / 100) * Linetotal;
	
	// -- Add tax on discounted amount.
	//	Linetotal = Linetotal - (tax / 100) * Linetotal;

			//alert('Linetotal = '+Linetotal+'+'+tax+'/ 100'+'*'+ Linetotal);

	if(isNaN(Linetotal)){Linetotal = parseFloat(0.00).toFixed(2);}else{Linetotal = parseFloat(Linetotal).toFixed(2);}
	
	document.getElementById("invoice_items_"+no+"_totals").value = Linetotal;
	
	// -- How many Item on the added add the subtotal for their line items.
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value);
//	alert("Debug LineCount" + LineCount);

	if(LineCount > 0)
	{	var iter = 0;
		var iter2 = 0;
//	alert(LineCount);

		for (iter = 0; iter < LineCount; iter++)
		{
			// Tax
			iter2 = arraryofindexes[iter];
			//alert("iter : "+iter+" LineCount = "+LineCount);
			tax = parseFloat(document.getElementById("invoice_items_"+iter2 +"_taxes").value);
			if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}

			// -- Linetotal
			LinetotalIter = parseFloat(document.getElementById("invoice_items_"+iter2 +"_totals").value);
			if(isNaN(LinetotalIter)){LinetotalIter = parseFloat(0.00).toFixed(2);}else{LinetotalIter = parseFloat(LinetotalIter).toFixed(2);}

			// -- Subtotal
			subtotal = parseFloat(subtotal) + parseFloat(LinetotalIter);
			if(isNaN(subtotal)){subtotal = parseFloat(0.00).toFixed(2);}else{subtotal = parseFloat(subtotal).toFixed(2);}
				//alert('subtotal : '+subtotal);
	//		alert('Totaltax = '+Totaltax+"+"+tax+"/ 100 * "+LinetotalIter);	

			// -- Total Tax
			Totaltax = parseFloat(Totaltax) + parseFloat((tax / 100)) * parseFloat(LinetotalIter);
			
			if(isNaN(Totaltax)){Totaltax = parseFloat(0.00).toFixed(2);}else{Totaltax = parseFloat(Totaltax).toFixed(2);}

			// -- Total inclusive of Tax
					Total = parseFloat(subtotal) + parseFloat(Totaltax);
					Total = parseFloat(Total).toFixed(2);
					
			// -- Get the Tax ID					
		}
		
		//if (isNaN(subtotal)) subtotal = 0.00;
		document.getElementById("Subtotal").value = subtotal;
		
		// -- Taxes
		//alert(tax);
		document.getElementById("taxamount").value = Totaltax;

		// -- Total inclusive of Tax
		document.getElementById("grossamount").value = Total;

jQuery("#invoice_items_"+no+"_taxes").on("change",function()
{
    var dataid = $("#invoice_items_"+no+"_taxes option:selected").attr('data-taxid');
	var element_tax_item = document.getElementById("invoice_items_"+no+"_tax_id");//tax_id
				//alert(dataid+' '+element_tax_item.toString());

	if (typeof(element_tax_item) != 'undefined' && element_tax_item != null)
	{

		element_tax_item.value = dataid;
	}
    
});

	}
}
/* EOC - Database BackedUp Scripts */