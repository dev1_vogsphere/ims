SET foreign_key_checks = 0;DROP TABLE IF EXISTS accounttype;

CREATE TABLE `accounttype` (
  `accounttypeid` int(11) NOT NULL,
  `accounttypedesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accounttypeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO accounttype VALUES("1","Cheque");
INSERT INTO accounttype VALUES("2","Saving");



DROP TABLE IF EXISTS bank;

CREATE TABLE `bank` (
  `bankid` varchar(255) NOT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bankid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO bank VALUES("ABSA","ABSA");
INSERT INTO bank VALUES("Bidvest Bank","Bidvest Bank");
INSERT INTO bank VALUES("Capitec","Capitec Bank");
INSERT INTO bank VALUES("FNB","First National Bank");
INSERT INTO bank VALUES("Investec","Investec");
INSERT INTO bank VALUES("Nedbank","NedBank");
INSERT INTO bank VALUES("Postbank","SA Post Bank (Post Office)");
INSERT INTO bank VALUES("SBSA","Standard Bank");



DROP TABLE IF EXISTS bankbranch;

CREATE TABLE `bankbranch` (
  `branchcode` varchar(255) NOT NULL,
  `bankid` varchar(255) DEFAULT NULL,
  `branchdesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`branchcode`),
  KEY `fk_bankid_idx` (`bankid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO bankbranch VALUES("632005","ABSA","ABSA Universal Branch Code");
INSERT INTO bankbranch VALUES("051001","SBSA","Universal");
INSERT INTO bankbranch VALUES("462005","Bidvest","Universal");
INSERT INTO bankbranch VALUES("470010","capitec","Universal");
INSERT INTO bankbranch VALUES("198765","nedbank","Universal");
INSERT INTO bankbranch VALUES("250655","FNB","FNB Universal Branch Code");



DROP TABLE IF EXISTS charge;

CREATE TABLE `charge` (
  `chargeid` int(11) NOT NULL,
  `chargename` varchar(45) DEFAULT NULL,
  `chargetypeid` int(11) DEFAULT NULL,
  `chargeoptionid` int(11) DEFAULT NULL,
  `loantypeid` int(11) DEFAULT NULL,
  `paymentmethodid` varchar(45) DEFAULT NULL,
  `active` varchar(45) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL,
  `amount` decimal(5,2) DEFAULT NULL,
  `min` decimal(5,2) DEFAULT NULL,
  `max` decimal(5,2) DEFAULT NULL,
  `graceperiod` int(11) DEFAULT NULL,
  PRIMARY KEY (`chargeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO charge VALUES("1","Disbursement - Immediate Transfer","2","1","1","DTO","X","0.00","45.00","0.00","0.00","0");
INSERT INTO charge VALUES("2","Late payment  penalty","3","1","1","DTO","X","0.00","4.00","1.00","30.00","0");
INSERT INTO charge VALUES("3","Monthly Service Fee","4","3","1","DTO","X","0.00","40.00","0.00","0.00","0");
INSERT INTO charge VALUES("4","Insurance Fee","3","2","1","DTO","X","0.00","0.00","0.00","1.00","1");
INSERT INTO charge VALUES("6","test","4","1","3","DTO","","0.00","0.00","0.00","0.00","0");



DROP TABLE IF EXISTS chargeoption;

CREATE TABLE `chargeoption` (
  `chargeoptionid` int(11) NOT NULL,
  `chargeoptionname` varchar(45) DEFAULT NULL,
  `chargeoptiondesc` varchar(255) DEFAULT NULL,
  `chargeoption` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`chargeoptionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO chargeoption VALUES("1","Fee","Fee - Flat Rate Amount","Fixed");
INSERT INTO chargeoption VALUES("2","Penalty","Penalties - amount due, depending on the grace period and days(min & max)","%");
INSERT INTO chargeoption VALUES("3","Interest Due","Interest Due on the amount","%");
INSERT INTO chargeoption VALUES("5","test","test 23","");



DROP TABLE IF EXISTS chargetype;

CREATE TABLE `chargetype` (
  `chargetypeid` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `chargetypedesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`chargetypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO chargetype VALUES("1","Specific Due Date","Select ‘Specified Due Date’ if the charge will be applied manually to a product on a date specified by the user. These charges can be applied more than once throughout a loan terms duration. ");
INSERT INTO chargetype VALUES("2","Disbursement Fee","Select ‘Disbursement’ if the charge is intended to be applied to a loan when it is disbursed. ");
INSERT INTO chargetype VALUES("3","OverDue Installment Fees ","Select ‘Overdue Instalment Amount’ if the charge is a penalty.");
INSERT INTO chargetype VALUES("4","Bounced Debit Order","The amount charged for a bounced debit order.");
INSERT INTO chargetype VALUES("5","Insurance Fee","The amount charged for insurance of the loan amount.");



DROP TABLE IF EXISTS common;

CREATE TABLE `common` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `series_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_identification` varchar(50) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `invoicing_address` longtext,
  `shipping_address` longtext,
  `customer_phone` varchar(100) DEFAULT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `terms` longtext,
  `notes` longtext,
  `base_amount` decimal(53,15) DEFAULT '0.000000000000000',
  `net_amount` decimal(53,15) DEFAULT '0.000000000000000',
  `discount_amount` decimal(53,15) DEFAULT '0.000000000000000',
  `gross_amount` decimal(53,15) DEFAULT '0.000000000000000',
  `paid_amount` decimal(53,15) DEFAULT '0.000000000000000',
  `tax_amount` decimal(53,15) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `draft` tinyint(1) DEFAULT '1',
  `closed` tinyint(1) DEFAULT '0',
  `sent_by_email` tinyint(1) DEFAULT '0',
  `number` int(11) DEFAULT NULL,
  `recurring_invoice_id` bigint(20) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `days_to_due` mediumint(9) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `max_occurrences` int(11) DEFAULT NULL,
  `must_occurrences` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `period_type` varchar(8) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `finishing_date` date DEFAULT NULL,
  `last_execution_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cstid_idx` (`customer_identification`),
  KEY `cstml_idx` (`customer_email`),
  KEY `cntct_idx` (`contact_person`),
  KEY `common_type_idx` (`type`),
  KEY `customer_id_idx` (`customer_id`),
  KEY `series_id_idx` (`series_id`),
  KEY `recurring_invoice_id_idx` (`recurring_invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8

;INSERT INTO common VALUES("1","3","1","Krustyco","7141253916O","deborah_hudson@example.com","Fake Dir n 244/\n/Madrid/\n/Spain","Fake Dir n 244/\n/Madrid/\n/Spain","","Deborah Hudson","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","586.170000000000000","586.170000000000000","0.000000000000000","627.200000000000000","0.000000000000000","41.031900000000000","2","RecurringInvoice","1","0","0","0","0","2014-08-26","2014-08-26","553","1","1","1","50","year","2006-06-30","","2014-08-26","2014-08-12 22:11:30","2014-08-26 22:24:30");
INSERT INTO common VALUES("2","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","1","RecurringInvoice","1","0","0","0","0","2014-08-26","2014-08-26","347","1","14","14","8","week","2004-12-02","2037-06-04","2014-08-26","2014-08-12 22:11:31","2014-08-26 22:24:32");
INSERT INTO common VALUES("3","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","1","RecurringInvoice","1","0","0","0","0","2014-08-26","2014-08-26","219","1","15","15","1","month","2005-03-10","2037-10-31","2014-08-26","2014-08-12 22:11:31","2014-08-26 22:24:34");
INSERT INTO common VALUES("5","1","4","Rouster and Sideways","1889335077","varria_leijten@example.com","Fake Dir n 871/\n/Madrid/\n/Spain","Fake Dir n 871/\n/Madrid/\n/Spain","","Varria Leijten","","","11571.890000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","1","Invoice","0","1","0","5","0","2008-11-09","2015-04-04","0","0","0","0","0","","","","","2009-04-17 09:45:36","2018-07-29 10:07:15");
INSERT INTO common VALUES("6","1","","Western Gas & Electric","3924395949G","marissa_peers@example.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","","Marissa Peers","","","11521.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2008-02-07","2015-10-28","0","0","0","0","0","","","","","2009-04-17 09:45:36","2018-07-28 08:07:28");
INSERT INTO common VALUES("7","1","6","Osato Chemicals","4499553670X","julie_radue@example.com","Fake Dir n 969/\n/Madrid/\n/Spain","Fake Dir n 969/\n/Madrid/\n/Spain","","Julie Radue","","","16232.920000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","10","0","2005-05-08","2006-09-02","0","0","0","0","0","","","","","2009-04-17 09:45:36","2018-07-28 04:07:36");
INSERT INTO common VALUES("8","3","7","123 Warehousing","77426541881","cyrus_decker@example.com","Fake Dir n 39/\n/Madrid/\n/Spain","Fake Dir n 39/\n/Madrid/\n/Spain","","Cyrus Decker","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","4802.700000000000000","3725.454600000000000","1077.245400000000000","3986.240000000000000","3986.230000000000000","260.781822000000000","0","Invoice","1","0","0","","","2008-08-06","2015-12-01","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:33");
INSERT INTO common VALUES("9","1","8","Allied Biscuit","6787068643G","willard_starling@example.com","Fake Dir n 617/\n/Madrid/\n/Spain","Fake Dir n 617/\n/Madrid/\n/Spain","","Willard Starling","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","19659.410000000000000","19659.410000000000000","0.000000000000000","22370.790000000000000","20804.790000000000000","2711.380400000000000","3","Invoice","0","0","0","2","","2008-11-04","2009-09-23","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:33");
INSERT INTO common VALUES("10","1","9","St. Anky Beer","9295043937","sharon_ward@example.com","Fake Dir n 424/\n/Madrid/\n/Spain","Fake Dir n 424/\n/Madrid/\n/Spain","","Sharon Ward","Vestibulum sagittis venenatis urna, in pellentesque ipsum pulvinar eu. In nec nulla, eu sagittis diam. Aenean egestas pharetra urna, et tristique metus egestas nec. Aliquam erat volutpat. Fusce pretium dapibus tellus. terms","Vestibulum sagittis venenatis urna, in pellentesque ipsum pulvinar eu. In nec <a href=\"http://www.google.com/\">nulla libero</a>, eu sagittis diam. Aenean egestas pharetra urna, et tristique metus egestas nec. Aliquam erat volutpat. Fusce pretium dapibus tellus.","67657.620000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","29","0","2008-02-02","2015-09-27","0","0","0","0","0","","","","","2009-04-17 09:45:36","2018-07-29 09:07:15");
INSERT INTO common VALUES("11","1","9","St. Anky Beer","9295043937","sharon_ward@example.com","Fake Dir n 155/\n/Madrid/\n/Spain","Fake Dir n 155/\n/Madrid/\n/Spain","","Sharon Ward","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","6469.090000000000000","5673.072000000000000","796.018000000000000","6580.760000000000000","6367.850000000000000","907.691520000000000","3","Invoice","0","0","0","4","","2006-05-03","2006-06-06","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("12","2","10","The Krusty Krab","8248936709","kusatsu_douglas@example.com","Fake Dir n 390/\n/Madrid/\n/Spain","Fake Dir n 390/\n/Madrid/\n/Spain","","Kusatsu Douglas","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","30063.500000000000000","30063.500000000000000","0.000000000000000","33058.160000000000000","25028.620000000000000","2994.655200000000000","3","Invoice","0","0","0","2","","2006-08-01","2015-09-16","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("13","3","11","Big Kahuna Burger","5714673104J","natima_seyetik@example.com","Fake Dir n 734/\n/Madrid/\n/Spain","Fake Dir n 734/\n/Madrid/\n/Spain","","Natima Seyetik","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","18963.520000000000000","18963.520000000000000","0.000000000000000","18820.620000000000000","22191.670000000000000","-142.903100000000000","3","Invoice","0","0","0","2","","2006-10-30","2007-03-01","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("14","3","12","SpringShield","4716162264W","dmitri_campio@example.com","Fake Dir n 824/\n/Madrid/\n/Spain","Fake Dir n 824/\n/Madrid/\n/Spain","","Dmitri Campio","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","25132.030000000000000","25132.030000000000000","0.000000000000000","26169.270000000000000","27224.250000000000000","1037.244200000000000","3","Invoice","0","0","0","3","","2007-01-28","2015-11-30","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("15","3","13","Sample, inc","7863841633Q","gerard_bennett@example.com","Fake Dir n 681/\n/Madrid/\n/Spain","Fake Dir n 681/\n/Madrid/\n/Spain","","Gerard Bennett","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1580.400000000000000","1580.400000000000000","0.000000000000000","1633.610000000000000","1876.770000000000000","53.211600000000000","3","Invoice","0","0","0","4","","2007-04-28","2008-08-26","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("16","2","14","Tessier-Ashpool","3378474903M","ronald_mccullen@example.com","Fake Dir n 802/\n/Madrid/\n/Spain","Fake Dir n 802/\n/Madrid/\n/Spain","","Ronald McCullen","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","26374.370000000000000","24865.529900000000000","1508.840100000000000","24878.590000000000000","29146.360000000000000","13.055492000000000","3","Invoice","0","0","0","3","","2007-07-27","2015-08-09","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("17","1","15","Fake Brothers","6009961348U","tristan_stone@example.com","Fake Dir n 839/\n/Madrid/\n/Spain","Fake Dir n 839/\n/Madrid/\n/Spain","","Tristan Stone","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","3481.950000000000000","3481.950000000000000","0.000000000000000","3199.270000000000000","3823.910000000000000","-282.678000000000000","0","Invoice","1","0","0","","","2007-10-25","2008-05-06","","0","","","","","","","","2009-04-17 09:45:37","2014-08-12 22:11:34");
INSERT INTO common VALUES("18","1","8","Allied Biscuit","6787068643G","willard_starling@example.com","Fake Dir n 538/\n/Madrid/\n/Spain","Fake Dir n 538/\n/Madrid/\n/Spain","","Willard Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","9561.690000000000000","9561.690000000000000","0.000000000000000","9268.470000000000000","10498.080000000000000","-293.219600000000000","3","Invoice","0","0","0","5","","2008-01-23","2015-09-02","","0","","","","","","","","2009-04-17 09:45:37","2016-05-21 12:58:06");
INSERT INTO common VALUES("19","1","16","Sonky Rubber Goods","40487600161","olivia_mirren@example.com","Fake Dir n 47/\n/Madrid/\n/Spain","Fake Dir n 47/\n/Madrid/\n/Spain","","Olivia Mirren","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","15940.250000000000000","15890.760800000000000","49.489200000000000","17452.830000000000000","15163.290000000000000","1562.067128000000000","3","Invoice","0","0","0","6","","2008-04-22","2015-09-18","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("20","3","17","BLAND Corporation","6812087434H","kestra_o\'malley@example.com","Fake Dir n 983/\n/Madrid/\n/Spain","Fake Dir n 983/\n/Madrid/\n/Spain","","Kestra O\'Malley","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","16827.550000000000000","15230.876000000000000","1596.674000000000000","14818.540000000000000","16936.690000000000000","-412.337960000000000","3","Invoice","0","0","0","5","","2008-07-21","2015-09-15","","0","","","","","","","","2009-04-17 09:45:37","2016-07-25 19:27:18");
INSERT INTO common VALUES("21","1","18","Initech","1674636932B","orfil_johnson@example.com","Fake Dir n 700/\n/Madrid/\n/Spain","Fake Dir n 700/\n/Madrid/\n/Spain","","Orfil Johnson","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","9014.490000000000000","7663.429800000000000","1351.060200000000000","8110.140000000000000","8110.140000000000000","446.708934000000000","1","Invoice","0","1","0","7","","2008-10-19","2015-06-28","","0","","","","","","","","2009-04-17 09:45:37","2014-08-12 22:11:34");
INSERT INTO common VALUES("22","2","11","Big Kahuna Burger","3641026692R","mary_ellen_ingram@example.com","Fake Dir n 763/\n/Madrid/\n/Spain","Fake Dir n 763/\n/Madrid/\n/Spain","","Mary Ellen Ingram","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","4800.480000000000000","3444.132000000000000","1356.348000000000000","3995.190000000000000","3468.240000000000000","551.061120000000000","3","Invoice","0","0","0","4","","2009-01-17","2015-08-10","","0","","","","","","","","2009-04-17 09:45:38","2016-07-25 19:27:18");
INSERT INTO common VALUES("23","1","13","Sample, inc","7863841633Q","gerard_bennett@example.com","Fake Dir n 687/\n/Madrid/\n/Spain","Fake Dir n 687/\n/Madrid/\n/Spain","","Gerard Bennett","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","12233.020000000000000","12233.020000000000000","0.000000000000000","13287.420000000000000","12566.940000000000000","1054.398400000000000","3","Invoice","0","0","0","8","","2009-04-17","2015-02-06","","0","","","","","","","","2009-04-17 09:45:38","2015-03-18 05:53:27");
INSERT INTO common VALUES("24","1","3","Smith and Co.","2450626775P","jody_nichols@example.com","Fake Dir n 262/\n/Madrid/\n/Spain","Fake Dir n 262/\n/Madrid/\n/Spain","","Jody Nichols","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","7198.850000000000000","7198.850000000000000","0.000000000000000","8233.650000000000000","","1034.799500000000000","1","Estimate","0","0","0","1","","2007-08-11","","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:34");
INSERT INTO common VALUES("25","1","4","Rouster and Sideways","1889335077","varria_leijten@example.com","Fake Dir n 871/\n/Madrid/\n/Spain","Fake Dir n 871/\n/Madrid/\n/Spain","","Varria Leijten","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","11571.890000000000000","11571.890000000000000","0.000000000000000","13407.920000000000000","","1836.026000000000000","1","Estimate","0","0","0","2","","2008-11-09","","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:34");
INSERT INTO common VALUES("26","1","5","Western Gas & Electric","3924395949G","marissa_peers@example.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","","Marissa Peers","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","11521.020000000000000","11521.020000000000000","0.000000000000000","11737.500000000000000","","216.480000000000000","2","Estimate","0","0","0","3","","2008-02-07","","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:34");
INSERT INTO common VALUES("27","1","6","Osato Chemicals","4499553670X","julie_radue@example.com","Fake Dir n 969/\n/Madrid/\n/Spain","Fake Dir n 969/\n/Madrid/\n/Spain","","Julie Radue","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","15419.960000000000000","15419.960000000000000","0.000000000000000","14291.560000000000000","","-1128.403200000000000","3","Estimate","0","0","0","4","","2005-05-08","","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:34");
INSERT INTO common VALUES("28","1","7","123 Warehousing","77426541881","cyrus_decker@example.com","Fake Dir n 39/\n/Madrid/\n/Spain","Fake Dir n 39/\n/Madrid/\n/Spain","","Cyrus Decker","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","4802.700000000000000","3725.454600000000000","1077.245400000000000","3986.240000000000000","","260.781822000000000","0","Estimate","1","0","0","","","2008-08-06","","","0","","","","","","","","2009-04-17 09:45:36","2014-08-12 22:11:35");
INSERT INTO common VALUES("29","1","5","Nicz Atterny","3924395949G","nico.maekiso@gmail.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","0787788372","Marissa Peers","INVOICE TERMS: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","Testing","179.540000000000000","179.540000000000000","0.000000000000000","179.540000000000000","","0.000000000000000","","Estimate","0","0","1","5","","","","","0","","","","","","","","2014-08-12 22:45:46","2014-08-12 22:48:48");
INSERT INTO common VALUES("30","1","19","Mmabatho High Shool","Client Legal Id","principalmhs@mweb.co.za","","","","SGB","","","7250.000000000000000","7250.000000000000000","0.000000000000000","7250.000000000000000","7250.000000000000000","0.000000000000000","1","Invoice","0","0","1","9","","2014-08-26","","","0","","","","","","","","2014-08-26 16:08:19","2014-08-26 16:15:45");
INSERT INTO common VALUES("31","1","5","Nicz Atterny","3924395949G","nico.maekiso@gmail.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","0787788372","Marissa Peers","INVOICE TERMS: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","","179.540000000000000","179.540000000000000","0.000000000000000","179.540000000000000","","0.000000000000000","3","RecurringInvoice","1","0","0","","","","","1","1","3","3","1","day","2014-08-26","2014-08-28","2014-08-27","2014-08-26 22:23:03","2014-08-28 10:33:09");
INSERT INTO common VALUES("32","3","1","Krustyco","7141253916O","deborah_hudson@example.com","Fake Dir n 244/\n/Madrid/\n/Spain","Fake Dir n 244/\n/Madrid/\n/Spain","","Deborah Hudson","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","586.170000000000000","586.170000000000000","0.000000000000000","627.200000000000000","0.000000000000000","41.031900000000000","3","Invoice","0","0","0","6","1","2014-08-26","2016-03-01","","0","","","","","","","","2014-08-26 22:24:30","2016-05-21 12:58:05");
INSERT INTO common VALUES("33","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","5","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("34","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","6","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("35","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","7","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("36","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","8","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("37","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","9","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("38","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","10","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("39","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","11","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:30","2015-08-09 21:08:00");
INSERT INTO common VALUES("40","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","12","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("41","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","13","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("42","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","14","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("43","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","15","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("44","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","16","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("45","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","17","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:31","2015-08-09 21:08:00");
INSERT INTO common VALUES("46","2","1","Krustyco","6852996407X","valeria_starling@example.com","Fake Dir n 899/\n/Madrid/\n/Spain","Fake Dir n 899/\n/Madrid/\n/Spain","","Valeria Starling","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","1699.680000000000000","1699.680000000000000","0.000000000000000","1971.630000000000000","0.000000000000000","271.948800000000000","3","Invoice","0","0","0","18","2","2014-08-26","2015-08-08","","0","","","","","","","","2014-08-26 22:24:32","2015-08-09 21:08:00");
INSERT INTO common VALUES("47","1","5","Nicz Atterny","3924395949G","nico.maekiso@gmail.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","0787788372","Marissa Peers","INVOICE TERMS: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","","179.540000000000000","179.540000000000000","0.000000000000000","179.540000000000000","0.000000000000000","0.000000000000000","3","Invoice","0","0","0","10","31","2014-08-26","2014-08-27","","0","","","","","","","","2014-08-26 22:24:32","2014-08-27 10:46:28");
INSERT INTO common VALUES("48","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","19","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:32","2015-07-20 12:37:20");
INSERT INTO common VALUES("49","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","20","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:32","2015-07-20 12:37:20");
INSERT INTO common VALUES("50","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","21","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:32","2015-07-20 12:37:20");
INSERT INTO common VALUES("51","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","22","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:32","2015-07-20 12:37:20");
INSERT INTO common VALUES("52","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","23","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("53","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","24","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("54","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","25","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("55","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","26","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("56","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","27","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("57","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","28","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:33","2015-07-20 12:37:20");
INSERT INTO common VALUES("58","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","29","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:34","2015-07-20 12:37:20");
INSERT INTO common VALUES("59","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","30","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:34","2015-07-20 12:37:20");
INSERT INTO common VALUES("60","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","31","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:34","2015-07-20 12:37:20");
INSERT INTO common VALUES("61","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","32","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:34","2015-07-20 12:37:20");
INSERT INTO common VALUES("62","2","2","Plow King","7885556345D","jennifer_karidian@example.com","Fake Dir n 812/\n/Madrid/\n/Spain","Fake Dir n 812/\n/Madrid/\n/Spain","","Jennifer Karidian","Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.","Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","5976.950000000000000","5976.950000000000000","0.000000000000000","6216.030000000000000","0.000000000000000","239.078000000000000","3","Invoice","0","0","0","33","3","2014-08-26","2015-04-02","","0","","","","","","","","2014-08-26 22:24:34","2015-07-20 12:37:20");
INSERT INTO common VALUES("63","1","5","Nicz Atterny","3924395949G","nico.maekiso@gmail.com","Fake Dir n 88/\n/Madrid/\n/Spain","Fake Dir n 88/\n/Madrid/\n/Spain","0787788372","Marissa Peers","INVOICE TERMS: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","","179.540000000000000","179.540000000000000","0.000000000000000","179.540000000000000","0.000000000000000","0.000000000000000","3","Invoice","0","0","0","11","31","2014-08-27","2014-08-28","","0","","","","","","","","2014-08-27 10:47:29","2014-08-28 10:33:09");
INSERT INTO common VALUES("64","1","20","Client Name","Small Business Hub","info@sbhub.co.za","Invoicing Address","","0835549326","Contact Person","INVOICE TERMS: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","","80.000000000000000","80.000000000000000","0.000000000000000","80.000000000000000","0.000000000000000","0.000000000000000","3","Invoice","0","0","0","12","","2014-10-16","","","0","","","","","","","","2014-10-16 09:00:34","2014-10-16 09:00:34");
INSERT INTO common VALUES("65","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","sadsd","fsdfdf","","Sharon Ward","terms","notes","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","1","Invoice","0","0","0","0","0","2018-07-20","2018-07-20","0","0","0","0","0","","","","","2018-07-20 06:07:15","2018-07-20 06:07:15");
INSERT INTO common VALUES("66","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:03","2018-07-22 10:07:03");
INSERT INTO common VALUES("67","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:29","2018-07-22 10:07:29");
INSERT INTO common VALUES("68","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:00","2018-07-22 10:07:00");
INSERT INTO common VALUES("69","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:51","2018-07-22 10:07:51");
INSERT INTO common VALUES("70","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:04","2018-07-22 10:07:04");
INSERT INTO common VALUES("71","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 10:07:09","2018-07-22 10:07:09");
INSERT INTO common VALUES("72","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 11:07:30","2018-07-22 11:07:30");
INSERT INTO common VALUES("73","1","9295043937","St. Anky Beer","9295043937","sharon_ward@example.com","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp","","Sharon Ward","dsd","fdfdf","403.500000000000000","60.530000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","2","0","2018-07-22","2018-07-22","0","0","0","0","0","","","","","2018-07-22 11:07:04","2018-07-22 11:07:04");
INSERT INTO common VALUES("74","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","bhg","gjhh","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2018-07-29","2018-07-29","0","0","0","0","0","","","","","2018-07-29 10:07:35","2018-07-29 10:07:35");
INSERT INTO common VALUES("75","1","1","Boikgantsho Consulting & Events CCMod","2014-002","info@bcei.co.za","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042 /\n/ /\n/ /\n/ /\n/","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042/\n/","","Mr T MoreWaQeRRRR","bnbb","nnjnmn","20278.690000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","3","0","2018-07-29","2018-07-29","0","0","0","0","0","","","","","2018-07-29 10:07:31","2018-07-29 10:07:31");
INSERT INTO common VALUES("76","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","sds","dsdsd","179.540000000000000","206.470000000000000","0.000000000000000","0.000000000000000","0.000000000000000","26.930000000000000","2","Invoice","0","0","0","1","0","2018-08-05","2018-08-05","0","0","0","0","0","","","","","2018-08-05 10:08:56","2018-08-07 11:08:53");
INSERT INTO common VALUES("77","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","179.540000000000000","206.470000000000000","0.000000000000000","0.000000000000000","0.000000000000000","26.930000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-07 11:08:16","2018-08-07 11:08:16");
INSERT INTO common VALUES("78","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","100.820000000000000","115.940000000000000","0.000000000000000","0.000000000000000","0.000000000000000","15.120000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-07 11:08:40","2018-08-07 11:08:40");
INSERT INTO common VALUES("79","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-07 11:08:33","2018-08-07 11:08:33");
INSERT INTO common VALUES("80","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-07 11:08:54","2018-08-07 11:08:54");
INSERT INTO common VALUES("81","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-08 12:08:47","2018-08-08 12:08:47");
INSERT INTO common VALUES("82","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-08 12:08:03","2018-08-08 12:08:03");
INSERT INTO common VALUES("83","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-08 12:08:30","2018-08-08 12:08:30");
INSERT INTO common VALUES("84","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-08 12:08:01","2018-08-08 12:08:01");
INSERT INTO common VALUES("85","1","37","Test","TEST","test@yahoo.com","Test2  /\n/","Test2","","Test 2","","","99.150000000000000","114.020000000000000","0.000000000000000","0.000000000000000","0.000000000000000","14.870000000000000","2","Invoice","0","0","0","1","0","2018-08-07","2018-08-07","0","0","0","0","0","","","","","2018-08-08 12:08:17","2018-08-08 12:08:17");
INSERT INTO common VALUES("86","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","912.360000000000000","1049.210000000000000","0.000000000000000","0.000000000000000","0.000000000000000","136.850000000000000","2","quotation","0","0","0","2","0","2018-08-08","2018-08-08","0","0","0","0","0","","","","","2018-08-08 10:08:51","2018-08-08 10:08:51");
INSERT INTO common VALUES("87","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","2","Estimate","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 04:08:11","2018-08-09 05:08:59");
INSERT INTO common VALUES("88","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Estimate","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 04:08:35","2018-08-09 04:08:35");
INSERT INTO common VALUES("89","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Estimate","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 04:08:46","2018-08-09 04:08:46");
INSERT INTO common VALUES("90","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 04:08:46","2018-08-09 04:08:46");
INSERT INTO common VALUES("91","1","","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 05:08:00","2018-08-09 05:08:00");
INSERT INTO common VALUES("92","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 05:08:01","2018-08-09 05:08:01");
INSERT INTO common VALUES("93","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 05:08:58","2018-08-09 05:08:58");
INSERT INTO common VALUES("94","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","","","179.540000000000000","179.540000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0.000000000000000","0","Invoice","0","0","0","1","0","2018-08-09","2018-08-09","0","0","0","0","0","","","","","2018-08-09 05:08:14","2018-08-09 05:08:14");
INSERT INTO common VALUES("95","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","Test Recurring - ONLY","Test Recurring - ONLY","20179.540000000000000","23206.470000000000000","0.000000000000000","0.000000000000000","0.000000000000000","3026.930000000000000","2","Invoice","0","0","0","2","0","2018-08-12","2018-08-12","0","0","0","0","0","","","","","2018-08-12 03:08:15","2018-08-12 04:08:12");
INSERT INTO common VALUES("96","1","8","Kitsowise","2014-008","thabi.kekae@yahoo.com","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","","Mr. Thabi Kekae","Test Recurring - ONLY","Test Recurring - ONLY","20179.540000000000000","23206.470000000000000","0.000000000000000","0.000000000000000","0.000000000000000","3026.930000000000000","2","RecurringInvoice","0","0","0","2","0","2018-08-12","2018-08-12","1","0","1","0","2","day","2018-08-12","2018-10-31","","2018-08-12 03:08:04","2018-08-12 04:08:54");



DROP TABLE IF EXISTS currency;

CREATE TABLE `currency` (
  `currencyid` varchar(20) NOT NULL,
  `currencyname` varchar(100) NOT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO currency VALUES("$","US Dollar($)");
INSERT INTO currency VALUES("L","Maluti(L)");
INSERT INTO currency VALUES("P","Pula(P)");
INSERT INTO currency VALUES("R","Rand(R)");



DROP TABLE IF EXISTS customer;

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `name_slug` varchar(100) DEFAULT NULL,
  `identification` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `invoicing_address` longtext,
  `shipping_address` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cstm_idx` (`name`),
  UNIQUE KEY `cstm_slug_idx` (`name_slug`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8

;INSERT INTO customer VALUES("1","Boikgantsho Consulting & Events CCMod","Boikgantsho Consulting & Events CCMod","2014-002","info@bcei.co.za","Mr T MoreWaQeRRRR","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042 /\n/ /\n/ /\n/ /\n/","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042/\n/");
INSERT INTO customer VALUES("2","Mr. Nicodema Maekiso","mrnicodemamaekiso","2014-001","nico.maekiso@gmail.com","Mr. Nico Maekiso","708 Toms Place/\n/227 Minnaar Str/\n/Pretoria/\n/0002","");
INSERT INTO customer VALUES("3","Paseka Chakela","pasekachakela","2014-003","paseka.chakela@yahoo.com","Mr P Chakela","","");
INSERT INTO customer VALUES("4","Mr. Tamaho Michael Pontsa","mrtamahomichaelpontsa","2014-004","tamahomichael@gmail.com","Mr TM Pontsa","","");
INSERT INTO customer VALUES("5","Mr Mphoeng Mphoeng A","Mr Mphoeng Mphoeng A","2014-005","smphoeng@gmail.com","Mr Mphoeng Mphoeng","smphoeng@gmail.com /\n/","smphoeng@gmail.com");
INSERT INTO customer VALUES("6","ModiriFM ","ModiriFM ","2014-006","mokgoja@modirifm.co.za","Mr. Pule Mokgoja masepa","P.O.Box 858 /\n/Delareyville/\n/2770 /\n/ /\n/","Stand No. 70/\n/Cnr 41th Avenue & 33rd Street/\n/Atamelang/\n/2732");
INSERT INTO customer VALUES("7","NG Labz","nglabz","2014-007","thabi.kekae@yahoo.com","Mr. Thabi Kekae","Unit 09/\n/Cedar Creeck/\n/Ormonde/\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde/\n/2091");
INSERT INTO customer VALUES("8","Kitsowise","kitsowise","2014-008","thabi.kekae@yahoo.com","Mr. Thabi Kekae","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091","Unit 09/\n/Cedar Creeck/\n/Ormonde /\n/2091");
INSERT INTO customer VALUES("9","Chomza","chomza","2014-009","info@chomza.co.za","Mr. Kok","4955 Motlhothlo Cul/\n/Unit 13/\n/Mmabatho/\n/Mafikeng/\n/2735/\n/","4955 Motlhothlo Cul/\n/Unit 13/\n/Mmabatho/\n/Mafikeng/\n/2735");
INSERT INTO customer VALUES("10","Rich Guys Petroleum","richguyspetroleum","2014-010","chabalala@icloud.com","MR Ian Chabalala","170 Vergelegen /\n/Bishop Creek Equestria","170 Vergelegen /\n/Bishop Creek Equestria");
INSERT INTO customer VALUES("11","ComData","comdata","2014-011","info@comdataconsulting.co.za","Mr. Mpholo Leboea","8A Wildekastaiing Avenue/\n/Heuweloord/\n/Centurion/\n/Gauteng/\n/0157 ","8A Wildekastaiing Avenue/\n/Heuweloord/\n/Centurion/\n/Gauteng/\n/0157 ");
INSERT INTO customer VALUES("12","ImageHair","imagehair","2014-012","info@ImageInu.com","Mrs. Dikeledi ","info@ImageInu.com","info@ImageInu.com");
INSERT INTO customer VALUES("13","Mpikoko","mpikoko","2014-013","info@mpikoko.co.za","Mr. Bongani Ndlovu","Mpikoko HD Protography/\n/info@mpikoko.co.za","Mpikoko HD Protography/\n/info@mpikoko.co.za");
INSERT INTO customer VALUES("14","Mr. Gideon","mrgideon","2014-014","info@gideon.co.za","Mr. Gideon","Mr. Gideon","Mr. Gideon");
INSERT INTO customer VALUES("15","Intelligent Consulting","intelligentconsulting","2014-015","info@IntelligentConsulting.co.za","Mr. Shane","","");
INSERT INTO customer VALUES("16","Mrs. Lemo Shale","mrslemoshale","2014-016","lemo@telkom.co.za","Mrs. Lemo Shale","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("17","Mrs. Queen Nkosi","mrsqueennkosi","2014-017","queen@telkom.co.za","Mrs. Queen Nkosi","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("18","Ms. Thato ","msthato","2014-018","thato@gmail.com","Ms. Thato","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("19","Mr. Lebogang Tsogang","mrlebogangtsogang","2014-019","lctsogang@gmail.com","Contact Person","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("20","Mr.Thys Maqadika","mrthysmaqadika","2014-020","maqadika@facebook.com","Mr.Thys Maqadika","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("21","Mr Athen Mokgoja","mrathenmokgoja","2014-021","amokgoja@gmail.com","Mr Athen Mokgoja","708 Toms Place/\n/227 Minnaar street/\n/Pretoria Central/\n/0002","Shipping Address");
INSERT INTO customer VALUES("22","Mr. Dzuni Mashimbye","mrdzunimashimbye","2014-022","mashimdb@telkom.co.za","Mr. Dzuni Mashimbye","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("23","Mr. Mulunghisi Mahwayi","mrmulunghisimahwayi","2014-023","mahwaymp@gmail.com","Mr. Mulunghisi Mahwayi","212 Eastlake/\n/2 Rose Street/\n/Florida/\n/1709 ","212 Eastlake/\n/2 Rose Street/\n/Florida/\n/1709 ");
INSERT INTO customer VALUES("24","Mr. Lebogang Ditibane","mrlebogangditibane","2014-024","lditibane@gmail.com","Mr. Lebogang Ditibane","Unit 20 Seoul Complex/\n/43 Mulder Street/\n/The Reeds/\n/Centurion/\n/0158","Shipping Address");
INSERT INTO customer VALUES("25","Mrs. Jeanett Marenyane","mrsjeanettmarenyane","2014-025","marenymj@telkom.co.za","Mrs. Jeanett Marenyane","marenymj@telkom.co.za","marenymj@telkom.co.za");
INSERT INTO customer VALUES("26","Mrs. Nobuhle","mrsnobuhle","2014-026","nobuhle@gmail.com","Mrs. Nobuhle","","");
INSERT INTO customer VALUES("28","Mr. Mpholo Leboea","mrmphololeboea","2014-027","mpholo.leboea@gmail.com","Mr. Mpholo Leboea","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("29","SBHub","sbhub","2014-028","info@chomza.co.za","Mr. Kok","4955 Motlhothlo Cul/\n/Unit 13/\n/Mmabatho/\n/Mafikeng/\n/2735/\n/","4955 Motlhothlo Cul/\n/Unit 13/\n/Mmabatho/\n/Mafikeng");
INSERT INTO customer VALUES("30","ITS (Pty) Ltd","itsptyltd","2014-029","info@bcei.co.za","Mr. T More","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042/\n/","538 Adela Street/\n/Garsfontein/\n/Pretoria East /\n/0042");
INSERT INTO customer VALUES("32","Mr. Tshiamo Modise","mrtshiamomodise","2014-0230","modisetm@gmail.com","Mr. Tshiamo Modise","modisetm@gmail.com","modisetm@gmail.com");
INSERT INTO customer VALUES("33"," M-Cubed Technologies ","mcubedtechnologies","2014-0231","mosesm@mcubedtechnologies.co.za","Mr. Moses Mphahlele","P.O. Box 73372/\n/Lynwood ridge/\n/0042","P.O. Box 73372/\n/Lynwood ridge/\n/0042");
INSERT INTO customer VALUES("34","Mr. Tumi Mothapo","mrtumimothapo","2014-032","tumza1m@gmail.com","Mr. Tumi Mothapo","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("35","Mashile Funeral Services","mashilefuneralservices","2014-033","info@mashilefunerals.co.za","Mr. Mashile","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("36","Nicodema Maekiso","nicodemamaekiso","2014-001","nico.maekiso@gmail.com","Nico Maekiso","708 Toms Place/\n/227 Minnaar Str/\n/Pretoria/\n/0002","");
INSERT INTO customer VALUES("37","Test","Test","TEST","test@yahoo.com","Test 2","Test2  /\n/","Test2");
INSERT INTO customer VALUES("38","Eilse","eilse","2014-034","info@chomza.co.za","Mr. Kok","4955 Motlhothlo Cul /\n/Unit 13 /\n/Mmabatho /\n/Mafikeng /\n/2735 ","4955 Motlhothlo Cul /\n/Unit 13 /\n/Mmabatho /\n/Mafikeng");
INSERT INTO customer VALUES("39","Mr. Itumeleng Duiker","mritumelengduiker","2014-035","duikeria@gmail.com","Mr. Itumeleng Duiker","duikeria@gmail.com","duikeria@gmail.com");
INSERT INTO customer VALUES("40","Mr.Thabo Mofokeng","mrthabomofokeng","2014-036","mofoketp@telkom.co.za","Mr.Thabo Mofokeng","mofoketp@telkom.co.za","mofoketp@telkom.co.za");
INSERT INTO customer VALUES("41","Business IPOM Group","businessipomgroup","2014-037","ericgooch@hotmail.com","Eric Ilunga","ericgooch@hotmail.com","ericgooch@hotmail.com");
INSERT INTO customer VALUES("44","Mr. Andrew Mthethwa","mrandrewmthethwa","2014-039","Andrew.Mthethwa@standardbank.co.za","Mr. Andrew Mthethwa","Andrew.Mthethwa@standardbank.co.za","Andrew.Mthethwa@standardbank.co.za");
INSERT INTO customer VALUES("45","Lazarus Mahlangu","lazarusmahlangu","Client Legal Id","lazarus.mahlangu@pbtgroup.co.za","","","");
INSERT INTO customer VALUES("46","Procsup Projects (Pty) Ltd","procsupprojectsptyltd","2014-0232","s.nkateko@yahoo.com","Nkateko","s.nkateko@yahoo.com","s.nkateko@yahoo.com");
INSERT INTO customer VALUES("47","Pekwa","pekwa","2014-38","thabi.kekae@yahoo.com","Mr. Thabi Kekae","Unit 09 /\n/Cedar Creeck/\n/Ormonde/\n/2091","Unit 09 /\n/Cedar Creeck/\n/Ormonde/\n/2091");
INSERT INTO customer VALUES("48","Mr.Tumelo Modise","mrtumelomodise","20144444","tumelomodise4@gmail.com","tumelomodise4@gmail.com","P.O.Box 8474/\n/Pretoria/\n/0001","Unit 31 Seoul/\n/43 Mulder Street/\n/The reeds/\n/Centurion/\n/0158");
INSERT INTO customer VALUES("49","Northern Cape Technical High Soccer Academy","northerncapetechnicalhighsocceracademy","2014-040","normanvorster@gmail.com","Norman Voster","normanvorster@gmail.com","normanvorster@gmail.com");
INSERT INTO customer VALUES("50","ProMS","proms","2014-039","kev.mkhonto@gmail.com","Mr. Kevin Mokhonto ","kev.mkhonto@gmail.com","kev.mkhonto@gmail.com");
INSERT INTO customer VALUES("51","MiLegit","milegit"," 2014-041","thabi.kekae@yahoo.com"," Mr. Thabi Kekae","/\n/Unit 09 Cedar CreeckOrmonde2091","/\n/Unit 09 Cedar CreeckOrmonde2091");
INSERT INTO customer VALUES("52","Client Name  MAHUTSI PRIMARY SCHOOL","clientnamemahutsiprimaryschool","Client Legal Id","mahuntsiprimary@gmail.com","Contact Person   HLAWULANI","Invoicing Address","Shipping Address");
INSERT INTO customer VALUES("53","Maiktronix ","maiktronix","2014-041","greg@maiktronix.co.za","Greg Majeng","greg@maiktronix.co.za","greg@maiktronix.co.za");
INSERT INTO customer VALUES("54","Afrihost","afrihost","2015-001","accounts@vogsphere.co.za","Afrihost","accounts@support.afrihost.com","accounts@support.afrihost.com");
INSERT INTO customer VALUES("55","Cybersmart","cybersmart","2015-002","info@vogsphere.co.za","Cybersmart","info@vogsphere.co.za","info@vogsphere.co.za");
INSERT INTO customer VALUES("56","Indigo","indigo","2015-003","tebogo2410@yahoo.com","Morare Orapeleng","tebogo2410@yahoo.com","tebogo2410@yahoo.com");
INSERT INTO customer VALUES("57","Monate Biscuits","monatebiscuits","2015-004","thabi.kekae@yahoo.com","Mr. Thabi Kekae","Unit 09 /\n/Cedar Creeck/\n/Ormonde/\n/2091","Unit 09 /\n/Cedar Creeck/\n/Ormonde/\n/2091");
INSERT INTO customer VALUES("58","Mr Nkosi  & Phalatse","mrnkosiphalatse","100","mthunzi.nkosi@supersport.com","Mr T Phalatse ","8 Linden Lime Complex/\n/33 Bramfischer Drive/\n/Randburg/\n/2194","8 Linden Lime Complex/\n/33 Bramfischer Drive/\n/Randburg/\n/2194");
INSERT INTO customer VALUES("59","Let Talk Viral","lettalkviral","2015-005","sgtsh1@gmail.com","Tshirangwana Sedzani","sgtsh1@gmail.com","sgtsh1@gmail.comShipping Address");
INSERT INTO customer VALUES("60","Mujahid Maleka","mujahidmaleka","86","MujahidMaleka@gmail.com","Mujahid Maleka","29548 Nku street/\n/Mamelodi /\n/East Vista View","29548 Nku street/\n/Mamelodi /\n/East Vista View");
INSERT INTO customer VALUES("61","Tumi Ngqondo","tumingqondo","","ngqondo@gmail.com","Tumi Ngqondo","172 Greeenwhich Village/\n/Holkam road/\n/Paulshof","172 Greeenwhich Village/\n/Holkam road/\n/Paulshof");
INSERT INTO customer VALUES("62","Chake Consulting (Pty) Ltd","chakeconsultingptyltd","2016-001","ochake@chakeconsulting.co.za","Mr. Omphemetse Chake","16th Road/\n/Midrand/\n/Constantia Square Office Park/\n/Unit 37 /\n/Entrance 4/\n/Stellenryk Building","16th Road/\n/Midrand/\n/Constantia Square Office Park/\n/Unit 37 /\n/Entrance 4/\n/Stellenryk Building");
INSERT INTO customer VALUES("63","Miss Buhlebuzile Kunene","missbuhlebuzilekunene","2016-002","Bkunene@tsti.co.za","Miss Buhlebuzile Kunene","P.O.Box 20756/\n/Crystal Park/\n/Benoni/\n/1515","32 Msauli Street/\n/Crystal Park/\n/Benoni/\n/1515");
INSERT INTO customer VALUES("64","Mr. Obed ObakengMalebye","mrobedobakengmalebye","VogsLaon8908305579084","obedm@cornastone.co.za","Mulunghisi Mahwayi","4734 Unit D, Temba, Hammanskraal, 0407","4734 Unit D, Temba, Hammanskraal, 0407");
INSERT INTO customer VALUES("65","Mr. Obed Malebye","mrobedmalebye","VogsLoan8908305579084","obedm@cornastone.co.za","Mulunghisi Mahwayi","43 Mulder street, 31 Seoul, Centurion,0158","43 Mulder street, 31 Seoul, Centurion,0158");
INSERT INTO customer VALUES("66","Mask Systems","masksystems","2016-003","mask.systems@webmail.co.za","Isah Kudu","mask.systems@webmail.co.za","mask.systems@webmail.co.za");
INSERT INTO customer VALUES("67","Elite Empire Estates","eliteempireestates","2016-004","thabangmolubi@gmail.com","Contact Person (Pty) Ltd","thabangmolubi@gmail.com","thabangmolubi@gmail.com");
INSERT INTO customer VALUES("68","","","","tumelomodise@gmail.com","","","");
INSERT INTO customer VALUES("104","motiti","motiti","Modise20236","","","","");
INSERT INTO customer VALUES("105","motiti2","motiti2","Modise20236","","","","");
INSERT INTO customer VALUES("113","motiti12345","motiti12345","Modise1234","tyyyy@gmail","vogsphere","","");
INSERT INTO customer VALUES("115","Testumtoto","Testumtoto","testumtoto","tyyytotity@gmail","vogsphere","","");
INSERT INTO customer VALUES("116","Testumtoto123","Testumtoto123","testumtoto12344","tyyytotity@gmail","vogsphere","","");
INSERT INTO customer VALUES("117","TestXXXX","TestXXXX","Modise20236bnbhbn","trttrrr@gy.com","vogspherevvv","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp");
INSERT INTO customer VALUES("118","TestasDDvvv","TestasDDvvv","fffededed","tyyyjivey@gmail","vogsphere","43 Mulder Street/\n//\n/31 Seoul Complex.","43 Mulder Street/\n//\n/31 Seoul Complex.");
INSERT INTO customer VALUES("119","TestXXXXbbbbvjv","TestXXXXbbbbvjv","nlkjljlj","tyyyyAAASSS@gmail","ttvbvv","43 Mulder Street/\n//\n/31 Seoul Complex.","43 Mulder Street/\n//\n/31 Seoul Complex.");
INSERT INTO customer VALUES("120","TestXXXXbbbbvjvbbb","TestXXXXbbbbvjvbbb","bbjvjgvjg","tyyyyASSS@gmail","ttvbvv","43 Mulder Street/\n//\n/31 Seoul Complex.","43 Mulder Street/\n//\n/31 Seoul Complex.");
INSERT INTO customer VALUES("121","TestXXXXvhvhvghvgg","TestXXXXvhvhvghvgg","ihgkhkhkh","vghgg@gg.cbb","kkkkgh","tftftf","ACggx");
INSERT INTO customer VALUES("122","nandi","nandi","2000-0510","nandi@gmail.com","nandi","nandi-invoicing","nandi-shippment");
INSERT INTO customer VALUES("124","anderson","anderson","2000-05109995","aderson@rubish.com","andy","43 Mulder Street/\n//\n/31 Seoul Complex.","43 Mulder Street/\n//\n/31 Seoul Complex.");
INSERT INTO customer VALUES("125","Tau","Tau","878788787878877","info@bcei.co.za","trstst","dnsdn","fdsfdf");
INSERT INTO customer VALUES("128"," St. Anky Beer"," St. Anky Beer","9295043937","sharon_ward@example.com","Sharon Ward","37 Kobie Krige Street/\n/Krugersdorp","37 Kobie Krige Street/\n/Krugersdorp");



DROP TABLE IF EXISTS customers;

CREATE TABLE `customers` (
  `CustomerId` varchar(20) NOT NULL,
  `Title` enum('Mr','Mrs','Miss','Ms','Dr') DEFAULT NULL,
  `FirstName` varchar(20) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `Street` varchar(20) DEFAULT NULL,
  `Suburb` varchar(20) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `State` enum('ACT','NSW','NT','QLD','SA','TAS','VIC','WA') NOT NULL,
  `PostCode` char(4) NOT NULL,
  `Dob` date NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CustomerId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO customers VALUES("6305240160082","Mrs","BONITA","THERON","155 bembesi street","riverlea","johannesburg","","2093","1963-05-24","0761979830");
INSERT INTO customers VALUES("6401250420084","Mr","Agnes","Buthelezi","House 2236, Katlehon","Germiston","Johannesburg","","1431","1964-01-25","0796284100");
INSERT INTO customers VALUES("6704125366089","Mr","Abie","Pitso","23, Bekker","Centurion","Centurion","","0157","1967-04-12","0834009110");
INSERT INTO customers VALUES("6911110151083","Mrs","michelle","botes","155 Bembesi street ","riverlea","johannesburg","","2093","1969-11-11","0736366888");
INSERT INTO customers VALUES("7512125283082","Mr","Rajab","Mirase","40 Chardale Crescent","Newlands West","Durban","","4037","1975-12-12","0717528088");
INSERT INTO customers VALUES("7611150155080","Mr","Masha","Rangan","7A Westdene","Westdene","Johannesburg","","2092","1976-11-15","0813073233");
INSERT INTO customers VALUES("7709120167084","Mrs","Delise","Wagner","30 barrow str","riverlea","jhb","","2093","1977-09-12","0616576991");
INSERT INTO customers VALUES("8004060552084","Mrs","Pumela ","Mhlomeli","21 Derinee Crescent","Bridgemeade","Port Elizabeth","","6025","1980-04-06","0722722920");
INSERT INTO customers VALUES("8105150506086","Mrs","Cebisa","Jada","7 Disseboompark,65 O","Elarduspark","Centurion","","0181","1981-05-15","0731429695");
INSERT INTO customers VALUES("8107300561086","Miss","Fundiswa ","Mayekiso ","43 Shana Park ","Midrand","Johannesburg","","1682","1981-07-30","0837607067");
INSERT INTO customers VALUES("8306145448083","Mr","Sifiso","Mthembu","67 Cuando Crescent","Zandspruit Ext 4","Randburg","","2169","1983-12-06","0721564714");
INSERT INTO customers VALUES("8309051276084","Mrs","Jeanett","Marenyane","593 Morutlwa street","Garankuwa View","Garankuwa","","0908","1983-09-05","0738852323");
INSERT INTO customers VALUES("8409055665082","Mr","Mabanna","Mogoasha Edison","2566 Zwane street","Rockville","Soweto","","1818","1984-09-05","0834059469");
INSERT INTO customers VALUES("8504045897082","Mr","Abram","Mopeloa","4373 Sibanyoni stree","Mamelodi West","Pretoria","","0122","1985-04-04","0833685767");
INSERT INTO customers VALUES("8504265079080","Mr","Stephen","Gruindelingh","83 wison","witfield","boksburg","","1619","1985-04-26","0826032839");
INSERT INTO customers VALUES("8601045310080","Mr","Nthwana Nicodema","Maekiso","46 Villa Dorado, 1 A","Heuweloord","Centurion ","","0173","1986-01-04","0787788372");
INSERT INTO customers VALUES("8610265252088","Mr","Gcobani","Mkontwana","48 Oak Avenue Park H","Centurion","Centurion","","0157","1986-10-26","817596159");
INSERT INTO customers VALUES("8611075771085","Mr","Motlatsi","Mokoaqatsa","Jean avenue","Centurion","Centurion","","0157","1986-11-07","0834477120");
INSERT INTO customers VALUES("8705315547089","Mr","Keaoleboga Godraciou","Nthebe","1 Quantum","Techno Park","Stellenbosch","","7600","1987-05-31","0613049247");
INSERT INTO customers VALUES("8708035545086","Mr","Tumelo","Modise","31 Seoul Complex,43 ","The Reeds","Centurion","","0157","1987-08-03","0731238839");
INSERT INTO customers VALUES("8708175079086","Mr","Jacobus Petrus","van der Merwe","37 Florence avenue","Bedfordview","Johannesburg","","2008","1987-08-17","0818279010");
INSERT INTO customers VALUES("8804046291081","Mr","Paseka","Chakela","Von Dessin ","Montgomery Park, Ran","Johannesburg","","2195","1988-04-04","0735672713");
INSERT INTO customers VALUES("8812156083081","Mr","Vikelathina","Dlamini","G475 Ncolosi Road","Umlazi Township","Durban","","4031","1988-12-15","0739840447");
INSERT INTO customers VALUES("8901075369083","Mr","Itumeleng","Seleka","34 Queens road","Parktown","Johannesburg","","2000","1989-01-07","0612881421");
INSERT INTO customers VALUES("8905230299085","Miss","Buhlebuzile","Kunene","32 Msauli Street","Crystal Park ","Benoni","","1515","1989-05-23","0764648597");
INSERT INTO customers VALUES("8906165263088","Mr","Thomas","Molete","Leogem Str","Midrand ","Johannesburg","","1682","1989-06-16","0847070009");
INSERT INTO customers VALUES("8908295327080","Mr","Thomas","Mabitle","3339 ZWANE STREET","Rockville","Soweto","","1818","1989-08-29","0810992575");
INSERT INTO customers VALUES("8908305579084","Mr","Obed Obakeng ","Malebye","109 Amber Hill","eco park","centurion","","0157","1998-08-30","0790708275");
INSERT INTO customers VALUES("9006015448085","Mr","Poele","Sebetha","59 Robert Sobukwe, 1","Sunnyside","Pretoria","","0002","1990-06-01","0620124709");
INSERT INTO customers VALUES("9009266033085","Mr","Ncedolethu","Matseke","03 Sir Walter ","Kuilsriver","Cape Town ","","7580","1990-09-26","0734527368");
INSERT INTO customers VALUES("9010040032088","Mr","Melanie","Ludik","12 santa fe, 46 caro","Mindalore","Krugersdorp","","1739","1990-10-04","0817144649");
INSERT INTO customers VALUES("9012265761086","Mr","Mulunghisi ","Mahwayi ","2 Rose Street","Florida","Roodeport","","1709","1990-12-26","0813073233");
INSERT INTO customers VALUES("9107010071086","Miss","marche","botes","155 Bembessi street","riverlea","joanmesburg","","2093","1991-07-01","0620215266");
INSERT INTO customers VALUES("9112245040088","Mr","kervin","davids","14 gazania street","riverlea","johannesburg","","2093","1991-12-24","0731756414");
INSERT INTO customers VALUES("9202185307082","Mr","Thabang","Phaleng","5203 EXCHANGE STREET","GRAND CENTRAL X1","MIDRAND","","1685","1992-02-18","0731491375");
INSERT INTO customers VALUES("9206035099088","Mr","HERMAN","KRITZINGER","13 LLOYS STREET","CELTISDAL","CENTURION","","0158","1992-06-03","0761178429");
INSERT INTO customers VALUES("9403120028086","Miss","Sharon-Lee","Liebenberg","76 Amaryllis Drive","Roodekrans","Roodepoort","","1724","1994-03-12","0820791180");
INSERT INTO customers VALUES("9901019999997","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","","0157","0000-00-00","");
INSERT INTO customers VALUES("9901019999998","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","","0157","0000-00-00","");
INSERT INTO customers VALUES("9901019999999","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","","0157","0000-00-00","");



DROP TABLE IF EXISTS dbbackup;

CREATE TABLE `dbbackup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbname` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS funder;

CREATE TABLE `funder` (
  `funderid` int(11) NOT NULL,
  `fundername` varchar(255) DEFAULT NULL,
  `contactname` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `Street` varchar(45) DEFAULT NULL,
  `Suburb` varchar(45) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `State` enum('EC','FS','GP','LP','MP','NW','NC','WC','KZ') DEFAULT NULL,
  `PostCode` char(4) DEFAULT NULL,
  `Dob` date DEFAULT NULL,
  `accountholdername` varchar(45) DEFAULT NULL,
  `bankname` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `accounttype` varchar(45) DEFAULT NULL,
  `branchcode` varchar(45) DEFAULT NULL,
  `FILEIDDOC` varchar(45) DEFAULT NULL,
  `FILEPROOFADR` varchar(45) DEFAULT NULL,
  `FILECONTRACT` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `applicationdate` date DEFAULT NULL,
  `sourceofincome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO funder VALUES("1","Vogsphere (Pty) Ltd","Mr. Mulunghisi Mahwayi","0791234854","info@vogsphere.co.za","","","","","","","","","","","","","","","","","");



DROP TABLE IF EXISTS globalsettings;

CREATE TABLE `globalsettings` (
  `globalsettingsid` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `logo` varchar(600) DEFAULT NULL,
  `legaltext` varchar(600) DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `suburb` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `registrationnumber` varchar(500) DEFAULT NULL,
  `ncr` varchar(1000) DEFAULT NULL,
  `terms` varchar(1000) DEFAULT NULL,
  `disclaimer` varchar(1000) DEFAULT NULL,
  `accountholdername` varchar(45) DEFAULT NULL,
  `bankname` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `branchcode` varchar(45) DEFAULT NULL,
  `accounttype` varchar(45) DEFAULT NULL,
  `homepage` varchar(5000) DEFAULT NULL,
  `adminpage` varchar(5000) DEFAULT NULL,
  `customerpage` varchar(5500) DEFAULT NULL,
  `vat` decimal(7,2) DEFAULT '0.00',
  `vatnumber` varchar(100) DEFAULT NULL,
  `mailhost` varchar(45) DEFAULT NULL,
  `port` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `SMTPSecure` varchar(45) DEFAULT NULL,
  `dev` varchar(1) DEFAULT NULL,
  `prod` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO globalsettings VALUES("3","Vogsphere Pty Ltd","0813073233","0793401754 ","info@vogsphere.co.za","http://www.vogsphere.co.za/eloan","ecashmeup.png","You must pay to us all amounts that are due and payable in terms of this Agreement, on or before the payment date,/\n/without any deduction or demand./\n/You have the right at any time to pay in advance any amounts owed to us without notice or penalty irrespective of/\n/whether or not they are due./\n/Bank charges are applicable for payments made via ATM and Internet banking transfers payment is free.","R"," 31 SEOUL 43 Mulder street","The Reeds","Centurion","GP","0158","2012/020274/74/07","NCRCP9272","You must pay to us all amounts that are due and payable in terms of this Agreement, on or before the payment date,/\n/without any deduction or demand./\n/You have the right at any time to pay in advance any amounts owed to us without notice or penalty irrespective of/\n/whether or not they are due./\n/Bank charges are applicable for payments made via ATM and Internet banking transfers payment is free.","Vogsphere (Pty) Ltd  is a registered Credit Provider (NCRCP9272)","Vogsphere (Pty) Ltd","FNB","62341624775","250655","1","","","","0.00","","","","","","","","");



DROP TABLE IF EXISTS item;

CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity` decimal(53,15) NOT NULL DEFAULT '1.000000000000000',
  `discount` decimal(53,2) NOT NULL DEFAULT '0.00',
  `common_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unitary_cost` decimal(53,15) NOT NULL DEFAULT '0.000000000000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8

;INSERT INTO item VALUES("1","7.000000000000000","0.00","4","1","Excepteur sint occaecat","179.540000000000000");
INSERT INTO item VALUES("2","8.000000000000000","0.00","4","2","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","207.080000000000000");
INSERT INTO item VALUES("3","2.000000000000000","0.00","4","","Ab illo inventore veritatis.","75.370000000000000");
INSERT INTO item VALUES("4","7.000000000000000","0.00","4","","Ab illo inventore veritatis.","304.110000000000000");
INSERT INTO item VALUES("5","4.000000000000000","0.00","4","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","501.480000000000000");
INSERT INTO item VALUES("6","2.000000000000000","0.00","5","3","Ullamco laboris nisi","99.150000000000000");
INSERT INTO item VALUES("7","4.000000000000000","0.00","5","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","762.330000000000000");
INSERT INTO item VALUES("8","2.000000000000000","0.00","5","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","195.130000000000000");
INSERT INTO item VALUES("9","1.000000000000000","0.00","5","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","171.960000000000000");
INSERT INTO item VALUES("10","9.000000000000000","0.00","5","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","862.450000000000000");
INSERT INTO item VALUES("11","9.000000000000000","0.00","6","","Ut enim ad minim","678.780000000000000");
INSERT INTO item VALUES("12","10.000000000000000","0.00","6","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","541.200000000000000");
INSERT INTO item VALUES("13","7.000000000000000","0.00","7","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("14","10.000000000000000","0.00","7","","Ullamco laboris nisi","183.490000000000000");
INSERT INTO item VALUES("15","1.000000000000000","0.00","7","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("16","7.000000000000000","0.00","7","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("17","5.000000000000000","0.00","7","","Ab illo inventore veritatis.","85.000000000000000");
INSERT INTO item VALUES("18","10.000000000000000","0.00","7","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("19","3.000000000000000","49.00","8","4","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","732.820000000000000");
INSERT INTO item VALUES("20","9.000000000000000","0.00","8","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","289.360000000000000");
INSERT INTO item VALUES("21","9.000000000000000","0.00","9","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","374.440000000000000");
INSERT INTO item VALUES("22","10.000000000000000","0.00","9","","Ab illo inventore veritatis.","235.260000000000000");
INSERT INTO item VALUES("23","5.000000000000000","0.00","9","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","564.020000000000000");
INSERT INTO item VALUES("24","7.000000000000000","0.00","9","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","778.330000000000000");
INSERT INTO item VALUES("25","6.000000000000000","0.00","9","","Ut enim ad minim","405.720000000000000");
INSERT INTO item VALUES("26","3.000000000000000","0.00","9","","Ullamco laboris nisi","265.870000000000000");
INSERT INTO item VALUES("27","3.000000000000000","0.00","9","","Ab illo inventore veritatis.","812.170000000000000");
INSERT INTO item VALUES("28","10.000000000000000","0.00","10","","Excepteur sint occaecat","363.890000000000000");
INSERT INTO item VALUES("29","10.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","611.030000000000000");
INSERT INTO item VALUES("30","2.000000000000000","0.00","10","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","520.550000000000000");
INSERT INTO item VALUES("31","4.000000000000000","0.00","10","","Excepteur sint occaecat","893.680000000000000");
INSERT INTO item VALUES("32","9.000000000000000","0.00","10","","Ab illo inventore veritatis.","284.850000000000000");
INSERT INTO item VALUES("33","6.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","718.040000000000000");
INSERT INTO item VALUES("34","10.000000000000000","62.00","11","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","128.390000000000000");
INSERT INTO item VALUES("35","1.000000000000000","0.00","11","","Ut enim ad minim","903.770000000000000");
INSERT INTO item VALUES("36","6.000000000000000","0.00","11","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","713.570000000000000");
INSERT INTO item VALUES("37","5.000000000000000","0.00","12","","Ut enim ad minim","980.990000000000000");
INSERT INTO item VALUES("38","9.000000000000000","0.00","12","","Excepteur sint occaecat","971.700000000000000");
INSERT INTO item VALUES("39","8.000000000000000","0.00","12","","Ullamco laboris nisi","486.850000000000000");
INSERT INTO item VALUES("40","7.000000000000000","0.00","12","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","665.750000000000000");
INSERT INTO item VALUES("41","1.000000000000000","0.00","12","","Ab illo inventore veritatis.","216.920000000000000");
INSERT INTO item VALUES("42","8.000000000000000","0.00","12","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","955.160000000000000");
INSERT INTO item VALUES("43","10.000000000000000","0.00","13","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","674.590000000000000");
INSERT INTO item VALUES("44","3.000000000000000","0.00","13","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","477.660000000000000");
INSERT INTO item VALUES("45","3.000000000000000","0.00","13","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","875.630000000000000");
INSERT INTO item VALUES("46","8.000000000000000","0.00","13","","Excepteur sint occaecat","215.920000000000000");
INSERT INTO item VALUES("47","9.000000000000000","0.00","13","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","498.990000000000000");
INSERT INTO item VALUES("48","4.000000000000000","0.00","13","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","484.870000000000000");
INSERT INTO item VALUES("49","7.000000000000000","0.00","14","","Excepteur sint occaecat","498.500000000000000");
INSERT INTO item VALUES("50","8.000000000000000","0.00","14","","Ullamco laboris nisi","433.020000000000000");
INSERT INTO item VALUES("51","5.000000000000000","0.00","14","","Ab illo inventore veritatis.","907.400000000000000");
INSERT INTO item VALUES("52","7.000000000000000","0.00","14","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","962.760000000000000");
INSERT INTO item VALUES("53","1.000000000000000","0.00","14","","Ab illo inventore veritatis.","809.860000000000000");
INSERT INTO item VALUES("54","9.000000000000000","0.00","14","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","676.910000000000000");
INSERT INTO item VALUES("55","3.000000000000000","0.00","15","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","312.120000000000000");
INSERT INTO item VALUES("56","3.000000000000000","0.00","15","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","214.680000000000000");
INSERT INTO item VALUES("57","1.000000000000000","0.00","16","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","751.670000000000000");
INSERT INTO item VALUES("58","7.000000000000000","0.00","16","","Excepteur sint occaecat","875.850000000000000");
INSERT INTO item VALUES("59","9.000000000000000","0.00","16","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","792.830000000000000");
INSERT INTO item VALUES("60","9.000000000000000","17.00","16","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","986.170000000000000");
INSERT INTO item VALUES("61","9.000000000000000","0.00","16","","Ut enim ad minim","386.750000000000000");
INSERT INTO item VALUES("62","3.000000000000000","0.00","17","","Ut enim ad minim","903.000000000000000");
INSERT INTO item VALUES("63","5.000000000000000","0.00","17","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","154.590000000000000");
INSERT INTO item VALUES("64","3.000000000000000","0.00","18","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","274.550000000000000");
INSERT INTO item VALUES("65","4.000000000000000","0.00","18","","Excepteur sint occaecat","427.860000000000000");
INSERT INTO item VALUES("66","5.000000000000000","0.00","18","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","737.140000000000000");
INSERT INTO item VALUES("67","1.000000000000000","0.00","18","","Ab illo inventore veritatis.","412.980000000000000");
INSERT INTO item VALUES("68","4.000000000000000","0.00","18","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","723.180000000000000");
INSERT INTO item VALUES("69","5.000000000000000","0.00","18","","Ullamco laboris nisi","7.040000000000000");
INSERT INTO item VALUES("70","6.000000000000000","6.00","19","","Excepteur sint occaecat","137.470000000000000");
INSERT INTO item VALUES("71","7.000000000000000","0.00","19","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","623.900000000000000");
INSERT INTO item VALUES("72","1.000000000000000","0.00","19","","Ab illo inventore veritatis.","67.070000000000000");
INSERT INTO item VALUES("73","1.000000000000000","0.00","19","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","30.980000000000000");
INSERT INTO item VALUES("74","10.000000000000000","0.00","19","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","222.390000000000000");
INSERT INTO item VALUES("75","6.000000000000000","0.00","19","","Ab illo inventore veritatis.","84.640000000000000");
INSERT INTO item VALUES("76","8.000000000000000","0.00","19","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","62.180000000000000");
INSERT INTO item VALUES("77","10.000000000000000","0.00","19","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","742.090000000000000");
INSERT INTO item VALUES("78","4.000000000000000","0.00","20","","Ullamco laboris nisi","780.380000000000000");
INSERT INTO item VALUES("79","10.000000000000000","0.00","20","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","99.680000000000000");
INSERT INTO item VALUES("80","6.000000000000000","0.00","20","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","490.360000000000000");
INSERT INTO item VALUES("81","7.000000000000000","0.00","20","","Ut enim ad minim","905.340000000000000");
INSERT INTO item VALUES("82","5.000000000000000","68.00","20","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","469.610000000000000");
INSERT INTO item VALUES("83","2.000000000000000","0.00","20","","Ut enim ad minim","540.820000000000000");
INSERT INTO item VALUES("84","9.000000000000000","42.00","21","","Ut enim ad minim","247.370000000000000");
INSERT INTO item VALUES("85","3.000000000000000","0.00","21","","Ullamco laboris nisi","281.760000000000000");
INSERT INTO item VALUES("86","9.000000000000000","7.00","21","","Ut enim ad minim","660.320000000000000");
INSERT INTO item VALUES("87","2.000000000000000","70.00","22","","Ab illo inventore veritatis.","968.820000000000000");
INSERT INTO item VALUES("88","3.000000000000000","0.00","22","","Ab illo inventore veritatis.","892.640000000000000");
INSERT INTO item VALUES("89","6.000000000000000","0.00","22","","Ab illo inventore veritatis.","30.820000000000000");
INSERT INTO item VALUES("90","9.000000000000000","0.00","23","","Excepteur sint occaecat","737.970000000000000");
INSERT INTO item VALUES("91","10.000000000000000","0.00","23","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","462.890000000000000");
INSERT INTO item VALUES("92","7.000000000000000","0.00","23","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","11.440000000000000");
INSERT INTO item VALUES("93","1.000000000000000","0.00","23","","Ab illo inventore veritatis.","882.310000000000000");
INSERT INTO item VALUES("94","1.000000000000000","0.00","1","","Iure reprehenderit qui in ea voluptate","586.170000000000000");
INSERT INTO item VALUES("95","1.000000000000000","0.00","2","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("96","7.000000000000000","0.00","2","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("97","3.000000000000000","0.00","2","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("98","3.000000000000000","0.00","3","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("99","3.000000000000000","0.00","3","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("100","4.000000000000000","0.00","3","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("101","7.000000000000000","0.00","24","","Excepteur sint occaecat","179.540000000000000");
INSERT INTO item VALUES("102","8.000000000000000","0.00","24","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","207.080000000000000");
INSERT INTO item VALUES("103","2.000000000000000","0.00","24","","Ab illo inventore veritatis.","75.370000000000000");
INSERT INTO item VALUES("104","7.000000000000000","0.00","24","","Ab illo inventore veritatis.","304.110000000000000");
INSERT INTO item VALUES("105","4.000000000000000","0.00","24","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","501.480000000000000");
INSERT INTO item VALUES("106","2.000000000000000","0.00","25","","Ullamco laboris nisi","99.150000000000000");
INSERT INTO item VALUES("107","4.000000000000000","0.00","25","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","762.330000000000000");
INSERT INTO item VALUES("108","2.000000000000000","0.00","25","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","195.130000000000000");
INSERT INTO item VALUES("109","1.000000000000000","0.00","25","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","171.960000000000000");
INSERT INTO item VALUES("110","9.000000000000000","0.00","25","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","862.450000000000000");
INSERT INTO item VALUES("111","9.000000000000000","0.00","26","","Ut enim ad minim","678.780000000000000");
INSERT INTO item VALUES("112","10.000000000000000","0.00","26","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","541.200000000000000");
INSERT INTO item VALUES("113","7.000000000000000","0.00","27","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","504.850000000000000");
INSERT INTO item VALUES("114","1.000000000000000","0.00","27","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","343.290000000000000");
INSERT INTO item VALUES("115","7.000000000000000","0.00","27","","Ab illo inventore veritatis.","303.460000000000000");
INSERT INTO item VALUES("116","5.000000000000000","0.00","27","","Ab illo inventore veritatis.","85.000000000000000");
INSERT INTO item VALUES("117","10.000000000000000","0.00","27","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","899.350000000000000");
INSERT INTO item VALUES("118","3.000000000000000","49.00","28","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","732.820000000000000");
INSERT INTO item VALUES("119","9.000000000000000","0.00","28","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","289.360000000000000");
INSERT INTO item VALUES("120","1.000000000000000","0.00","29","1","test product 1 description","179.540000000000000");
INSERT INTO item VALUES("121","29.000000000000000","0.00","30","","Lab preparation for exam and antivirus installation","250.000000000000000");
INSERT INTO item VALUES("122","1.000000000000000","0.00","31","1","test product 1 description","179.540000000000000");
INSERT INTO item VALUES("123","1.000000000000000","0.00","32","","Iure reprehenderit qui in ea voluptate","586.170000000000000");
INSERT INTO item VALUES("124","1.000000000000000","0.00","33","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("125","7.000000000000000","0.00","33","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("126","3.000000000000000","0.00","33","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("127","1.000000000000000","0.00","34","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("128","7.000000000000000","0.00","34","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("129","3.000000000000000","0.00","34","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("130","1.000000000000000","0.00","35","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("131","7.000000000000000","0.00","35","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("132","3.000000000000000","0.00","35","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("133","1.000000000000000","0.00","36","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("134","7.000000000000000","0.00","36","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("135","3.000000000000000","0.00","36","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("136","1.000000000000000","0.00","37","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("137","7.000000000000000","0.00","37","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("138","3.000000000000000","0.00","37","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("139","1.000000000000000","0.00","38","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("140","7.000000000000000","0.00","38","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("141","3.000000000000000","0.00","38","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("142","1.000000000000000","0.00","39","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("143","7.000000000000000","0.00","39","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("144","3.000000000000000","0.00","39","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("145","1.000000000000000","0.00","40","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("146","7.000000000000000","0.00","40","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("147","3.000000000000000","0.00","40","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("148","1.000000000000000","0.00","41","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("149","7.000000000000000","0.00","41","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("150","3.000000000000000","0.00","41","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("151","1.000000000000000","0.00","42","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("152","7.000000000000000","0.00","42","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("153","3.000000000000000","0.00","42","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("154","1.000000000000000","0.00","43","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("155","7.000000000000000","0.00","43","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("156","3.000000000000000","0.00","43","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("157","1.000000000000000","0.00","44","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("158","7.000000000000000","0.00","44","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("159","3.000000000000000","0.00","44","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("160","1.000000000000000","0.00","45","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("161","7.000000000000000","0.00","45","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("162","3.000000000000000","0.00","45","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("163","1.000000000000000","0.00","46","","Nemo enim ipsam voluptatem quia voluptas","881.860000000000000");
INSERT INTO item VALUES("164","7.000000000000000","0.00","46","","Iure reprehenderit qui in ea voluptate","64.370000000000000");
INSERT INTO item VALUES("165","3.000000000000000","0.00","46","","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.","122.410000000000000");
INSERT INTO item VALUES("166","1.000000000000000","0.00","47","1","test product 1 description","179.540000000000000");
INSERT INTO item VALUES("167","3.000000000000000","0.00","48","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("168","3.000000000000000","0.00","48","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("169","4.000000000000000","0.00","48","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("170","3.000000000000000","0.00","49","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("171","3.000000000000000","0.00","49","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("172","4.000000000000000","0.00","49","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("173","3.000000000000000","0.00","50","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("174","3.000000000000000","0.00","50","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("175","4.000000000000000","0.00","50","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("176","3.000000000000000","0.00","51","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("177","3.000000000000000","0.00","51","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("178","4.000000000000000","0.00","51","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("179","3.000000000000000","0.00","52","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("180","3.000000000000000","0.00","52","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("181","4.000000000000000","0.00","52","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("182","3.000000000000000","0.00","53","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("183","3.000000000000000","0.00","53","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("184","4.000000000000000","0.00","53","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("185","3.000000000000000","0.00","54","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("186","3.000000000000000","0.00","54","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("187","4.000000000000000","0.00","54","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("188","3.000000000000000","0.00","55","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("189","3.000000000000000","0.00","55","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("190","4.000000000000000","0.00","55","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("191","3.000000000000000","0.00","56","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("192","3.000000000000000","0.00","56","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("193","4.000000000000000","0.00","56","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("194","3.000000000000000","0.00","57","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("195","3.000000000000000","0.00","57","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("196","4.000000000000000","0.00","57","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("197","3.000000000000000","0.00","58","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("198","3.000000000000000","0.00","58","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("199","4.000000000000000","0.00","58","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("200","3.000000000000000","0.00","59","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("201","3.000000000000000","0.00","59","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("202","4.000000000000000","0.00","59","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("203","3.000000000000000","0.00","60","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("204","3.000000000000000","0.00","60","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("205","4.000000000000000","0.00","60","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("206","3.000000000000000","0.00","61","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("207","3.000000000000000","0.00","61","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("208","4.000000000000000","0.00","61","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("209","3.000000000000000","0.00","62","","Adipisci velit, sed quia non numquam","936.650000000000000");
INSERT INTO item VALUES("210","3.000000000000000","0.00","62","","Ut enim ad minima","80.520000000000000");
INSERT INTO item VALUES("211","4.000000000000000","0.00","62","","Ullamco laboris nisi","731.360000000000000");
INSERT INTO item VALUES("212","1.000000000000000","0.00","63","1","test product 1 description","179.540000000000000");
INSERT INTO item VALUES("213","1.000000000000000","0.00","64","","2GB Web hosting","80.000000000000000");
INSERT INTO item VALUES("214","3.000000000000000","0.00","67","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("215","1.000000000000000","50.00","67","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("216","3.000000000000000","0.00","68","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("217","1.000000000000000","50.00","68","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("218","3.000000000000000","0.00","69","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("219","1.000000000000000","50.00","69","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("220","3.000000000000000","0.00","70","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("221","1.000000000000000","50.00","70","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("222","3.000000000000000","0.00","71","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("223","1.000000000000000","50.00","71","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("224","3.000000000000000","0.00","72","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("225","1.000000000000000","50.00","72","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("226","3.000000000000000","0.00","73","1","nnnn","101.000000000000000");
INSERT INTO item VALUES("227","1.000000000000000","50.00","73","2","dsdsd","201.000000000000000");
INSERT INTO item VALUES("228","10.000000000000000","0.00","10","","Excepteur sint occaecat","363.890000000000000");
INSERT INTO item VALUES("229","10.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","611.030000000000000");
INSERT INTO item VALUES("230","2.000000000000000","0.00","10","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","520.550000000000000");
INSERT INTO item VALUES("231","4.000000000000000","0.00","10","","Excepteur sint occaecat","893.680000000000000");
INSERT INTO item VALUES("232","9.000000000000000","0.00","10","","Ab illo inventore veritatis.","284.850000000000000");
INSERT INTO item VALUES("233","6.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","718.040000000000000");
INSERT INTO item VALUES("234","10.000000000000000","0.00","10","","Excepteur sint occaecat","363.890000000000000");
INSERT INTO item VALUES("235","10.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","611.030000000000000");
INSERT INTO item VALUES("236","2.000000000000000","0.00","10","","Lorem ipsum dolor sit amet, consectetur adipisicing elit.","520.550000000000000");
INSERT INTO item VALUES("237","4.000000000000000","0.00","10","","Excepteur sint occaecat","893.680000000000000");
INSERT INTO item VALUES("238","9.000000000000000","0.00","10","","Ab illo inventore veritatis.","284.850000000000000");
INSERT INTO item VALUES("239","6.000000000000000","0.00","10","","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","718.040000000000000");
INSERT INTO item VALUES("240","1.000000000000000","0.00","7","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("241","1.000000000000000","0.00","7","0","dsdsd","20.000000000000000");
INSERT INTO item VALUES("242","1.000000000000000","0.00","7","0","dsdsd","20.000000000000000");
INSERT INTO item VALUES("243","1.000000000000000","0.00","7","0","dsd","20.000000000000000");
INSERT INTO item VALUES("244","1.000000000000000","0.00","10","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("245","1.000000000000000","0.00","10","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("246","1.000000000000000","0.00","10","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("247","1.000000000000000","0.00","10","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("248","1.000000000000000","0.00","10","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("249","1.000000000000000","0.00","10","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("250","1.000000000000000","0.00","10","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("251","1.000000000000000","0.00","10","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("252","1.000000000000000","0.00","10","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("253","1.000000000000000","0.00","10","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("254","1.000000000000000","0.00","10","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("255","1.000000000000000","0.00","75","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("256","1.000000000000000","0.00","75","13","		  		  tsetsests		  		  		  		  ","20000.000000000000000");
INSERT INTO item VALUES("257","1.000000000000000","0.00","75","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("258","1.000000000000000","0.00","76","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("259","1.000000000000000","0.00","77","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("260","1.000000000000000","0.00","78","4","test product 4 description","100.820000000000000");
INSERT INTO item VALUES("261","1.000000000000000","0.00","79","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("262","1.000000000000000","0.00","80","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("263","1.000000000000000","0.00","81","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("264","1.000000000000000","0.00","82","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("265","1.000000000000000","0.00","83","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("266","1.000000000000000","0.00","84","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("267","1.000000000000000","0.00","85","3","test product 3 description","99.150000000000000");
INSERT INTO item VALUES("268","1.000000000000000","0.00","86","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("269","1.000000000000000","0.00","86","4","test product 4 description","732.820000000000000");
INSERT INTO item VALUES("270","1.000000000000000","0.00","87","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("271","1.000000000000000","0.00","88","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("272","1.000000000000000","0.00","89","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("273","1.000000000000000","0.00","90","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("274","1.000000000000000","0.00","91","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("275","1.000000000000000","0.00","92","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("276","1.000000000000000","0.00","93","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("277","1.000000000000000","0.00","94","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("278","1.000000000000000","0.00","95","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("279","1.000000000000000","0.00","95","13","		  		  tsetsests		  		  		  		  ","20000.000000000000000");
INSERT INTO item VALUES("280","1.000000000000000","0.00","96","1","test product 1 description - modise","179.540000000000000");
INSERT INTO item VALUES("281","1.000000000000000","0.00","96","13","		  		  tsetsests		  		  		  		  ","20000.000000000000000");



DROP TABLE IF EXISTS item_tax;

CREATE TABLE `item_tax` (
  `item_id` bigint(20) NOT NULL DEFAULT '0',
  `tax_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO item_tax VALUES("1","1");
INSERT INTO item_tax VALUES("1","4");
INSERT INTO item_tax VALUES("2","3");
INSERT INTO item_tax VALUES("3","3");
INSERT INTO item_tax VALUES("4","1");
INSERT INTO item_tax VALUES("4","2");
INSERT INTO item_tax VALUES("4","3");
INSERT INTO item_tax VALUES("5","1");
INSERT INTO item_tax VALUES("6","1");
INSERT INTO item_tax VALUES("7","1");
INSERT INTO item_tax VALUES("8","1");
INSERT INTO item_tax VALUES("9","3");
INSERT INTO item_tax VALUES("10","1");
INSERT INTO item_tax VALUES("11","1");
INSERT INTO item_tax VALUES("12","2");
INSERT INTO item_tax VALUES("13","1");
INSERT INTO item_tax VALUES("14","1");
INSERT INTO item_tax VALUES("15","1");
INSERT INTO item_tax VALUES("16","3");
INSERT INTO item_tax VALUES("17","2");
INSERT INTO item_tax VALUES("18","4");
INSERT INTO item_tax VALUES("19","3");
INSERT INTO item_tax VALUES("20","3");
INSERT INTO item_tax VALUES("21","1");
INSERT INTO item_tax VALUES("22","1");
INSERT INTO item_tax VALUES("23","2");
INSERT INTO item_tax VALUES("24","1");
INSERT INTO item_tax VALUES("25","1");
INSERT INTO item_tax VALUES("26","2");
INSERT INTO item_tax VALUES("27","1");
INSERT INTO item_tax VALUES("28","4");
INSERT INTO item_tax VALUES("29","1");
INSERT INTO item_tax VALUES("30","1");
INSERT INTO item_tax VALUES("31","2");
INSERT INTO item_tax VALUES("32","1");
INSERT INTO item_tax VALUES("33","1");
INSERT INTO item_tax VALUES("34","1");
INSERT INTO item_tax VALUES("35","1");
INSERT INTO item_tax VALUES("36","1");
INSERT INTO item_tax VALUES("37","2");
INSERT INTO item_tax VALUES("38","1");
INSERT INTO item_tax VALUES("39","4");
INSERT INTO item_tax VALUES("40","1");
INSERT INTO item_tax VALUES("41","3");
INSERT INTO item_tax VALUES("42","1");
INSERT INTO item_tax VALUES("43","4");
INSERT INTO item_tax VALUES("44","1");
INSERT INTO item_tax VALUES("45","2");
INSERT INTO item_tax VALUES("46","1");
INSERT INTO item_tax VALUES("47","1");
INSERT INTO item_tax VALUES("47","4");
INSERT INTO item_tax VALUES("48","2");
INSERT INTO item_tax VALUES("48","3");
INSERT INTO item_tax VALUES("49","4");
INSERT INTO item_tax VALUES("50","4");
INSERT INTO item_tax VALUES("51","1");
INSERT INTO item_tax VALUES("52","1");
INSERT INTO item_tax VALUES("53","2");
INSERT INTO item_tax VALUES("54","2");
INSERT INTO item_tax VALUES("55","1");
INSERT INTO item_tax VALUES("56","4");
INSERT INTO item_tax VALUES("57","2");
INSERT INTO item_tax VALUES("58","3");
INSERT INTO item_tax VALUES("59","4");
INSERT INTO item_tax VALUES("60","1");
INSERT INTO item_tax VALUES("60","3");
INSERT INTO item_tax VALUES("60","4");
INSERT INTO item_tax VALUES("61","1");
INSERT INTO item_tax VALUES("61","4");
INSERT INTO item_tax VALUES("62","4");
INSERT INTO item_tax VALUES("63","1");
INSERT INTO item_tax VALUES("64","1");
INSERT INTO item_tax VALUES("65","2");
INSERT INTO item_tax VALUES("66","4");
INSERT INTO item_tax VALUES("67","4");
INSERT INTO item_tax VALUES("68","2");
INSERT INTO item_tax VALUES("69","1");
INSERT INTO item_tax VALUES("70","1");
INSERT INTO item_tax VALUES("71","1");
INSERT INTO item_tax VALUES("72","1");
INSERT INTO item_tax VALUES("73","1");
INSERT INTO item_tax VALUES("74","2");
INSERT INTO item_tax VALUES("75","3");
INSERT INTO item_tax VALUES("76","1");
INSERT INTO item_tax VALUES("77","3");
INSERT INTO item_tax VALUES("78","4");
INSERT INTO item_tax VALUES("79","4");
INSERT INTO item_tax VALUES("80","4");
INSERT INTO item_tax VALUES("81","3");
INSERT INTO item_tax VALUES("82","2");
INSERT INTO item_tax VALUES("83","1");
INSERT INTO item_tax VALUES("84","3");
INSERT INTO item_tax VALUES("85","1");
INSERT INTO item_tax VALUES("86","2");
INSERT INTO item_tax VALUES("87","1");
INSERT INTO item_tax VALUES("88","1");
INSERT INTO item_tax VALUES("89","1");
INSERT INTO item_tax VALUES("90","2");
INSERT INTO item_tax VALUES("91","1");
INSERT INTO item_tax VALUES("92","1");
INSERT INTO item_tax VALUES("93","2");
INSERT INTO item_tax VALUES("94","3");
INSERT INTO item_tax VALUES("95","1");
INSERT INTO item_tax VALUES("96","1");
INSERT INTO item_tax VALUES("97","1");
INSERT INTO item_tax VALUES("98","2");
INSERT INTO item_tax VALUES("99","2");
INSERT INTO item_tax VALUES("100","2");
INSERT INTO item_tax VALUES("101","1");
INSERT INTO item_tax VALUES("101","4");
INSERT INTO item_tax VALUES("102","3");
INSERT INTO item_tax VALUES("103","3");
INSERT INTO item_tax VALUES("104","1");
INSERT INTO item_tax VALUES("104","2");
INSERT INTO item_tax VALUES("104","3");
INSERT INTO item_tax VALUES("105","1");
INSERT INTO item_tax VALUES("106","1");
INSERT INTO item_tax VALUES("107","1");
INSERT INTO item_tax VALUES("108","1");
INSERT INTO item_tax VALUES("109","3");
INSERT INTO item_tax VALUES("110","1");
INSERT INTO item_tax VALUES("112","2");
INSERT INTO item_tax VALUES("114","1");
INSERT INTO item_tax VALUES("115","3");
INSERT INTO item_tax VALUES("116","2");
INSERT INTO item_tax VALUES("117","4");
INSERT INTO item_tax VALUES("118","3");
INSERT INTO item_tax VALUES("119","3");
INSERT INTO item_tax VALUES("123","3");
INSERT INTO item_tax VALUES("124","1");
INSERT INTO item_tax VALUES("125","1");
INSERT INTO item_tax VALUES("126","1");
INSERT INTO item_tax VALUES("127","1");
INSERT INTO item_tax VALUES("128","1");
INSERT INTO item_tax VALUES("129","1");
INSERT INTO item_tax VALUES("130","1");
INSERT INTO item_tax VALUES("131","1");
INSERT INTO item_tax VALUES("132","1");
INSERT INTO item_tax VALUES("133","1");
INSERT INTO item_tax VALUES("134","1");
INSERT INTO item_tax VALUES("135","1");
INSERT INTO item_tax VALUES("136","1");
INSERT INTO item_tax VALUES("137","1");
INSERT INTO item_tax VALUES("138","1");
INSERT INTO item_tax VALUES("139","1");
INSERT INTO item_tax VALUES("140","1");
INSERT INTO item_tax VALUES("141","1");
INSERT INTO item_tax VALUES("142","1");
INSERT INTO item_tax VALUES("143","1");
INSERT INTO item_tax VALUES("144","1");
INSERT INTO item_tax VALUES("145","1");
INSERT INTO item_tax VALUES("146","1");
INSERT INTO item_tax VALUES("147","1");
INSERT INTO item_tax VALUES("148","1");
INSERT INTO item_tax VALUES("149","1");
INSERT INTO item_tax VALUES("150","1");
INSERT INTO item_tax VALUES("151","1");
INSERT INTO item_tax VALUES("152","1");
INSERT INTO item_tax VALUES("153","1");
INSERT INTO item_tax VALUES("154","1");
INSERT INTO item_tax VALUES("155","1");
INSERT INTO item_tax VALUES("156","1");
INSERT INTO item_tax VALUES("157","1");
INSERT INTO item_tax VALUES("158","1");
INSERT INTO item_tax VALUES("159","1");
INSERT INTO item_tax VALUES("160","1");
INSERT INTO item_tax VALUES("161","1");
INSERT INTO item_tax VALUES("162","1");
INSERT INTO item_tax VALUES("163","1");
INSERT INTO item_tax VALUES("164","1");
INSERT INTO item_tax VALUES("165","1");
INSERT INTO item_tax VALUES("167","2");
INSERT INTO item_tax VALUES("168","2");
INSERT INTO item_tax VALUES("169","2");
INSERT INTO item_tax VALUES("170","2");
INSERT INTO item_tax VALUES("171","2");
INSERT INTO item_tax VALUES("172","2");
INSERT INTO item_tax VALUES("173","2");
INSERT INTO item_tax VALUES("174","2");
INSERT INTO item_tax VALUES("175","2");
INSERT INTO item_tax VALUES("176","2");
INSERT INTO item_tax VALUES("177","2");
INSERT INTO item_tax VALUES("178","2");
INSERT INTO item_tax VALUES("179","2");
INSERT INTO item_tax VALUES("180","2");
INSERT INTO item_tax VALUES("181","2");
INSERT INTO item_tax VALUES("182","2");
INSERT INTO item_tax VALUES("183","2");
INSERT INTO item_tax VALUES("184","2");
INSERT INTO item_tax VALUES("185","2");
INSERT INTO item_tax VALUES("186","2");
INSERT INTO item_tax VALUES("187","2");
INSERT INTO item_tax VALUES("188","2");
INSERT INTO item_tax VALUES("189","2");
INSERT INTO item_tax VALUES("190","2");
INSERT INTO item_tax VALUES("191","2");
INSERT INTO item_tax VALUES("192","2");
INSERT INTO item_tax VALUES("193","2");
INSERT INTO item_tax VALUES("194","2");
INSERT INTO item_tax VALUES("195","2");
INSERT INTO item_tax VALUES("196","2");
INSERT INTO item_tax VALUES("197","2");
INSERT INTO item_tax VALUES("198","2");
INSERT INTO item_tax VALUES("199","2");
INSERT INTO item_tax VALUES("200","2");
INSERT INTO item_tax VALUES("201","2");
INSERT INTO item_tax VALUES("202","2");
INSERT INTO item_tax VALUES("203","2");
INSERT INTO item_tax VALUES("204","2");
INSERT INTO item_tax VALUES("205","2");
INSERT INTO item_tax VALUES("206","2");
INSERT INTO item_tax VALUES("207","2");
INSERT INTO item_tax VALUES("208","2");
INSERT INTO item_tax VALUES("209","2");
INSERT INTO item_tax VALUES("210","2");
INSERT INTO item_tax VALUES("211","2");
INSERT INTO item_tax VALUES("258","5");
INSERT INTO item_tax VALUES("261","5");
INSERT INTO item_tax VALUES("262","5");
INSERT INTO item_tax VALUES("263","5");
INSERT INTO item_tax VALUES("264","5");
INSERT INTO item_tax VALUES("265","5");
INSERT INTO item_tax VALUES("266","5");
INSERT INTO item_tax VALUES("267","5");
INSERT INTO item_tax VALUES("268","5");
INSERT INTO item_tax VALUES("278","5");
INSERT INTO item_tax VALUES("280","5");



DROP TABLE IF EXISTS loanapp;

CREATE TABLE `loanapp` (
  `ApplicationId` int(11) NOT NULL,
  `CustomerId` bigint(13) NOT NULL,
  `loantypeid` int(11) NOT NULL,
  `MonthlyIncome` decimal(7,2) DEFAULT NULL,
  `MonthlyExpenditure` decimal(7,2) DEFAULT NULL,
  `TotalAssets` decimal(7,2) DEFAULT NULL,
  `ReqLoadValue` decimal(7,2) DEFAULT NULL,
  `ExecApproval` enum('APPR','PEN','CAN','REJ','SET') DEFAULT 'PEN',
  `DateAccpt` date DEFAULT NULL,
  `StaffId` int(11) DEFAULT NULL,
  `InterestRate` decimal(7,4) NOT NULL,
  `LoanDuration` int(10) unsigned NOT NULL,
  `LoanValue` decimal(7,2) NOT NULL,
  `surety` varchar(45) NOT NULL,
  `Repayment` decimal(7,2) NOT NULL,
  `paymentmethod` varchar(45) NOT NULL,
  `FirstPaymentDate` date NOT NULL,
  `lastPaymentDate` date NOT NULL,
  `monthlypayment` decimal(7,2) NOT NULL,
  `paymentfrequency` varchar(45) DEFAULT NULL,
  `FILEIDDOC` varchar(200) DEFAULT NULL,
  `FILECONTRACT` varchar(200) DEFAULT NULL,
  `FILEBANKSTATEMENT` varchar(200) DEFAULT NULL,
  `FILEPROOFEMP` varchar(200) DEFAULT NULL,
  `FILEFICA` varchar(200) DEFAULT NULL,
  `accountholdername` varchar(45) DEFAULT NULL,
  `bankname` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `branchcode` varchar(45) DEFAULT NULL,
  `accounttype` varchar(45) DEFAULT NULL,
  `funderInterest` decimal(7,4) DEFAULT '1.0000',
  `rejectreason` varchar(255) DEFAULT NULL,
  `FILECREDITREP` varchar(65) DEFAULT NULL,
  `FILECONSENTFORM` varchar(65) DEFAULT NULL,
  `FILEOTHER` varchar(65) DEFAULT NULL,
  `FILEOTHER2` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO loanapp VALUES("39","8705315547089","1","16655.96","12000.00","0.00","5000.00","SET","2016-08-16","1","20.0000","3","0.00","IPhone 5","6000.00","DTO","2016-09-28","2016-12-28","2000.00","MON","Kea_ID.pdf","11-09-2016-18-49-22knthebe_contract.pdf","CapitecBankStatement_07072016-06082016.pdf","11-09-2016-18-49-10June Payslip.xps","CapitecBankStatement_07072016-06082016.pdf","Keaoleboga Godraciou","ABSA","999999999999999","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("41","8905230299085","1","25000.00","6500.00","0.00","25000.00","REJ","2016-08-18","1","20.0000","6","0.00","Vodacom Tablet","30000.00","EFT","2016-09-30","0000-00-00","5000.00","MON","","","","","","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("44","8908295327080","1","14000.00","1750.00","0.00","10000.00","REJ","2016-09-01","1","20.0000","12","0.00","Washing Machine, 40inch TV","12000.00","EFT","2016-09-30","0000-00-00","1000.00","MON","ID.pdf","","BANK STATEMENT.pdf","PROOF OF ADDRESS.pdf","PROOF OF ADDRESS.pdf","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("46","8908295327080","1","14000.00","1750.00","0.00","5000.00","APPR","2016-09-02","1","20.0000","6","0.00","40inch plasma","6000.00","DTO","2017-05-30","2017-11-30","1000.00","MON","PROOF OF ADDRESS.pdf","","BANK STATEMENT.pdf","Signed Appointment letter.pdf","PROOF OF ADDRESS.pdf","Mr Thomas Mabitle","ABSA","99999999999","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("47","8611075771085","1","24834.49","12000.00","0.00","5000.00","SET","2016-09-02","1","20.0000","6","0.00","TV , Laptop","6000.00","DTO","2016-09-30","2017-03-30","1000.00","MON","ID.pdf","Contract_09072016.pdf","account_statement (1).pdf","payslip.pdf","Proof of residence.pdf","Motlatsi Mokoaqatsa","ABSA","99999999999","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("48","9012265761086","1","28000.00","15000.00","0.00","6000.00","SET","2016-09-05","1","20.0000","6","0.00","Car Ford Figo","7200.00","EFT","2015-12-26","2016-06-26","1200.00","MON","","","","","","MP Mahwayi","ABSA","4073174082","6320005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("49","9012265761086","1","28000.00","15000.00","0.00","11000.00","SET","2016-09-05","1","20.0000","6","0.00","Car Ford Figo","13200.00","DTO","2016-08-28","2017-02-28","2200.00","MON","","","","","","Mr MP Mahwayi","ABSA","4073174082","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("50","9012265761086","1","28000.00","15000.00","0.00","9000.00","APPR","2016-09-05","1","20.0000","9","0.00","Car Ford Figo","10800.00","EFT","2016-09-26","2017-06-26","1200.00","MON","","","","","","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("51","8601045310080","1","52000.00","28000.00","0.00","8000.00","SET","2016-09-06","1","20.0000","2","0.00","Smart TV","9600.00","DTO","2016-09-06","0000-00-00","4800.00","MON","","","","","","Nthwana Nicodema","ABSA","99999999999","462005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("52","8409055665082","1","10000.00","5000.00","0.00","1000.00","CAN","2016-10-02","1","20.0000","1","0.00","house contents","1200.00","EFT","2016-09-26","2016-10-26","1200.00","MON","Driver license.pdf","","Bank statement.pdf","Pay slip.pdf","Bank statement.pdf","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("53","8708035545086","1","15000.00","6000.00","0.00","5638.00","SET","2016-09-11","1","20.0000","6","0.00","Samsung Galaxy, Mini Cooper","6765.60","EFT","2015-11-20","2016-05-20","1127.60","MON","11-09-2016-18-11-04DOC001.PDF","DOC001.PDF","","","11-09-2016-18-16-41ISU_PDF_GEN_1_Email (1).pd","Tumelo Macdonald Modise","ABSA","4805548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("54","8708035545086","1","15000.00","6000.00","0.00","5000.00","SET","2016-09-11","1","20.0000","6","0.00","Samsung Galaxy, Mini Cooper","6000.00","EFT","2016-03-29","2016-09-29","1000.00","MON","11-09-2016-18-14-59DOC001.PDF","","","","11-09-2016-18-19-00ISU_PDF_GEN_1_Email.pdf","Tumelo Macdonald Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("55","8309051276084","1","20000.00","12000.00","0.00","20000.00","SET","2016-09-11","1","20.0000","6","0.00","Nissan Family Car","24000.00","EFT","2015-12-25","2016-06-25","4000.00","MON","","","","","","M J Marenyane","capitec","1366948015","470010","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("56","8309051276084","1","20000.00","12000.00","0.00","20000.00","SET","2016-09-11","1","20.0000","4","0.00","Nissan Family Car","24000.00","EFT","2014-02-25","2014-06-25","6000.00","MON","","","","","","M J Marenyane","capitec","1366948015","470010","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("57","8908305579084","1","13238.00","6000.00","0.00","6000.00","SET","2016-09-16","1","20.0000","3","0.00","Lenovo 11.6 NoteBook Laptop and LG LED TV","7200.00","EFT","2016-04-25","2016-07-25","2400.00","MON","","","","","","Mr Obed Malebye","StandardBank","10037121152","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("58","8908305579084","1","13238.00","6000.00","0.00","500.00","SET","2016-09-16","1","20.0000","1","0.00","Lenovo 11.6 NoteBook Laptop and LG LED TV","600.00","EFT","2016-09-28","2016-10-28","600.00","MON","","","","","","Mr Obed Malebye","StandardBank","10037121152","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("59","8908305579084","1","13238.00","6000.00","0.00","4000.00","SET","2016-09-16","1","20.0000","3","0.00","Lenovo 11.6 NoteBook Laptop and LG LED TV","4800.00","EFT","2016-07-01","0000-00-00","1600.00","MON","","","","","","Mr Obed Malebye","StandardBank","10037121152","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("60","8905230299085","1","20000.00","10000.00","0.00","3500.00","SET","2016-09-16","1","20.0000","4","0.00","Vodacom Tablet","4200.00","EFT","2016-03-25","2016-07-25","1050.00","MON","","","","","","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("61","8905230299085","1","20000.00","10000.00","0.00","1500.00","SET","2016-09-16","1","20.0000","2","0.00","Vodacom Tablet","1800.00","EFT","2016-01-25","2016-03-25","900.00","MON","","","","","","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("62","8105150506086","1","25000.00","15000.00","0.00","2000.00","SET","2016-09-16","1","20.0000","1","0.00","Cell Phone","2400.00","EFT","2016-08-16","2016-09-16","2400.00","MON","","","","","","C.C.Jada","ABSA","4086728505","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("63","8908295327080","1","14000.00","1750.00","0.00","5000.00","SET","2016-09-16","1","20.0000","3","0.00","Washing Machine, 40inch TV","6000.00","DTO","2016-06-27","2016-09-27","2000.00","MON","","","","","","Mr Thomas Mabitle","ABSA","99999999999","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("64","8504045897082","1","8000.00","4500.00","0.00","5000.00","CAN","2016-10-02","1","20.0000","6","0.00","Polo TSI","6000.00","EFT","2016-10-15","2017-04-15","1000.00","MON","","","","","","","","","","","1.0000","","","","","");
INSERT INTO loanapp VALUES("65","8708035545086","1","15000.00","8000.00","0.00","10000.00","SET","2016-09-21","1","20.0000","6","0.00","Mini Cooper AND Samsung Galaxy Note III","12000.00","DTO","2016-10-17","2017-04-17","2000.00","MON","21-09-2016-15-46-42IDMODISE.PDF","13-10-2016-08-40-36Contract.PDF","","","21-09-2016-15-46-55ISU_PDF_GEN_1_Email.pdf","Tumelo Macdonald Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("66","8908305579084","1","11200.00","8000.00","0.00","2000.00","SET","2016-10-01","1","20.0000","3","0.00","LG 32LN5700: 32 inch Class 1080p LED TV","2400.00","EFT","2016-10-25","2017-01-25","800.00","MON","03-10-2016-20-15-56Copy of ID (1).pdf","03-10-2016-11-24-39obed contract.pdf","","03-10-2016-20-21-38MAOB 30 Sep 2016.pdf","03-10-2016-20-41-03Obed documents 1.pdf","Obed Obakeng Malebye","StandardBank","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("67","8504045897082","1","7000.00","4900.00","0.00","5000.00","SET","2016-10-05","1","20.0000","6","0.00","Polo TSI 2014","6000.00","EFT","2016-10-17","2017-04-17","1000.00","MON","05-10-2016-22-14-36Abram ID.pdf","06-10-2016-19-21-32signed contract .pdf","05-10-2016-22-17-50Bank Statement.pdf","05-10-2016-22-18-08Payslip.pdf","05-10-2016-22-18-37Bank Statement.pdf","Setlogelo A Mopeloa","fnb","5277965301","462005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("68","8610265252088","1","12000.00","4500.00","0.00","1650.00","SET","2016-10-06","1","20.0000","3","0.00","0817596159","1980.00","EFT","2016-10-25","2017-01-25","660.00","MON","10-10-2016-16-15-22id dpf.pdf","10-10-2016-16-24-12updated-av-davit.pdf","07-10-2016-07-32-1362568310123 2016-09-29.pdf","07-10-2016-07-28-37MKGC 30 Sep 2016.pdf","07-10-2016-07-32-5662568310123 2016-09-29.pdf","Gcobani Mkontwana","fnb","62568310123","200510","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("69","9006015448085","1","18500.00","9000.00","0.00","5000.00","SET","2016-10-18","1","20.0000","6","0.00","iPhone 6","6000.00","DTO","2016-11-15","2017-05-15","1000.00","MON","18-10-2016-08-55-22ID COPY.pdf","19-10-2016-10-40-05contract.PDF","18-10-2016-14-01-26statement.pdf","18-10-2016-08-55-52payslip.pdf","18-10-2016-08-55-41proofofres.pdf","Sebetha Poele","fnb","62422486292","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("70","8306145448083","1","25000.00","20000.00","0.00","45000.00","CAN","2016-11-30","1","20.0000","24","0.00","Chevrolet Cruze, 2009 Model","54000.00","EFT","2017-01-01","0000-00-00","2250.00","MON","21-11-2016-09-34-17Sifiso ID.pdf","","21-11-2016-09-35-01ChecqueAcc_Nov2016_monthly","21-11-2016-09-36-34paySlip_November2016.pdf","21-11-2016-09-36-00Statement-JoburgCity_Oct20","Sifiso Mthembu","ABSA","62256611693","200810","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("71","8804046291081","1","38500.00","18000.00","0.00","2000.00","SET","2016-11-08","1","20.0000","4","0.00","Huawei G8","2400.00","EFT","2016-12-01","0000-00-00","600.00","MON","08-11-2016-15-33-04ID and Driver\'s license.pd","10-11-2016-20-15-33PP Chakela Agrement.pdf","08-11-2016-15-15-50combinepdf.pdf","09-11-2016-19-48-01ProofEmployment.pdf","08-11-2016-15-18-28November statement.pdf","Paseka Chakeka","ABSA","62575560076","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("72","8804046291081","1","38000.00","18000.00","0.00","2000.00","CAN","2016-11-08","1","20.0000","4","0.00","Huawei G8","2400.00","EFT","2016-12-01","0000-00-00","600.00","MON","","","","","","Paseka Chakela","fnb","62575560076","254905","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("73","8004060552084","1","15540.00","8500.00","0.00","5000.00","SET","2016-11-08","1","20.0000","8","0.00","Samsung S6","6000.00","DTO","2016-12-23","2017-08-23","750.00","MON","09-11-2016-17-42-15MhlomeliID.gif","10-11-2016-10-16-59contract.PDF","08-11-2016-16-12-31FNB Bank Statement.pdf","08-11-2016-16-13-13Payslip 25707_0Print Versi","08-11-2016-16-15-56Proof of Address.pdf","PL Mhlomeli","ABSA","62541839801","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("74","6401250420084","1","3467.00","2000.00","0.00","5000.00","REJ","2016-12-16","1","20.0000","15","0.00","none","6000.00","EFT","2017-01-30","0000-00-00","400.00","MON","30-11-2016-12-55-596401250420084-ID-Copy.pdf","","30-11-2016-12-56-236401250420084-BankStatemen","30-11-2016-13-03-406401250420084-Payslip-3009","30-11-2016-13-03-176401250420084-ProofOfAddre","MISS AGNES N BUTHELEZI","ABSA","62036035740","251905","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("75","8105150506086","1","36500.00","22000.00","0.00","5000.00","SET","2016-12-09","1","20.0000","8","0.00","Huawei p9","6000.00","DTO","2017-02-15","2017-10-15","750.00","MON","09-12-2016-14-06-37CJadaRampeo ID.PDF","12-12-2016-14-58-46Contract CJada.PDF","09-12-2016-15-21-56Bank Statement.pdf","09-12-2016-15-14-18payslip.pdf","09-12-2016-14-06-56ISU_PDF_GEN_1_Email.pdf","Cebisa Cecilia Jada","ABSA","4086728505","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("76","8708035545086","1","25000.00","15000.00","0.00","4000.00","SET","2016-12-19","1","20.0000","2","0.00","Samsung Smart Tv","4800.00","DTO","2017-02-15","2017-04-15","2400.00","MON","19-12-2016-19-03-09IDMODISE.PDF","20-12-2016-13-35-55cntDecember.PDF","","","19-12-2016-19-03-31SU_PDF_GEN_1_Email.pdf","Tumelo Macdonald Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("77","8908305579084","1","12000.00","8000.00","0.00","700.00","SET","2016-12-29","1","20.0000","1","0.00","lg tv","840.00","EFT","2017-01-25","2017-02-25","840.00","MON","","11-01-2017-12-55-11MAOB 31 Dec 2016.pdf","","","","obed malebye","StandardBank","10037121152","051001","Savings","1.0000","","","","","");
INSERT INTO loanapp VALUES("78","8908305579084","1","12.00","8000.00","0.00","700.00","CAN","2016-12-30","1","20.0000","1","0.00","lg tv","840.00","EFT","2017-01-25","2017-02-25","840.00","MON","11-01-2017-12-46-36contract.pdf","","","","","obed malebye","ABSA","10037121152","051001","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("79","9107010071086","1","13000.00","2000.00","0.00","5000.00","SET","2016-12-29","1","20.0000","6","0.00","Samsung fone","6000.00","DTO","2017-01-25","2017-07-25","1000.00","MON","29-12-2016-16-51-08marche id 001.jpg","31-12-2016-15-24-06AGREEMENT 007.jpg","29-12-2016-16-53-52bs 008.jpg","29-12-2016-16-54-29payslip 001.jpg","29-12-2016-16-54-54proof of address 001.jpg","mc botes","SBSA","302231811","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("80","8708035545086","1","15000.00","7000.00","0.00","1000.00","SET","2017-01-04","1","20.0000","1","0.00","Samsung Galaxy Phone","1200.00","DTO","2017-02-15","2017-03-15","1200.00","MON","","","","","","Tumelo Macdonald Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("81","8601045310080","1","52000.00","28000.00","0.00","5000.00","SET","2017-01-08","1","20.0000","2","0.00","Smart TV","6000.00","EFT","2017-01-25","2017-03-25","3000.00","MON","","10-01-2017-11-23-45Vog_Contract_2017-1-10_102","","","","N N Maekiso","fnb","62169204725","251655","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("82","8905230299085","1","20000.00","5500.00","0.00","50000.00","CAN","2017-01-17","1","20.0000","5","0.00","iPhone 5","60000.00","EFT","2017-01-25","2017-06-25","12000.00","MON","17-01-2017-12-38-156512Decstatemen.pdf","","17-01-2017-12-41-20ID copy .pdf","17-01-2017-12-42-12BuhleKunene.pdf","17-01-2017-12-43-08Proof of Residence .pdf","Buhlebuzile Kunene","StandardBank","332626512","005001","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("83","9202185307082","1","29000.00","15000.00","0.00","5000.00","SET","2017-01-19","1","20.0000","6","0.00","Phone","6000.00","DTO","2017-02-28","2017-08-28","1000.00","MON","20-01-2017-15-33-07201604011012371000.jpg","20-01-2017-16-40-23example_001.pdf","20-01-2017-15-34-21FNB GOLD CHEQUE ACCOUNT 19","20-01-2017-15-34-29payslip.pdf","20-01-2017-15-34-37invoice_996770 (3).pdf","Thabang Phaleng","FNB","62538150335","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("84","8906165263088","1","31600.00","25000.00","0.00","5000.00","SET","2017-01-21","1","20.0000","6","0.00","Computer","6000.00","DTO","2017-02-20","2017-08-20","1000.00","MON","23-01-2017-15-56-5218.pdf","24-01-2017-13-52-15MT Molete - Vogsphere.pdf","","23-01-2017-16-43-52Jan Pay Slip.pdf","23-01-2017-16-48-18proof of address.pdf","Thomas Molete","SBSA","041936493","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("85","7709120167084","1","14000.00","3500.00","0.00","5000.00","SET","2017-01-25","1","20.0000","6","0.00","samsung","6000.00","DTO","2017-02-20","2017-08-20","1000.00","MON","","31-01-2017-10-32-12Contract Agreement.pdf","31-01-2017-10-23-15Bank Statement.pdf","","","dl wagner","SBSA","300075715","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("86","8107300561086","1","31248.00","28000.00","0.00","5000.00","SET","2017-02-08","1","25.0000","6","0.00","none","6250.00","DTO","2017-03-25","2017-09-25","1041.67","MON","09-02-2017-11-53-24IDCopy-Fundi-.pdf","23-02-2017-16-11-38Vlogsphere Loan ContractvSigned.pdf","09-02-2017-11-54-57Bank Statement_and_Payslip.pdf","09-02-2017-11-55-07Bank Statement_and_Payslip.pdf","09-02-2017-11-52-07ProofofAddress.pdf","Fundiswa Mayekiso ","ABSA","10079377910","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("87","8908305579084","1","12100.00","8000.00","0.00","5000.00","SET","2017-02-13","1","25.0000","3","0.00","LG LED TV","6250.00","DTO","2017-03-25","2017-06-25","2083.33","MON","20-02-2017-10-04-47Copy of ID (2).pdf","22-02-2017-12-57-43doc01269020170222145450.pdf","20-02-2017-21-12-28statement-10-03-712-115-2 (1).pdf","20-02-2017-10-04-24MAOB 31 Jan 2017.pdf","20-02-2017-10-04-34doc00606020160620122614.pdf","Obakeng Malebye","StandardBank","10037121152","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("90","9107010071086","1","12500.00","1500.00","0.00","8000.00","APPR","2017-04-04","1","25.0000","6","0.00","samsung phone","10000.00","EFT","2017-04-25","2017-10-25","1666.67","MON","04-04-2017-21-30-46certified copy of id.pdf","05-04-2017-19-43-50Contract Agreement.pdf","04-04-2017-21-31-09Bank_Statement.pdf","04-04-2017-21-50-32Payslip.jpg","04-04-2017-21-31-20proof of address.pdf","mc botes","StandardBank","302231811","051001","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("91","9112245040088","1","15000.00","2000.00","0.00","5000.00","APPR","2017-04-05","1","25.0000","6","0.00","samsung phone","6250.00","EFT","2017-04-25","2017-10-25","1041.67","MON","05-04-2017-19-57-41kervins id.pdf","07-04-2017-12-57-08Kervin.pdf","05-04-2017-19-57-53NEW FNB BANK STATEMENT.PDF","05-04-2017-19-58-12MergedPayslips_605_tlakub.pdf","","ka davids","fnb","62598650945","250655","Cheque","1.0000","","","","","");
INSERT INTO loanapp VALUES("92","8504045897082","1","7000.00","4900.00","0.00","5000.00","APPR","2017-04-13","1","25.0000","6","0.00","Polo TSI 2014","6250.00","DTO","2017-04-25","2017-10-25","1041.67","MON","13-04-2017-04-20-03ID Document.pdf","14-04-2017-09-43-22Signed Contract.pdf","","13-04-2017-04-21-03Payslip.jpg","13-04-2017-04-21-49Proof of Address.jpg","Mr Abram Mopeloa","ABSA","5277965301","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("93","7709120167084","1","13000.00","3000.00","0.00","6000.00","APPR","2017-04-13","1","25.0000","6","0.00","samsung phone","7500.00","DTO","2017-04-30","2017-10-30","1250.00","MON","","25-04-2017-01-20-32Contract.pdf","","","","dl wagner","StandardBank","300075715","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("94","7512125283082","1","27000.00","13500.00","0.00","5000.00","APPR","2017-04-15","1","25.0000","6","0.00","N/A","6250.00","DTO","2017-05-30","2017-11-30","1041.67","MON","15-04-2017-11-16-24ID COPY.PDF","24-04-2017-08-21-30Scan Loan Contract.pdf","15-04-2017-11-21-34BANK STATEMENT.pdf","15-04-2017-11-22-54PAYSLIP.pdf","","Mr R Mirase","ABSA","9310687575","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("99","8601045310080","1","46000.00","20000.00","0.00","700.00","SET","2017-04-21","1","25.0000","1","0.00","Samsung S5","875.00","DTO","2017-04-25","2017-05-25","875.00","MON","","","","","","NN Maekiso","fnb","62169204725","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("100","8908305579084","1","12100.00","8000.00","0.00","2000.00","SET","2017-04-25","1","25.0000","2","0.00","+27790708275","2500.00","DTO","2017-06-25","2017-08-25","1250.00","MON","25-04-2017-11-47-45Copy of ID (2).pdf","02-05-2017-09-52-01Contract.pdf","25-04-2017-11-59-11statement-10-03-712-115-2.pdf","25-04-2017-11-53-43Payslip 2017_03_31.pdf","25-04-2017-11-56-36Residence.pdf","Obakeng Malebye","SBSA","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("101","8708175079086","1","23444.00","10000.00","0.00","5000.00","APPR","2017-05-04","1","25.0000","6","0.00","None","6250.00","DTO","2017-05-20","2017-11-20","1041.67","MON","04-05-2017-11-53-11ID colour.pdf","05-05-2017-13-59-46JP.pdf","04-05-2017-11-54-58STAF TJEKREKENING 15.pdf","04-05-2017-11-56-09Salary Slip.pdf","04-05-2017-11-58-29NOTSET[Untitled]NOTSET.PDF","JP van der Merwe","fnb","62589970906","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("102","8812156083081","1","10215.58","1100.00","0.00","5000.00","APPR","2017-05-08","1","25.0000","6","0.00","Salary","6250.00","DTO","2017-05-31","2017-11-01","1041.67","MON","09-05-2017-08-44-25ID COPY.pdf","18-05-2017-08-42-54Contract Agreement.pdf","09-05-2017-07-46-00Capitec BankStatement_08022017_08052017.pdf","09-05-2017-07-46-33SALARY.pdf","11-05-2017-15-17-26(FICA) VE Dlamini.pdf","Mr Vikelathina Dlamini","capitec","1373166302","470010","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("103","9012265761086","1","30000.00","15000.00","0.00","600.00","SET","2017-05-13","1","25.0000","1","0.00","iPhone5","750.00","DTO","2017-05-25","2017-06-25","750.00","MON","18-05-2017-05-55-28Scan_Doc0002.pdf","","","","","MP Mahwayi","fnb","62558299907","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("104","6911110151083","1","8500.00","600.00","0.00","5000.00","REJ","2017-06-01","1","25.0000","6","0.00","samsung phone","6250.00","DTO","2017-06-30","2017-12-30","1041.67","MON","02-06-2017-08-50-09Michelle ID.pdf","","02-06-2017-08-50-28Michelle Bank statements.pdf","02-06-2017-08-50-42Michelle Payslip.pdf","02-06-2017-08-50-57Michelle Proof of address.pdf","mc botes","capitec","1305472738","470010","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("105","8908305579084","1","12000.00","8000.00","0.00","200.00","SET","2017-06-13","1","25.0000","2","0.00","Defy Fridge","250.00","DTO","2017-06-26","2017-08-26","125.00","MON","13-06-2017-19-42-18Copy of ID (2).pdf","14-06-2017-09-29-47June_Contract.pdf","","13-06-2017-19-43-10Payslip.pdf","13-06-2017-19-43-37Residence.pdf","OO Malebye","SBSA","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("106","8908305579084","1","12100.00","8000.00","0.00","2500.00","SET","2017-06-23","1","25.0000","2","0.00","Defy Fridge","3125.00","DTO","2017-06-25","2017-08-25","1562.50","MON","23-06-2017-13-06-19Copy of ID (2).pdf","27-06-2017-11-34-47Contract.pdf","","23-06-2017-13-08-24Payslip 2017-05-31.pdf","23-06-2017-13-08-02Residence.pdf","OO Malebye","ABSA","10037121152","632005","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("107","8908305579084","1","12000.00","8000.00","0.00","100.00","CAN","2017-07-24","1","25.0000","1","0.00","hisense cellphone","125.00","DTO","2017-07-25","2017-08-25","125.00","MON","17-07-2017-13-33-47Copy of ID (2).pdf","","","17-07-2017-13-38-47Payslip 2017-06-30.pdf","17-07-2017-13-37-42doc00606020160620122614.pdf","o malebye","SBSA","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("108","8908305579084","1","12000.00","8000.00","0.00","2300.00","APPR","2017-07-26","1","25.0000","3","0.00","Defy Fridge","2875.00","DTO","2017-08-25","2017-11-25","958.33","MON","26-07-2017-11-28-48Copy of ID (2).pdf","31-07-2017-17-46-08Pre-agreement and Quotation and Terms and Conditions Mr Malebye.pdf","","26-07-2017-11-29-41Payslip 2017-06-30.pdf","26-07-2017-11-29-15Residence.pdf","OO Malebye","SBSA","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("109","9107010071086","1","12500.00","1500.00","0.00","3000.00","CAN","2017-07-31","1","25.0000","3","0.00","Samsung","3750.00","DTO","2017-08-25","2017-11-25","1250.00","MON","27-07-2017-08-31-09certified copy of id (2).pdf","","27-07-2017-08-32-04Bank statement 2.pdf","27-07-2017-08-32-20Payslip July.pdf","27-07-2017-08-33-15951007565.pdf","mc botes","SBSA","302231811","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("110","6305240160082","1","11200.00","2000.00","0.00","3000.00","APPR","2017-08-05","1","25.0000","3","0.00","house","3750.00","DTO","2017-09-25","2017-12-25","1250.00","MON","27-07-2017-12-49-08Bonita ID.jpg","","","18-08-2017-10-23-56Payslip.pdf","","bp theron","ABSA","9256225691","632005","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("111","8708035545086","1","16000.00","1000.00","0.00","100.00","CAN","2017-08-04","1","25.0000","2","0.00","Testing ONLY in DEV","125.00","DTO","2017-08-15","2017-10-15","62.50","MON","","","","","","Tumelo Macdonald Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("113","9202185307082","1","30000.00","15000.00","0.00","3000.00","APPR","2017-08-06","1","25.0000","3","0.00","Samsung S8","3750.00","DTO","2017-08-25","2017-11-25","1250.00","MON","06-08-2017-17-13-18ID Document.jpg","","06-08-2017-17-05-18Bank Statement.pdf","","06-08-2017-17-13-36ProofOfAddress.pdf","Mr T Phaleng","Nedbank","1146024673","198765","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("114","8107300561086","1","33139.70","28000.00","0.00","3000.00","APPR","2017-08-10","1","25.0000","3","0.00","none ","3750.00","DTO","2017-09-25","2017-12-25","1250.00","MON","10-08-2017-15-45-19ID copy - MayekisoFE.pdf","18-08-2017-15-17-21NewContractAndQoute.pdf","10-08-2017-15-48-47statement-10-07-937-791-0.pdf","10-08-2017-15-49-03Jul-pay.pdf","10-08-2017-15-53-15proofofresidence.pdf","Miss FE Mayekiso","SBSA","10079377910","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("115","9010040032088","1","10714.00","4000.00","0.00","3000.00","REJ","2017-08-29","1","25.0000","3","0.00","NA","3750.00","DTO","2017-09-26","2017-12-26","1250.00","MON","14-09-2017-21-53-08ID[1].pdf","15-09-2017-14-26-52Pre-Agreement Statement and Quotation.pdf","14-09-2017-21-55-05combinepdf_(2)[1].pdf","14-09-2017-21-55-27MyPayslip_6-30-2017_0[1].pdf","","Melanie ludik","FNB","62472946080","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("116","8908305579084","1","12000.00","9000.00","0.00","500.00","SET","2017-09-04","1","25.0000","1","0.00","Defy Fridge","625.00","DTO","2017-09-25","2017-10-25","625.00","MON","11-09-2017-14-32-0826-07-2017-11-28-48Copy of ID (2).pdf","14-09-2017-10-24-19Contract.pdf","","11-09-2017-14-33-06Payslip 2017-08-31.pdf","11-09-2017-14-32-4126-07-2017-11-29-15Residence.pdf","Obakeng Malebye","SBSA","10037121152","051001","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("117","8611075771085","1","24814.03","16000.00","0.00","3000.00","APPR","2017-09-07","1","25.0000","3","0.00","Samsung tab and galaxy prime + phone","3750.00","DTO","2017-09-29","2017-12-29","1250.00","MON","14-09-2017-20-20-56ID.pdf","15-09-2017-10-01-08Contract.pdf","14-09-2017-21-13-19Statement[1].pdf","14-09-2017-21-13-36July[1].pdf","14-09-2017-21-14-32Tenant-OV2103-2017-09_(1)[1].pdf","Motlatsi Mokoaqatsa","Capitec","1336239717","470010","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("118","8901075369083","1","4500.00","2000.00","0.00","3000.00","REJ","2017-09-14","1","25.0000","3","0.00","Iphone4","3750.00","DTO","2017-09-25","2017-12-25","1250.00","MON","14-09-2017-08-25-05Id copy.pdf","","14-09-2017-08-21-46account_statement (1).pdf","14-09-2017-08-35-26confirmation letter (lenasia).pdf","14-09-2017-08-21-0662474242527 2017-08-01 (2).pdf","Itumeleng Seleka","Capitec","1492626145","470010","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("119","9202185307082","1","30000.00","9800.00","0.00","3000.00","REJ","2017-10-02","1","25.0000","3","0.00","LG G3, Sony Aqua, LG 49","3750.00","DTO","2017-11-27","2018-02-27","1250.00","MON","","","","","","Thabang Phaleng","FNB","1146024673","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("120","8804046291081","1","39000.00","25000.00","0.00","2000.00","APPR","2017-10-09","1","25.0000","3","0.00","Audi A3, TFSI","2500.00","DTO","2017-11-01","2018-02-01","833.33","MON","17-10-2017-21-34-21Paseka Chakela ID.pdf","19-10-2017-21-58-23PP Chakela.pdf","17-10-2017-21-36-26merged.pdf","17-10-2017-21-49-20September payslip.pdf","17-10-2017-21-37-23Consent form Credit Report (1).pdf","Paseka Chakela","FNB"," 62575560076","250655","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("121","7611150155080","1","26025.56","5000.00","0.00","3000.00","PEN","2017-10-09","1","25.0000","3","0.00","Kruger Coins 1/ 10th ","3750.00","DTO","2001-11-17","0000-00-00","1250.00","MON","09-10-2017-22-23-56ID Document.pdf","","09-10-2017-22-24-203 months bank Statement.pdf","09-10-2017-22-24-40Latest Salary slip.pdf","","Masha Rangan","SBSA","001664158","051001","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("122","8708035545086","1","15000.00","6000.00","0.00","2000.00","PEN","2017-10-25","1","25.0000","1","0.00","Samsung Phone Notepad II","2500.00","DTO","2017-12-15","2018-01-15","2500.00","MON","","","","","","Tumelo Modise","ABSA","4065548160","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("123","8504265079080","1","1000.00","1500.00","0.00","1000.00","PEN","2018-02-08","1","25.0000","2","0.00","samsung s5","1250.00","DTO","2018-02-28","2018-04-28","625.00","MON","","","","","","stephen gruindelingh","Capitec","1389110158","470010","2","1.0000","","","","","");
INSERT INTO loanapp VALUES("124","9403120028086","1","10000.00","4000.00","0.00","3000.00","PEN","2018-02-15","1","25.0000","3","0.00","BlackBerry Z3","0.00","DTO","2018-11-20","0000-00-00","0.00","MON","","","","","","SL Liebenberg","ABSA","4082550859","632005","1","1.0000","","","","","");
INSERT INTO loanapp VALUES("125","9206035099088","1","12000.00","5000.00","0.00","3000.00","PEN","2018-06-27","1","25.0000","3","0.00","IPHONE 6S, JVC 55","3750.00","DTO","2018-08-31","2018-11-01","1250.00","MON","","","","","","HERMAN KRITZINGER","FNB","62653400300","250655","1","1.0000","","","","","");



DROP TABLE IF EXISTS loanappcharges;

CREATE TABLE `loanappcharges` (
  `loanappchargesid` int(11) NOT NULL,
  `chargeid` int(11) DEFAULT NULL,
  `applicationid` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO loanappcharges VALUES("205","2","92","1");
INSERT INTO loanappcharges VALUES("206","2","92","2");
INSERT INTO loanappcharges VALUES("207","2","101","0");
INSERT INTO loanappcharges VALUES("208","2","101","1");
INSERT INTO loanappcharges VALUES("213","1","84","-1");
INSERT INTO loanappcharges VALUES("214","2","79","4");
INSERT INTO loanappcharges VALUES("215","2","90","1");
INSERT INTO loanappcharges VALUES("216","2","90","2");
INSERT INTO loanappcharges VALUES("217","2","91","1");
INSERT INTO loanappcharges VALUES("218","1","100","-1");
INSERT INTO loanappcharges VALUES("219","2","105","0");
INSERT INTO loanappcharges VALUES("220","2","100","0");
INSERT INTO loanappcharges VALUES("221","1","94","-1");
INSERT INTO loanappcharges VALUES("222","2","94","0");
INSERT INTO loanappcharges VALUES("223","2","94","1");
INSERT INTO loanappcharges VALUES("224","2","93","2");
INSERT INTO loanappcharges VALUES("225","1","102","-1");
INSERT INTO loanappcharges VALUES("226","2","102","0");
INSERT INTO loanappcharges VALUES("227","2","102","1");
INSERT INTO loanappcharges VALUES("228","2","46","0");
INSERT INTO loanappcharges VALUES("229","2","46","1");
INSERT INTO loanappcharges VALUES("230","2","83","2");
INSERT INTO loanappcharges VALUES("231","2","83","3");
INSERT INTO loanappcharges VALUES("232","2","93","3");
INSERT INTO loanappcharges VALUES("233","2","90","3");
INSERT INTO loanappcharges VALUES("234","2","46","3");
INSERT INTO loanappcharges VALUES("235","2","46","2");
INSERT INTO loanappcharges VALUES("236","2","100","1");
INSERT INTO loanappcharges VALUES("237","3","108","0");
INSERT INTO loanappcharges VALUES("239","1","108","-1");
INSERT INTO loanappcharges VALUES("240","2","105","1");
INSERT INTO loanappcharges VALUES("241","3","113","0");
INSERT INTO loanappcharges VALUES("242","2","91","3");
INSERT INTO loanappcharges VALUES("243","2","102","2");
INSERT INTO loanappcharges VALUES("244","2","94","2");
INSERT INTO loanappcharges VALUES("245","2","101","2");
INSERT INTO loanappcharges VALUES("246","1","116","-1");
INSERT INTO loanappcharges VALUES("247","1","117","-1");
INSERT INTO loanappcharges VALUES("248","2","91","4");
INSERT INTO loanappcharges VALUES("249","2","91","5");
INSERT INTO loanappcharges VALUES("250","2","90","4");
INSERT INTO loanappcharges VALUES("251","2","90","0");
INSERT INTO loanappcharges VALUES("252","2","90","-1");
INSERT INTO loanappcharges VALUES("253","2","90","5");
INSERT INTO loanappcharges VALUES("254","2","108","0");
INSERT INTO loanappcharges VALUES("255","3","108","1");
INSERT INTO loanappcharges VALUES("256","3","113","1");
INSERT INTO loanappcharges VALUES("257","3","117","0");
INSERT INTO loanappcharges VALUES("258","2","101","3");
INSERT INTO loanappcharges VALUES("259","2","101","4");
INSERT INTO loanappcharges VALUES("260","2","102","3");
INSERT INTO loanappcharges VALUES("261","2","102","4");
INSERT INTO loanappcharges VALUES("262","2","93","5");
INSERT INTO loanappcharges VALUES("263","2","94","3");
INSERT INTO loanappcharges VALUES("264","2","94","4");
INSERT INTO loanappcharges VALUES("265","2","46","4");
INSERT INTO loanappcharges VALUES("269","1","110","-1");
INSERT INTO loanappcharges VALUES("270","2","92","4");
INSERT INTO loanappcharges VALUES("271","2","92","5");
INSERT INTO loanappcharges VALUES("274","2","110","0");
INSERT INTO loanappcharges VALUES("275","3","110","0");



DROP TABLE IF EXISTS loantype;

CREATE TABLE `loantype` (
  `loantypeid` int(11) NOT NULL,
  `loantypedesc` varchar(255) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `currency` varchar(45) NOT NULL,
  `lastchangedby` bigint(13) NOT NULL,
  `lastchangedbydate` date NOT NULL,
  `fromduration` int(11) NOT NULL DEFAULT '1',
  `toduration` int(11) NOT NULL DEFAULT '1',
  `fromamount` decimal(7,2) NOT NULL DEFAULT '1.00',
  `toamount` decimal(7,2) NOT NULL DEFAULT '1.00',
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `qualifyfrom` decimal(7,2) NOT NULL DEFAULT '1.00',
  `qualifyto` decimal(7,2) NOT NULL DEFAULT '1.00',
  `funderInterest` decimal(7,4) DEFAULT '1.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO loantype VALUES("1","Personal Loan","25","R","9901019999999","2017-07-06","1","4","100.00","3000.00","2012-01-01","2017-12-31","1000.00","3000.00","1.0000");



DROP TABLE IF EXISTS migration_version;

CREATE TABLE `migration_version` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO migration_version VALUES("4");



DROP TABLE IF EXISTS payment;

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount` decimal(53,15) DEFAULT NULL,
  `notes` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8

;INSERT INTO payment VALUES("1","4","2004-10-21","8610.680000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("2","5","2005-01-18","13317.870000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("3","6","2005-04-03","11780.260000000000000","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");
INSERT INTO payment VALUES("4","7","2005-10-17","9675.130000000000000","Excepteur sint occaecat");
INSERT INTO payment VALUES("5","7","2006-03-25","4837.570000000000000","Ut enim ad minim");
INSERT INTO payment VALUES("6","8","2005-09-21","3986.230000000000000","Ut enim ad minim");
INSERT INTO payment VALUES("7","9","2005-12-17","20804.790000000000000","Ab illo inventore veritatis.");
INSERT INTO payment VALUES("8","10","2006-08-10","23510.310000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("9","11","2006-05-13","6367.850000000000000","Ab illo inventore veritatis.");
INSERT INTO payment VALUES("10","12","2006-09-14","7929.860000000000000","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO payment VALUES("11","12","2006-08-30","5947.400000000000000","Ab illo inventore veritatis.");
INSERT INTO payment VALUES("12","12","2006-09-04","8921.090000000000000","Lorem ipsum dolor sit amet, consectetur adipisicing elit.");
INSERT INTO payment VALUES("13","12","2006-09-08","2230.270000000000000","Ut enim ad minim");
INSERT INTO payment VALUES("14","13","2006-12-07","22191.670000000000000","Excepteur sint occaecat");
INSERT INTO payment VALUES("15","14","2008-05-26","27224.250000000000000","Ab illo inventore veritatis.");
INSERT INTO payment VALUES("16","15","2007-11-19","1876.770000000000000","Ab illo inventore veritatis.");
INSERT INTO payment VALUES("17","16","2007-08-04","29146.360000000000000","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO payment VALUES("18","17","2007-10-31","3008.980000000000000","Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");
INSERT INTO payment VALUES("19","17","2008-04-26","752.240000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("20","17","2008-05-01","62.690000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("21","18","2008-04-11","10498.080000000000000","Ut enim ad minim");
INSERT INTO payment VALUES("22","19","2008-06-30","8664.740000000000000","Excepteur sint occaecat");
INSERT INTO payment VALUES("23","19","2008-06-20","6498.550000000000000","Excepteur sint occaecat");
INSERT INTO payment VALUES("24","20","2008-11-22","16936.690000000000000","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO payment VALUES("25","21","2010-01-30","8110.140000000000000","Ut enim ad minim");
INSERT INTO payment VALUES("26","22","2009-11-21","3468.240000000000000","Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
INSERT INTO payment VALUES("27","23","2009-09-18","12566.940000000000000","Ullamco laboris nisi");
INSERT INTO payment VALUES("28","30","2014-08-26","7250.000000000000000","");
INSERT INTO payment VALUES("29","10","2014-08-26","200.000000000000000","ytyttyt");
INSERT INTO payment VALUES("34","10","2018-07-31","60.000000000000000","testing.");
INSERT INTO payment VALUES("35","10","2018-07-31","60.000000000000000","testing.");
INSERT INTO payment VALUES("37","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("38","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("39","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("40","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("41","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("42","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("43","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("44","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("45","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("46","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("47","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("48","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("49","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("51","10","2018-07-04","600.000000000000000","testing 20");
INSERT INTO payment VALUES("54","8","2018-07-04","60.000000000000000","");
INSERT INTO payment VALUES("55","64","2018-07-04","10.000000000000000","test");



DROP TABLE IF EXISTS paymentfrequency;

CREATE TABLE `paymentfrequency` (
  `paymentfrequencyid` varchar(45) NOT NULL,
  `paymentfrequencydesc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO paymentfrequency VALUES("MON","Monthly");



DROP TABLE IF EXISTS paymentmethod;

CREATE TABLE `paymentmethod` (
  `paymentmethodid` varchar(45) NOT NULL,
  `paymentmethoddesc` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO paymentmethod VALUES("DTO","Debit Order");



DROP TABLE IF EXISTS paymentschedule;

CREATE TABLE `paymentschedule` (
  `ApplicationId` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `scheduleddate` date DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `paidby` varchar(45) DEFAULT NULL,
  `disbursement` decimal(7,2) DEFAULT NULL,
  `amountdue` decimal(7,2) DEFAULT NULL,
  `balance` decimal(7,2) DEFAULT NULL,
  `interestdue` decimal(7,2) DEFAULT NULL,
  `fees` decimal(7,2) DEFAULT NULL,
  `penalty` decimal(7,2) DEFAULT NULL,
  `amountpaid` decimal(7,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO paymentschedule VALUES("44","-1","2016-09-01","0","","10000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","0","2016-09-30","280","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","1","2016-10-30","250","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","2","2016-11-30","219","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","3","2016-12-30","189","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","4","2017-01-30","158","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","5","2017-02-28","129","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","6","2017-03-28","101","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","7","2017-04-28","70","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","8","2017-05-28","40","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","9","2017-06-28","9","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","10","2017-07-28","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("44","11","2017-08-28","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","-1","2016-09-02","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","0","2017-05-30","38","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","1","2017-06-30","7","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","2","2017-07-30","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","3","2017-08-30","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","4","2017-09-30","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("46","5","2017-10-30","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","-1","2016-09-02","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","0","2016-09-30","280","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","1","2016-10-30","250","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","2","2016-11-30","219","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","3","2016-12-30","189","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","4","2017-01-30","158","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("47","5","2017-02-28","129","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","-1","2016-09-05","0","","6000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","0","2015-12-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","1","2016-01-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","2","2016-02-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","3","2016-03-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","4","2016-04-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("48","5","2016-05-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","-1","2016-09-05","0","","11000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","0","2016-08-28","0","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","1","2016-09-28","0","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","2","2016-10-28","0","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","3","2016-11-28","0","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","4","2016-12-28","0","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("49","5","2017-01-28","114","","0.00","2200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","-1","2016-09-05","0","","9000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","0","2016-09-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","1","2016-10-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","2","2016-11-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","3","2016-12-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","4","2017-01-26","114","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","5","2017-02-26","114","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","6","2017-03-26","114","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","7","2017-04-26","83","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("50","8","2017-05-26","53","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("51","-1","2016-09-06","0","","8000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("51","0","2016-09-06","293","","0.00","4800.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("51","1","2016-10-06","263","","0.00","4800.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","-1","2016-09-11","0","","5638.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","0","2015-11-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","1","2015-12-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","2","2016-01-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","3","2016-02-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","4","2016-03-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("53","5","2016-04-20","0","","0.00","1127.60","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","-1","2016-09-11","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","0","2016-03-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","1","2016-04-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","2","2016-05-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","3","2016-06-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","4","2016-07-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("54","5","2016-08-29","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","-1","2016-09-11","0","","20000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","0","2015-12-25","549","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","1","2016-01-25","518","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","2","2016-02-25","487","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","3","2016-03-25","458","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","4","2016-04-25","427","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("55","5","2016-05-25","397","","0.00","4000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("57","-1","2016-09-16","0","","6000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("57","0","2016-04-25","0","","0.00","2400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("57","1","2016-05-25","0","","0.00","2400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("57","2","2016-06-25","0","","0.00","2400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("58","-1","2016-09-16","0","","500.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("58","0","2016-09-28","282","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("59","-1","2016-09-16","0","","4000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("59","0","2016-07-01","371","","0.00","1600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("59","1","2016-08-01","340","","0.00","1600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("59","2","2016-09-01","309","","0.00","1600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("63","-1","2016-09-16","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("63","0","2016-06-27","364","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("63","1","2016-07-27","334","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("63","2","2016-08-27","303","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","-1","2016-10-02","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","0","2016-10-15","259","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","1","2016-11-15","228","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","2","2016-12-15","198","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","3","2017-01-15","167","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","4","2017-02-15","136","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("64","5","2017-03-15","108","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","-1","2016-09-21","0","","10000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","0","2016-10-17","0","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","1","2016-11-17","0","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","2","2016-12-17","0","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","3","2017-01-17","114","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","4","2017-02-17","114","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("65","5","2017-03-17","114","","0.00","2000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("66","-1","2016-10-01","0","","2000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("66","0","2016-10-25","255","","0.00","800.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("66","1","2016-11-25","224","","0.00","800.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("66","2","2016-12-25","194","","0.00","800.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","-1","2016-10-05","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","0","2016-10-17","257","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","1","2016-11-17","226","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","2","2016-12-17","196","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","3","2017-01-17","165","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","4","2017-02-17","134","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("67","5","2017-03-17","106","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","-1","2016-10-18","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","0","2016-11-15","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","1","2016-12-15","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","2","2017-01-15","118","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","3","2017-02-15","118","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","4","2017-03-15","118","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("69","5","2017-04-15","87","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","-1","2016-11-30","0","","45000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","0","2017-01-01","116","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","1","2017-02-01","116","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","2","2017-03-01","116","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","3","2017-04-01","85","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","4","2017-05-01","55","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","5","2017-06-01","23","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","6","2017-07-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","7","2017-08-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","8","2017-09-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","9","2017-10-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","10","2017-11-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","11","2017-12-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","12","2018-01-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","13","2018-02-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","14","2018-03-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","15","2018-04-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","16","2018-05-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","17","2018-06-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","18","2018-07-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","19","2018-08-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","20","2018-09-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","21","2018-10-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","22","2018-11-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("70","23","2018-12-01","0","","0.00","2250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","-1","2016-11-08","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","0","2016-12-23","196","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","1","2017-01-23","165","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","2","2017-02-23","134","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","3","2017-03-23","106","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","4","2017-04-23","75","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","5","2017-05-23","45","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","6","2017-06-23","14","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("73","7","2017-07-23","0","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","-1","2016-12-16","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","0","2017-01-30","114","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","1","2017-02-28","114","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","2","2017-03-28","114","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","3","2017-04-28","83","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","4","2017-05-28","53","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","5","2017-06-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","6","2017-07-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","7","2017-08-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","8","2017-09-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","9","2017-10-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","10","2017-11-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","11","2017-12-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","12","2018-01-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","13","2018-02-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("74","14","2018-03-28","0","","0.00","400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","-1","2016-12-09","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","0","2017-02-15","117","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","1","2017-03-15","117","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","2","2017-04-15","86","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","3","2017-05-15","56","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","4","2017-06-15","10","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","5","2017-07-15","0","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","6","2017-08-15","0","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("75","7","2017-09-15","0","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("76","-1","2016-12-19","0","","4000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("76","0","2017-02-15","114","","0.00","2400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("76","1","2017-03-15","114","","0.00","2400.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("77","-1","2016-12-29","0","","700.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("77","0","2017-01-25","163","","0.00","840.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("78","-1","2016-12-30","0","","700.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("78","0","2017-01-25","163","","0.00","840.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","-1","2016-12-29","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","0","2017-01-25","115","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","1","2017-02-25","115","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","2","2017-03-25","115","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","3","2017-04-25","84","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","4","2017-05-25","54","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("79","5","2017-06-25","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("80","-1","2017-01-04","0","","1000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("80","0","2017-02-15","114","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("81","-1","2017-01-08","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("81","0","2017-01-25","163","","0.00","3000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("81","1","2017-02-25","132","","0.00","3000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","-1","2017-01-19","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","0","2017-02-28","116","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","1","2017-03-28","116","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","2","2017-04-28","85","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","3","2017-05-28","55","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","4","2017-06-28","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("83","5","2017-07-28","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","-1","2017-01-21","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","0","2017-02-20","134","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","1","2017-03-20","106","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","2","2017-04-20","75","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","3","2017-05-20","45","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","4","2017-06-20","14","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("84","5","2017-07-20","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","-1","2017-01-25","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","0","2017-02-20","137","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","1","2017-03-20","109","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","2","2017-04-20","78","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","3","2017-05-20","48","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","4","2017-06-20","17","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("85","5","2017-07-20","0","","0.00","1000.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","-1","2017-02-08","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","0","2017-03-25","93","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","1","2017-04-25","62","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","2","2017-05-25","32","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","3","2017-06-25","1","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","4","2017-07-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("86","5","2017-08-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("87","-1","2017-02-13","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("87","0","2017-03-25","104","","0.00","2083.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("87","1","2017-04-25","73","","0.00","2083.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("87","2","2017-05-25","43","","0.00","2083.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","-1","2017-04-04","0","","8000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","0","2017-04-25","84","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","1","2017-05-25","54","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","2","2017-06-25","0","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","3","2017-07-25","0","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","4","2017-08-25","0","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("90","5","2017-09-25","0","","0.00","1666.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","-1","2017-04-05","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","0","2017-04-25","83","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","1","2017-05-25","53","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","2","2017-06-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","3","2017-07-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","4","2017-08-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("91","5","2017-09-25","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","-1","2017-04-13","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","0","2017-04-17","75","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","1","2017-05-17","45","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","2","2017-06-17","14","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","3","2017-07-17","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","4","2017-08-17","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("92","5","2017-09-17","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","-1","2017-04-13","0","","6000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","0","2017-04-30","68","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","1","2017-05-30","38","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","2","2017-06-30","7","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","3","2017-07-30","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","4","2017-08-30","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("93","5","2017-09-30","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","-1","2017-04-15","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","0","2017-05-30","53","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","1","2017-06-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","2","2017-07-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","3","2017-08-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","4","2017-09-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("94","5","2017-10-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("99","-1","2017-04-21","0","","700.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("99","0","2017-04-25","73","","0.00","875.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("100","-1","2017-04-25","0","","2000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("100","0","2017-06-25","12","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("100","1","2017-07-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","-1","2017-05-04","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","0","2017-05-20","45","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","1","2017-06-20","14","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","2","2017-07-20","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","3","2017-08-20","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","4","2017-09-20","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("101","5","2017-10-20","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","-1","2017-05-08","0","","5000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","0","2017-05-31","37","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","1","2017-06-30","7","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","2","2017-07-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","3","2017-08-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","4","2017-09-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("102","5","2017-10-30","0","","0.00","1041.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("103","-1","2017-05-13","0","","600.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("103","0","2017-05-25","53","","0.00","750.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("105","-1","2017-06-13","0","","200.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("105","0","2017-06-26","11","","0.00","250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("105","1","2017-07-26","17","","0.00","125.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("106","-1","2017-06-23","0","","2500.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("106","0","2017-08-25","0","","0.00","1562.50","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("106","1","2017-09-25","0","","0.00","1562.50","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("107","-1","2017-07-17","0","","100.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("107","0","2017-07-25","0","","0.00","125.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("108","-1","2017-07-26","0","","2300.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("108","0","2017-08-25","0","","0.00","958.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("108","1","2017-09-25","0","","0.00","958.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("108","2","2017-10-25","0","","0.00","958.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("110","-1","2017-08-05","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("110","0","2017-09-25","13","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("110","1","2017-10-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("110","2","2017-11-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("113","-1","2017-08-06","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("113","0","2017-08-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("113","1","2017-09-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("113","2","2017-10-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("114","-1","2017-08-10","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("114","0","2017-09-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("114","1","2017-10-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("114","2","2017-11-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("116","-1","2017-09-04","0","","500.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("116","0","2017-09-25","0","","0.00","625.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("117","-1","2017-09-07","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("117","0","2017-09-29","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("117","1","2017-10-29","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("117","2","2017-11-29","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("118","-1","2017-09-14","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("118","0","2017-09-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("118","1","2017-10-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("118","2","2017-11-25","0","","0.00","1250.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("120","-1","2017-10-09","0","","2000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("120","0","2017-11-01","0","","0.00","833.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("120","1","2017-12-01","0","","0.00","833.33","0.00","0.00","0.00","0.00","0.00");
INSERT INTO paymentschedule VALUES("120","2","2018-01-01","0","","0.00","833.33","0.00","0.00","0.00","0.00","0.00");



DROP TABLE IF EXISTS phpjobscheduler;

CREATE TABLE `phpjobscheduler` (
  `id` int(11) NOT NULL,
  `scriptpath` varchar(255) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `time_interval` int(11) DEFAULT NULL,
  `fire_time` int(11) NOT NULL DEFAULT '0',
  `time_last_fired` int(11) DEFAULT NULL,
  `run_only_once` tinyint(1) NOT NULL DEFAULT '0',
  `currently_running` tinyint(1) NOT NULL DEFAULT '0',
  `paused` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO phpjobscheduler VALUES("1","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","Email site stats to boss","60","1509865229","1509865169","0","0","0");
INSERT INTO phpjobscheduler VALUES("2","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","Email site stats to boss3","60","1509872012","1509871952","0","0","0");



DROP TABLE IF EXISTS phpjobscheduler_logs;

CREATE TABLE `phpjobscheduler_logs` (
  `id` int(11) NOT NULL,
  `date_added` int(11) DEFAULT NULL,
  `script` varchar(128) DEFAULT NULL,
  `output` text,
  `execution_time` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO phpjobscheduler_logs VALUES("1","1509864348","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.12683 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("2","1509868886","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.10655 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("3","1509868940","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.09101 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("4","1509871388","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.09246 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("5","1509871389","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","","0.97280 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("6","1509871517","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.09766 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("7","1509871517","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","","0.43310 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("8","1509871518","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","","1.01633 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("9","1509871519","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","","0.42606 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("10","1509874510","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.07234 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("11","1509874511","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.47189 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("12","1509874521","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.06959 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("13","1509874521","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.40678 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("14","1509874557","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.09517 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("15","1509874557","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.44639 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("16","1509874558","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.02615 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("17","1509874559","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.42800 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("18","1509874569","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.07423 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("19","1509874570","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.43761 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("20","1509874599","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.08875 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("21","1509874600","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.40409 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("22","1509874603","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","1.09166 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("23","1509874604","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.40387 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("24","1509878124","http://vogsphere.co.za/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;H3&gt;correctly run this file:&lt;br&gt;&lt;br&gt;/eloan/phpjobscheduler/eg_email_site_stats_to_boss.php","2.24933 seconds via PHP CURL ");
INSERT INTO phpjobscheduler_logs VALUES("25","1509878124","http://www.dwalker.co.uk/phpjobscheduler/eg_email_site_stats_to_boss.php","&lt;p&gt;could be set to complete any task not just emailing someone...&lt;/p&gt;/\n/&lt;p&gt;schedule a task, just like cron, but easier and more manageable using:/\n/&lt;a href=&quot;http://www.phpjobscheduler.co.uk/&quot;&gt;http://www.phpjobscheduler.co.uk/&lt;/a&gt;/\n/&lt;/p&gt;/\n/","0.41611 seconds via PHP CURL ");



DROP TABLE IF EXISTS product;

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reference` varchar(100) NOT NULL,
  `description` longtext,
  `price` decimal(53,15) NOT NULL DEFAULT '0.000000000000000',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8

;INSERT INTO product VALUES("1","test product 10","test product 1 description - modise","179.540000000000000","2014-08-12 22:11:30","2018-07-23 09:07:53");
INSERT INTO product VALUES("2","test product 2","test product 2 description","207.080000000000000","2014-08-12 22:11:30","2018-07-29 09:07:00");
INSERT INTO product VALUES("3","test product 3","test product 3 description","99.150000000000000","2014-08-12 22:11:30","2014-08-12 22:11:30");
INSERT INTO product VALUES("4","test product 4","test product 4 description","732.820000000000000","2014-08-12 22:11:30","2014-08-12 22:11:30");
INSERT INTO product VALUES("6","prod 6","description of prod 6","21780.260000000000000","2014-08-12 22:11:30","2014-08-12 22:11:30");
INSERT INTO product VALUES("13","test pord5","		  		  tsetsests		  		  		  		  ","20000.000000000000000","2018-07-29 08:07:08","2018-07-29 08:07:08");



DROP TABLE IF EXISTS property;

CREATE TABLE `property` (
  `keey` varchar(50) NOT NULL DEFAULT '',
  `value` longtext,
  PRIMARY KEY (`keey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO property VALUES("captcha","d");
INSERT INTO property VALUES("company_address","																																																																		43 Mulder Str,31 SEOUL,Centurion,0158																																																																											");
INSERT INTO property VALUES("company_email","info@vogsphere.co.za");
INSERT INTO property VALUES("company_fax","0711232011");
INSERT INTO property VALUES("company_logo","logo.jpeg");
INSERT INTO property VALUES("company_name","Vogsphere Pty Ltd");
INSERT INTO property VALUES("company_phone","07312310111");
INSERT INTO property VALUES("company_reg","2012/020274/74/07");
INSERT INTO property VALUES("company_url","http://www.vogsphere.co.za");
INSERT INTO property VALUES("currency","R");
INSERT INTO property VALUES("currency_decimals","2");
INSERT INTO property VALUES("default_template","1");
INSERT INTO property VALUES("dev","d");
INSERT INTO property VALUES("emailpassword","fCAnzmz910111*");
INSERT INTO property VALUES("emailusername","info@ecashmeup.com");
INSERT INTO property VALUES("host","mail.ecashmeup.com");
INSERT INTO property VALUES("last_calculation_date","\"2016-07-25\"");
INSERT INTO property VALUES("legal_terms","INVOICE TERMS:test.");
INSERT INTO property VALUES("pdf_orientation","Portrait");
INSERT INTO property VALUES("pdf_size","a4");
INSERT INTO property VALUES("port","587");
INSERT INTO property VALUES("prod","");
INSERT INTO property VALUES("sample_data_load","0");
INSERT INTO property VALUES("secure","tls");
INSERT INTO property VALUES("siwapp_modules","[\"customers\",\"estimates\",\"products\"]");
INSERT INTO property VALUES("VAT","sasasas");



DROP TABLE IF EXISTS province;

CREATE TABLE `province` (
  `provinceid` varchar(45) NOT NULL,
  `provincename` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO province VALUES("EC","Eastern Cape");
INSERT INTO province VALUES("FS","Free State");
INSERT INTO province VALUES("GP","Gauteng");
INSERT INTO province VALUES("LP","Limpopo");
INSERT INTO province VALUES("MP","Mpumalanga");
INSERT INTO province VALUES("NW","North West");
INSERT INTO province VALUES("NC","Northern Cape");
INSERT INTO province VALUES("WC","Western Cape");
INSERT INTO province VALUES("KZN","KwaZulu-Natal");



DROP TABLE IF EXISTS series;

CREATE TABLE `series` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `first_number` int(11) DEFAULT '1',
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8

;INSERT INTO series VALUES("1","Design","Design-","1","1");
INSERT INTO series VALUES("2","Design","Design-","1","1");
INSERT INTO series VALUES("3","Design","Design-","1","1");



DROP TABLE IF EXISTS sf_guard_group;

CREATE TABLE `sf_guard_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS sf_guard_group_permission;

CREATE TABLE `sf_guard_group_permission` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `permission_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS sf_guard_permission;

CREATE TABLE `sf_guard_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS sf_guard_remember_key;

CREATE TABLE `sf_guard_remember_key` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `remember_key` varchar(32) DEFAULT NULL,
  `ip_address` varchar(50) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO sf_guard_remember_key VALUES("7","1","dd756347ced4067bb253011c0d82b418","154.69.29.19","2014-10-20 19:36:41","2014-10-20 19:36:41");



DROP TABLE IF EXISTS sf_guard_user;

CREATE TABLE `sf_guard_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `algorithm` varchar(128) NOT NULL DEFAULT 'sha1',
  `salt` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_super_admin` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8

;INSERT INTO sf_guard_user VALUES("1","vogspesw","sha1","0312e3ef36c6f0c219b0c519c95da098","$2y$07$BCryptRequires22Chrcte/VlQH0piJtjXl.0t1XkA8pw9dMXTpOq","1","1","2014-10-20 07:36:41","2014-08-12 22:08:28","2014-10-20 19:36:41");



DROP TABLE IF EXISTS sf_guard_user_group;

CREATE TABLE `sf_guard_user_group` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS sf_guard_user_permission;

CREATE TABLE `sf_guard_user_permission` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `permission_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;


DROP TABLE IF EXISTS sf_guard_user_profile;

CREATE TABLE `sf_guard_user_profile` (
  `id` bigint(20) NOT NULL,
  `sf_guard_user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nb_display_results` smallint(6) DEFAULT NULL,
  `language` varchar(3) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `search_filter` varchar(30) DEFAULT NULL,
  `series` varchar(50) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO sf_guard_user_profile VALUES("1","1","Nico","Maekiso","info@vogsphere.co.za","","en","","","1","");



DROP TABLE IF EXISTS tab_globalsettings;

CREATE TABLE `tab_globalsettings` (
  `tab_globalsettingsid` int(11) NOT NULL,
  `greentime` int(11) NOT NULL,
  `orangetime` int(11) NOT NULL,
  `redtime` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;INSERT INTO tab_globalsettings VALUES("1","1","2","3");



DROP TABLE IF EXISTS tab_truckmonitor;

CREATE TABLE `tab_truckmonitor` (
  `monitor_id` int(11) NOT NULL,
  `TruckNumber` varchar(45) DEFAULT NULL,
  `TruckType` varchar(45) DEFAULT NULL,
  `CheckInDate` varchar(45) DEFAULT NULL,
  `CheckInTime` varchar(45) DEFAULT NULL,
  `CheckOutTime` varchar(45) DEFAULT NULL,
  `Action` varchar(45) DEFAULT NULL,
  `TimeCount` varchar(45) DEFAULT NULL,
  `Indicator` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO tab_truckmonitor VALUES("80","ESW89KMD","SD","2017-05-07","05:27:49","05:30:51","CheckedOUT","0 : 3 : 2","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("81","TEST1","SD","2017-05-07","05:31:17","09:07:22","CheckedOUT","3 : 36 : 5","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("82","iuyt","SD","2017-05-07","05:45:06","09:13:00","CheckedOUT","3 : 27 : 54","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("83","DJVL86GP","SD","2017-05-08","09:50:58","10:03:18","CheckedOUT","0 : 12 : 20","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("84","test1","SD","2017-05-08","10:03:53","10:22:10","CheckedOUT","0 : 18 : 17","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("85","test2","SD","2017-05-08","10:04:03","10:52:41","CheckedOUT","0 : 48 : 38","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("86","test3","SD","2017-05-09","06:33:14","11:19:49","CheckedOUT","4 : 46 : 35","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("87","test6","SD","2017-05-09","06:33:25","10:52:46","CheckedOUT","4 : 19 : 21","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("88","test54","PD","2017-05-09","06:35:57","10:53:06","CheckedOUT","4 : 17 : 9","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("89","Mlu-Modise","SD","2017-05-09","10:34:24","10:53:13","CheckedOUT","0 : 18 : 49","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("90","Tests-Modise","SD","2017-05-09","10:38:43","10:53:00","CheckedOUT","0 : 14 : 17","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("91","OrangeTest","SD","2017-05-09","10:43:19","10:52:53","CheckedOUT","0 : 9 : 34","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("92","CDJC86VLGP","SD","2017-05-09","11:10:59","11:16:36","CheckedOUT","0 : 5 : 37","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("93","mLU18","SD","2017-05-09","11:12:28","11:19:27","CheckedOUT","0 : 6 : 59","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("94","bbffgg98","SD","2017-05-09","11:20:26","03:25:34","CheckedOUT","7 : 54 : 52","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("95","Test74","SD","2017-05-09","11:21:43","03:24:38","CheckedOUT","7 : 57 : 5","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("96","test3","SD","2017-05-09","11:22:07","03:24:52","CheckedOUT","7 : 57 : 15","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("97","test3","SD","2017-05-09","11:22:53","03:24:15","CheckedOUT","7 : 58 : 38","Green.jpg");
INSERT INTO tab_truckmonitor VALUES("98","cdftergp","PD","2017-05-09","11:30:30","03:25:48","CheckedOUT","8 : 4 : 42","Green.jpg");



DROP TABLE IF EXISTS tag;

CREATE TABLE `tag` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `is_triple` tinyint(1) DEFAULT NULL,
  `triple_namespace` varchar(100) DEFAULT NULL,
  `triple_key` varchar(100) DEFAULT NULL,
  `triple_value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO tag VALUES("1","veniam","0","","","");
INSERT INTO tag VALUES("2","non","0","","","");
INSERT INTO tag VALUES("3","velit","0","","","");
INSERT INTO tag VALUES("4","commodo","0","","","");
INSERT INTO tag VALUES("5","sit","0","","","");
INSERT INTO tag VALUES("6","exercitation","0","","","");
INSERT INTO tag VALUES("7","do","0","","","");
INSERT INTO tag VALUES("8","lorem","0","","","");
INSERT INTO tag VALUES("9","laboris","0","","","");
INSERT INTO tag VALUES("10","nostrud","0","","","");



DROP TABLE IF EXISTS tagging;

CREATE TABLE `tagging` (
  `id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `taggable_model` varchar(30) DEFAULT NULL,
  `taggable_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO tagging VALUES("1","1","Invoice","1");
INSERT INTO tagging VALUES("2","2","Invoice","1");
INSERT INTO tagging VALUES("3","3","Invoice","2");
INSERT INTO tagging VALUES("4","3","Invoice","3");
INSERT INTO tagging VALUES("5","4","Invoice","3");
INSERT INTO tagging VALUES("6","5","Invoice","3");
INSERT INTO tagging VALUES("7","6","Invoice","3");
INSERT INTO tagging VALUES("8","7","Invoice","3");
INSERT INTO tagging VALUES("9","8","Invoice","4");
INSERT INTO tagging VALUES("10","9","Invoice","4");
INSERT INTO tagging VALUES("11","10","Invoice","4");
INSERT INTO tagging VALUES("12","4","Invoice","7");
INSERT INTO tagging VALUES("13","9","Invoice","8");
INSERT INTO tagging VALUES("14","1","Invoice","8");
INSERT INTO tagging VALUES("15","4","Invoice","9");
INSERT INTO tagging VALUES("16","9","Invoice","14");
INSERT INTO tagging VALUES("17","5","Invoice","15");
INSERT INTO tagging VALUES("18","7","Invoice","16");
INSERT INTO tagging VALUES("19","4","Invoice","17");
INSERT INTO tagging VALUES("20","9","Invoice","18");



DROP TABLE IF EXISTS tax;

CREATE TABLE `tax` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` decimal(53,2) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8

;INSERT INTO tax VALUES("1","IVA 16%","16.00","0","0");
INSERT INTO tax VALUES("2","IVA 4%","4.00","0","0");
INSERT INTO tax VALUES("3","IVA 7%","7.00","0","0");
INSERT INTO tax VALUES("4","IRPF","-15.00","0","0");
INSERT INTO tax VALUES("5","VAT 15%","15.00","1","1");



DROP TABLE IF EXISTS template;

CREATE TABLE `template` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `template` longtext,
  `models` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO template VALUES("1","Invoice Template","<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">/\n/<html lang=\"{{lang}}\" xmlns=\"http://www.w3.org/1999/xhtml\">/\n/<head>/\n/<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />/\n/<title>Invoice</title>/\n//\n//\n//\n//\n/<style type=\"text/css\">/\n//* Custom CSS code *//\n/table {border-spacing:0; border-collapse: collapse; }/\n/ul {list-style-type: none; padding-left:0;}/\n/body, input, textarea { font-family:helvetica,sans-serif; font-size:8pt; }/\n/body { color:#464648; margin:2cm 1.5cm; }/\n/h2 { color:black; font-size:16pt; font-weight:bold; line-height:1.2em; border-bottom:1px solid #0099FF; margin-center:220px }/\n/h3 { color:black; font-size:13pt; font-weight:bold; margin-bottom: 0em}/\n/label {color:black;}/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/table th.right,/\n/table td.right { text-align:right; }/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/.customer-data { padding:1em 0; }/\n/.customer-data table { width:100%; }/\n/.customer-data table td { width:50%; }/\n/.customer-data td span { display:block; margin:0 0 5pt; padding-bottom:2pt; border-bottom:1px solid #DCDCDC; }/\n/.customer-data td span.left { margin-right:1em; }/\n/.customer-data label { display:block; font-weight:bold; font-size:8pt; }/\n/.payment-data { padding:1em 0; }/\n/.payment-data table { width:100%; }/\n/.payment-data th,/\n/.payment-data td { line-height:1em; padding:5pt 8pt 5pt; border:1px solid #DCDCDC; }/\n/.payment-data thead th { background:#FAFAFA; }/\n/.payment-data th { font-weight:bold; white-space:nowrap; }/\n/.payment-data .bottomleft { border-color:white; border-top:inherit; border-right:inherit; }/\n/.payment-data span.tax { display:block; white-space:nowrap; }/\n/.terms, .notes { padding:9pt 0 0; font-size:7pt; line-height:9pt; }/\n/.company-data { text-align: right; }/\n/.section { margin-bottom: 1em; }/\n/.logo { text-align: left; }/\n/</style>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<style type=\"text/css\">/\n//* CSS code for printing *//\n/@media print {/\n/body { margin:auto; }/\n/.section { page-break-inside:avoid; }/\n/div#sfWebDebug { display:none; }/\n/}/\n/</style>/\n/</head>/\n/<body>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">/\n/<tr>/\n/<td>/\n/{% if settings.company_logo %}/\n/<span class=\"logo\" align=\"left\">/\n/<img src=\"{{ settings.company_logo }}\" alt=\"{{ settings.company_name }}\" />/\n/</span>/\n/{% endif %}/\n/</td>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<td>/\n/<span class=\"company-data\">/\n/<ul>/\n/<li>Company: {{settings.company_name}}</li>/\n/<li>Address: {{settings.company_address}}</li>/\n/<li>Phone: {{settings.company_phone}}</li>/\n/<li>Registration No: {{settings.company_fax}}</li>/\n/<li>Email: {{settings.company_email}}</li>/\n/<li>Web: {{settings.company_url}}</li> /\n/</ul>/\n/</span>/\n/</td>/\n/</tr>/\n/</table>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"h2\">/\n/<h2 style=\"text-align:center;\">Invoice for {{invoice.customer_name}} #{{invoice}}</h2>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Client info</h3>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"customer-data\">/\n/<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Customer:</label>/\n/{{invoice.customer_name}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Customer identification:</label>/\n/{{invoice.customer_identification}}/\n/</span>/\n/</td>/\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Contact person:</label>/\n/{{invoice.contact_person}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Email:</label>/\n/{{invoice.customer_email}}/\n/</span>/\n/</td>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Phone:</label>/\n/{{invoice.customer_phone}}/\n/</span>/\n/</td>/\n/<td>/\n/</tr>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Invoicing address:</label>/\n/{{invoice.invoicing_address|format}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Shipping address:</label>/\n/{{invoice.shipping_address|format}}/\n/</span>/\n/</td>/\n/</tr>/\n/</table>/\n/</div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Payment details</h3>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"payment-data\">/\n/<table style=\"background-color:#E0F0FF;\">/\n/<thead>/\n/<tr>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\">Description</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Unit Cost</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Qty</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Taxes</th>/\n/{# show discounts only if there is some discount #}/\n/{% if invoice.discount_amount %}/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Discount</th>/\n/{% endif %}/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Price</th>/\n/</tr>/\n/</thead>/\n/<tbody>/\n/{% for item in invoice.Items %}/\n/<tr>/\n/<td>/\n/{{item.description}}/\n/</td>/\n/<td class=\"right\">{{item.unitary_cost|currency}}</td>/\n/<td class=\"right\">{{item.quantity}}</td>/\n/<td class=\"right\">/\n/{% for tax in item.Taxes %}/\n/<span class=\"tax\">{{tax.name}}</span>/\n/{% endfor %}/\n/</td>/\n/{% if invoice.discount_amount %}/\n/<td class=\"right\">{{item.discount_amount|currency}}</td>/\n/{% endif %}/\n/<td class=\"right\">{{item.gross_amount|currency}}</td>/\n/</tr>/\n/{% endfor %}/\n/</tbody>/\n/<tfoot>/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Base</th>/\n/<td class=\"right\">{{invoice.base_amount|currency}}</td>/\n/</tr>/\n/{% if invoice.discount_amount %}/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Discount</th>/\n/<td class=\"td_global_discount right\">{{invoice.discount_amount|currency}}</td>/\n/</tr>/\n/{% endif %}/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Subtotal</th>/\n/<td class=\"td_subtotal right\">{{invoice.net_amount|currency}}</td>/\n/</tr>/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Taxes</th>/\n/<td class=\"td_total_taxes right\">{{invoice.tax_amount|currency}}</td>/\n/</tr>/\n/<tr class=\"strong\">/\n/<td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Total</th>/\n/<td class=\"td_total right\">{{invoice.gross_amount|currency}}</td>/\n/</tr>/\n/</tfoot>/\n/</table>/\n/</div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/ <h3>Notes</h3>/\n/ <div class=\"terms\">/\n/   {{invoice.notes}}/\n/ </div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Terms & conditions</h3>/\n/<div class=\"terms\">/\n/{{invoice.terms}}/\n/</div>/\n/</div>/\n/</body>/\n/</html>/\n/","Invoice","2014-08-12 22:08:28","2014-08-26 16:51:07","invoice-template");
INSERT INTO template VALUES("2","Template with product","<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">/\n/<html lang=\"{{lang}}\" xmlns=\"http://www.w3.org/1999/xhtml\">/\n/<head>/\n/ <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />/\n/ <title>Invoice</title>/\n//\n/ <style type=\"text/css\">/\n/   /* Custom CSS code *//\n/   table {border-spacing:0; border-collapse: collapse;}/\n/   ul {list-style-type: none; padding-left:0;}/\n/   body, input, textarea { font-family:helvetica,sans-serif; font-size:8pt; }/\n/   body { color:#464648; margin:2cm 1.5cm; }/\n/   h2   { color:#535255; font-size:16pt; font-weight:normal; line-height:1.2em; border-bottom:1px solid #DB4823; margin-right:220px }/\n/   h3   { color:#9A9A9A; font-size:13pt; font-weight:normal; margin-bottom: 0em}/\n//\n/   table th.right,/\n/   table td.right              { text-align:right; }/\n//\n/   .customer-data              { padding:1em 0; }/\n/   .customer-data table        { width:100%;       }/\n/   .customer-data table td     { width:50%;        }/\n/   .customer-data td span      { display:block; margin:0 0 5pt; padding-bottom:2pt; border-bottom:1px solid #DCDCDC; }/\n/   .customer-data td span.left { margin-right:1em; }/\n/   .customer-data label        { display:block; font-weight:bold; font-size:8pt; }/\n/   .payment-data               { padding:1em 0;    }/\n/   .payment-data table         { width:100%;       }/\n/   .payment-data th,/\n/   .payment-data td            { line-height:1em; padding:5pt 8pt 5pt; border:1px solid #DCDCDC; }/\n/   .payment-data thead th      { background:#FAFAFA; }/\n/   .payment-data th            { font-weight:bold; white-space:nowrap; }/\n/   .payment-data .bottomleft   { border-color:white; border-top:inherit; border-right:inherit; }/\n/   .payment-data span.tax      { display:block; white-space:nowrap; }/\n/   .terms, .notes              { padding:9pt 0 0; font-size:7pt; line-height:9pt; }/\n//\n/   .section                    { margin-bottom: 1em; }/\n/   .logo                       { text-align: right; }/\n/ </style>/\n//\n/ <style type=\"text/css\">/\n/   /* CSS code for printing *//\n/   @media print {/\n/     body           { margin:auto; }/\n/     .section       { page-break-inside:avoid; }/\n/     div#sfWebDebug { display:none; }/\n/   }/\n/ </style>/\n/</head>/\n/<body>/\n//\n/ {% if settings.company_logo %}/\n/   <div class=\"logo\">/\n/     <img src=\"{{ settings.company_logo }}\" alt=\"{{ settings.company_name }}\" />/\n/   </div>/\n/ {% endif %}/\n//\n/ <div class=\"h2\">/\n/   <h2>Invoice #{{invoice.number}}</h2>/\n/ </div>/\n//\n/ <div class=\"section\">/\n/   <div class=\"company-data\">/\n/     <ul>/\n/       <li>Company: {{settings.company_name}}</li>/\n/       <li>Address: {{settings.company_address|format}}</li>/\n/       <li>Phone: {{settings.company_phone}}</li>/\n/       <li>Fax: {{settings.company_fax}}</li>/\n/       <li>Email: {{settings.company_email}}</li>/\n/       <li>Web: {{settings.company_url}}</li>/\n/     </ul>/\n/   </div>/\n/ </div>/\n//\n/ <div class=\"section\">/\n/   <h3>Client info</h3>/\n//\n/   <div class=\"customer-data\">/\n/     <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">/\n/       <tr>/\n/         <td>/\n/           <span class=\"left\">/\n/             <label>Customer:</label>/\n/             {{invoice.customer_name}}/\n/           </span>/\n/         </td>/\n/         <td>/\n/           <span class=\"right\">/\n/             <label>Customer identification:</label>/\n/             {{invoice.customer_identification}}/\n/           </span>/\n/         </td>/\n/       </tr>/\n/       <tr>/\n/         <td>/\n/           <span class=\"left\">/\n/             <label>Contact person:</label>/\n/             {{invoice.contact_person}}/\n/           </span>/\n/         </td>/\n/         <td>/\n/           <span class=\"right\">/\n/             <label>Email:</label>/\n/             {{invoice.customer_email}}/\n/           </span>/\n/         </td>/\n/       </tr>/\n/       <tr>/\n/         <td>/\n/           <span class=\"left\">/\n/             <label>Invoicing address:</label>/\n/             {{invoice.invoicing_address|format}}/\n/           </span>/\n/         </td>/\n/         <td>/\n/           <span class=\"right\">/\n/             <label>Shipping address:</label>/\n/             {{invoice.shipping_address|format}}/\n/           </span>/\n/         </td>/\n/       </tr>/\n/     </table>/\n/   </div>/\n/ </div>/\n//\n/ <div class=\"section\">/\n/   <h3>Payment details</h3>/\n//\n/   <div class=\"payment-data\">/\n/     <table>/\n/       <thead>/\n/         <tr>/\n/           <th>Reference</th>/\n/           <th>Description</th>/\n/           <th class=\"right\">Unit Cost</th>/\n/           <th class=\"right\">Qty</th>/\n/           <th class=\"right\">TVA</th>/\n/           {# show discounts only if there is some discount #}/\n/           {% if invoice.discount_amount %}/\n/           <th class=\"right\">Discount</th>/\n/           {% endif %}/\n/           <th class=\"right\">Price</th>/\n/         </tr>/\n/       </thead>/\n/       <tbody>/\n/         {% for item in invoice.Items %}/\n/           <tr>/\n/             <td>/\n/               {{item.product_id|product_reference}}/\n/             </td>/\n/             <td>/\n/               {{item.description}}/\n/             </td>/\n/             <td class=\"right\">{{item.unitary_cost|currency}}</td>/\n/             <td class=\"right\">{{item.quantity}}</td>/\n/             <td class=\"right\">/\n/               {% for tax in item.Taxes %}/\n/                 <span class=\"tax\">{{tax.name}}</span>/\n/               {% endfor %}/\n/             </td>/\n/             {% if invoice.discount_amount %}/\n/             <td class=\"right\">{{item.discount|currency}}</td>/\n/             {% endif %}/\n/             <td class=\"right\">{{item.gross|currency}}</td>/\n/           </tr>/\n/         {% endfor %}/\n/       </tbody>/\n/       <tfoot>/\n/         <tr>/\n/           <td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}5{% else %}4{% endif %}\"></td>/\n/           <th class=\"right\">Base</th>/\n/           <td class=\"right\">{{invoice.base_amount|currency}}</td>/\n/         </tr>/\n/         {% if invoice.discount_amount %}/\n/         <tr>/\n/           <td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}5{% else %}4{% endif %}\"></td>/\n/           <th class=\"right\">Discount</th>/\n/           <td class=\"td_global_discount right\">{{invoice.discount_amount|currency}}</td>/\n/         </tr>/\n/         {% endif %}/\n/         <tr>/\n/           <td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}5{% else %}4{% endif %}\"></td>/\n/           <th class=\"right\">Subtotal</th>/\n/           <td class=\"td_subtotal right\">{{invoice.net_amount|currency}}</td>/\n/         </tr>/\n/         <tr>/\n/           <td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}5{% else %}4{% endif %}\"></td>/\n/           <th class=\"right\">Taxes</th>/\n/           <td class=\"td_total_taxes right\">{{invoice.tax_amount|currency}}</td>/\n/         </tr>/\n/         <tr class=\"strong\">/\n/           <td class=\"bottomleft\" colspan=\"{% if invoice.discount_amount %}5{% else %}4{% endif %}\"></td>/\n/           <th class=\"right\">Total</th>/\n/           <td class=\"td_total right\">{{invoice.gross_amount|currency}}</td>/\n/         </tr>/\n/       </tfoot>/\n/     </table>/\n/   </div>/\n/ </div>/\n//\n/ <div class=\"section\">/\n/   <h3>Terms & conditions</h3>/\n/   <div class=\"terms\">/\n/     {{invoice.terms|format}}/\n/   </div>/\n/ </div>/\n/</body>/\n/</html>/\n/","Invoice","2014-08-12 22:08:28","2014-08-12 22:08:28","template-with-product");
INSERT INTO template VALUES("3","Quotation Template","<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">/\n/<html lang=\"{{lang}}\" xmlns=\"http://www.w3.org/1999/xhtml\">/\n/<head>/\n/<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />/\n/<title>Quotation</title>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<style type=\"text/css\">/\n//* Custom CSS code *//\n/table {border-spacing:0; border-collapse: collapse; }/\n/ul {list-style-type: none; padding-left:0;}/\n/body, input, textarea { font-family:helvetica,sans-serif; font-size:8pt; }/\n/body { color:#464648; margin:2cm 1.5cm; }/\n/h2 { color:black; font-size:16pt; font-weight:bold; line-height:1.2em; border-bottom:1px solid #0099FF; margin-center:220px }/\n/h3 { color:black; font-size:13pt; font-weight:bold; margin-bottom: 0em}/\n/label {color:black;}/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/table th.right,/\n/table td.right { text-align:right; }/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/.customer-data { padding:1em 0; }/\n/.customer-data table { width:100%; }/\n/.customer-data table td { width:50%; }/\n/.customer-data td span { display:block; margin:0 0 5pt; padding-bottom:2pt; border-bottom:1px solid #DCDCDC; }/\n/.customer-data td span.left { margin-right:1em; }/\n/.customer-data label { display:block; font-weight:bold; font-size:8pt; }/\n/.payment-data { padding:1em 0; }/\n/.payment-data table { width:100%; }/\n/.payment-data th,/\n/.payment-data td { line-height:1em; padding:5pt 8pt 5pt; border:1px solid #DCDCDC; }/\n/.payment-data thead th { background:#FAFAFA; }/\n/.payment-data th { font-weight:bold; white-space:nowrap; }/\n/.payment-data .bottomleft { border-color:white; border-top:inherit; border-right:inherit; }/\n/.payment-data span.tax { display:block; white-space:nowrap; }/\n/.terms, .notes { padding:9pt 0 0; font-size:7pt; line-height:9pt; }/\n/.company-data { text-align: right; }/\n/.section { margin-bottom: 1em; }/\n/.logo { text-align: left; }/\n/</style>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<style type=\"text/css\">/\n//* CSS code for printing *//\n/@media print {/\n/body { margin:auto; }/\n/.section { page-break-inside:avoid; }/\n/div#sfWebDebug { display:none; }/\n/}/\n/</style>/\n/</head>/\n/<body>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">/\n/<tr>/\n/<td>/\n/{% if settings.company_logo %}/\n/<span class=\"logo\" align=\"left\">/\n/<img src=\"{{ settings.company_logo }}\" alt=\"{{ settings.company_name }}\" />/\n/</span>/\n/{% endif %}/\n/</td>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<td>/\n/<span class=\"company-data\">/\n/<ul>/\n/<li>Company: {{settings.company_name}}</li>/\n/<li>Address: {{settings.company_address}}</li>/\n/<li>Phone: {{settings.company_phone}}</li>/\n/<li>Registration No: {{settings.company_fax}}</li>/\n/<li>Email: {{settings.company_email}}</li>/\n/<li>Web: {{settings.company_url}}</li>/\n/</ul>/\n/</span>/\n/</td>/\n/</tr>/\n/</table>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"h2\">/\n/<h2 style=\"text-align:center;\">Quotation for {{estimate.customer_name}}  #{{estimate}}</h2>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Client info</h3>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"customer-data\">/\n/<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Customer:</label>/\n/{{estimate.customer_name}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Customer identification:</label>/\n/{{estimate.customer_identification}}/\n/</span>/\n/</td>/\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Contact person:</label>/\n/{{estimate.contact_person}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Email:</label>/\n/{{estimate.customer_email}}/\n/</span>/\n/</td>/\n//\n//\n//\n//\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Phone:</label>/\n/{{invoice.customer_phone}}/\n/</span>/\n/</td>/\n/<td>/\n/</tr>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/</tr>/\n/<tr>/\n/<td>/\n/<span class=\"left\">/\n/<label>Invoicing address:</label>/\n/{{estimate.invoicing_address|format}}/\n/</span>/\n/</td>/\n/<td>/\n/<span class=\"right\">/\n/<label>Shipping address:</label>/\n/{{estimate.shipping_address|format}}/\n/</span>/\n/</td>/\n/</tr>/\n/</table>/\n/</div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Payment details</h3>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"payment-data\">/\n/<table style=\"background-color:#E0F0FF;\">/\n/<thead>/\n/<tr>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\">Description</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Unit Cost</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Qty</th>/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Taxes</th>/\n/{# show discounts only if there is some discount #}/\n/{% if estimate.discount_amount %}/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Discount</th>/\n/{% endif %}/\n/<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Price</th>/\n/</tr>/\n/</thead>/\n/<tbody>/\n/{% for item in estimate.Items %}/\n/<tr>/\n/<td>/\n/{{item.description}}/\n/</td>/\n/<td class=\"right\">{{item.unitary_cost|currency}}</td>/\n/<td class=\"right\">{{item.quantity}}</td>/\n/<td class=\"right\">/\n/{% for tax in item.Taxes %}/\n/<span class=\"tax\">{{tax.name}}</span>/\n/{% endfor %}/\n/</td>/\n/{% if estimate.discount_amount %}/\n/<td class=\"right\">{{item.discount_amount|currency}}</td>/\n/{% endif %}/\n/<td class=\"right\">{{item.gross_amount|currency}}</td>/\n/</tr>/\n/{% endfor %}/\n/</tbody>/\n/<tfoot>/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if estimate.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Base</th>/\n/<td class=\"right\">{{estimate.base_amount|currency}}</td>/\n/</tr>/\n/{% if estimate.discount_amount %}/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if estimate.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Discount</th>/\n/<td class=\"td_global_discount right\">{{estimate.discount_amount|currency}}</td>/\n/</tr>/\n/{% endif %}/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if estimate.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Subtotal</th>/\n/<td class=\"td_subtotal right\">{{estimate.net_amount|currency}}</td>/\n/</tr>/\n/<tr>/\n/<td class=\"bottomleft\" colspan=\"{% if estimate.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Taxes</th>/\n/<td class=\"td_total_taxes right\">{{estimate.tax_amount|currency}}</td>/\n/</tr>/\n/<tr class=\"strong\">/\n/<td class=\"bottomleft\" colspan=\"{% if estimate.discount_amount %}4{% else %}3{% endif %}\"></td>/\n/<th class=\"right\" style=\"color:black;\">Total</th>/\n/<td class=\"td_total right\">{{estimate.gross_amount|currency}}</td>/\n/</tr>/\n/</tfoot>/\n/</table>/\n/</div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/ <h3>Notes</h3>/\n/ <div class=\"terms\">/\n/   {{estimate.notes}}/\n/ </div>/\n/</div>/\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n//\n/<div class=\"section\">/\n/<h3>Terms & conditions</h3>/\n/<div class=\"terms\">/\n/{{estimate.terms}}/\n/</div>/\n/</div>/\n/</body>/\n/</html>","Estimate","2014-08-12 22:08:28","2014-08-26 16:52:39","estimate-template");



DROP TABLE IF EXISTS user;

CREATE TABLE `user` (
  `userid` bigint(13) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(45) NOT NULL,
  `role` enum('customer','admin') NOT NULL,
  `failedAttempt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;INSERT INTO user VALUES("6305240160082","010791","bonittheron8@gmail.com","customer","0");
INSERT INTO user VALUES("6401250420084","NOMA57","nomasontob57@gmail.com","customer","0");
INSERT INTO user VALUES("6704125366089","Nthabie4*","abie.pitso@gmail.com","customer","0");
INSERT INTO user VALUES("6911110151083","010791","lechebotes41@gmail.com","customer","0");
INSERT INTO user VALUES("7512125283082","Lubanzi1","miraserajab@outlook.com","customer","0");
INSERT INTO user VALUES("7611150155080","Marsha123Safe","ranganm@sabc.co.za","customer","0");
INSERT INTO user VALUES("7709120167084","010791","delise@jetdemolition.co.za","customer","0");
INSERT INTO user VALUES("8004060552084","Lucia8080","mhlomeli.pumela@gmail.com","customer","0");
INSERT INTO user VALUES("8105150506086","8105150506086","cebisaj@gmail.com","customer","0");
INSERT INTO user VALUES("8107300561086","fem3007","fmayekiso@gmail.com","customer","0");
INSERT INTO user VALUES("8306145448083","CellC123","ssmthembu@gmail.com","customer","0");
INSERT INTO user VALUES("8309051276084","jeany","jeanymotlokoa@gmail.com","customer","0");
INSERT INTO user VALUES("8409055665082","mf05740","mogoasha.mabanna@absa.co.za","customer","0");
INSERT INTO user VALUES("8504045897082","Mops","suzansuziano@gmail.com","customer","0");
INSERT INTO user VALUES("8504265079080","22duifie","stephen@gearupcouriers.co.za","customer","0");
INSERT INTO user VALUES("8601045310080","Lenkie04","nico.maekiso@gmail.com","customer","0");
INSERT INTO user VALUES("8610265252088","Telkom2525","mkontwg@telkom.co.za","customer","0");
INSERT INTO user VALUES("8611075771085","19861107","M.mokoaqatsa@gmail.com","customer","0");
INSERT INTO user VALUES("8705315547089","870531","keaoleboga.nthebe@gmail.com","customer","0");
INSERT INTO user VALUES("8708035545086","pepele","tumelomodise4@gmail.com","customer","0");
INSERT INTO user VALUES("8708175079086","Deurikak1@","jpvdmerwe17@gmail.com","customer","0");
INSERT INTO user VALUES("8804046291081","Petrus@1","paseka.chakela@yahoo.com","customer","0");
INSERT INTO user VALUES("8812156083081","Vikelathina1","vikelathina@gmail.com","customer","0");
INSERT INTO user VALUES("8901075369083","Itumeleng@1234567","Itumelengs002@gmail.com","customer","0");
INSERT INTO user VALUES("8905230299085","Zuluboy01","www.buhlebuzile@gmail.com","customer","0");
INSERT INTO user VALUES("8906165263088","tomtom1989","molete.thomas@gmail.com","customer","0");
INSERT INTO user VALUES("8908295327080","Mabitle@1989","mabitlethomas@gmail.com","customer","0");
INSERT INTO user VALUES("8908305579084","8908305579084","omalebye@gmail.com","customer","0");
INSERT INTO user VALUES("9006015448085","0746186980","sebethap@gmail.com","customer","0");
INSERT INTO user VALUES("9009266033085","9009266033085","ncedosss@gmail.com","customer","0");
INSERT INTO user VALUES("9010040032088","lovebunny","Melanie.ludik@gmail.com","customer","0");
INSERT INTO user VALUES("9012265708712","dummy","mahwayibusiness@gmail.com","customer","0");
INSERT INTO user VALUES("9012265761086","19912625","mahwaymp@gmail.com","admin","0");
INSERT INTO user VALUES("9107010071086","010791","marchebotes65@Gmail.com","customer","0");
INSERT INTO user VALUES("9112245040088","010791","kervin.davids@huawei.com","customer","0");
INSERT INTO user VALUES("9202185307082","thabz2324","thabanglesetjaphaleng@gmail.com","customer","0");
INSERT INTO user VALUES("9206035099088","Hermank92","kritzingerherman@gmail.com","customer","0");
INSERT INTO user VALUES("9403120028086","Liebenberg0312","Sharon.Liebenberg@absa.co.za","customer","0");
INSERT INTO user VALUES("9901015545086","DummyTest10111*","info@vogsphere.co.zax","customer","0");
INSERT INTO user VALUES("9901019999997","forget10111*","vogspesw_siwapp_v2@vogsphere.co.za","admin","0");
INSERT INTO user VALUES("9901019999998","tshepo10111*","admin@vogsphere.co.za","admin","0");
INSERT INTO user VALUES("9901019999999","tsere10111*","info@vogsphere.co.za","admin","0");



DROP TABLE IF EXISTS vehicle;

CREATE TABLE `vehicle` (
  `VehicleRego` char(10) NOT NULL,
  `Make` varchar(20) DEFAULT NULL,
  `Model` varchar(20) DEFAULT NULL,
  `ModelYear` year(4) DEFAULT NULL,
  `PurchasePrice` decimal(7,2) DEFAULT NULL,
  `ValuationReq` enum('Y','N') DEFAULT NULL,
  `ApplicationId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8

;


