<?php
/* -- BOC - Global Settings -- */
// -- Global Setting Controller.
$globalsetting  = new globalsetting();
$globalsettings = $globalsetting->showAll();	

// -- Bind Global Data into Session.
foreach($globalsettings as $globalsetting)
  {
 // -- Global Settings	  
  if($globalsetting['keey']=='company_name')   {$_SESSION['company_name'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_address'){$_SESSION['company_address'] = $globalsetting['value'];}
  if($globalsetting['keey']=='company_email')  {$_SESSION['company_email']   = $globalsetting['value'];}
  if($globalsetting['keey']=='company_fax')    {$_SESSION['company_fax'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_logo')   {$_SESSION['company_logo'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_phone')  {$_SESSION['company_phone'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_url')    {$_SESSION['company_url'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='currency')       {$_SESSION['currency'] 		 = $globalsetting['value'];}
  if($globalsetting['keey']=='currency_decimals'){$_SESSION['currency_decimals'] = $globalsetting['value'];}
  if($globalsetting['keey']=='default_template'){$_SESSION['default_template'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='last_calculation_date'){$_SESSION['last_calculation_date'] = $globalsetting['value'];}
  if($globalsetting['keey']=='legal_terms')     {$_SESSION['legal_terms']		= $globalsetting['value'];}
  if($globalsetting['keey']=='pdf_orientation') {$_SESSION['pdf_orientation'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='pdf_size')        {$_SESSION['pdf_size']			= $globalsetting['value'];}
  if($globalsetting['keey']=='sample_data_load'){$_SESSION['sample_data_load'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='siwapp_modules')  {$_SESSION['siwapp_modules'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='company_reg')  {$_SESSION['company_reg'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='VAT')  {$_SESSION['VAT'] 	= $globalsetting['value'];}
  
  // -- SMTP Settings
  if($globalsetting['keey']=='host') {$_SESSION['host'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='port') {$_SESSION['port']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='secure') {$_SESSION['secure']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='emailusername') {$_SESSION['emailusername'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='emailpassword') {$_SESSION['emailpassword'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='prod') {$_SESSION['prod']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='dev') {$_SESSION['dev']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='captcha') {$_SESSION['captcha'] 	 = $globalsetting['value'];}

  }

?>