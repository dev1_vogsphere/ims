<?php
define('ROOT', str_replace("webservices/ws_recurringinvoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

/* require the user as the parameter 
customerid
ApplicationId
guid
repayment

PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json
*/
// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;

$customerid = null;
$ApplicationId = null;
$dataUserEmail = '';
$role = '';
$repaymentAmount = null;
$found = false;
$RemainingTerm = 0;
$paymentid = 0;		
$credit = 0.00;
$xml = "";
$found = true;
						
/* soak in the passed variable or set our own */					
$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
					
// -- If all Parameters are populated. 
if($found) 
{
	/* soak in the passed variable or set our own */
	//$format = 'xml';//strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
	try
	{
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 		 			 
		// -- Invoice Data.	
			$invoice= new invoice();
			$dataCommon = $invoice->showAllRecurring();
	
			$dataInvoice = null;
			$xml = '<invoices>';
			foreach($dataCommon as $rowCommon)
			{
					//$invoice_id = $rowCommon['id'];					  					  
					$xml = $xml.'<invoice>';					  
					$xml = $xml.'<id>'.$rowCommon['id'].'</id>';
					$xml = $xml.'<series_id>'.$rowCommon['series_id'].'</series_id>'; 
					$xml = $xml.'<customer_id>'.$rowCommon['customer_id'].'</customer_id>';
					$xml = $xml.'<customer_name>'.$rowCommon['customer_name'].'</customer_name>';
					$xml = $xml.'<customer_identification>'.$rowCommon['customer_identification'].'</customer_identification>';
					$xml = $xml.'<customer_email>'.$rowCommon['customer_email'].'</customer_email>';
					$xml = $xml.'<invoicing_address>'.$rowCommon['invoicing_address'].'</invoicing_address>';
					$xml = $xml.'<shipping_address>'.$rowCommon['shipping_address'].'</shipping_address>'; 
					$xml = $xml.'<contact_person>'.$rowCommon['contact_person'].'</contact_person>'; 
					$xml = $xml.'<terms>'.$rowCommon['terms'].'</terms>';
					$xml = $xml.'<notes>'.$rowCommon['notes'].'</notes>'; 
					$xml = $xml.'<base_amount>'.$rowCommon['base_amount'].'</base_amount>'; 
					$xml = $xml.'<discount_amount>'.$rowCommon['discount_amount'].'</discount_amount>'; 
					$xml = $xml.'<net_amount>'.$rowCommon['net_amount'].'</net_amount>';
					$xml = $xml.'<gross_amount>'.$rowCommon['gross_amount'].'</gross_amount>';
					$xml = $xml.'<paid_amount>'.$rowCommon['paid_amount'].'</paid_amount>';
					$xml = $xml.'<tax_amount>'.$rowCommon['tax_amount'].'</tax_amount>';
					$xml = $xml.'<status>'.$rowCommon['status'].'</status>';
					$xml = $xml.'<type>'.$rowCommon['type'].'</type>';
					$xml = $xml.'<draft>'.$rowCommon['draft'].'</draft>';
					$xml = $xml.'<closed>'.$rowCommon['closed'].'</closed>'; 
					$xml = $xml.'<sent_by_email>'.$rowCommon['sent_by_email'].'</sent_by_email>';
					$xml = $xml.'<number>'.$rowCommon['number'].'</number>';
					$xml = $xml.'<recurring_invoice_id>'.$rowCommon['recurring_invoice_id'].'</recurring_invoice_id>'; 
					$xml = $xml.'<issue_date>'.$rowCommon['issue_date'].'</issue_date>'; 
					$xml = $xml.'<due_date>'.$rowCommon['due_date'].'</due_date>';
					$xml = $xml.'<days_to_due>'.$rowCommon['days_to_due'].'</days_to_due>'; 
					$xml = $xml.'<enabled>'.$rowCommon['enabled'].'</enabled>'; 
					$xml = $xml.'<max_occurrences>'.$rowCommon['max_occurrences'].'</max_occurrences>'; 
					$xml = $xml.'<must_occurrences>'.$rowCommon['must_occurrences'].'</must_occurrences>'; 
					$xml = $xml.'<period>'.$rowCommon['period'].'</period>';
					$xml = $xml.'<period_type>'.$rowCommon['period_type'].'</period_type>'; 
					$xml = $xml.'<starting_date>'.$rowCommon['starting_date'].'</starting_date>'; 
					$xml = $xml.'<finishing_date>'.$rowCommon['finishing_date'].'</finishing_date>'; 
					$xml = $xml.'<last_execution_date>'.$rowCommon['last_execution_date'].'</last_execution_date>';
					$xml = $xml.'<created_at>'.$rowCommon['created_at'].'</created_at>'; 
					$xml = $xml.'<updated_at>'.$rowCommon['updated_at'].'</updated_at>'; 
					$xml = $xml.'<customer_phone>'.$rowCommon['customer_phone'].'</customer_phone>';
					
					// -- Invoice Items Data.	
					$dataCommonItems = $invoice->show($rowCommon['id']);
					$xml = $xml.'<items>';
					foreach($dataCommonItems as $rowCommonItem)
					{	$xml = $xml. '<item>';
							$xml = $xml.'<id>'.$rowCommonItem['id'].'</id>';
							$xml = $xml.'<quantity>'.$rowCommonItem['quantity'].'</quantity>';
							$xml = $xml.'<discount>'.$rowCommonItem['discount'].'</discount>';
							$xml = $xml.'<common_id>'.$rowCommonItem['common_id'].'</common_id>';
							$xml = $xml.'<product_id>'.$rowCommonItem['product_id'].'</product_id>';
							//$xml = $xml.'<description>'.$rowCommonItem['description'].'</description>';
							$xml = $xml.'<description></description>';
							$xml = $xml.'<unitary_cost>'.$rowCommonItem['unitary_cost'].'</unitary_cost>';
							$xml = $xml.'<tax_id>'.$rowCommonItem['tax_id'].'</tax_id>';							
						$xml = $xml.'</item>';
					}
					$xml = $xml. '</items>';
					$xml = $xml. '</invoice>';
			}	// End all Customer Invoices
				$xml = $xml. '</invoices>';
				
			// -- Determine the format.
			
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database2::disconnect();	
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
	}	
	// -- EOC 31.10.2017 -------- //
?>