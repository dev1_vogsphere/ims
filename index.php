<?php
session_start();
//error_reporting( E_ALL ); 
//ini_set('display_errors', 1);

define('WEBROOT', str_replace("index.php", "", $_SERVER["SCRIPT_FILENAME"]));
define('ROOT', str_replace("index.php", "", $_SERVER["SCRIPT_FILENAME"]));
define ('DIR','ims');

// -- Default Definition
define("DRAFT", 0);
define("CLOSED", 1);
define("OPENED", 2);
define("OVERDUE", 3);
define("DECIMAL", 0.000000000000000);

require(ROOT . 'config/core.php');

require(ROOT . 'router.php');
require(ROOT . 'request.php');
require(ROOT . 'dispatcher.php');

//require(ROOT . 'pdf/pdf_core.php');

$dispatch = new Dispatcher();
$dispatch->dispatch();
?>