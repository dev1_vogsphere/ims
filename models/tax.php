<?php
if (!class_exists('tax')) 
{
class tax extends Model
{
	var $tablename = 'tax';
	var $taxItems = 'item_tax';

    public function create($name,$value,$active,$is_default)
    {		
		$sql = "INSERT INTO $this->tablename
		(name,value,active,is_default)
		VALUES (?,?,?,?)";
		
		$req = Database::getBdd()->prepare($sql);
	    $req->execute(array($name,$value,$active,$is_default));
		
		$id = Database::getBdd()->lastInsertId();
		 
		return $id;
    }

	    public function createItemTax($item_id,$tax_id)
    {		
		$sql = "INSERT INTO $this->taxItems
		(item_id,tax_id)
		VALUES (?,?)";
		
		$req = Database::getBdd()->prepare($sql);
	    $req->execute(array($item_id,$tax_id));
		
		$id = Database::getBdd()->lastInsertId();
		 				//print_r($req->errorInfo());

		return $id;
    }

	
    public function show($id)
    {
        $sql = "SELECT * FROM tax WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

	// -- Invoice Item taxes.
	public function invoice_item_taxes($id)
    {
        $sql = "SELECT * FROM $this->taxItems WHERE item_id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
        return $req->fetchAll();
    }
	
	public function showAllItemTax()
    {
        $sql = "SELECT * FROM $this->taxItems";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				 //   print_r($req->errorInfo());

        return $req->fetchAll();
    }
	
    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename WHERE active = '1'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				 //   print_r($req->errorInfo());

        return $req->fetchAll();
    }

	public function showAll2()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				 //   print_r($req->errorInfo());

        return $req->fetchAll();
    }
	
    public function edit($id,$name,$value,$active,$is_default)
    {
		
		$sql = "UPDATE $this->tablename
SET
name = ?,
value = ?,
active = ?,
is_default = ?
WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
	    $req->execute(array($name,$value,$active,$is_default,$id));
		$id = Database::getBdd()->lastInsertId();
		 
				//    print_r($req->errorInfo());
 
		return $id;

    }

    public function delete($id)
    {
        $sql = 'DELETE FROM tasks WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
}
?>