<?php
if (!class_exists('whitepapers')) 
{
class whitepapers extends Model
{
	var $tablename = 'whitepapers';
    public function create($name,$description,$date,$category,$picture,$createdby,$doc,$organisation)
	{	
        $sql = "INSERT INTO $this->tablename(name,description,date,category,picture,createdby,document,organisation) VALUES(?,?,?,?,?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($name,$description,$date,$category,$picture,$createdby,$doc,$organisation));
		$id = Database::getBdd()->lastInsertId();
		return $id;
    }

    public function show($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }
	
	public function showbyname($name,$org)
    {
        $sql = "SELECT * FROM $this->tablename WHERE name =" . $name." AND organisation = '".$org."'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAll($name)
    {
        $sql = "SELECT * FROM $this->tablename WHERE organisation = '".$name."'";	
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
	public function showAllById($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =". $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
    public function edit($id,$name,$description,$category,$picture,$changedby,$document)
    {
		$updated_at = Date('Y-m-d h:m:s');
		$sql = "UPDATE $this->tablename
				SET
				name = ?,
				description = ?,
				category = ?,
				picture = ?,
				changedby = ?,
				changedon = ?,
				document = ?
				WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($name,$description,$category,$picture,$changedby,$updated_at,$document,$id));
		
		  //  print_r($req->errorInfo());

        		return $id;
 
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute(array($id));
    }
 }
}
?>