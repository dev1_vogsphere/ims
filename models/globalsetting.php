<?php
if (!class_exists('globalsetting')) 
{
class globalsetting extends Model
{
	var $tablename = 'organisation';
	//var $tablename = 'property';

    public function create($array_organisation)
	{	
		$created_at = Date('Y-m-d h:m:s');//22
		$updated_at = Date('Y-m-d h:m:s');//23
		$array_organisation[] = $created_at;	
		$array_organisation[] = $updated_at;		

		$sql = "INSERT INTO $this->tablename
		(account,accounttype,
		 bankname,branchcode,
		 branchname,company_address,
		 company_email,company_fax,
		 company_logo,company_name,
		 company_phone,company_reg,
		 company_url,currency,
		 legal_terms,pdf_orientation,
		 pdf_size,VAT,
		 created_at,updated_at,
		 updatedby)
VALUES (?,?,
		?,?,
		?,?,
		?,?,
		?,?,
		?,?,
		?,?,
		?,?,
		?,?,?,
		?,?)";

        $pdo = Database::getBdd();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		$req = $pdo->prepare($sql);
		$req->execute(array($array_organisation[14],$array_organisation[15],
							$array_organisation[16],$array_organisation[17],
							$array_organisation[18],$array_organisation[2] ,
							$array_organisation[5] ,$array_organisation[4] ,
							$array_organisation[11],$array_organisation[1] ,
							$array_organisation[3] ,$array_organisation[19],
							$array_organisation[6] ,$array_organisation[7] ,
							$array_organisation[8] ,$array_organisation[10],
							$array_organisation[9] ,$array_organisation[12],
							$array_organisation[20],$array_organisation[21],
							$array_organisation[13]));
		//print($req);					
		$id = Database::getBdd()->lastInsertId();
		$_SESSION['id'] = $id;//1
		//echo ''.$id;
		return $id;
    }

    public function show($id)
    {		
		$globsetting = null;
		$user_table = 'sf_guard_user';
        $sql = "SELECT * FROM $user_table WHERE username ='" . $id."'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		$user = $req->fetch(PDO::FETCH_ASSOC);
		//print_r($user );
		if(!empty($user['organisation']))
		{
			$sql = "SELECT * FROM $this->tablename WHERE company_name ='" . $user['organisation']."'";
			$req2 = Database::getBdd()->prepare($sql);
			$req2->execute();
			$globsetting = $req2->fetch(PDO::FETCH_ASSOC);
		}
        return $globsetting;
    }
	
	public function showbyorganisation($id)
	{
		$globsetting = null;
		$user_table = 'sf_guard_user';
		$sql = "SELECT * FROM $user_table WHERE organisation ='" .$id."'";
		$req2 = Database::getBdd()->prepare($sql);
        $req2->execute(array($id));
		$globsetting = $req2->fetch(PDO::FETCH_ASSOC);
        return $globsetting;
	}
	
    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		
        return $req->fetchAll();
    }
	
	public function search_product($term)
    {
		$term = "'%".$term."%'";
		
        $sql = "SELECT * FROM $this->tablename WHERE reference LIKE $term";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				    //print_r($req->errorInfo());

        return $req->fetchAll();
    }
	

    public function edit($array_organisation)
    {
		$created_at = Date('Y-m-d h:m:s');//22
		$updated_at = Date('Y-m-d h:m:s');//23
		$array_organisation[] = $created_at;	
		$array_organisation[] = $updated_at;		

		$sql = "UPDATE $this->tablename
		SET account = ?,accounttype = ?,
			bankname = ?,branchcode = ?,
			branchname = ?,company_address = ?,
			company_email = ?,company_fax = ?,
			company_logo = ?,company_name = ?,
			company_phone = ?,company_reg = ?,
			company_url = ?,currency = ?,
			legal_terms = ?,pdf_orientation = ?,
			pdf_size = ?,VAT = ?,
			created_at = ?,updated_at = ?,
			updatedby = ?
			WHERE id = ?";
			//print_r($array_organisation);
        $pdo = Database::getBdd();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		$req = $pdo->prepare($sql);
		$req->execute(array($array_organisation[14],$array_organisation[15],
							$array_organisation[16],$array_organisation[17],
							$array_organisation[18],$array_organisation[2] ,
							$array_organisation[5] ,$array_organisation[4] ,
							$array_organisation[11],$array_organisation[1] ,
							$array_organisation[3] ,$array_organisation[19],
							$array_organisation[6] ,$array_organisation[7] ,
							$array_organisation[8] ,$array_organisation[10],
							$array_organisation[9] ,$array_organisation[12],
							$array_organisation[20],$array_organisation[21],
							$array_organisation[13],$array_organisation[0] ));
		$id = $array_organisation[0];
		//echo ''.$array_organisation[0];		
		return $id;
	}
	
    public function edit_smtp($array_organisation)
    {
		$created_at = Date('Y-m-d h:m:s');//7
		$updated_at = Date('Y-m-d h:m:s');//8
		$array_organisation[] = $created_at;	
		$array_organisation[] = $updated_at;		
		$sql = "UPDATE $this->tablename
		SET host   = ?,port = ?,
			secure = ?,emailUsername = ?,
			emailPassword = ?,dev = ?,
			prod = ?,captcha = ?,
			updatedby = ?,created_at = ?,
			updated_at = ?
			WHERE id = ?";

        $pdo = Database::getBdd();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		$req = $pdo->prepare($sql);
		$req->execute(array($array_organisation[1],$array_organisation[2],
							$array_organisation[3],$array_organisation[4],
							$array_organisation[5],$array_organisation[6],
							$array_organisation[7] ,$array_organisation[8],
							$array_organisation[9] ,$array_organisation[10],$array_organisation[11],							
							$array_organisation[0] ));
		$id = $array_organisation[0];
		//echo ''.$array_organisation[0];		
		return $id;
	}
	

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute(array($id));
    }
 }
}
?>