<?php
class Task extends Model
{
    public function create($title, $description)
    {
		$email = 'tumelomodise@gmail.com';
		
        $sql = "INSERT INTO customer (name, identification,email) VALUES (?, ?, ?)";

        $req = Database::getBdd()->prepare($sql);

       /* return $req->execute([
            'name' => $title,
            'identification' => $description
        ]);
		*/
		return $req->execute(array($title,$description,$email));
    }

    public function showTask($id)
    {
        $sql = "SELECT * FROM customer WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAllTasks()
    {
        $sql = "SELECT * FROM customer";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function edit($id, $title, $description)
    {
        $sql = "UPDATE customer SET name = :name, identification = :identification WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'id' => $id,
            'name' => $title,
            'identification' => $description
        ]);
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM tasks WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
?>