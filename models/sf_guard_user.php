<?php
if (!class_exists('sf_guard_user')) 
{
class sf_guard_user extends Model
{
	var $tablename = 'sf_guard_user';
    public function create($reference,
						   $description,
						   $price)
	{	
		$created_at = Date('Y-m-d h:m:s');
		$updated_at = Date('Y-m-d h:m:s');
        $sql = "INSERT INTO $this->tablename(reference,description,price,created_at,updated_at) VALUES(?,?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$created_at,$updated_at));
		$id = Database::getBdd()->lastInsertId();
		 //   print_r($req->errorInfo());

		return $id;
    }

	public function logindata($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE username = '".$id."'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		//print($sql);
				 // print_r($req->errorInfo());
        return $req->fetch();
    }
	
    public function show($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				//    print_r($req->errorInfo());

        return $req->fetch();
    }

    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		
        return $req->fetchAll();
    }
	
	public function search_product($term)
    {
		$term = "'%".$term."%'";
		
        $sql = "SELECT * FROM $this->tablename WHERE reference LIKE $term";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				    //print_r($req->errorInfo());

        return $req->fetchAll();
    }
	

    public function edit($id,$reference,$description,$price)
    {
		$updated_at = Date('Y-m-d h:m:s');
		$sql = "UPDATE $this->tablename
				SET
				reference = ?,
				description = ?,
				price = ?,
				updated_at = ?
				WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$updated_at,$id));
		
		  //  print_r($req->errorInfo());

        		return $id;
 
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute(array($id));
    }
 }
}
?>