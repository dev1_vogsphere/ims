<?php
if (!class_exists('product')) 
{
class product extends Model
{
	var $tablename = 'product';
    public function create($reference,
						   $description,
						   $price,$companyname)
	{	
		$created_at = Date('Y-m-d h:m:s');
		$updated_at = Date('Y-m-d h:m:s');
        $sql = "INSERT INTO $this->tablename(reference,description,price,created_at,updated_at,organisation) VALUES(?,?,?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$created_at,$updated_at,$companyname));
		$id = Database::getBdd()->lastInsertId();
		 //   print_r($req->errorInfo());

		return $id;
    }
	
	public function createSponsor($reference,
						   $description,
						   $price,$companyname,
						   $customer_id,$customer_name,
						   $email,$identification)
	{	
		$created_at = Date('Y-m-d h:m:s');
		$updated_at = Date('Y-m-d h:m:s');
        $sql = "INSERT INTO $this->tablename(reference,description,price,created_at,updated_at,organisation,customer_id,customer_name,email,identification) 
		VALUES(?,?,?,?,?,?,?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$created_at,$updated_at,$companyname,
		$customer_id,$customer_name,$email,$identification));
		$id = Database::getBdd()->lastInsertId();
		 //   print_r($req->errorInfo());

		return $id;
    }
	
	
    public function show($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				//    print_r($req->errorInfo());

        return $req->fetch();
    }
	public function getproductby_name($name,$organisation)
    {
        $sql = "SELECT * FROM $this->tablename  WHERE reference = ? AND organisation = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($name,$organisation));
		return $req->fetchAll(PDO::FETCH_ASSOC);
    }
	
    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		
        return $req->fetchAll();
    }
	
	public function showSponsoredAll($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_id =".$id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
    public function dynamic_showAll($organisation)
    {
        $sql = "SELECT * FROM $this->tablename WHERE organisation = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($organisation));		
        return $req->fetchAll();
    }
	
	public function search_product($term,$organisation)
    {
		$term = "'%".$term."%'";
		$organisation = "'".$organisation."'";
		
        $sql = "SELECT * FROM $this->tablename WHERE reference LIKE $term AND organisation = $organisation";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				    //print_r($req->errorInfo());

        return $req->fetchAll();
    }
	

    public function edit($id,$reference,$description,$price)
    {
		$updated_at = Date('Y-m-d h:m:s');
		$sql = "UPDATE $this->tablename
				SET
				reference = ?,
				description = ?,
				price = ?,
				updated_at = ?
				WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$updated_at,$id));
		
		  //  print_r($req->errorInfo());

        		return $id;
 
    }
	 public function editSponsor($id,$reference,$description,$price,
	 						     $customer_id,$customer_name,
							     $email,$identification)
    {
		$updated_at = Date('Y-m-d h:m:s');
		$sql = "UPDATE $this->tablename
				SET
				reference = ?,
				description = ?,
				price = ?,
				updated_at = ?,
				customer_id = ?,
				customer_name  = ?,
				email  = ?,
				identification  = ?
				
				WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($reference,$description,$price,$updated_at,
							$customer_id,$customer_name,
							$email,$identification,$id));
		
		  //  print_r($req->errorInfo());

        		return $id;
 
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute(array($id));
    }
 }
}
?>