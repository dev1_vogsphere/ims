<?php
if (!class_exists('series')) 
{
class series extends Model
{
	var $tablename = 'series';
    public function create($name,$value,$first_number,$enabled)
	{	
        $sql = "INSERT INTO $this->tablename(name,value,first_number,enabled) VALUES(?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($name,$value,$first_number,$enabled));
		$id = Database::getBdd()->lastInsertId();
		 //  print_r($req->errorInfo());

		return $id;
    }

    public function show($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				//    print_r($req->errorInfo());

        return $req->fetch();
    }

    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		
        return $req->fetchAll();
    }
	
    public function edit($id,$name,$value,$first_number,$enabled)
    {
		$updated_at = Date('Y-m-d h:m:s');
		$sql = "UPDATE $this->tablename
				SET
				name = ?,
				value = ?,
				first_number = ?,
				enabled = ?
				WHERE id = ?";

        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($name,$value,$first_number,$enabled,$id));
		
		  //  print_r($req->errorInfo());

        		return $id;
 
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute(array($id));
    }
 }
}
?>