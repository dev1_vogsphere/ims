<?php
if (!class_exists('quotation')) 
{
// -- Same as Invoice Model, synch the changes between Invoice and Quotation.
class quotation extends Model
{
	var $tablename = 'common';
	var $tablePayment = 'payment';
	var $tableInvoiceItems = 'item';
	var $type = 'Estimate';
	var $errorLog = '';
	
/** 36 fields **/			
public function create(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date)		
{	

$created_at = Date('Y-m-d h:m:s');
$updated_at = Date('Y-m-d h:m:s');
$buildQuestions = '?';
/*Build ? fields for values.*/

$ij = 1;
for($i = 1;$i <37;$i++)
{
	$buildQuestions = $buildQuestions.',?';
	$ij = $ij + 1;
}

/** 37 fields **/		
$sql = "INSERT INTO $this->tablename
(series_id,
customer_id,
customer_name,
customer_identification,
customer_email,
invoicing_address,
shipping_address,
customer_phone,
contact_person,
terms,
notes,
base_amount,
discount_amount,
net_amount,
gross_amount,
paid_amount,
tax_amount,				   
status,
type,
draft,
closed,
sent_by_email,
number,
recurring_invoice_id,
issue_date,
due_date,
days_to_due,
enabled,
max_occurrences,
must_occurrences,
period,
period_type,
starting_date,
finishing_date,
last_execution_date,	
created_at,
updated_at)
VALUES
($buildQuestions)";
$req = Database::getBdd()->prepare($sql);
$req->execute(array(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
$created_at,
$updated_at));

//print_r($req->errorInfo());
$id = Database::getBdd()->lastInsertId();		 
return $id;	
}

// -- Edit invoice.
/** 36 fields **/			
public function edit($id,
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date)		
{	

//$created_at = Date('Y-m-d h:m:s');
$updated_at = Date('Y-m-d h:m:s');

/** 37 fields **/		
$sql = "UPDATE $this->tablename
SET 
series_id= ?,
customer_id= ?,
customer_name= ?,
customer_identification = ?,
customer_email = ?,
invoicing_address = ?,
shipping_address = ?,
customer_phone = ?,
contact_person = ?,
terms = ?,
notes = ?,
base_amount = ?,
discount_amount = ?,
net_amount = ?,
gross_amount = ?,
paid_amount = ?,
tax_amount = ?,				   
status = ?,
type = ?,
draft = ?,
closed = ?,
sent_by_email = ?,
number = ?,
recurring_invoice_id = ?,
issue_date = ?,
due_date = ?,
days_to_due = ?,
enabled = ?,
max_occurrences = ?,
must_occurrences = ?,
period = ?,
period_type = ?,
starting_date = ?,
finishing_date = ?,
last_execution_date = ?,	
updated_at = ?
WHERE id = ?";
$req = Database::getBdd()->prepare($sql);
$req->execute(array(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
//$created_at,
$updated_at,
$id));
		//print_r($req->errorInfo());

// -- Error return
foreach( $req->errorInfo() as $error )
{
	if($error[0][0] != 0)
	{
		//print($error[0][0]);
		$this->errorLog = '1';
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
	
}

// -- Edit Invoices Items
public function editItems($id,$quantity,$discount,$common_id,$product_id,$description,$unitary_cost)
{
// -- No item_id mean new entry.
if(empty($id))
{
	return $this->createItems($quantity,$discount,$common_id,$product_id,$description,$unitary_cost);
}
// -- There is item_id updated entry.
else
{
	if(empty($product_id))
	{
		$sql = "UPDATE $this->tableInvoiceItems
		SET quantity = ?,discount = ?,common_id = ?,description = ?,unitary_cost = ?
		WHERE id = ?";

		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($quantity,$discount,$common_id,$description,$unitary_cost,$id));
	}
	else
	{
	  	$sql = "UPDATE $this->tableInvoiceItems
		SET quantity = ?,discount = ?,common_id = ?,product_id = ?,description = ?,unitary_cost = ?
		WHERE id = ?";

		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost,$id));
	}

// -- Error return
foreach( $req->errorInfo() as $error => $x_value )
{
	$x_value = strval($x_value);
	if(!empty($x_value))
	{
		if($x_value != '00000')
		{
		//print("Modise : ".$x_value);
		$this->errorLog = '1';
		}
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
 }

}

// -- Create Invoices Items
public function createItems($quantity,$discount,$common_id,$product_id,$description,$unitary_cost)
{

if(empty($product_id))
{
	$product_id = 0;
$sql = "INSERT INTO $this->tableInvoiceItems
(quantity,discount,common_id,product_id,description,unitary_cost)
VALUES (?,?,?,?,?,?)";

$req = Database::getBdd()->prepare($sql);
$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost));
}
else
{
	$sql = "INSERT INTO $this->tableInvoiceItems
(quantity,discount,common_id,product_id,description,unitary_cost)
VALUES (?,?,?,?,?,?)";

$req = Database::getBdd()->prepare($sql);
$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost));

}
$id = Database::getBdd()->lastInsertId();		 

// -- Error return
foreach( $req->errorInfo() as $error => $x_value )
{
	$x_value = strval($x_value);
	if(!empty($x_value))
	{
		if($x_value != '00000')
		{
		//print($product_id.'Prod :'.$x_value);
		$this->errorLog = '1';
		
		
		}
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
}

// -- Quotation
    public function showInvoice($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
		
		return $req->fetch(PDO::FETCH_ASSOC);
    }

	// -- Selected - Quotation
    public function showSelectedQuotation($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
		return $req->fetchAll();
    }
	
// -- Quotation Items
    public function show($id)
    {
        $sql = "SELECT * FROM $this->tableInvoiceItems as A left join item_tax as B on A.id = B.item_id  WHERE common_id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
        return $req->fetchAll();
    }
	
// -- Quotation Paymens
	public function payments($id)
    {
        $sql = "SELECT * FROM $this->tablePayment WHERE invoice_id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
        return $req->fetchAll();
    }

// -- Quotations
    public function showAll()
    {
		$sql = "SELECT * FROM $this->tablename WHERE type = ? ORDER BY issue_date DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($this->type));

        return $req->fetchAll();
    }
	
	/*public function getDueAmount()
    {
        if ($this->isDraft()) {
            return null;
        }
        return $this->getGrossAmount() - $this->getPaidAmount();
    }*/
	
	public function search_estimate($term)
    {
        $sql = "SELECT * FROM $this->tablename LIKE %$term%";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
	/*by customer */
	public function search_byname($term)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_name = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($term));
        return $req->fetchAll();
    }

	public function search_customer_quotation($term)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_identification = ?  ORDER BY issue_date DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($term));
        return $req->fetchAll();
    }
    // -- Dynamic Query.
	public function search_dynamic_quotation($queryArr)
    {
		$queryStr = implode(" AND ", $queryArr);
        $sql = "SELECT * FROM $this->tablename WHERE {$queryStr} ORDER BY issue_date DESC";
		//print($sql);
        $req = Database::getBdd()->query($sql);
		//print($sql);

        return $req;
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
		//print_r($req->errorInfo());
        return $req->execute([$id]);
    }
	
	public function getStatusString()
    {
        switch ($this->status) {
            case invoice::DRAFT;
                $status = 'draft';
             break;
            case invoice::CLOSED;
                $status = 'closed';
            break;
            case invoice::OPENED;
                $status = 'opened';
            break;
            case invoice::OVERDUE:
                $status = 'overdue';
                break;
            default:
                $status = 'unknown';
                break;
        }
        return $status;
    }
	
}}
?>