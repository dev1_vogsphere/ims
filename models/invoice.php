<?php
if (!class_exists('invoice')) 
{

class invoice extends Model
{
	var $tablename = 'common';
	var $tablePayment = 'payment';
	var $tableInvoiceItems = 'item';
	var $type = 'Invoice';
	var $Recurringtype = 'RecurringInvoice';
	var $Quotationtype = 'Quotation';
	var $errorLog = '';
	
/** 36 fields **/			
public function create(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
$createdby)		
{	
$created_at = Date('Y-m-d h:m:s');
$updated_at = Date('Y-m-d h:m:s');
$buildQuestions = '?';
/*Build ? fields for values.*/

$ij = 1;
for($i = 1;$i <38;$i++)
{
	$buildQuestions = $buildQuestions.',?';
	$ij = $ij + 1;
}

/** 37 fields **/		
$sql = "INSERT INTO $this->tablename
(series_id,
customer_id,
customer_name,
customer_identification,
customer_email,
invoicing_address,
shipping_address,
customer_phone,
contact_person,
terms,
notes,
base_amount,
discount_amount,
net_amount,
gross_amount,
paid_amount,
tax_amount,				   
status,
type,
draft,
closed,
sent_by_email,
number,
recurring_invoice_id,
issue_date,
due_date,
days_to_due,
enabled,
max_occurrences,
must_occurrences,
period,
period_type,
starting_date,
finishing_date,
last_execution_date,	
created_at,
updated_at,
createdby)
VALUES
($buildQuestions)";
$req = Database::getBdd()->prepare($sql);
$req->execute(array(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
$created_at,
$updated_at,
$createdby));

//print_r($req->errorInfo());
$id = Database::getBdd()->lastInsertId();		 
return $id;	
}

// -- Edit invoice.
/** 36 fields **/			
public function edit($id,
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
$updatedby)		
{	

//$created_at = Date('Y-m-d h:m:s');
$updated_at = Date('Y-m-d h:m:s');

/** 37 fields **/		
$sql = "UPDATE $this->tablename
SET 
series_id= ?,
customer_id= ?,
customer_name= ?,
customer_identification = ?,
customer_email = ?,
invoicing_address = ?,
shipping_address = ?,
customer_phone = ?,
contact_person = ?,
terms = ?,
notes = ?,
base_amount = ?,
discount_amount = ?,
net_amount = ?,
gross_amount = ?,
paid_amount = ?,
tax_amount = ?,				   
status = ?,
type = ?,
draft = ?,
closed = ?,
sent_by_email = ?,
number = ?,
recurring_invoice_id = ?,
issue_date = ?,
due_date = ?,
days_to_due = ?,
enabled = ?,
max_occurrences = ?,
must_occurrences = ?,
period = ?,
period_type = ?,
starting_date = ?,
finishing_date = ?,
last_execution_date = ?,	
updated_at = ?,
updatedby = ?
WHERE id = ?";
$req = Database::getBdd()->prepare($sql);
$req->execute(array(
$series_id,
$customer_id,
$customer_name,
$customer_identification,
$customer_email,
$invoicing_address,
$shipping_address,
$customer_phone,
$contact_person,
$terms,
$notes,
$base_amount,
$discount_amount,
$net_amount,
$gross_amount,
$paid_amount,
$tax_amount,
$status,
$type,
$draft,
$closed,
$sent_by_email,
$number,
$recurring_invoice_id,
$issue_date,
$due_date,
$days_to_due,
$enabled,
$max_occurrences,
$must_occurrences,
$period,
$period_type,
$starting_date,
$finishing_date,
$last_execution_date,
//$created_at,
$updated_at,
$updatedby,
$id));
		//print_r($req->errorInfo());

// -- Error return
foreach( $req->errorInfo() as $error )
{
	if($error[0][0] != 0)
	{
		//print($error[0][0]);
		$this->errorLog = '1';
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
	
}

// -- Edit Invoices Items
public function editItems($id,$quantity,$discount,$common_id,$product_id,$description,$unitary_cost)
{
// -- No item_id mean new entry.
if(empty($id))
{
	return $this->createItems($quantity,$discount,$common_id,$product_id,$description,$unitary_cost);
}
// -- There is item_id updated entry.
else
{
	if(empty($product_id))
	{
		$sql = "UPDATE $this->tableInvoiceItems
		SET quantity = ?,discount = ?,common_id = ?,description = ?,unitary_cost = ?
		WHERE id = ?";

		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($quantity,$discount,$common_id,$description,$unitary_cost,$id));
	}
	else
	{
	  	$sql = "UPDATE $this->tableInvoiceItems
		SET quantity = ?,discount = ?,common_id = ?,product_id = ?,description = ?,unitary_cost = ?
		WHERE id = ?";

		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost,$id));
	}

// -- Error return
foreach( $req->errorInfo() as $error => $x_value )
{
	$x_value = strval($x_value);
	if(!empty($x_value))
	{
		if($x_value != '00000')
		{
		//print("Modise : ".$x_value);
		$this->errorLog = '1';
		}
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
 }

}

// -- Create Invoices Items
public function createItems($quantity,$discount,$common_id,$product_id,$description,$unitary_cost)
{

if(empty($product_id))
{
	$product_id = 0;
$sql = "INSERT INTO $this->tableInvoiceItems
(quantity,discount,common_id,product_id,description,unitary_cost)
VALUES (?,?,?,?,?,?)";

$req = Database::getBdd()->prepare($sql);
$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost));
}
else
{
	$sql = "INSERT INTO $this->tableInvoiceItems
(quantity,discount,common_id,product_id,description,unitary_cost)
VALUES (?,?,?,?,?,?)";

$req = Database::getBdd()->prepare($sql);
$req->execute(array($quantity,$discount,$common_id,$product_id,$description,$unitary_cost));

}
$id = Database::getBdd()->lastInsertId();		 

// -- Error return
foreach( $req->errorInfo() as $error => $x_value )
{
	$x_value = strval($x_value);
	if(!empty($x_value))
	{
		if($x_value != '00000')
		{
		//print($product_id.'Prod :'.$x_value);
		$this->errorLog = '1';
		
		
		}
	}
}
//-- if errorLog is not empty its a error
	if(!empty($this->errorLog))
	{
		//print_r($req->errorInfo());
		return $req->errorInfo();	
	}
//-- if errorLog is empty its a success.
	else
	{	
	   return array('id' => $id);
	}
}

// -- Invoice
    public function showInvoice($id)
    {
        $sql = "SELECT * FROM $this->tablename  WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));		
		return $req->fetch(PDO::FETCH_ASSOC);
    }

   public function GET_IMS_INVOICE_PAYMENTS($dataInput)
   {
	$paymentid  	 = '';
	$iddocument 	 = '';
	$path	    	 = '';
	$tot_paid   	 = 0.00;
	$tot_outstanding = 0.00;
	$repaymentAmount = $dataInput['amount'];	
	$dataInvoice 	 = null;
	$dataCommon      = null;
	$customerid      = $dataInput['customerid'];
	$ApplicationId   = $dataInput['ApplicationId'];
// -- SQL Invoice
$sql = "SELECT * FROM common as c inner join payment as p on c.id = p.invoice_id 
where c.customer_identification = ? AND p.notes = ?";

		$q = Database::getBdd()->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($dataCommon as $rowCommon)
		{
			$paymentid  = $rowCommon['id'];
			$iddocument = $rowCommon['FILE'];
			$path = str_replace(" ", '%20', $iddocument);
			$tot_paid = $tot_paid + $rowCommon['amount'];
		}	// End all Customer Invoices
		if(!empty($paymentid))
		{
		    $tot_outstanding = $repaymentAmount - $tot_paid;
		}

		$dataInvoice['paymentid'] 		= $paymentid ;
		$dataInvoice['iddocument'] 		= $iddocument;
		$dataInvoice['path'] 			= $path;
		$dataInvoice['tot_paid'] 		= $tot_paid;
		$dataInvoice['tot_outstanding'] = $tot_outstanding;
$dataInvoices[] = $dataInvoice;
	return $dataInvoices;
}

	// -- Selected - Invoices
    public function showSelectedInvoice($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
		return $req->fetchAll();
    }
	
	public function showSelectedInvoice_daterange($id,$from,$to)
    {//" . $id." 
        $sql = "SELECT * FROM $this->tablename WHERE id = ? AND type = ? AND issue_date between ? and ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id,'Invoice',$from,$to));
		return $req->fetchAll();
    }
	
// -- Invoices Items
    public function show($id)
    {
        $sql = "SELECT * FROM $this->tableInvoiceItems as A left join item_tax as B on A.id = B.item_id  WHERE common_id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
        return $req->fetchAll();
    }
	
// -- Invoices Paymens
	public function payments($id)
    {
        $sql = "SELECT * FROM $this->tablePayment WHERE invoice_id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
        return $req->fetchAll();
    }

// -- All Payment for user's organisation.
	public function allpayments($organisation)
    {
		$sql = "SELECT * FROM $this->tablePayment WHERE invoice_id in (select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = ?)";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($organisation)); // your data go in here !!		
        return $req->fetchAll();
    }
// -- Invoices
    public function showAll()
    {
		$sql = "SELECT * FROM $this->tablename WHERE type = ? ORDER BY issue_date DESC, id DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($this->type));

// -- Update paid Amounts
// -- Get the Invoice Payments details.
		$invoices = $req->fetchAll();
		$data = array();
		
        foreach ($invoices as $rowInvoice)
        {
			$payments_data = $this->payments($rowInvoice['id']);
			$totalPayment = 0;
			$balance = 0.00;
		
			foreach ($payments_data as $row) 
			{
				$amount = 0;
				if(is_numeric($row['amount']))
				{
					$amount = $row['amount'];
				}
				$totalPayment = $totalPayment+$amount;
			}
			
			//echo $rowInvoice['id'].";amount = ".$totalPayment." \n";
			$rowInvoice['paid_amount'] = $totalPayment;
			
			// -- Outstanding amount 
			$rowInvoice['outstanding_amount'] = number_format(($rowInvoice['base_amount']+$rowInvoice['tax_amount']) - $rowInvoice['paid_amount'],2);
			array_push($data,$rowInvoice);
		}

        return $data;//$req->fetchAll();
    }
	
// -- ShowAll OVERDUE - Invoices
    public function showAllOverDue()
    {
		$status = '3';
		$sql = "SELECT * FROM $this->tablename WHERE type = ? AND status = ? ORDER BY issue_date DESC, id DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($this->type,$status));

        return $req->fetchAll();
    }	
	
	// -- Recurring Invoices. 
    public function showAllRecurring()
    {
		$sql = "SELECT * FROM $this->tablename WHERE type = ? ORDER BY issue_date DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($this->Recurringtype));

        return $req->fetchAll();
    }
	

// -- BOC Recurring Invoice Development --- //
	// -- check if $recurringInvoiceId is within the max_occurrences. 
	public function isWithinMaxOccurance($recurringInvoiceId, $max_occurrences)
	{
		// -- SQL read Count already created.
		 $RecurringInvoiceIdCount = 0;
		 $sql = "SELECT * FROM $this->tablename WHERE recurring_invoice_id = ?";
		 $req = Database::getBdd()->prepare($sql);
         $req->execute(array($recurringInvoiceId));
		 $RecurringInvoiceIdCount = $req->rowCount();
		if($max_occurrences >= $RecurringInvoiceIdCount)
		{
			return true;
		}
		else
		{
		   return false;
		}
	}
/**
 * @param DateTime $date Date that is to be checked if it falls between 
 * $startDate and $endDate
 * @param DateTime $startDate Date should be after this date to return true
 * @param DateTime $endDate Date should be before this date to return true
 * return bool
 */
public function isDateBetweenDates(DateTime $date, DateTime $startDate, DateTime $endDate) 
{
	//echo '<br/>'.date('Y-m-d').'Today : '.date_format($date, 'Y-m-d').' start :'.date_format($startDate,'Y-m-d').', end date :'.date_format($endDate,'Y-m-d');
    return (($date >= $startDate) && ($date <= $endDate));
}

/* @param DateTime $today Date that is to be checked if it falls between */
public function isPastDueDate(DateTime $today, DateTime $dueDate) 
{
    return $today > $dueDate;
}

public function checkInvoiceExist($recurringInvoiceId,$invoicedate,$frequencyType)
{
	// -- SQL read recurringInvoiceId & invoicedate already created.
	$RecurringInvoiceIdCount = 0;
	$invoiceData = null;
	if ($frequencyType == 'day')
	{
		// full date check;recurringinvoiceid.
		 $sql = "SELECT * FROM $this->tablename WHERE recurring_invoice_id = ? AND issue_date = ?";
		 $req = Database::getBdd()->prepare($sql);
         $req->execute(array($recurringInvoiceId,$invoicedate));
		 $RecurringInvoiceIdCount = $req->rowCount();
		 $invoiceData = $req->fetch(PDO::FETCH_ASSOC);
	}
	else if($frequencyType == 'month')
	{
		// -- use sql like Y-m
		 $likeYM = (string)date('Y-m').'%';
		 $sql = "SELECT * FROM $this->tablename WHERE recurring_invoice_id = ? AND issue_date LIKE ?";
		 $req = Database::getBdd()->prepare($sql);
         $req->execute(array($recurringInvoiceId,$likeYM));
		 $RecurringInvoiceIdCount = $req->rowCount();		 
		 $invoiceData = $req->fetch(PDO::FETCH_ASSOC);
	}
	else if($frequencyType == 'year')
	{
		// -- use sql like Y
		 $likeYM = (string)date('Y').'%';
		 $sql = "SELECT * FROM $this->tablename WHERE recurring_invoice_id = ? AND issue_date LIKE ?";
		 $req = Database::getBdd()->prepare($sql);
         $req->execute(array($recurringInvoiceId,$likeYM));
		 $RecurringInvoiceIdCount = $req->rowCount();
		 $invoiceData = $req->fetch(PDO::FETCH_ASSOC);
	}
	
/* -- decision to create and invoice.	
	if($RecurringInvoiceIdCount > 0)
	{
		// -- Do not create a new invoice.
		return true;
	}
	else
	{
		// -- create a new invoice from recurringInvoiceId.
		return false;

	}*/
	
	return $invoiceData;
}
// -- EOC Recurring Invoice Development --- //	
	/*public function getDueAmount()
    {
        if ($this->isDraft()) {
            return null;
        }
        return $this->getGrossAmount() - $this->getPaidAmount();
    }*/
	
	// -- Dashboad - Total Invoice Amount	
	public function totalinvoiceamount()
    {
        $sql = "SELECT SUM(base_amount)  FROM $this->tablename WHERE type = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($this->type));
		$total = $req->fetch(PDO::FETCH_NUM);
		$payments = $total[0];
        return $payments;
    }
	
	// -- Dashboad - Total Invoice Amount	
	public function organisation_totalinvoiceamount($organisation)
    {
        //$sql = "SELECT SUM(base_amount)  FROM $this->tablename WHERE type = ?";
		$sql = "SELECT SUM(quantity * unitary_cost) FROM common join item on common.id = item.common_id where common.id in (select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = ? and i.type = ?)";  
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($organisation,$this->type));
		$total = $req->fetch(PDO::FETCH_NUM);
		$payments = $total[0];
        return $payments;
    }
	
	public function search_estimate($term)
    {
        $sql = "SELECT * FROM $this->tablename LIKE %$term%";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
	/*by customer */
	public function search_byname($term)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_name = ?  ORDER BY issue_date DESC, id DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($term));
        return $req->fetchAll();
    }

	public function search_customer_invoice($term)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_identification = ? AND type = ? ORDER BY issue_date DESC, id DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($term,'Invoice'));
        return $req->fetchAll();
    }
	public function search_customer_invoice_daterange($term,$from,$to)
    {
        $sql = "SELECT * FROM $this->tablename WHERE customer_identification = ? AND type = ? AND issue_date between ? and ? ORDER BY issue_date DESC, id DESC";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($term,'Invoice',$from,$to));
        return $req->fetchAll();
    }
    // -- Dynamic Query.
	public function search_dynamic_invoice($queryArr)
    {
		$queryStr = implode(" AND ", $queryArr);
        $sql = "SELECT * FROM $this->tablename WHERE {$queryStr} ORDER BY issue_date DESC, id DESC";
		//print($sql);
        $req = Database::getBdd()->query($sql);
		//print($sql);

        return $req;
    }
	
	// -- Dynamic Query.
	public function organisation_search_dynamic_invoice($queryArr)
    {
		$queryStr = implode(" AND ", $queryArr);
        $sql = "SELECT * FROM $this->tablename WHERE {$queryStr} and type ='Invoice' ORDER BY issue_date DESC";
		//print($sql);
        $req = Database::getBdd()->query($sql);
		//print($sql);

        return $req;
    }

    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
		//print_r($req->errorInfo());
        return $req->execute([$id]);
    }
	
	// -- UPDATE sent_by_email.
	public function updateSentByEmail($id)
    {
		$sent_by_email = 1;
		$sql = "UPDATE $this->tablename SET sent_by_email = ? WHERE id = ?";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($sent_by_email,$id));
    }
	
	// -- UPDATE Status of an Invoice and also force closed.
	public function updateStatusForceClose($id)
    {
		$closed = 1;
		$status = 1;
		$sql = "UPDATE $this->tablename SET status = ?,closed = ? WHERE id = ?";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($status,$closed,$id));
    }
	// -- COUNT Invoice for Dashboad.[Invoice]
	public function tot_statusCount($status,$type)
	{	
		$sql = "SELECT COUNT(*) FROM common WHERE status = ? AND type = ?";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($status,$type)); // your data go in here !!
		$num_rows = $req->fetchColumn();
		return $num_rows; 
	}	
	
	// -- COUNT Invoice for Dashboad.[Invoice]
	public function organisation_tot_statusCount($status,$type,$organisation)
	{	
		$sql = "SELECT COUNT(*) FROM common WHERE id in (select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = ? and i.status = ? AND i.type = ?)";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($organisation,$status,$type)); // your data go in here !!
		$num_rows = $req->fetchColumn();
		return $num_rows; 
	}
	
	// -- UPDATE Status of an Invoice.
	public function updateStatus($id,$status)
    {
		$sql = "UPDATE $this->tablename SET status = ? WHERE id = ?";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($status,$id));
    }
	// -- UPDATE Last Exectution date for recurring Invoice.
	public function updatelast_execution_date($id)
    {
		$today 	= date("Y-m-d");
		$status = 1;
		$sql = "UPDATE $this->tablename SET last_execution_date	 = ? WHERE id = ?";
		$req = Database::getBdd()->prepare($sql);
		$req->execute(array($today,$id));
    }
	
	public function getStatusString()
    {
        switch ($this->status) {
            case invoice::DRAFT;
                $status = 'draft';
             break;
            case invoice::CLOSED;
                $status = 'closed';
            break;
            case invoice::OPENED;
                $status = 'opened';
            break;
            case invoice::OVERDUE:
                $status = 'overdue';
                break;
            default:
                $status = 'unknown';
                break;
        }
        return $status;
    }
	
	function defaultSearch($companyname,$type)
	{
		$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = '".$type."'  ORDER BY issue_date DESC, id DESC)";
		$whereArr[] = "id in {$value}";
		$whereArr[] = "type = '{$type}'";
		
		return(array) $whereArr; 
	}
	
}}
?>