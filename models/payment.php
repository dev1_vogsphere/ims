<?php
if (!class_exists('payment')) 
{
class payment extends Model
{
	var $tablename = 'payment';
    public function create($invoice_id,
						   $created_at,
						   $amount,
						   $notes)
	{	
        $sql = "INSERT INTO $this->tablename(invoice_id,date,amount,notes) VALUES(?,?,?,?)";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($invoice_id,$created_at,$amount,$notes));
		$id = Database::getBdd()->lastInsertId();
		//print_r($req->errorInfo());
// -- start
		// -- Error return
		foreach( $req->errorInfo() as $error => $x_value )
		{
			$x_value = strval($x_value);
			if(!empty($x_value))
			{
				if($x_value != '00000')
				{
				print($product_id.'Prod :'.$x_value);
				$this->errorLog = '1';
				
				
				}
			}
		}
		//-- if errorLog is not empty its a error
			if(!empty($this->errorLog))
			{
				//print_r($req->errorInfo());
				return $req->errorInfo();	
			}
		//-- if errorLog is empty its a success.
			else
			{	
			   return array('id' => $id);
			}
// -- end
	}

    public function show($id)
    {
        $sql = "SELECT * FROM $this->tablename WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				//    print_r($req->errorInfo());

        return $req->fetch();
    }
// -- Dashboad - Total Payment	
	public function totalpayments()
    {
        $sql = "SELECT SUM(amount)  FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		$total = $req->fetch(PDO::FETCH_NUM);
		$payments = $total[0];
        return $payments;
    }

// -- Dashboad - Total Payment	
	public function organisation_totalpayments($organisation)
    {
		$type = 'Invoice';
		$sql = "SELECT SUM(amount) FROM payment where invoice_id in (select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = ? and i.type = ?)";  
        //$sql = "SELECT SUM(amount)  FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($organisation,$type));
		$total = $req->fetch(PDO::FETCH_NUM);
		$payments = $total[0];
        return $payments;
    }
	

    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		
        return $req->fetchAll();
    }
	
    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename  WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
		//$req->execute([$id]);
		//print_r($req->errorInfo());
        return $req->execute([$id]);
    }
 }
}
?>