<?php
if (!class_exists('customer')) 
{
class customer extends Model
{
	var $tablename = 'customer';
    public function create($name,$name_slug,$identification,$email,$contact_person,$invoicing_address,$shipping_address,
	$organisation,$customer_office_number,$customer_cell_number,$customer_vat,$companyregistration)
    {		
		// -- 	Customer Code(format first 3 letters of the company name plus three numbers(start at 000))
		$customercode = 000;

		// -- Get the latest code & increment.
		$Datacustomers = $this->search_customer_by_organisation($organisation);

        foreach ($Datacustomers as $customer)
        {
			if(isset($customer['customercode']))
			{
				$customercode = (int)$customer['customercode'];
			}
		}
		$customercode = $customercode + 1;
		$customercode = str_pad($customercode, 3, '0', STR_PAD_LEFT);
		
		// -- Customer Identification
		$identification = substr($name, 0,3);
		$identification = $identification.$customercode;
		
		$sql = "INSERT INTO $this->tablename
		(name,name_slug,identification,email,contact_person,invoicing_address,shipping_address,organisation,
		officenumber,cellnumber,vatnumber,customercode,companyregistration)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		$req = Database::getBdd()->prepare($sql);
	    $req->execute(array($name,$name_slug,$identification,$email,$contact_person,$invoicing_address,$shipping_address,$organisation,
		$customer_office_number,$customer_cell_number,$customer_vat,$customercode,$companyregistration));
		
		$id = Database::getBdd()->lastInsertId();
		// print_r($req->errorInfo());
		$data_array = array();
		$data_array['id'] 			  = $id;
		$data_array['identification'] = $identification;
		
		return $data_array;
    }
    public function show($id)
    {
        $sql = "SELECT * FROM customer WHERE id ='".$id."'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
		return $req->fetch();
    }
	
	public function showbyidentification($id)
    {
        $sql = "SELECT * FROM customer WHERE identification ='" .$id."'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($id));
		return $req->fetchAll(PDO::FETCH_ASSOC);
    }
	
	public function getcustomerby_name($name,$organisation)
    {
        $sql = "SELECT * FROM customer WHERE name = ? AND organisation = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute(array($name,$organisation));
		return $req->fetchAll(PDO::FETCH_ASSOC);
    }
	
	public function autocomplete($iterm)
    {
        $sql = "SELECT * FROM customer WHERE name LIKE '%". $iterm."%'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function showAll()
    {
        $sql = "SELECT * FROM $this->tablename";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				    //print_r($req->errorInfo());

        return $req->fetchAll();
    }

    public function edit($id,$name,$name_slug,$identification,$contact_person,$email,$invoicing_address,$shipping_address,
	$customer_office_number,$customer_cell_number,$customer_vat,$companyregistration,$status)
    {
		$sql = "UPDATE $this->tablename
SET
name = ?,
name_slug = ?,
identification = ?,
email = ?,
contact_person = ?,
invoicing_address = ?,
shipping_address = ?,
officenumber = ?,
cellnumber = ?,
vatnumber = ?,
companyregistration = ?,
status = ?
WHERE id = ?";

		
        $req = Database::getBdd()->prepare($sql);
	    $req->execute(array($name,$name_slug,$identification,$email,$contact_person,$invoicing_address,$shipping_address,
			$customer_office_number,$customer_cell_number,$customer_vat,$companyregistration,$status,$id));
		//$id = Database::getBdd()->lastInsertId();
		 
				    //print_r($req->errorInfo());
 
		return $id;

    }

	public function search_customer($term,$org)
    {
		$term = "'%".$term."%'";
		
        $sql = "SELECT * FROM $this->tablename WHERE name LIKE $term and organisation = $org";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
				    //print_r($req->errorInfo());

        return $req->fetchAll();
    }
	
	public function search_customer_by_status($term,$status)
    {		
		$status = "'".$status."'";
		$term = "'".$term."'";
		
		if($status == "''")
		{
            $sql = "SELECT * FROM $this->tablename WHERE organisation = $term and status IS NULL OR status = $status";       			
		}
		else if($status == "'All'")
		{
			$sql = "SELECT * FROM $this->tablename WHERE organisation = $term";	
		}
		else
		{
            $sql = "SELECT * FROM $this->tablename WHERE organisation = $term and status = $status";       						
		}
	    //echo $sql;
		$req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
	public function search_customer_by_organisation($term)
    {
		$term = "'".$term."'";
		
        $sql = "SELECT * FROM $this->tablename WHERE organisation = $term";
		//echo $sql;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
						    //print_r($req->errorInfo());

        return $req->fetchAll();
    }
	
    public function remove($id)
    {
        $sql = "DELETE FROM $this->tablename WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
		$req->execute(array($id));
						   // print_r($req->errorInfo());

        return $req->execute(array($id));
    }
}
}
?>