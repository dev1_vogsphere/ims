<?php    
 error_reporting( E_ALL ); 
 ini_set('display_errors', 1);

require('auto_db.php');

// Get search term
$searchTerm = '';
$searchTerm2 = '';
if(isset($_GET['term']))
{$searchTerm = $_GET['term'];}

if(isset($_GET['companyname']))
{$searchTerm2 = $_GET['companyname'];}

$query = autocomplete($searchTerm,$searchTerm2);

// Generate customers data array
$customerData = array();
$json = '';
foreach($query as $customer)
{
	$customer['id'] = $customer['id'];
	$customer['value'] = $customer['name'];
	//print($customer['id']);
    array_push($customerData, $customer);
}
// Return results as json encoded array
echo safe_json_encode($customerData,0,512);
 
function autocomplete($iterm,$searchTerm2)
    {
		if(empty($searchTerm2))
        {$sql = "SELECT * FROM customer WHERE name LIKE '%". $iterm."%'";}
		else
        {$sql = "SELECT * FROM customer WHERE name LIKE '%". $iterm."%' AND organisation = '".$searchTerm2."'";}
			
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }
	
	
	
	function safe_json_encode($value, $options = 0, $depth = 512)
	{
    $encoded = json_encode($value, $options, $depth);
    switch (json_last_error()) 
	{
        case JSON_ERROR_NONE:
            return $encoded;
        case JSON_ERROR_DEPTH:
            return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_STATE_MISMATCH:
            return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_CTRL_CHAR:
            return 'Unexpected control character found';
        case JSON_ERROR_SYNTAX:
            return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_UTF8:
            $clean = utf8ize($value);
            return safe_json_encode($clean, $options, $depth);
        default:
            return 'Unknown error'; // or trigger_error() or throw new Exception()

    }
}

function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}
?>