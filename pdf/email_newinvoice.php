<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
//-------------------------------------------------------------------
//           				BOC - GLOBAL DATA						-
//-------------------------------------------------------------------
$password = '';
$website  = '';
$companywebsite  = '';

//-------------------------------------------------------------------
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$idnumber 		= '';
$AccountNumber 	= '';
$InvoiceNumber 	= '';
$DueDate 		= '';
$AmountDue 		= '';
$Currency 		= '';
$space 			= '';
$InvoiceMonth 	= '';
$logo 			= '';

// -- 09.11.2018
$bankname   	= '';
$account	    = '';
$accounttype    = '';
$branchname     = '';
$branchcode     = '';
$swiftcode      = '';
$companyEmail = '';
$company_phone	='';
$invoicetype 	= '';
$invoicetypedue = '';

// ====================== Global Settings =========================== //
$GlobalCompany  = '';
// ======================= EOC Global Settings ====================== //
if(isset($_SESSION))
{
// ====================== Invoice Settings =========================== //
if(isset($_SESSION['invFirstName'])){$FirstName 		= $_SESSION['invFirstName'];}
if(isset($_SESSION['invInvoiceNumber'])){$InvoiceNumber  = $_SESSION['invInvoiceNumber'];}
if(isset($_SESSION['invDueDate'])){$DueDate 		= $_SESSION['invDueDate'];}
if(isset($_SESSION['invAmountDue'])){$AmountDue 		= $_SESSION['invAmountDue'];}
$AmountDue = number_format($AmountDue,2);
// ====================== Global Settings =========================== //
if(isset($_SESSION['company_name'])){$GlobalCompany  = $_SESSION['company_name'];}
if(isset($_SESSION['company_url'])){$website 		= $_SESSION['company_url'];}
if(isset($_SESSION['company_logo'])){$logo 			= $_SESSION['company_logo'];}
if(isset($_SESSION['company_email']	    )){$companyEmail	= $_SESSION['company_email'];    }
if(!empty($_SESSION['company_phone']))  {$company_phone	 = $_SESSION['company_phone'];}

if(isset($_SESSION['bankname'])){$bankname   	= $_SESSION['bankname'];}
if(isset($_SESSION['account'])){$account	    = $_SESSION['account'];}
if(isset($_SESSION['accounttype'])){$accounttype    = $_SESSION['accounttype'];}
if(isset($_SESSION['branchname'])){$branchname     = $_SESSION['branchname'];}
if(isset($_SESSION['branchcode'])){$branchcode     = $_SESSION['branchcode'];}
if(isset($_SESSION['swiftcode'])){$swiftcode      = $_SESSION['swiftcode'];}

if(isset($_SESSION['invoicetype'])){$invoicetype      = $_SESSION['invoicetype'];}

if($invoicetype != 'Invoice')
{
	$invoicetypedue = 'expires';
}
else
{
	$invoicetypedue = 'is due';
}
/* --- BOC Production or Development */
$companywebsite = $website;

// -- Application website.
//$website = "http://www.vogsphere.co.za/ims/";
$href = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$href = str_replace("pdf_invoice.php","",$href);
$href = str_replace("pdf_quotation.php","",$href);
$href = str_replace("/pdf/","",$href);

// -- from webservices
$href = str_replace("webservices","",$href);
$href = str_replace("generateinvoices.php","",$href);

$website = $href ;

// -- Email Server Hosting -- //
if(isset($_SESSION['prod'])){$Globalprod = $_SESSION['prod'];}
// -- Email Server Hosting -- //

/*	Production - Live System */
if($Globalprod == 'p')
{

}
/*	Development - Testing System */
else
{
	$href = str_replace("localhost","www.vogsphere.co.za",$href);
	$website = $href;
}
/* --- ECO Production or Development */
// ======================= EOC Global Settings ====================== //
}

//-------------------------------------------------------------------
//           				EOC - GLOBAL DATA						-		-
//-------------------------------------------------------------------
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Favicon -->
	<link href="<?php if(!empty($website)){echo $website."/assets/img/Icon_vogs.ico";}else{}?>" rel="shortcut icon">

	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }

    </style>

    <!-- Progressive Enhancements -->
    <style>

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

        }

    </style>

</head>
<body bgcolor="#f0f0f0" width="100%" style="margin: 0;">
    <center style="width: 100%; background: ##f0f0f0;">
        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
        </table>
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
				<td bgcolor="#f0f0f0">
					<?php echo "<img src='".$website.'/images/'.$logo."' width='600' height='' alt='".$website.'/images/'.$logo."' border='0' align='center' style='width: 100%; max-width: 600px;'/>";?>
				</td>
            </tr>
            <!-- Hero Image, Flush : END -->

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: left; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px;">
<?php

if ($invoicetype=='Invoice')
{
	
		echo "<p>Dear Valued Customers</p>
			<p>Please find the attached invoice $invoicetype ($InvoiceNumber), due on $DueDate.</p> 
			<p>Kindly use the invoice number as the reference when making your payment.</p>
			<p>We would appreciate it if you could forward proof of payment to $companyEmail.</p>
			<p>Thank you for your business.</p>
			<p>Kind Regards/p>			
			<p>$GlobalCompany</p>
			<p>Office: $company_phone</p>
			<p>Email: $companyEmail</p>
			<p>Web: $companywebsite</p>";
	
}else
{
	echo "<p>Dear Valued Customers</p>
			<p>Please find the attached invoice $invoicetype ($InvoiceNumber), due on $DueDate.</p> 
			<p>Kindly use the invoice number as the reference when making your payment.</p>
			<p>We would appreciate it if you could forward proof of payment to $companyEmail.</p>
			<p>Thank you for your business.</p>
			<p>Kind Regards/p>			
			<p>$GlobalCompany</p>
			<p>Office: $company_phone</p>
			<p>Email: $companyEmail</p>
			<p>Web: $companywebsite</p>";
}

?>
              </td>
            </tr>
        </table>
    </center>
</body>
</html>
