<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

if (!defined('ROOT'))
{
	define('ROOT', str_replace("pdf/pdf_invoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
    define('ROOT_PDF', str_replace("pdf/pdf_invoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
	require(ROOT. '/core/Model.php');
	require(ROOT. '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
else
{
	require(ROOT. '/core/Model.php');
	require(ROOT . '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
// -------------------------------------------------- DATA -------------------------------------- // 
// -- Retrieve the invoice id.
$id = null;
if ( !empty($_GET['id'])) 
{
	$id = $_GET['id'];	
}
else
{
	$id  = 0;
}

/* -- Multiple invoice id.*/
if(isset($_POST['id']))
{
	$id = $_POST['id'];
}

// -- check id SESSIONS
if(isset($_SESSION['id']))
{
	if(!empty($_SESSION['id']))
	{
		if($_SESSION['id'] != 0)
		{
			$id = $_SESSION['id'];
			$_SESSION['id'] = '';
		}
	}
}

// -- 1.Download to pdf.
$download = '';
if ( !empty($_GET['download'])) 
{
	$download = $_GET['download'];	
}

// -- 2.EmailFlag.
$emailFlag = '';
if ( !empty($_GET['emailFlag'])) 
{
	$emailFlag = $_GET['emailFlag'];	
}
/* -- Send Multiple Emails */
if(isset($_POST['emailFlag']))
{
	$emailFlag = $_POST['emailFlag'];
}

// -- check emailFlag SESSIONS
if(isset($_SESSION['emailFlag']))
{
		if(!empty($_SESSION['emailFlag']))
		  {
			$emailFlag = $_SESSION['emailFlag'];	
			$_SESSION['emailFlag'] = '';
		  }
}

// -- Get the Invoice details.
// -- Get Tax Master Data.
	$tax = new tax();
	$dTax['taxes'] = $tax->showAll();	
	
// -- Invoice Data.	
    $invoice= new invoice();
    $invoice_data = $invoice->showInvoice($id);
	
// -- Get the Invoice Items details.
    $invoice_items = $invoice->show($id);

// -- Get the Invoice Payments details.
	$payments_data = $invoice->payments($id);
	
/* -- BOC - Global Settings -- */
// -- Global Setting Controller.
$globalsetting  = new globalsetting();
$globalsettings = $globalsetting->showAll();	

// -- Bind Global Data into Session.
foreach($globalsettings as $globalsetting)
  {
 // -- Global Settings	  
  if($globalsetting['keey']=='company_name')   {$_SESSION['company_name'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_address'){$_SESSION['company_address'] = $globalsetting['value'];}
  if($globalsetting['keey']=='company_email')  {$_SESSION['company_email']   = $globalsetting['value'];}
  if($globalsetting['keey']=='company_fax')    {$_SESSION['company_fax'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_logo')   {$_SESSION['company_logo'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_phone')  {$_SESSION['company_phone'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='company_url')    {$_SESSION['company_url'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='currency')       {$_SESSION['currency'] 		 = $globalsetting['value'];}
  if($globalsetting['keey']=='currency_decimals'){$_SESSION['currency_decimals'] = $globalsetting['value'];}
  if($globalsetting['keey']=='default_template'){$_SESSION['default_template'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='last_calculation_date'){$_SESSION['last_calculation_date'] = $globalsetting['value'];}
  if($globalsetting['keey']=='legal_terms')     {$_SESSION['legal_terms']		= $globalsetting['value'];}
  if($globalsetting['keey']=='pdf_orientation') {$_SESSION['pdf_orientation'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='pdf_size')        {$_SESSION['pdf_size']			= $globalsetting['value'];}
  if($globalsetting['keey']=='sample_data_load'){$_SESSION['sample_data_load'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='siwapp_modules')  {$_SESSION['siwapp_modules'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='company_reg')  {$_SESSION['company_reg'] 	= $globalsetting['value'];}
  if($globalsetting['keey']=='VAT')  {$_SESSION['VAT'] 	= $globalsetting['value'];}
  
  // -- SMTP Settings
  if($globalsetting['keey']=='host') {$_SESSION['host'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='port') {$_SESSION['port']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='secure') {$_SESSION['secure']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='emailusername') {$_SESSION['emailusername'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='emailpassword') {$_SESSION['emailpassword'] 	 = $globalsetting['value'];}
  if($globalsetting['keey']=='prod') {$_SESSION['prod']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='dev') {$_SESSION['dev']	 = $globalsetting['value'];}
  if($globalsetting['keey']=='captcha') {$_SESSION['captcha'] 	 = $globalsetting['value'];}

  }
/* -- EOC - Global Settings -- */	
// -- $currency.
$currency 		= $_SESSION['currency'];
$companyname 	= $_SESSION['company_name'];
$companyAddress = $_SESSION['company_address'];
$companyphone   = $_SESSION['company_phone'] ;
$companyEmail	= $_SESSION['company_email'];
$companyReg		= $_SESSION['company_reg'];
$companyLogo 	= $_SESSION['company_logo'];
$companyUrl     = $_SESSION['company_url'];
//  ------------------------------------------------------ END OF DATA  -------------------------------------

// -- Print content to pdf.
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);


//$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER
class MYPDF extends TCPDF 
{
	public static $companyname = '';
	public static $companyAddress = '';
	public static $companyReg = '';
	public static $companyEmail = '';
	public static $companyUrl = '';

	public function Header() 
	{
		$this->setJPEGQuality(90);
		//$this->Image('ecashmeup.png', 120, 10, 75, 0, 'PNG', 'https://www.finalwebsites.com');
 		$this->Image('http://www.vogsphere.co.za/ims/images/logo.jpeg', 120, 1, 75, 0, 'JPEG', 'https://www.vogsphere.co.za');
		// -- needs to be change to be dynamic
		
 // Company Details 
$this->CreateTextBox('Company: '.MYPDF::$companyname  , 0, 10, 0, 10, 10, '','B');
$this->CreateTextBox('Address: '.MYPDF::$companyAddress,        0, 15, 80, 10, 10);
$this->CreateTextBox('Registration No: '.MYPDF::$companyReg,     0, 20, 80, 10, 10);
$this->CreateTextBox('Email: '.MYPDF::$companyEmail,     0, 25, 80, 10, 10);
$this->CreateTextBox('Web: '.MYPDF::$companyUrl,     0, 30, 80, 10, 10);
					
 $this->Line(0, 40, 395, 40);

	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont(PDF_FONT_NAME_MAIN, 'I', 8);
		$this->Cell(0, 10, MYPDF::$companyname.' - Registration Number: '.MYPDF::$companyReg, 0, false, 'C');
	}

	public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 10, $fontsize = 10, $fontstyle = '', $align = 'L') {
		$this->SetXY($x+20, $y); // 20 = margin left
		$this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
		$this->Cell($width, $height, $textval, 0, false, $align);
	}
}

// create a PDF object
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	MYPDF::$companyname = $companyname;
	MYPDF::$companyname 	= $companyname;
	MYPDF::$companyAddress 	= trim($companyAddress);
	MYPDF::$companyReg 		= $companyReg;
	MYPDF::$companyEmail 	= $companyEmail;
	MYPDF::$companyUrl 		= $companyUrl;
	
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$html = <<<EOD

EOD;

//<table><tr><td>Tumelo</td><td>Modise</td></tr></table>';

// Print text using writeHTMLCell()
//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// -- 30 Invoice Items per page
$count = 0;
$perPage = 20;

// Company Details 
/*$pdf->CreateTextBox('Company: Vogsphere (Pty) Ltd'  , 0, 10, 0, 10, 10, '','B');
$pdf->CreateTextBox('Address: 43 Mulder Str, 31 SEOUL, Centurion, 0158',        0, 15, 80, 10, 10);
$pdf->CreateTextBox('Registration No: 2012/020274/74/07',     0, 20, 80, 10, 10);
$pdf->CreateTextBox('Email: info@vogsphere.co.za',     0, 25, 80, 10, 10);
$pdf->CreateTextBox('Web: www.vogsphere.co.za',     0, 30, 80, 10, 10);
*/

// create address box
$pdf->CreateTextBox('Invoiced To', 0, 50, 80, 10, 10, 'B');
$pdf->CreateTextBox($invoice_data["customer_name"], 0, 55, 80, 10, 10, '	');
$pdf->CreateTextBox($invoice_data["customer_identification"], 0, 60, 80, 10, 10);
$pdf->CreateTextBox($invoice_data["contact_person"], 0, 65, 80, 10, 10);
$pdf->CreateTextBox($invoice_data["invoicing_address"], 0, 70, 80, 10, 10);
$pdf->CreateTextBox($invoice_data["customer_email"], 0, 75, 80, 10, 10);
 
// invoice title / number
$pdf->CreateTextBox('Invoice #'.$invoice_data["id"], 0, 90, 120, 20, 16);
 
// date, order ref
$pdf->CreateTextBox('Issue Date: '.$invoice_data["issue_date"], 0, 100, 0, 10, 10, '', 'R');
$pdf->CreateTextBox('Due Date:'.$invoice_data["due_date"], 0, 105, 0, 10, 10, '', 'R');

// 
// list headers
$pdf->CreateTextBox('Product or service', 0, 120, 90, 10, 10, 'B');
$pdf->CreateTextBox('Price', 50, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Quantity', 80, 120, 20, 10, 10, 'B', 'C');
$pdf->CreateTextBox('Taxes', 85, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Discount', 110, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Amount', 140, 120, 30, 10, 10, 'B', 'R');
 
$pdf->Line(20, 129, 195, 129);
 
// some example data
$orders[] = array('quant' => 5, 'descr' => '.com domain registration', 'price' => 9.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 3, 'descr' => '.net domain name renewal', 'price' => 11.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 1, 'descr' => 'SSL certificate 256-Byte encryption', 'price' => 99.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 1, 'descr' => '25GB VPS Hosting, 200GB Bandwidth', 'price' => 19.95,'taxes' => 0.00,'disc' => 0.00);
 
$currY = 128;
$total = 0;
foreach ($invoice_items as $row) 
{
	$text = tokenTruncate($row['description'],40);
	//$text = $row['description'];
	
	$pdf->CreateTextBox($text, 0, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox($currency.number_format($row['unitary_cost'],2), 50, $currY, 30, 10, 10, '', 'R');
	$pdf->CreateTextBox(number_format($row['quantity'],0), 80, $currY, 20, 10, 10, '', 'C');
	$amount = $row['quantity']*$row['unitary_cost'];
	$pdf->CreateTextBox($row['discount'], 100, $currY, 20, 10, 10, '', 'C');//-taxes
	$pdf->CreateTextBox($row['discount'], 120, $currY, 20, 10, 10, '', 'C');
	$pdf->CreateTextBox($currency.$amount, 140, $currY, 30, 10, 10, '', 'R');

	$currY = $currY+5;
	$total = $total+$amount;
	//$pdf->Line(20, $currY+4, 195, $currY+4);

	$count++;
	// -- Add a new page after 30 invoice items.
	if ($count % $perPage == 0) {$pdf->AddPage(); $currY = 40;}
}
$pdf->Line(20, $currY+4, 195, $currY+4);

	
// output the total row
$pdf->CreateTextBox('Subtotal', 10, $currY+5, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($total, 2, '.', ''), 140, $currY+5, 30, 10, 10, 'B', 'R');

$pdf->CreateTextBox('Discount', 10, $currY+10, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($invoice_data["discount_amount"], 2, '.', ''), 140, $currY+10, 30, 10, 10, 'B', 'R');

$pdf->CreateTextBox('Taxes', 10, $currY+15, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($invoice_data["tax_amount"], 2, '.', ''), 140, $currY+15, 30, 10, 10, 'B', 'R');

$pdf->CreateTextBox('Total', 10, $currY+20, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($total, 2, '.', ''), 140, $currY+20, 30, 10, 10, 'B', 'R');
 
 // Payments headers
$currY = $currY + 20;
// invoice title / number
$pdf->CreateTextBox('Payments Transactions', 0, $currY, 120, 20, 16);
$currY = $currY + 10;

$pdf->CreateTextBox('Payment Date', 0, $currY, 90, 10, 10, 'B');
$pdf->CreateTextBox('Payment Reference', 35, $currY, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Amount', 100, $currY, 20, 10, 10, 'B', 'C');

$currY = $currY + 10;

$pdf->Line(20, $currY, 195, $currY);

// some payment instructions or information
$payments[] = array('date' => date('Y-m-d'), 'prn' => '.com domain registration', 'amount' => 9.95);
$totalPayment = 0;
$balance = 0.00;

$count = 0;

foreach ($payments_data as $row) 
{
	$pdf->CreateTextBox($row['date'], 0, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox($row['notes'], 30, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox(number_format($row['amount'],2), 105, $currY, 90, 10, 10, '');
	$amount = $row['amount'];

	$currY = $currY+5;
	$totalPayment = $totalPayment+$amount;
	
		$count++;
	// -- Add a new page after 30 invoice items.
	if ($count % $perPage == 0) {$pdf->AddPage(); $currY = 40;}

}
$pdf->Line(20, $currY+4, 195, $currY+4);
$pdf->CreateTextBox('Total', 10, $currY+5, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($totalPayment, 2, '.', ''), 140, $currY+5, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Balance', 10, $currY+10, 135, 10, 10, 'B', 'R');
$balance = $totalPayment - $total;
$pdf->CreateTextBox($currency.number_format($balance, 2, '.', ''), 140, $currY+10, 30, 10, 10, 'B', 'R');

// -- Notes
// invoice title / number
$pdf->CreateTextBox('Notes', 0, $currY+6, 120, 20, 16);
$pdf->setXY(20, $currY+20);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 17, $invoice_data["notes"], 0, 'L', 0, 1, '', '', true, null, true);

/* -- Terms -- */
$pdf->CreateTextBox('Terms and Conditions', 0, $currY+36, 120, 20, 16);
$pdf->setXY(20, $currY+50);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 10, $invoice_data["terms"], 0, 'L', 0, 1, '', '', true, null, true);

/*
$pdf->setXY(20, $currY+20);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 10, '<em>Lorem ipsum dolor sit amet, consectetur adipiscing elit</em>. 
Vestibulum sagittis venenatis urna, in pellentesque ipsum pulvinar eu. In nec <a href="http://www.google.com/">nulla libero</a>, eu sagittis diam. Aenean egestas pharetra urna, et tristique metus egestas nec. Aliquam erat volutpat. Fusce pretium dapibus tellus.', 0, 'L', 0, 1, '', '', true, null, true);
*/

$eStatement = 'invoice_'.$id;
$fileName = $eStatement.'.pdf';
//$_SESSION['invFileName'] = $fileName;


if(empty($emailFlag))
{
	if(empty($download))	
	{
		$pdf->Output($fileName, 'I');
	}
	elseif($download  == 'Y')
	{// -- F = Save to file.
	 // -- D = Download
	 // -- S = 
		$pdf->Output($fileName, 'D');
	}
	else
	{
		echo 'id - download - empty = '.$id;
	}	
}
else
{
	if($id != 0 && !empty($id))
	{
		$emailaddr = $invoice_data["customer_email"];
		$subject = 'Invoice from '.$companyname;
		$body = 'Dear '.$invoice_data["customer_name"].' Your invoice and statement are attached.'.'Best regards,The '.$companyname.' Team.';
		$invoice->updateSentByEmail($id);
		$fileatt = $pdf->Output($fileName, 'S');
		$message = Helper::SendEmailWithAttachmentGeneric($fileatt,$fileName,$companyEmail,$emailaddr,$subject,$body);
	}	
}
function tokenTruncate($string, $your_desired_width) {
  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
  $parts_count = count($parts);

  $length = 0;
  $last_part = 0;
  for (; $last_part < $parts_count; ++$last_part) {
    $length += strlen($parts[$last_part]);
    if ($length > $your_desired_width) { break; }
  }

  return implode(array_slice($parts, 0, $last_part));
}

?>