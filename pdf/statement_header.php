<style>
th{font-weight: bold;}
</style>
	
<table class="table table-borderless" width="100%">
   <tr><td bgcolor="#337ab7" color="White" width="40%">Client</td><td bgcolor="#337ab7" color="White" width="63%" align="right">Summary</td></tr>
   <!-- <tr><td>[TONAME]</td><td align="right">Summary</td></tr>
   <tr><td></td></tr> -->
</table>
<table class="table table-borderless" width="100%">
    <thead>
	  <tr><td>Statement Date:</td><td colspan="2">: [date]</td></tr>	
	  <tr>
        <td>Customer Name</td><td colspan="2">: [TONAME]</td>							
	  </tr>
    </thead>
    <tbody>
      <tr>
        <td>Customer Reference</td><td colspan="2">: [CUSTOMERCODE]</td>					
	 </tr>
      <tr>
        <td>Customer Postal Address</td><td>: [CUSTOMERPOSTALADDRESS]</td>	
	 </tr>
	 <tr>
        <td>Customer Physical Address</td><td>: [CUSTOMERPHYSICALADDRESS]</td>		
	</tr>
	<tr>
        <td>Email</td><td>: [CUSTOMEREMAIL]</td>						
	 </tr>
	<tr>
		<td>Office Number</td><td>: [officenumber]</td>
	</tr>	
	<tr>
		<td>Cell Phone</td><td>: [cellnumber]</td>
	</tr>	
	</tbody> 
</table>
<br/>
<br/>
<table class="table table-borderless" width="100%">
    <thead>
	  <tr>
        <td bgcolor="#337ab7" color="White"  width="15%">Date</td>
        <td bgcolor="#337ab7" color="White"  width="15%">Due Date</td>				
        <td bgcolor="#337ab7" color="White">Reference</td>		
        <td bgcolor="#337ab7" color="White" width="20%">Description</td>						
        <td bgcolor="#337ab7" color="White" align="right">Amount</td>						
        <td bgcolor="#337ab7" color="White" align="right" width="20%">Balance</td>						
	  </tr>
    </thead>
</table>
<br/>