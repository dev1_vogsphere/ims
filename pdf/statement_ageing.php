<style>
th{font-weight: bold;}
</style>
<br/>
<br/>
<!---------------------------- Ageing ------------------------------->
<table class="table table-borderless" width="100%">
    <thead>
	  <tr>
        <td bgcolor="#337ab7" color="White">120+ DAYS</td>	
        <td bgcolor="#337ab7" color="White">90 DAYS</td>		
        <td bgcolor="#337ab7" color="White">60 DAYS</td>						
        <td bgcolor="#337ab7" color="White">30 DAYS</td>						
        <td bgcolor="#337ab7" color="White">CURRENT</td>
        <td bgcolor="#337ab7" color="White">AMOUNT DUE</td>
        <td bgcolor="#337ab7" color="White">AMOUNT PAID</td>								
	  </tr>
    </thead>
    <tbody>
	<tr>
        <td>[120DAYS]</td>	
        <td>[90DAYS]</td>		
        <td>[60DAYS]</td>						
        <td>[30DAYS]</td>						
        <td>[CURRENT]</td>	
        <td>[AMOUNTDUE]</td>
        <td>[AMOUNTPAID]</td>										
	</tr>    
	</tbody>
</table>
<br/>	  
<!---------------------------- Company Banking Details ------------------------------->
<table><tr><td bgcolor="#337ab7" color="White" colspan="2" align="right">Company Banking Details</td></tr>
<tr><td align="right">Beneficiary Name:</td><td align="right">[accountholdername]</td></tr>
<tr><td align="right">Bank Name:</td><td align="right">[bankname]</td></tr>
<tr><td align="right">Account Number:</td><td align="right">[accountnumber]</td></tr>
<tr><td align="right">Branch Code:</td><td align="right">[branchcode]</td></tr>
<tr><td align="right">Reference Number:</td><td align="right">[CUSTOMERCODE]</td></tr>
</table>
<!---------------------------- Company NRC/FSP Registration --------------------------
<table><tr><th bgcolor="#337ab7" color="White">Company Compliance Disclaimer</th></tr>
<tr><td>[disclaimer]</td>
</tr></table> -->
<!--------------------------- Terms and Conditions ------------------------------->
<table>
	<tr>
	  <th bgcolor="#337ab7" color="White">Terms and Conditions</th>
	</tr>
	<tr>
		<td>[terms]</td>
	</tr>
</table>