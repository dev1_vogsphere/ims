<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
if(!function_exists('output_pdf'))
{
	function output_pdf($pdf,$emailFlag,$download,$fileName,$id,$invoice_data,$companyname,$companyEmail)
	{
		if(empty($emailFlag))
		{
			if(empty($download))	
			{
				$pdf->Output($fileName, 'I');
			}
			elseif($download  == 'Y')
			{// -- F = Save to file.
			 // -- D = Download
			 // -- S = 
				$pdf->Output($fileName, 'D');
			}
			else
			{
				echo 'id - download - empty = '.$id;
			}	
		}
		else
		{
			if($id != 0 && !empty($id))
			{
				$emailaddr = $invoice_data["email"];
				$subject   = 'statement from '.$companyname;
				$body 	   = ROOT."pdf/"."email_statement.php";
				// -- Email Template.
				if(isset($_SESSION['email_template']))
				{
					if(!empty($_SESSION['email_template']))
					{
						$body = ROOT."pdf/".$_SESSION['email_template'];
					}
				}
				// -- Email Subject.
				if(isset($_SESSION['email_subject']))
				{
					if(!empty($_SESSION['email_subject']))
					{
						$subject = $_SESSION['email_subject'];
					}
				}
				//'Dear '.$invoice_data["customer_name"].' Your invoice and statement are attached.'.'Best regards,The '.$companyname.' Team.';
				//$invoice->updateSentByEmail($id);
				$fileatt = $pdf->Output($fileName, 'S');
				$message = Helper::SendEmailWithAttachmentGeneric($fileatt,$fileName,$companyEmail,$emailaddr,$subject,$body);
			}	
	   }
   }
}	
if(!function_exists('gethref'))
{
	function gethref_pdf($companyLogo)
	{
		$href = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$href = str_replace("pdf_invoice.php",$companyLogo,$href);
		$href = str_replace("pdf_quotation.php",$companyLogo,$href);	
		$href = str_replace("pdf_statement.php",$companyLogo,$href);	
		$href = str_replace("pdf","images",$href);
		
		// -- from invoice/index page
		$href = str_replace("index.php","images/".$companyLogo,$href);

		// -- from webservices
		$href = str_replace("webservices","images",$href);		
		$href = str_replace("generateinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_updateinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_followup_dueinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_updaterecurringinvoices.php",$companyLogo,$href);
		
		return $href;
	}
}		
if(!function_exists('get_sponsored_invoices'))
{
function get_sponsored_invoices($id,$items,$invoice,$sponsoredInvoices,$type,$datefrom,$dateto)
{
	 $invoice_transactional_data = null;
	 foreach($sponsoredInvoices as $rowSp)
	 {
// -- Use product(s) get Invoice id(s).
	  	$itemsponsor = $items->showAllByProductId($rowSp['id']); 
		//print_r($itemsponsor);
		if(!empty($itemsponsor))
		{
			foreach($itemsponsor as $row) 
			{
				if(empty($type))
				{
					// -- Merge Invoice with sponsored's Invoices.
					if(!empty($invoice_transactional_data))
					{$invoice_transactional_data = array_merge($invoice_transactional_data,$invoice->showSelectedInvoice($row['common_id']));}
					else
					{$invoice_transactional_data = $invoice->showSelectedInvoice($row['common_id']);}
				}
				else
				{
				// -- showSelectedInvoice_daterange($id,$from,$to)
					if(!empty($invoice_transactional_data))
					{$invoice_transactional_data = array_merge($invoice_transactional_data,$invoice->showSelectedInvoice_daterange($row['common_id'],$datefrom,$dateto));}
					else
					{$invoice_transactional_data = $invoice->showSelectedInvoice_daterange($row['common_id'],$datefrom,$dateto);}				
				}
			}
		}
	  }
		return  $invoice_transactional_data;
}}
if(!function_exists('sort_by'))
{
	function sort_by($multiArray,$name)
	{
		$tmp = Array();
		foreach($multiArray as &$ma)
			$tmp[] = &$ma[$name];
		array_multisort($tmp, $multiArray);
		return $multiArray;
}}
if (!defined('ROOT'))
{
	define('ROOT', str_replace("pdf/pdf_statement.php", "", $_SERVER["SCRIPT_FILENAME"]));	
    define('ROOT_PDF', str_replace("pdf/pdf_invoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
	require(ROOT. '/core/Model.php');
	require(ROOT. '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/customer.php');	
	require(ROOT. '/models/product.php');	
	require(ROOT. '/models/item.php');		
	require(ROOT. '/models/globalsetting.php');		
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
else
{
	require(ROOT. '/core/Model.php');
	require(ROOT . '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/customer.php');	
	require(ROOT. '/models/product.php');	
	require(ROOT. '/models/item.php');		
	
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
// -------------------------------------------------- DATA -------------------------------------- // 
// -- Retrieve the invoice id.
$id = null;
$username = '';
$currency 		= '';
$companyname 	= '';
$companyAddress = '';
$companyphone   = '';
$companyEmail	= '';
$companyReg		= '';
$companyLogo 	= '';
$companyUrl     = '';
$VAT 	  = '';
$ext 	  = '';
$type	  = '';
$datefrom = '';
$dateto   = '';

// -- GET if we do print & preview
if(!empty($_GET['id'])) {$id = $_GET['id'];}else{$id  = 0;}
if(!empty($_GET['username'])){$username = $_GET['username'];}
if(!empty($_GET['type'])){$type = $_GET['type'];}

if(!empty($_GET['from'])){$datefrom = $_GET['from'];}
if(!empty($_GET['to'])){$dateto = $_GET['to'];}

// -- POST if posting..
if(isset($_POST['id'])){$id = $_POST['id'];}
if(!empty($_POST['username'])){$username = $_POST['username'];}
if(isset($_POST['type'])){$type = $_POST['type'];}

if(isset($_POST['from'])){$datefrom = $_POST['from'];}
if(isset($_POST['to'])){$dateto = $_POST['to'];}

// -- check id SESSIONS
if(isset($_SESSION['id']))
{
	if(!empty($_SESSION['id']))
	{
		if($_SESSION['id'] != 0)
		{
			$id = $_SESSION['id'];
			$_SESSION['id'] = '';
		}
	}
}

// -- 1.Download to pdf.
$download = '';
if ( !empty($_GET['download'])) 
{
	$download = $_GET['download'];	
}

// -- 2.EmailFlag.
$emailFlag = '';
if ( !empty($_GET['emailFlag'])) 
{
	$emailFlag = $_GET['emailFlag'];	
}
if(isset($_POST['emailFlag']))
{
	$emailFlag = $_POST['emailFlag'];
}

// -- check emailFlag SESSIONS
if(isset($_SESSION['emailFlag']))
{
		if(!empty($_SESSION['emailFlag']))
		  {
			$emailFlag = $_SESSION['emailFlag'];	
			$_SESSION['emailFlag'] = '';
		  }
}

// -- Get the Invoice Items details.
	$item = new item();
	
// -- Get Tax Master Data.
	$tax = new tax();
	$dTax['taxes'] = $tax->showAll();	
	
// -- Invoice Data.	
    $invoice= new invoice();
    $invoice_data = $invoice->showInvoice($id);

// -- Get product(s) sponsored
    $product = new product();
	$items 		 = new item();

	$sponsoredProducts = null;
	$sponsoredProducts = $product->showSponsoredAll($id);
	
// -- Get the Invoice Items details.
    //$invoice_items = $invoice->show($id);

// -- Get the Invoice Payments details.
	$payments_data = $invoice->payments($id);

// -- Read from invoice.
	if(empty($username))
	{
		$username = $invoice_data['createdby'];
	}	
	
// -- Read from organsation details
$customerObject = NULL;

		// -- Customer.
		$cusobj = new customer();
		$identification = $id;//$invoice_data['customer_identification'];
		$customerObject = $cusobj->show($identification);
		$organsation    = $customerObject['organisation']; 
		// -- User.
		$userobj = new globalsetting();		
		$userobject     = $userobj->showbyorganisation($organsation);
		if(empty($username)){
		$username 		= $userobject['username'];}
		
// -- Get customer invoices.
$invoice_transactional_data = null;
if(isset($customerObject['identification']))
{
	$identification  = $customerObject['identification'];
	if(empty($type))
    {
		$invoice_transactional_data = $invoice->search_customer_invoice($identification);
	}
	else
	{	
		$fromdate = $datefrom;
		$todate   = $dateto;
		$invoice_transactional_data = $invoice->search_customer_invoice_daterange($identification,$fromdate,$todate);		
	}
}

//print_r($sponsoredProducts);
	if(!empty($sponsoredProducts))
	{
// -- Use product(s) get Invoice id(s).		
		$sponsoredInvoices = get_sponsored_invoices($id,$items,$invoice,$sponsoredProducts,$type,$datefrom,$dateto);
		//print_r($sponsoredInvoices);
		$invoice_transactional_data = array_merge($invoice_transactional_data,$sponsoredInvoices);
	}
// -- Global Setting Controller.
$globalsetting  = new globalsetting();
$globalsettings = $globalsetting->show($username);	

// -- Bind Global Data into Session.
$globalsetting = $globalsettings;

if(!empty($globalsettings))
{	  
 // -- Global Settings	  
  if(!empty($globalsetting['company_name']))   {$_SESSION['company_name'] 	 = $globalsetting['company_name'];}
  if(!empty($globalsetting['company_address'])){$_SESSION['company_address'] = $globalsetting['company_address'];}
  if(!empty($globalsetting['company_email'])) {$_SESSION['company_email']   = $globalsetting['company_email'];}
  if(!empty($globalsetting['company_fax']))    {$_SESSION['company_fax'] 	 = $globalsetting['company_fax'];}
  if(!empty($globalsetting['company_logo']))   {$_SESSION['company_logo'] 	 = $globalsetting['company_logo']; $companyLogo  = $globalsetting['company_logo'];}
  if(!empty($globalsetting['company_phone']))  {$_SESSION['company_phone'] 	 = $globalsetting['company_phone'];}
  if(!empty($globalsetting['company_url']))    {$_SESSION['company_url'] 	 = $globalsetting['company_url'];}
  if(!empty($globalsetting['currency']))       {$_SESSION['currency'] 		 = $globalsetting['currency'];}
  if(!empty($globalsetting['currency_decimals'])){$_SESSION['currency_decimals'] = $globalsetting['currency_decimals'];}
  if(!empty($globalsetting['default_template'])){$_SESSION['default_template'] 	 = $globalsetting['default_template'];}
  if(!empty($globalsetting['last_calculation_date'])){$_SESSION['last_calculation_date'] = $globalsetting['last_calculation_date'];}
  if(!empty($globalsetting['legal_terms'])){$_SESSION['legal_terms']		= $globalsetting['legal_terms'];}
  if(!empty($globalsetting['pdf_orientation'])){$_SESSION['pdf_orientation'] 	= $globalsetting['pdf_orientation'];}
  if(!empty($globalsetting['pdf_size'])){$_SESSION['pdf_size']			= $globalsetting['pdf_size'];}
  if(!empty($globalsetting['sample_data_load'])){$_SESSION['sample_data_load'] 	= $globalsetting['sample_data_load'];}
  if(!empty($globalsetting['siwapp_modules'])){$_SESSION['siwapp_modules'] 	= $globalsetting['siwapp_modules'];}
  if(!empty($globalsetting['company_reg'])){$_SESSION['company_reg'] 	= $globalsetting['company_reg'];}
  if(!empty($globalsetting['VAT'])){$_SESSION['VAT'] 	= $globalsetting['VAT'];}
  
  // -- SMTP Settings
  if(!empty($globalsetting['host'])) {$_SESSION['host'] 	 = $globalsetting['host'];}
  if(!empty($globalsetting['port'])) {$_SESSION['port']	 = $globalsetting['port'];}
  if(!empty($globalsetting['secure'])) {$_SESSION['secure']	 = $globalsetting['secure'];}
  if(!empty($globalsetting['emailusername'])) {$_SESSION['emailusername'] 	 = $globalsetting['emailusername'];}
  if(!empty($globalsetting['emailpassword'])) {$_SESSION['emailpassword'] 	 = $globalsetting['emailpassword'];}
  if(!empty($globalsetting['prod'])) {$_SESSION['prod']	 = $globalsetting['prod'];}
  if(!empty($globalsetting['dev'])) {$_SESSION['dev']	 = $globalsetting['dev'];}
  if(!empty($globalsetting['captcha'])) {$_SESSION['captcha'] 	 = $globalsetting['captcha'];}
	
 // -- Bank Details
   if(!empty($globalsetting['account']))	{$_SESSION['account'] 	= $globalsetting['account'];}
   if(!empty($globalsetting['accounttype'])){$_SESSION['accounttype'] 	= $globalsetting['accounttype'];}
   if(!empty($globalsetting['bankname']))	{$_SESSION['bankname'] 	= $globalsetting['bankname'];}
   if(!empty($globalsetting['branchcode'])) {$_SESSION['branchcode'] 	= $globalsetting['branchcode'];}
   if(!empty($globalsetting['branchname'])) {$_SESSION['branchname'] 	= $globalsetting['branchname'];}
   if(!empty($globalsetting['swiftcode'])) {$_SESSION['swiftcode'] 	= $globalsetting['swiftcode'];}
  }

// -- $currency.
if(isset($_SESSION['currency'])){$currency 		= $_SESSION['currency'];     }
if(isset($_SESSION['company_name'] 	    )){$companyname 	= $_SESSION['company_name'];     }
if(isset($_SESSION['company_address']   )){$companyAddress = $_SESSION['company_address'];}
if(isset($_SESSION['companyphone']     )){$companyphone   = $_SESSION['company_phone'] ; }
if(isset($_SESSION['company_email']	    )){$companyEmail	= $_SESSION['company_email'];    }
if(isset($_SESSION['company_reg']		)){$companyReg		= $_SESSION['company_reg'];  }
if(isset($_SESSION['company_logo'] 	    )){$companyLogo 	= $_SESSION['company_logo'];     }
if(isset($_SESSION['company_url']       )){$companyUrl     = $_SESSION['company_url'];    }
if(isset($_SESSION['VAT']       )){$VAT     = $_SESSION['VAT'];    }
$_SESSION['invoicetype'] = 'Invoice';

$ext = pathinfo($companyLogo, PATHINFO_EXTENSION);
$href = gethref_pdf($companyLogo);

//echo $companyLogo.'<br/>';
//echo $href;
//  ------------------------------------------------------ END OF DATA  -------------------------------------

// -- Print content to pdf.
//
 // Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 // to provide "ID number" as password and ID number as master password as well.
 //
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);


//$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER
if (!function_exists('tokenTruncate')) 
{
	function tokenTruncate($string, $your_desired_width) 
	{
	  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	  $parts_count = count($parts);

	  $length = 0;
	  $last_part = 0;
	  for (; $last_part < $parts_count; ++$last_part) {
		$length += strlen($parts[$last_part]);
		if ($length > $your_desired_width) { break; }
	  }

	  return implode(array_slice($parts, 0, $last_part));
	}
}

if (!function_exists('getItemTaxPercentage'))
{
	function getItemTaxPercentage($itemid)
	{	
		// -- Get Tax Master Data.
		$tax = new tax();
		$invoice_items_tax = $tax->invoice_item_taxes($itemid);
		$item_tax = null;
		$tax_id = 0;
		$tax_percentage = 0.00;
		foreach ($invoice_items_tax as $row) 
		{
			$tax_id = $row['tax_id'];
		}
		// -- check if tax id is empty.
		if(!empty($tax_id))
		{
			$item_tax = $tax->show($tax_id );
			$tax_percentage = $item_tax['value'];
		}
		return $tax_percentage; 
	}
}
if(!function_exists('mapping_data_header'))
{
function mapping_data_header($html,$data)
{ 
$html = str_replace("[date]",date("Y-m-d"),$html); 
//$html = str_replace("[page]",$dataComp['page'],$html); 
$html = str_replace("[TONAME]",$data['name'],$html); 
$html = str_replace("[CUSTOMERVAT]",$data['vatnumber'],$html); 
$html = str_replace("[CUSTOMERPOSTALADDRESS]",$data['shipping_address'],$html); 
$html = str_replace("[CUSTOMERPHYSICALADDRESS]",$data['invoicing_address'],$html); 
$html = str_replace("[CUSTOMERCODE]",$data['identification'],$html); 
$html = str_replace("[CUSTOMERCONTACT]",$data['contact_person'],$html); 
$html = str_replace("[CUSTOMEREMAIL]",$data['email'],$html); 
$html = str_replace("[officenumber]",$data['officenumber'],$html);
$html = str_replace("[cellnumber]",$data['cellnumber'],$html); 
return $html;
}}
		
if(!function_exists('mapping_data_ageing'))
{
function mapping_data_ageing($html,$data)
{
$html = str_replace("[date]",date("y-m-d"),$html); 
//$html = str_replace("[page]",$dataComp['page'],$html); 
$html = str_replace("[accountholdername]",$data['company_name'],$html); 
$html = str_replace("[bankname]",$data['bankname'],$html); 
$html = str_replace("[accountnumber]",$data['account'],$html); 
$html = str_replace("[branchcode]",$data['branchcode'],$html); 
$html = str_replace("[terms]",$data['legal_terms'],$html); 

// -- 
$html = str_replace("[120DAYS]",$data['120days'],$html); 
$html = str_replace("[90DAYS]",$data['90days'],$html); 
$html = str_replace("[60DAYS]",$data['60days'],$html); 
$html = str_replace("[30DAYS]",$data['30days'],$html); 
$html = str_replace("[CURRENT]",$data['current'],$html); 
$html = str_replace("[AMOUNTDUE]",$data['AMOUNTDUE'],$html); 
$html = str_replace("[AMOUNTPAID]",$data['AMOUNTPAID'],$html); 

// -- 'legal_terms'
return $html;
}}

if(!function_exists('mapping_data_closing'))
{
	function mapping_data_closing($data)
	{	$html = '';
		$html = $html."<tr>";
		$html = $html."<td>".$data['date']."</td>";
		$html = $html."<td></td>";
		$html = $html."<td>Closing Balance</td>";
		$html = $html."<td></td>";
		$html = $html."<td>".$data['currency'].$data['balance']."</td>";		
		$html = $html."</tr>";
		return $html;
	}
}
if(!function_exists('mapping_data_payments'))
{
	function mapping_data_payments($data)
	{
		$html = '';
		$html = $html."<tr>";
		$html = $html.'<td width="15%">'.$data['date']."</td>";		
		//$html = $html.'<td width="15%"></td>';		
		$html = $html."<td>Payment#".$data['id']."</td>";
		$html = $html."<td>".$data['description']."</td>";
		$html = $html.'<td>'.$data["currency"].$data["debit"].'</td>';						
		$html = $html.'<td>'.$data["currency"].$data["balance"].'</td>';				
		$html = $html."</tr>";	
		return $html;	
	}
}
if(!function_exists('mapping_data_invoices'))
{
	function mapping_data_invoices($data)
	{
		$html = '';
				$html = $html."<tr>";
		$html = $html.'<td width="15%">'.$data['date']."</td>";
		//$html = $html.'<td width="15%">'.$data['due_date']."</td>";		
		$html = $html."<td>Invoice#".$data['id']."</td>";
		$html = $html.'<td width="30%">'.$data['description'].'</td>';
		//$html = $html."<td width='20%'>".$data['currency'].$data['debit']."</td>";
		$html = $html.'<td>'.$data["currency"].$data["debit"].'</td>';								
		$html = $html.'<td>'.$data["currency"].$data["balance"].'</td>';		
		$html = $html."</tr>";	
		return $html;	
	}
}
if(!function_exists('mapping_data_invoice_items'))
{
	function mapping_data_invoice_items($data)
	{
		$html = '';
				$html = $html."<tr>";
		$html = $html.'<td width="15%">'.$data['date']."</td>";
		$html = $html.'<td width="15%">'.$data['due_date']."</td>";		
		$html = $html."<td>Invoice#".$data['id']."</td>";
		$html = $html.'<td width="30%">'.$data['description'].'</td>';
		//$html = $html."<td width='20%'>".$data['currency'].$data['debit']."</td>";
		$html = $html.'<td>'.$data["currency"].$data["debit"].'</td>';								
		$html = $html.'<td>'.$data["currency"].$data["balance"].'</td>';		
		$html = $html."</tr>";	
		return $html;	
	}
}
// -- To avoid : Redeclaring trim.
if (!function_exists('trim_input'))
{
	// -- Trim Input
	function trim_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);

	  return $data;
	}
}

//////////////////////////////////////////////////////////////////////
if(!function_exists('dateDifference'))
{
	function dateDifference($date_1 , $date_2,$differenceFormat )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
		$interval = date_diff($datetime1, $datetime2);
		return $interval->format($differenceFormat);
	}
}

if(!function_exists('mapping_data_items'))
{
function mapping_data_items($data,$invoiceObj,$itemObj,$TaxObj,$type)
{
	
$htmlRoot = '<style>
th{font-weight: bold;}
</style>
<br/>
<table class="table table-borderless" width="100%">
    <tbody>';
		$balance  = 0.00;
		$currency = 'R';
		$html 	  = '';
		$dataAgeing = null;
// -- Ageing 		
		$dataAgeing['120days']	 = 0.00;
		$dataAgeing['90days']    = 0.00;
		$dataAgeing['60days']    = 0.00;
		$dataAgeing['30days']    = 0.00;
		$dataAgeing['current']   = 0.00;
		
		$dataAgeing['AMOUNTPAID']   = 0.00;
		$dataAgeing['AMOUNTDUE']    = 0.00;
		
		$oustanding = 0.00;
		
	foreach($data as $row) 
	{
		$html = '';
		$credit = '0.00';
		$debit = '0.00';
		$oustanding = 0.00;
		$base_amount = '0.00';
		$tax_amount  = '0.00';
		$issue_date  = '';
		$due_date = '';
		$payment_date  = '';
		
		$customer_name = '';
		$customer_code = '';
		
		$invoice_id = '';
		$notes		= '';
		// -- Payments
		$payments_data = $invoiceObj->payments($row['id']);
		$totalPayment = 0;
		$credit = '0.00';
	
		if(isset($row['issue_date'])){$issue_date = $row['issue_date'];}
		if(isset($row['customer_name'])){$customer_name = $row['customer_name'];}
		
		if(isset($row['base_amount'])){$base_amount = $row['base_amount'];}
		if(isset($row['tax_amount'])){$tax_amount = $row['tax_amount'];}
		if(isset($row['due_date'])){$due_date = $row['due_date'];}

		if(isset($row['customer_identification'])){$customer_code = $row['customer_identification'];}


		// -- Invoice #display
		$dataFilter = null;
		$dataFilter['date'] 		= $issue_date;
		$dataFilter['due_date'] 	= $due_date;		
		$dataFilter['id'] 	  		= $row['id'];
		$dataFilter['currency'] 	= $currency;

		if(empty($type))
		{
			$debit 	 = $base_amount+$tax_amount; 
			$balance = $balance + $debit;
			$value   =  number_format($balance ,2);
			$value2  = number_format($debit ,2);
	
			$dataFilter['debit'] 		= $value2;
			$dataFilter['balance'] 		= $value;
			$dataFilter['description']  = $customer_code;
		
			$html = $html.mapping_data_invoices($dataFilter);
		}
		else
		{
			// -- Invoice Items #display
			$invoiceitems_data = $invoiceObj->show($row['id']);
			foreach ($invoiceitems_data as $rowItem) 
			{	$debitItemV = 0;
				$dataFilter['description'] = $rowItem['description'];
				$debitItem 	 = $rowItem['quantity'] * $rowItem['unitary_cost']; 				
				// -- Item Tax.
				$taxid = 0;
				$tax   = null;
				$taxPercentage = 0;
				$invoice_item_taxes = $TaxObj->invoice_item_taxes($rowItem['item_id']);
				foreach ($invoice_item_taxes as $rowItemTaxes)
				{
					$taxid = $rowItemTaxes['tax_id'];
				}
				// -- Tax percentage %.
				if(!empty($taxid))
				{
					 $tax = $TaxObj->show($taxid);
					 $taxPercentage = $tax['value'];
				}
				// -- Tax Percentage = Debit Amnt + (taxPercentage / 100)
				// -- e.g.Amnt Plus Tax Percentage = Debit Amnt + (Debit Amnt * Tax Percentage)
				// -- e.g.Amnt Plus Tax Percentage = 100 + (100 * 0.15) = 100 + 15 = 115
				if(!empty($taxPercentage))
				{
					$taxPercentage = $taxPercentage / 100;
					$debitItem = $debitItem + ($debitItem * $taxPercentage);
				}
				
				$balance = $balance + $debitItem;
				$value   =  number_format($balance ,2);
				$value2  = number_format($debitItem ,2);
	
				$dataFilter['debit'] 		= $value2;
				$dataFilter['balance'] 		= $value;
			
				//$debitItemV  = number_format($debitItem ,2);
				$dataFilter['debit'] = $value2;			

				$html = $html.mapping_data_invoice_items($dataFilter); 
			}
		}
		// -- Payments #display.
		$paymentYes   = '';
		$totalPayment = 0;	
		foreach ($payments_data as $rowPay) 
		{
			$dataAgeing['AMOUNTPAID'] = $dataAgeing['AMOUNTPAID'] + $rowPay['amount'];
			$amount = $rowPay['amount'] * -1;
			$balance = $balance + $amount;
			$totalPayment = $totalPayment+$rowPay['amount'];
			if(isset($rowPay['date'])){$payment_date = $rowPay['date'];}
			if(isset($rowPay['invoice_id'])){$invoice_id = $rowPay['invoice_id'];}
			if(isset($rowPay['notes'])){ if(!empty($rowPay['notes'])){$notes = " - ".$rowPay['notes'];}}
			$amount = number_format($amount,2); 
			$value  =  number_format($balance ,2);
			
			$dataFilter = null;
			$dataFilter['date'] 		= $payment_date;
			$dataFilter['id'] 	  		= $rowPay['id'];
			$dataFilter['currency'] 	= $currency;
			$dataFilter['debit'] 		= $amount;
			$dataFilter['balance'] 		= $value;
			$dataFilter['description']  = 'Payment Received Inv#'.$invoice_id.$notes;
			$html = $html.mapping_data_payments($dataFilter);
			$paymentYes = 'X';
		}		
		$today = date('Y-m-d');
		// -- No/Partial Payments ~ Age the Debt
		if(!empty($paymentYes))
		{
			$oustanding = $debit - $totalPayment;
			if($oustanding != 0)
			{
				$paymentYes = '';
				$debit = $oustanding;
			} // partially settled
		}
	
		if(empty($paymentYes))
		{
				$period = dateDifference( $today, $row['due_date'] ,'%a')." - ";
				//echo '['.$row['due_date'].' - '.$period.' '.$today.']';
				// -- 120+ days 
				if($period >= 120)
				{
					$dataAgeing['120days']   = $dataAgeing['120days'] + $debit;						
				}
				// -- 90 days
				elseif($period >= 90)
				{
					$dataAgeing['90days']   = $dataAgeing['90days'] + $debit;						
				}
				// -- 60 days
				elseif($period >= 60)
				{
					$dataAgeing['60days']   = $dataAgeing['60days'] + $debit;	
					
				}				
									
				// -- current
				if($period < 30)
				{
					$dataAgeing['current']   = $dataAgeing['current'] + $debit;						
				}
				// -- 30 days
				else if($period <= 59)
				{
					$dataAgeing['30days']   = $dataAgeing['30days'] + $debit;	
				}
		
		}
		$htmlRoot = $htmlRoot.$html;
	}
// -- CLOSING BALANCE.
	$balance = number_format($balance,2); 
	$dataAgeing['AMOUNTDUE'] = $balance;
	$dataFilter = null;
	$dataFilter['balance'] 	  = $balance;
	$dataFilter['date'] 	  = date('Y-m-d');
	$dataFilter['currency']   = $currency;
	$html = '';
	$html = mapping_data_closing($dataFilter);
	
	$htmlRoot = $htmlRoot.$html;
	$htmlRoot = $htmlRoot.'</tbody>
</table>	
<br/>';
// -- data Items
// -- Ageing 		
	$dataAgeing['120days']	 = number_format($dataAgeing['120days'],2);
	$dataAgeing['90days']    = number_format($dataAgeing['90days'],2);
	$dataAgeing['60days']    = number_format($dataAgeing['60days'],2);
	$dataAgeing['30days']    = number_format($dataAgeing['30days'],2);
	$dataAgeing['current']   = number_format($dataAgeing['current'],2);
		
	$dataAgeing['AMOUNTPAID'] = number_format($dataAgeing['AMOUNTPAID'],2);
// ---	
	$data_items = null;
	$data_items['html']		  = $htmlRoot;
	$data_items['dataAgeing'] = $dataAgeing;	
	return $data_items;
}
}
/////////////////////////////////////////////////////////////////////////
// ------------------------	SETUP PDF -------------------------------- //
////////////////////////////////////////////////////////////////////////
$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER
$title = 'eStatement';
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor( $globalsetting['company_name']);
$pdf->SetTitle( $globalsetting['company_name']);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject($globalsetting['company_name']." - $title");
$pdf->SetKeywords('Invoice, eStatement');
//\t\t\t\t\t
$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t";

$pdf_title = $gtab. $globalsetting['company_name'].' - '.$title;

define ('PDF_HEADER_STRING2',$gtab.$globalsetting['company_name']."\n"
							 .$gtab.trim_input($globalsetting['company_address'])."\n"
							 //.$tab.$globalsetting['company_suburb']."\n"
							 //.$tab.$globalsetting['company_postcode']."\n"
							 //.$tab.$globalsetting['company_city']."\n"
							 .$gtab.$globalsetting['company_phone']."\n"
							 .$gtab.$globalsetting['company_email']."\n"
							 .$gtab.$globalsetting['company_reg']."\n"
							 .$gtab.$globalsetting['company_url']
							 );

define ('PDF_HEADER_LOGO_CUSTOM',$globalsetting['company_logo']);
define ('PDF_HEADER_LOGO_WIDTH_CUSTOM', 70);

// set default header data PDF_HEADER_STRING // -- array(0,64,255) rgb color:
$pdf->SetHeaderData(PDF_HEADER_LOGO_CUSTOM, PDF_HEADER_LOGO_WIDTH_CUSTOM, $pdf_title, PDF_HEADER_STRING2, array(0,0,0), array(0,0,0));

$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 10, '', true);
$pdf->AddPage();
/////////////////////////////////////////////////////////////////////////
// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 
'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = file_get_contents('statement_header.php');
$data = $customerObject;
$html = mapping_data_header($html,$data);
$dataItems  = null;
$html_items = '';
$invoice_transactional_data = array_unique($invoice_transactional_data, SORT_REGULAR);
// -- sort by id.
// -- Reference : 
$invoice_transactional_data = sort_by($invoice_transactional_data,'issue_date');
//print_r($invoice_transactional_data);
//$unique_invoice_data = array_unique($invoice_transactional_data);
$dataItems 	= mapping_data_items($invoice_transactional_data,$invoice,$item,$tax,$type);
$html_items = $dataItems['html'];
$dataAgeing = $dataItems['dataAgeing'];
// -- Merge two arrys.
$globalsetting = array_merge($globalsetting, $dataAgeing);
$html_ageing = file_get_contents('statement_ageing.php');
$html_ageing = mapping_data_header($html_ageing,$data);
$html_ageing = mapping_data_ageing($html_ageing,$globalsetting);
$html = $html.$html_items.$html_ageing;
//echo $html;
////////////////////////////////////////////////////////////////////////
$fileName = 'statement.pdf';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
//$pdf->Output($fileName, 'I');
output_pdf($pdf,$emailFlag,$download,$fileName,$id,$data,$companyname,$companyEmail);
//echo "Tumelo";
?>