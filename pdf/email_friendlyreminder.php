<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php 	
if(!function_exists('gethref'))
{
	function gethref()
	{
		$href = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$href = str_replace("pdf_invoice.php","",$href);
		$href = str_replace("pdf_quotation.php","",$href);
		$href = str_replace("/pdf/","",$href);

		// -- from webservices
		$href = str_replace("webservices","",$href);
		$href = str_replace("generateinvoices.php","",$href);
		$href = str_replace("auto_updateinvoices.php","",$href);
		$href = str_replace("auto_followup_dueinvoices.php","",$href);
		$href = str_replace("auto_updaterecurringinvoices.php","",$href);
		
		return $href;
	}
}							
//------------------------------------------------------------------- 
//           				BOC - GLOBAL DATA						-
//-------------------------------------------------------------------
$password = '';
$website  = '';
				
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$idnumber 		= '';
$AccountNumber 	= '';
$InvoiceNumber 	= '';
$DueDate 		= '';
$AmountDue 		= '';
$Currency 		= '';
$space 			= '';
$InvoiceMonth 	= '';
$logo 			= '';

// -- 09.11.2018
$bankname   	= '';
$account	    = '';
$accounttype    = '';
$branchname     = '';
$branchcode     = '';
$swiftcode      = '';

// ====================== Global Settings =========================== //
$GlobalCompany  = '';
// ======================= EOC Global Settings ====================== //
if(isset($_SESSION))
{	
// ====================== Invoice Settings =========================== //
$FirstName 		= $_SESSION['invFirstName'];
$InvoiceNumber  = $_SESSION['invInvoiceNumber'];
$DueDate 		= $_SESSION['invDueDate'];
$AmountDue 		= $_SESSION['invAmountDue'];

// ====================== Global Settings =========================== //
$GlobalCompany  = $_SESSION['company_name'];
$website 		= $_SESSION['company_url'];
$logo 			= $_SESSION['company_logo'];

$bankname   	= $_SESSION['bankname'];
$account	    = $_SESSION['account'];
$accounttype    = $_SESSION['accounttype'];
$branchname     = $_SESSION['branchname'];
$branchcode     = $_SESSION['branchcode'];
$swiftcode      = $_SESSION['swiftcode'];


//$website = "http://www.vogsphere.co.za/ims/";
$href = gethref();
$website = $href ;

/* --- BOC Production or Development */
// -- Email Server Hosting -- //
$Globalprod = $_SESSION['prod'];		
// -- Email Server Hosting -- //
	
/*	Production - Live System */
if($Globalprod == 'p')
{
	
}
/*	Development - Testing System */	
else
{
	$website = "http://www.vogsphere.co.za/ims/index";	
	$logo 	 = "vogsphere.png";
}
/* --- ECO Production or Development */
// ======================= EOC Global Settings ====================== //
}

//------------------------------------------------------------------- 
//           				EOC - GLOBAL DATA						-		-
//-------------------------------------------------------------------
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
		
    <!-- Favicon -->
	<link href="<?php if(!empty($website)){echo $website."/assets/img/Icon_vogs.ico";}else{}?>" rel="shortcut icon">
			
	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>
<body bgcolor="#f0f0f0" width="100%" style="margin: 0;">
    <center style="width: 100%; background: ##f0f0f0;">
        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
        </table>
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">            
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
				<td bgcolor="#f0f0f0">
					<?php echo "<img src='".$website.'/images/'.$logo."' width='600' height='' alt='".$website.'/images/'.$logo."' border='0' align='center' style='width: 100%; max-width: 600px;'/>";?>
				</td>
            </tr>
            <!-- Hero Image, Flush : END -->

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px;">
<?php 
				 echo "<h3>Dear $FirstName </h3>
					  <p>We are contacting you in regard to Invoice# 1 that is currently outstanding.</p>
					  <p>This is a friendly reminder that the balance of R $AmountDue is due by $DueDate.</p>
					  <p>You can make the payment to the following banking details:</p>
					  <p>Please specify the invoice number in the payment reference – thank you.</p>
					  <p>Bank name    : $bankname </p>
					  <p>Account      : $account</p>
					  <p>Account Type : $accounttype</p>
					  <p>Branch Name  : $branchname</p>
					  <p>Branch code  : $branchcode</p>
					  <p>Swift code   : $swiftcode</p>
					  <p>If the invoice has already been paid, please disregard this message.</p>
					  <p>We greatly appreciate your business.</p>
					  <p>Kind Regards,</p>
					  <p>$GlobalCompany
					  </p>";	  
?>              </td>
            </tr>
        </table>
    </center>
</body>
</html>