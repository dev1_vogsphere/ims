<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
if(!function_exists('gethref_pdf'))
{
	function gethref_pdf($companyLogo)
	{
		$href = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$href = str_replace("pdf_invoice.php",$companyLogo,$href);
		$href = str_replace("pdf_quotation.php",$companyLogo,$href);	
		$href = str_replace("pdf","images",$href);
		
		// -- from invoice/index page
		$href = str_replace("index.php","images/".$companyLogo,$href);

		// -- from webservices
		$href = str_replace("webservices","images",$href);		
		$href = str_replace("generateinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_updateinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_followup_dueinvoices.php",$companyLogo,$href);
		$href = str_replace("auto_updaterecurringinvoices.php",$companyLogo,$href);
		
		return $href;
	}
}		
if (!defined('ROOT'))
{
	define('ROOT', str_replace("pdf/pdf_invoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
    define('ROOT_PDF', str_replace("pdf/pdf_invoice.php", "", $_SERVER["SCRIPT_FILENAME"]));
	require(ROOT. '/core/Model.php');
	require(ROOT. '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/customer.php');	
	require(ROOT. '/models/globalsetting.php');		
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
else
{
	require(ROOT. '/core/Model.php');
	require(ROOT . '/config/tcpdf_config_alt.php');
	require(ROOT. '/config/db.php');
	require(ROOT. '/models/invoice.php');
	require(ROOT. '/models/customer.php');	
	require(ROOT. '/models/tax.php');
	require(ROOT. 'models/globalsetting.php');
	require(ROOT. '/core/Helper.php');
	// Logo are update on page tcpdf_autoconfig.php (Under root directory)
	// Include the main TCPDF library (search for installation path).
	require_once(ROOT. 'tcpdf_include.php');
	require_once(ROOT. 'tcpdf.php');
	require_once(ROOT. "FPDI/fpdi.php");
}
// -------------------------------------------------- DATA -------------------------------------- // 
// -- Retrieve the invoice id.
$id = null;
$username = '';
$currency 		= '';
$companyname 	= '';
$companyAddress = '';
$companyphone   = '';
$companyEmail	= '';
$companyReg		= '';
$companyLogo 	= '';
$companyUrl     = '';
$VAT = '';
$ext = '';

// -- GET if we do print & preview
if ( !empty($_GET['id'])) 
{
	$id = $_GET['id'];	
}
else
{
	$id  = 0;
}

if(!empty($_GET['username'])){$username = $_GET['username'];}

/* -- Multiple invoice id.*/
if(isset($_POST['id']))
{
	$id = $_POST['id'];
}

if(!empty($_POST['username'])){$username = $_POST['username'];}

// -- check id SESSIONS
if(isset($_SESSION['id']))
{
	if(!empty($_SESSION['id']))
	{
		if($_SESSION['id'] != 0)
		{
			$id = $_SESSION['id'];
			$_SESSION['id'] = '';
		}
	}
}

// -- 1.Download to pdf.
$download = '';
if ( !empty($_GET['download'])) 
{
	$download = $_GET['download'];	
}

// -- 2.EmailFlag.
$emailFlag = '';
if ( !empty($_GET['emailFlag'])) 
{
	$emailFlag = $_GET['emailFlag'];	
}
/* -- Send Multiple Emails */
if(isset($_POST['emailFlag']))
{
	$emailFlag = $_POST['emailFlag'];
}

// -- check emailFlag SESSIONS
if(isset($_SESSION['emailFlag']))
{
		if(!empty($_SESSION['emailFlag']))
		  {
			$emailFlag = $_SESSION['emailFlag'];	
			$_SESSION['emailFlag'] = '';
		  }
}

// -- Get the Invoice details.
// -- Get Tax Master Data.
	$tax = new tax();
	$dTax['taxes'] = $tax->showAll();	
	
// -- Invoice Data.	
    $invoice= new invoice();
    $invoice_data = $invoice->showInvoice($id);
	
// -- Get the Invoice Items details.
    $invoice_items = $invoice->show($id);

// -- Get the Invoice Payments details.
	$payments_data = $invoice->payments($id);

// -- Read from invoice.
	if(empty($username))
	{
		$username = $invoice_data['createdby'];
	}	
	
// -- Read from organsation details
	if(empty($username))
	{
		// -- Customer.
		$cusobj = new customer();
		$identification = $invoice_data['customer_identification'];
		$customerObject = $cusobj->showbyidentification($identification);
		$organsation    = $customerObject[0]['organisation']; 
		// -- User.
		$userobj = new globalsetting();		
		$userobject     = $userobj->showbyorganisation($organsation);
		$username 		= $userobject['username'];
	}	
	
/* -- BOC - Global Settings -- */
// -- Global Setting Controller.
$globalsetting  = new globalsetting();
$globalsettings = $globalsetting->show($username);	
//print('emailFlag : '.$id);
//print('username : '.$username);
//print('emailFlag : '.$emailFlag);
	
// -- Bind Global Data into Session.
$globalsetting = $globalsettings;

if(!empty($globalsettings))
{	  
 // -- Global Settings	  
  if(!empty($globalsetting['company_name']))   {$_SESSION['company_name'] 	 = $globalsetting['company_name'];}
  if(!empty($globalsetting['company_address'])){$_SESSION['company_address'] = $globalsetting['company_address'];}
  if(!empty($globalsetting['company_email'])) {$_SESSION['company_email']   = $globalsetting['company_email'];}
  if(!empty($globalsetting['company_fax']))    {$_SESSION['company_fax'] 	 = $globalsetting['company_fax'];}
  if(!empty($globalsetting['company_logo']))   {$_SESSION['company_logo'] 	 = $globalsetting['company_logo']; $companyLogo  = $globalsetting['company_logo'];}
  if(!empty($globalsetting['company_phone']))  {$_SESSION['company_phone'] 	 = $globalsetting['company_phone'];}
  if(!empty($globalsetting['company_url']))    {$_SESSION['company_url'] 	 = $globalsetting['company_url'];}
  if(!empty($globalsetting['currency']))       {$_SESSION['currency'] 		 = $globalsetting['currency'];}
  if(!empty($globalsetting['currency_decimals'])){$_SESSION['currency_decimals'] = $globalsetting['currency_decimals'];}
  if(!empty($globalsetting['default_template'])){$_SESSION['default_template'] 	 = $globalsetting['default_template'];}
  if(!empty($globalsetting['last_calculation_date'])){$_SESSION['last_calculation_date'] = $globalsetting['last_calculation_date'];}
  if(!empty($globalsetting['legal_terms'])){$_SESSION['legal_terms']		= $globalsetting['legal_terms'];}
  if(!empty($globalsetting['pdf_orientation'])){$_SESSION['pdf_orientation'] 	= $globalsetting['pdf_orientation'];}
  if(!empty($globalsetting['pdf_size'])){$_SESSION['pdf_size']			= $globalsetting['pdf_size'];}
  if(!empty($globalsetting['sample_data_load'])){$_SESSION['sample_data_load'] 	= $globalsetting['sample_data_load'];}
  if(!empty($globalsetting['siwapp_modules'])){$_SESSION['siwapp_modules'] 	= $globalsetting['siwapp_modules'];}
  if(!empty($globalsetting['company_reg'])){$_SESSION['company_reg'] 	= $globalsetting['company_reg'];}
  if(!empty($globalsetting['VAT'])){$_SESSION['VAT'] 	= $globalsetting['VAT'];}
  
  // -- SMTP Settings
  if(!empty($globalsetting['host'])) {$_SESSION['host'] 	 = $globalsetting['host'];}
  if(!empty($globalsetting['port'])) {$_SESSION['port']	 = $globalsetting['port'];}
  if(!empty($globalsetting['secure'])) {$_SESSION['secure']	 = $globalsetting['secure'];}
  if(!empty($globalsetting['emailusername'])) {$_SESSION['emailusername'] 	 = $globalsetting['emailusername'];}
  if(!empty($globalsetting['emailpassword'])) {$_SESSION['emailpassword'] 	 = $globalsetting['emailpassword'];}
  if(!empty($globalsetting['prod'])) {$_SESSION['prod']	 = $globalsetting['prod'];}
  if(!empty($globalsetting['dev'])) {$_SESSION['dev']	 = $globalsetting['dev'];}
  if(!empty($globalsetting['captcha'])) {$_SESSION['captcha'] 	 = $globalsetting['captcha'];}
	
 // -- Bank Details
   if(!empty($globalsetting['account']))	{$_SESSION['account'] 	= $globalsetting['account'];}
   if(!empty($globalsetting['accounttype'])){$_SESSION['accounttype'] 	= $globalsetting['accounttype'];}
   if(!empty($globalsetting['bankname']))	{$_SESSION['bankname'] 	= $globalsetting['bankname'];}
   if(!empty($globalsetting['branchcode'])) {$_SESSION['branchcode'] 	= $globalsetting['branchcode'];}
   if(!empty($globalsetting['branchname'])) {$_SESSION['branchname'] 	= $globalsetting['branchname'];}
   if(!empty($globalsetting['swiftcode'])) {$_SESSION['swiftcode'] 	= $globalsetting['swiftcode'];}
  }
/* -- EOC - Global Settings -- */	

// -- $currency.
if(isset($_SESSION['currency'])){$currency 		= $_SESSION['currency'];     }
if(isset($_SESSION['company_name'] 	    )){$companyname 	= $_SESSION['company_name'];     }
if(isset($_SESSION['company_address']   )){$companyAddress = $_SESSION['company_address'];}
if(isset($_SESSION['companyphone']     )){$companyphone   = $_SESSION['company_phone'] ; }
if(isset($_SESSION['company_email']	    )){$companyEmail	= $_SESSION['company_email'];    }
if(isset($_SESSION['company_reg']		)){$companyReg		= $_SESSION['company_reg'];  }
if(isset($_SESSION['company_logo'] 	    )){$companyLogo 	= $_SESSION['company_logo'];     }
if(isset($_SESSION['company_url']       )){$companyUrl     = $_SESSION['company_url'];    }
if(isset($_SESSION['VAT']       )){$VAT     = $_SESSION['VAT'];    }
$_SESSION['invoicetype'] = 'Invoice';

$ext = pathinfo($companyLogo, PATHINFO_EXTENSION);
$href = gethref_pdf($companyLogo);
//echo $companyLogo.'<br/>';
//  ------------------------------------------------------ END OF DATA  -------------------------------------

// -- Print content to pdf.
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);


//$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER
if (!function_exists('tokenTruncate')) 
{
	function tokenTruncate($string, $your_desired_width) 
	{
	  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	  $parts_count = count($parts);

	  $length = 0;
	  $last_part = 0;
	  for (; $last_part < $parts_count; ++$last_part) {
		$length += strlen($parts[$last_part]);
		if ($length > $your_desired_width) { break; }
	  }

	  return implode(array_slice($parts, 0, $last_part));
	}
}

if (!function_exists('getItemTaxPercentage'))
{
	function getItemTaxPercentage($itemid)
	{	
		// -- Get Tax Master Data.
		$tax = new tax();
		$invoice_items_tax = $tax->invoice_item_taxes($itemid);
		$item_tax = null;
		$tax_id = 0;
		$tax_percentage = 0.00;
		foreach ($invoice_items_tax as $row) 
		{
			$tax_id = $row['tax_id'];
		}
		// -- check if tax id is empty.
		if(!empty($tax_id))
		{
			$item_tax = $tax->show($tax_id );
			$tax_percentage = $item_tax['value'];
		}
		return $tax_percentage; 
	}
}

if (!class_exists('MYPDF')) 
{
class MYPDF extends TCPDF 
{
	public static $companyname = '';
	public static $companyAddress = '';
	public static $companyReg = '';
	public static $companyEmail = '';
	public static $companyUrl = '';
	public static $VAT = '';
	public static $href = '';
	public static $ext = '';
	
	public function Header() 
	{
		$this->setJPEGQuality(90);
		//$this->Image('ecashmeup.png', 120, 10, 75, 0, 'PNG', 'https://www.finalwebsites.com');
 		$this->Image(MYPDF::$href, 120, 1, 75, 0, MYPDF::$ext , MYPDF::$companyUrl);
		// -- needs to be change to be dynamic
		
 // Company Details 
$this->CreateTextBox('Company: '.MYPDF::$companyname  , 0, 10, 0, 10, 10, '','B');
$this->CreateTextBox('Address: '.MYPDF::$companyAddress,        0, 15, 80, 10, 10);
$this->CreateTextBox('Registration No: '.MYPDF::$companyReg,     0, 20, 80, 10, 10);
$this->CreateTextBox('Email: '.MYPDF::$companyEmail,     0, 25, 80, 10, 10);
$this->CreateTextBox('Web: '.MYPDF::$companyUrl,     0, 30, 80, 10, 10);
					
// -- VAT number.
$this->CreateTextBox('VAT: '.MYPDF::$VAT,     		0, 35, 80, 10, 10);
$position = 45;
//$this->Line(0, $position, 395, $position);


	}
	public function Footer() {
		$this->SetY(-15);
		$this->SetFont(PDF_FONT_NAME_MAIN, 'I', 8);
		$this->Cell(0, 10, MYPDF::$companyname.' - Registration Number: '.MYPDF::$companyReg, 0, false, 'C');
	}

	public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 10, $fontsize = 10, $fontstyle = '', $align = 'L') {
		$this->SetXY($x+20, $y); // 20 = margin left
		$this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
		$this->Cell($width, $height, $textval, 0, false, $align);
	}
}
}
// create a PDF object
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	MYPDF::$companyname 	= $companyname;
	MYPDF::$companyname 	= $companyname;
	MYPDF::$companyAddress 	= trim($companyAddress);
	MYPDF::$companyReg 		= $companyReg;
	MYPDF::$companyEmail 	= $companyEmail;
	MYPDF::$companyUrl 		= $companyUrl;
	MYPDF::$VAT 			= $VAT;
	MYPDF::$href 			= $href;
    MYPDF::$ext				= $ext;

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$html = <<<EOD

EOD;

//<table><tr><td>Tumelo</td><td>Modise</td></tr></table>';

// Print text using writeHTMLCell()
//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// -- 30 Invoice Items per page
$count = 0;
$perPage = 20;

// Company Details 
/*$pdf->CreateTextBox('Company: Vogsphere (Pty) Ltd'  , 0, 10, 0, 10, 10, '','B');
$pdf->CreateTextBox('Address: 43 Mulder Str, 31 SEOUL, Centurion, 0158',        0, 15, 80, 10, 10);
$pdf->CreateTextBox('Registration No: 2012/020274/74/07',     0, 20, 80, 10, 10);
$pdf->CreateTextBox('Email: info@vogsphere.co.za',     0, 25, 80, 10, 10);
$pdf->CreateTextBox('Web: www.vogsphere.co.za',     0, 30, 80, 10, 10);
*/

// create address box
$pdf->CreateTextBox('Invoiced To', 0, 50, 80, 10, 10, 'B');
$pdf->CreateTextBox('Customer Name :'.$invoice_data["customer_name"], 0, 55, 80, 10, 10, '	');
$pdf->CreateTextBox('Customer Code :'.$invoice_data["customer_identification"], 0, 60, 80, 10, 10);
$pdf->CreateTextBox('Contact Name :'.$invoice_data["contact_person"], 0, 65, 80, 10, 10);
$pdf->CreateTextBox('Physical Address :'.$invoice_data["invoicing_address"], 0, 70, 80, 10, 10);
$pdf->CreateTextBox('Email :'.$invoice_data["customer_email"], 0, 75, 80, 10, 10);
 
// invoice title / number
//$pdf->CreateTextBox('TAX INVOICE #'.$invoice_data["id"], 0, 90, 120, 20, 16);
 
// date, order ref
$pdf->CreateTextBox('Issue Date: '.$invoice_data["issue_date"], 0, 100, 0, 10, 10, '', 'R');
$pdf->CreateTextBox('Due Date:'.$invoice_data["due_date"], 0, 105, 0, 10, 10, '', 'R');

// 
// list headers
$pdf->CreateTextBox('Product or service', 0, 120, 90, 10, 10, 'B');
$pdf->CreateTextBox('Price', 50, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Quantity', 80, 120, 20, 10, 10, 'B', 'C');
$pdf->CreateTextBox('Taxes', 85, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Discount', 110, 120, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Amount', 140, 120, 30, 10, 10, 'B', 'R');
 
$pdf->Line(20, 129, 195, 129);
 
// some example data
$orders[] = array('quant' => 5, 'descr' => '.com domain registration', 'price' => 9.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 3, 'descr' => '.net domain name renewal', 'price' => 11.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 1, 'descr' => 'SSL certificate 256-Byte encryption', 'price' => 99.95,'taxes' => 0.00,'disc' => 0.00);
$orders[] = array('quant' => 1, 'descr' => '25GB VPS Hosting, 200GB Bandwidth', 'price' => 19.95,'taxes' => 0.00,'disc' => 0.00);
 
$currY = 128;
$total = 0;
$discount_amnt = 0.00;
$total_discount_amnt = 0.00;

$tax_invoice = 0.00;
foreach ($invoice_items as $row) 
{
	$text = tokenTruncate($row['description'],40);
	$discount_percentage = 0;
	$itemid = $row['id'];
	$tax_percentage = 0.00;
	$discount_amnt = 0.00;

	//$text = $row['description'];
	// -- tax
	$tax_percentage = getItemTaxPercentage($itemid);
	$tax_percentage = number_format($tax_percentage,2);
	$tax_invoice  = $tax_invoice + $tax_percentage;
	$pdf->CreateTextBox($text, 0, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox($currency.number_format($row['unitary_cost'],2), 50, $currY, 30, 10, 10, '', 'R');
	$pdf->CreateTextBox(number_format($row['quantity'],0), 80, $currY, 20, 10, 10, '', 'C');
	$amount = $row['quantity']*$row['unitary_cost'];
	$pdf->CreateTextBox($tax_percentage.'%', 100, $currY, 20, 10, 10, '', 'C');//-taxes
	$pdf->CreateTextBox($row['discount'].'%', 120, $currY, 20, 10, 10, '', 'C');
	//$pdf->CreateTextBox($currency.$amount, 140, $currY, 30, 10, 10, '', 'R');
	
// -- discount 
  if(!empty($row["discount"]))
  {
	$discount_percentage  = number_format($row["discount"], 2, '.', '');
	$discount_amnt = $discount_amnt + (( $discount_percentage / 100) * $amount);
	$amount = $amount - $discount_amnt;
  }
   
   $total_discount_amnt =  $total_discount_amnt + $discount_amnt;
   $pdf->CreateTextBox($currency.number_format($amount,2), 140, $currY, 30, 10, 10, '', 'R');

	$currY = $currY+5;
	$total = $total+$amount;
	//$pdf->Line(20, $currY+4, 195, $currY+4);

	$count++;
	// -- Add a new page after 30 invoice items.
	if ($count % $perPage == 0) {$pdf->AddPage(); $currY = 40;}
}
$pdf->Line(20, $currY+4, 195, $currY+4);

// -- Title of the Invoice
if($tax_invoice > 0)
{
// invoice title / number
$pdf->CreateTextBox('TAX INVOICE #'.$invoice_data["id"], 0, 90, 120, 20, 16);
}
// -- Normal Invoice
else
{
// invoice title / number
$pdf->CreateTextBox('INVOICE #'.$invoice_data["id"], 0, 90, 120, 20, 16);	
}
// output the total row
$pdf->CreateTextBox('Subtotal', 10, $currY+5, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($total, 2, '.', ''), 140, $currY+5, 30, 10, 10, 'B', 'R');

// Add Total tax to Total Invoice Amount.
$total = $total + number_format($invoice_data["tax_amount"], 2, '.', '');

$pdf->CreateTextBox('Discount', 10, $currY+10, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($total_discount_amnt, 2, '.', ''), 140, $currY+10, 30, 10, 10, 'B', 'R');

$pdf->CreateTextBox('Taxes', 10, $currY+15, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($invoice_data["tax_amount"], 2, '.', ''), 140, $currY+15, 30, 10, 10, 'B', 'R');

// -- Add Tax to Total Amount and substracting discount amount.
$amount = $invoice_data["tax_amount"];
$total = $total-$invoice_data["discount_amount"];

$pdf->CreateTextBox('Total', 10, $currY+20, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($total, 2, '.', ''), 140, $currY+20, 30, 10, 10, 'B', 'R');

/*Comment out payment Transactions - 05.11.2018
 // Payments headers
$currY = $currY + 20;
// invoice title / number
$pdf->CreateTextBox('Payments Transactions', 0, $currY, 120, 20, 16);
$currY = $currY + 10;

$pdf->CreateTextBox('Payment Date', 0, $currY, 90, 10, 10, 'B');
$pdf->CreateTextBox('Payment Reference', 35, $currY, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Amount', 100, $currY, 20, 10, 10, 'B', 'C');

$currY = $currY + 10;

$pdf->Line(20, $currY, 195, $currY);

// some payment instructions or information
$payments[] = array('date' => date('Y-m-d'), 'prn' => '.com domain registration', 'amount' => 9.95);
$totalPayment = 0;
$balance = 0.00;

$count = 0;

foreach ($payments_data as $row) 
{
	$pdf->CreateTextBox($row['date'], 0, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox($row['notes'], 30, $currY, 90, 10, 10, '');
	$pdf->CreateTextBox(number_format($row['amount'],2), 105, $currY, 90, 10, 10, '');
	$amount = $row['amount'];

	$currY = $currY+5;
	$totalPayment = $totalPayment+$amount;
	
		$count++;
	// -- Add a new page after 30 invoice items.
	if ($count % $perPage == 0) {$pdf->AddPage(); $currY = 40;}

}

$pdf->Line(20, $currY+4, 195, $currY+4);
$pdf->CreateTextBox('Total', 10, $currY+5, 135, 10, 10, 'B', 'R');
$pdf->CreateTextBox($currency.number_format($totalPayment, 2, '.', ''), 140, $currY+5, 30, 10, 10, 'B', 'R');
$pdf->CreateTextBox('Balance', 10, $currY+10, 135, 10, 10, 'B', 'R');
$balance = $totalPayment - $total;
$pdf->CreateTextBox($currency.number_format($balance, 2, '.', ''), 140, $currY+10, 30, 10, 10, 'B', 'R');
Comment out payment Transactions  - 05.11.2018*/

// ====================== Invoice Settings Session =========================== //
$_SESSION['invFirstName']	  = $invoice_data["customer_name"];
$_SESSION['invInvoiceNumber'] = $invoice_data["id"];
$_SESSION['invDueDate'] 	  = $invoice_data["due_date"];
$_SESSION['invAmountDue']	  = $total;

// -- Notes
// invoice title / number
$pdf->CreateTextBox('Notes', 0, $currY+6, 120, 20, 16);
$pdf->setXY(20, $currY+20);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 17, $invoice_data["notes"], 0, 'L', 0, 1, '', '', true, null, true);

/* -- Terms -- */
$pdf->CreateTextBox('Terms and Conditions', 0, $currY+36, 120, 20, 16);
$pdf->setXY(20, $currY+50);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 10, $invoice_data["terms"], 0, 'L', 0, 1, '', '', true, null, true);

/*
$pdf->setXY(20, $currY+20);
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
$pdf->MultiCell(175, 10, '<em>Lorem ipsum dolor sit amet, consectetur adipiscing elit</em>. 
Vestibulum sagittis venenatis urna, in pellentesque ipsum pulvinar eu. In nec <a href="http://www.google.com/">nulla libero</a>, eu sagittis diam. Aenean egestas pharetra urna, et tristique metus egestas nec. Aliquam erat volutpat. Fusce pretium dapibus tellus.', 0, 'L', 0, 1, '', '', true, null, true);
*/

$eStatement = 'invoice_'.$id;
$fileName = $eStatement.'.pdf';
//$_SESSION['invFileName'] = $fileName;


if(empty($emailFlag))
{
	if(empty($download))	
	{
		$pdf->Output($fileName, 'I');
	}
	elseif($download  == 'Y')
	{// -- F = Save to file.
	 // -- D = Download
	 // -- S = 
		$pdf->Output($fileName, 'D');
	}
	else
	{
		echo 'id - download - empty = '.$id;
	}	
}
else
{
	if($id != 0 && !empty($id))
	{
		$emailaddr = $invoice_data["customer_email"];
		$subject   = 'Invoice from '.$companyname;
		$body 	   = ROOT."pdf/"."email_newinvoice.php";
		
		// -- Email Template.
		if(isset($_SESSION['email_template']))
		{
			if(!empty($_SESSION['email_template']))
			{
				$body = ROOT."pdf/".$_SESSION['email_template'];
			}
		}
		
		// -- Email Subject.
		if(isset($_SESSION['email_subject']))
		{
			if(!empty($_SESSION['email_subject']))
			{
				$subject = $_SESSION['email_subject'];
			}
		}
		
		//'Dear '.$invoice_data["customer_name"].' Your invoice and statement are attached.'.'Best regards,The '.$companyname.' Team.';
		$invoice->updateSentByEmail($id);
		$fileatt = $pdf->Output($fileName, 'S');
		$message = Helper::SendEmailWithAttachmentGeneric($fileatt,$fileName,$companyEmail,$emailaddr,$subject,$body);
	}	
}

?>