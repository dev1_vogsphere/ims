<?php
Helper::menu('','','','','','active');
$menu = ROOT.'views/Layouts/menu.php';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

include_once($menu);?>  
  <section id="bd">
    
<div class="container">
<?php if(isset($_POST['productid']))
{
	if(($_POST['productid']) > 0)
	{?>	
			
		<div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            Product <? echo $_POST['productid'];?> added. 
		</div>
<?php 
	}
}?>
<?php
if(isset($_POST['error']))
{
	if(!empty($_POST['error']))
	{
	echo '<div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            '.$_POST['error'].'
	</div>';
	}
}
?>
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
      <h2>Product </h2>
    </header>

    <form name="product" method="post" action="#" class="form-stacked">
     <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
	
      <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="product_reference">Product/Service Name</label>
		  <input type="text" id="product_reference" maxlength="35" name="product_reference" required="required" maxlength="100" class="form-control" value="<?php if(isset($_POST['product_reference'])){echo $_POST['product_reference'];}else{ echo '';}?>"/></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="product_price">Price</label>
		  <input type="number" placeholder = "0.00" id="product_price" name="product_price" step="0.01" required="required" class="form-control" value="<?php if(isset($_POST['product_price'])){echo $_POST['product_price'];}else{ echo '0.00';}?>"/></div>
        </div>
		</div>      
	   <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group">
		  <label class="control-label" for="product_description">Description</label>
			<textarea id="product_description" maxlength="35"  required="required" name="product_description" class="form-control"><?php if(isset($_POST['product_description'])){echo $_POST['product_description'];}else{echo '';}?></textarea>
		  </div>
        </div>
		<div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="title">IMS Organisation Name</label>
				<input type="text" class="form-control" id="organisation" placeholder="name of organisation" name="organisation" readonly value="<?php if(!empty($companyname)){echo $companyname;}else{ echo '';}?>">
			</div>
		</div>
		</div>
	    <h3>Investor/Sponsor/Customer data <a class="btn btn-default btn-primary" href = "javascript:clear();" name="clear">Clear</a></h3>
        <div class="row">     
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_customer_name">Name</label><input type="text" id="invoice_customer_name" name="invoice_customer_name" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_name'])){echo $_POST['invoice_customer_name'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_customer_identification">Customer Code</label><input readonly type="text" id="invoice_customer_identification" name="invoice_customer_identification" maxlength="128" class="form-control" value="<?php if(isset($_POST['invoice_customer_identification'])){echo $_POST['invoice_customer_identification'];}else{ echo '';}?>"/></div>
            </div>
		</div>
        <div class="row">
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_contact_person">Email Address</label>
               <input readonly type="email" id="invoice_customer_email" name="invoice_customer_email"  maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_email'])){echo $_POST['invoice_customer_email'];}else{ echo '';}?>"/>
              </div>
            </div>
			<div class="col-md-4 clearfix">
				<div class="form-group"><label class="control-label" for="invoice_customer_id">Customer ID</label>
				  <input type="text" id="invoice_customer_id" name="invoice_customer_id" maxlength="255" class="form-control" readonly value="<?php if(isset($_POST['invoice_customer_id'])){echo $_POST['invoice_customer_id'];}else{ echo '';}?>"/></div>			
			</div>			
		</div>
      <input type="hidden" id="product__token" name="product[_token]" value="mR9PiAbv_0cYx6WCg_kBTgHVMz20AZ7na8_CxxMYHIU" />
   	  <input type="hidden" id="error" name="error" value="<?php if(isset($_POST['error'])){echo $_POST['error'];}else{echo '';}?>" />
 
	  <input type="hidden" id="productid" name="productid" value="<?php if(isset($_POST['productid'])){echo $_POST['productid'];}else{echo 0;}?>" />

      <div class="form-actions">
      <!--  <input type="submit" class="btn btn-default btn-primary" name="save" value="Save"> -->
		
		    <button type="submit" class="btn btn-default btn-primary">Save</button>

        <div class="float-right">
  		   <a class="btn btn-default btn-primary" href = "/ims/product/index" name="back">Back</a>		
        </div>
      </div>
    
    </form>

  </article>

    </div>
  </section>
<script>
	function clear()
	{
		jQuery('#invoice_customer_id').val("");
		jQuery('#invoice_customer_name').val("");
		jQuery('#invoice_customer_email').val("");
		jQuery('#invoice_customer_identification').val("");
	}
  jQuery(document).ready(function () 
  {
	comp = document.getElementById("companyname").value;
	compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;
	 
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#invoice_customer_id').val(ui.item.id);
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
});	
</script>	