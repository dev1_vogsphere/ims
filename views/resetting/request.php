<div class="row">
  <div class="col-sm-6 col-md-4 col-md-offset-4">

    <h1 class="text-center login-title"><a href="/<?php echo DIR; ?>/login/index">Sign in</a> to continue to Siwapp</h1>
    <div class="account-wall">
      <div class="logo text-center">
        <img src="/<?php echo DIR; ?>/libs/img/logo.svg" alt="Siwapp" />
      </div>
      
<form action="/ims2/web/resetting/send-email" method="POST" class="fos_user_resetting_request form-signin">
    <div class="form-group">
                <label for="username">Username or email address</label>
        <input type="text" id="username" name="username" required="required"  class="form-control" />
    </div>
    <div>
        <input type="submit" value="Reset password" class="btn btn-lg btn-primary btn-block" />
    </div>
</form>

    </div>
  </div>