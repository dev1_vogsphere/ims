<?php 
Helper::menu('','','','active','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

include_once($menu);?>
<section id="bd">
          <div class="searchform">
        <div class="container">
        <input type="hidden" id="companyname" name="companyname" value ="<?php echo $companyname; ?>"  />
		<input type="hidden" id="username" name="username" value ="<?php echo $username; ?>"  />
          
<form name="search_invoice" method="post" action="#" class="search form-inline">
<!-- /<?php //echo DIR; ?>/invoice> -->
  <fieldset>

    <div class="form-group"><label class="control-label" for="search_invoice_terms">Customer Name</label>
	<input type="text" id="search_invoice_terms" name="search_invoice_terms" class="form-control" 
	value="<?php if(isset($_POST['search_invoice_terms'])){ echo $_POST['search_invoice_terms'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_from">Date from</label>
	<input type="date" id="search_invoice_date_from" name="search_invoice_date_from" class="form-control" value="<?php if(isset($_POST['search_invoice_date_from'])){ echo $_POST['search_invoice_date_from'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_to">to</label>
	<input type="date" id="search_invoice_date_to" name="search_invoice_date_to" class="form-control" value="<?php if(isset($_POST['search_invoice_date_to'])){ echo $_POST['search_invoice_date_to'];}?>"/></div>
	
    <div class="form-group float-right btn-toolbar">
      <div class="btn-group">
        <button id="searchsubmit" name="searchsubmit"  type="submit" class="btn btn-default btn-primary">Search</button>
        <a href="#search-secondary" data-toggle="collapse" class="btn btn-default"" aria-controls="search-secondary">
          Advanced        <span class="caret"></span>
        </a>
      </div>
      <div class="btn-group">
        <a id="search-reset" href="/<?php echo DIR; ?>/invoice/index" class="btn btn-default btn-warning">Reset</a>
      </div>
    </div>

  </fieldset>

<fieldset id="search-secondary" class="collapse">
<?php  
$statusDraft   = '';
$statusOpened  = '';
$statusOverdue = '';
$statusClosed  = '';
$status = '';

if(isset($_POST['search_invoice_status']))
		{ 	
					switch ($_POST['search_invoice_status']) 
					{
					case "0":
						$statusDraft   = 'selected';
						break;
					case "1":
						$statusClosed  = 'selected';
						break;
					case "2":
						$statusOpened  = 'selected';
						break;
					case "3":
						$statusOverdue = 'selected';
						break;
					default:
						$status = 'selected';
					}
		}else
		{ 	$status = '';
}
?>
      <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label>
	  <select id="search_invoice_status" name="search_invoice_status" class="form-control">
	  <option value=""  <?php echo $status;?>></option>
	  <option value="0" <?php echo  $statusDraft;?>>Draft</option>
	  <option value="2" <?php echo $statusOpened;?>>Opened</option>
	  <option value="3" <?php echo $statusOverdue;?>>Overdue</option>
	  <option value="1" <?php echo $statusClosed;?>>Closed</option></select></div>
      <div class="form-group"><label class="control-label" for="search_invoice_customer">Customer</label><input type="text" id="customer_identification" name="customer_identification" class="form-control" value="<?php if(isset($_POST['customer_identification'])){ echo $_POST['customer_identification'];}?>"/></div>
</fieldset>

    </form>
        </div>
      </div>
    
    <div class="container">
<!-- Email Quotations BOC -->              
		<div class="popup" data-popup="popup-1">
			<div class="popup-inner">
				<h2>eMail Quotation - Feedback</h2>
				<div id="myProgress">
					<div id="myBar">10%</div>
				</div>
				<div id="divSuccess">
				
				</div>
				<p><a data-popup-close="popup-1" href="#">Close</a></p>
				<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
			</div>
		</div>
	    <div id="divWebsites">
			<?php 
			$count = count($eMailInvoices); 
			for($i=0;$i<$count;$i++)
			{
			  $href = $eMailInvoices[$i];
			  echo '<button type="button" data-popup-open="popup-1" class="btn btn-info" onclick="setupLinks('.$href.')">eMail Invoice</button>';
			}
			?>	
		</div>		
<!-- Email Quotations EOC -->                  
<div id="invoices-latest-invoices">
<form name="invoice_list" method="post" action="/<?php echo DIR; ?>/quotation/index">
<ul id="toolbar" class="table-actions list-inline list-unstyled">
  <li>
    <button type="submit" name="print" id="print" class="btn btn-default" title="Print selected"><span class="glyphicon glyphicon-print"></span></button>
  </li>
  <li>
    <button type="submit" name="pdf" id="pdf" class="btn btn-default" title="Download selected as PDF"><span class="glyphicon glyphicon-download-alt"></span></button>
  </li>
  <li>
    <!-- <button hidden type="submit" name="email" id="email" class="btn btn-default" title="E-mail selected"><span class="glyphicon glyphicon-envelope"></span></button> -->
  </li>
</ul>

<table class="table table-condensed table-striped align-middle" data-type="invoices">
  <thead>
    <tr>
      <th class="cell-size-tiny cell-align-center"><input type="checkbox" name="all" /></th>
      <th class="cell-size-medium"><a class="sortable" href="#" title="Number">Number</a></th>
      <th><a class="sortable" href="#"  title="Customer">Customer</a></th>
      <th class="cell-size-medium"><a class="sortable" href="#"  title="Date">Date</a></th>
	  <th class="cell-size-small-medium"><a class="sortable" href="#" title="Sent">Sent</a></th>	  
      <th class="cell-size-small-medium cell-align-center"><a class="sortable" href="#"  title="Status">Status</a></th>
      <th class="cell-size-medium cell-align-right"><a class="sortable" href="#"  title="Total">Total</a></th>
      <th class="cell-size-medium"></th>
    </tr>
  </thead>
  <tbody>
  <?php
  	   $i = 0;
	   $invoices_count = 0;

        foreach ($invoices as $invoice)
        {
			// -- statuses.
			$Statusclass = ''; 
			if($invoice['status'] == DRAFT)
			{
				$Statusclass = 'draft'; 
			}

			if($invoice['status'] == CLOSED)
			{
				$Statusclass = 'closed'; 
			}

			if($invoice['status'] == OPENED)
			{
				$Statusclass = 'opened'; 
			}

			if($invoice['status'] == OVERDUE)
			{
				$Statusclass = 'overdue'; 
			}			
$tot = $invoice['base_amount']+$invoice['tax_amount'];
$href = htmlentities("href='/ims/pdf/pdf_quotation.php?id=".$invoice['id']."&emailFlag=Y"."&username=".$username."'");
$emailStatus = '';
$emailStatusText = '';

		if($invoice['sent_by_email'] == '0')
		{
			$emailStatus = 'draft';
			$emailStatusText = 'Not sent by e-mail';
		}
		else
		{
			$emailStatus = 'closed';
			$emailStatusText = 'sent by e-mail';
		}
			echo"<tr data-link='/".DIR."/invoice/editquotation/".$invoice['id']."'>
					<td class='table-action cell-align-center no-link'>
						<div class='form-group'><div class='checkbox'>                                        
						<label><input type='checkbox' id='invoice_".$i."' name='invoice_".$i."' value='".$invoice['id']."' /> </label>
						</div></div>
					</td>
					<td class='cell-size-medium'>".$invoice['id']."</td>
					<td>".$invoice['customer_name']."</td>
					<td>".$invoice['issue_date']."</td> 
					<td><span class='label $emailStatus'>$emailStatusText</span></td>					
					<td class='cell-align-center'><span class='label $Statusclass'>$Statusclass</span></td>
					<td class='cell-align-right'>$currency".number_format($tot,2)."</td>
					<td class='cell-align-right payments'>";
					echo '<button type="button" data-popup-open="popup-1" class="btn btn-info" onclick="setupLinks('.$href.')"><span class="glyphicon glyphicon-envelope"></span> eMail</button>';
					echo "
					</td>
				</tr>";
			  	   $i = $i + 1;

	  }
	  $invoice_count = $i;
	?>
 </tbody>
</table>	 
  <!-- //pending //notice //tax <input type="hidden" id="invoice_list__token" name="invoice_list[_token]" value="J3x1pXDJMqyI-u1bZGRFK2lqLdqlNSVzLlkJYK86TqM" />-->
    <input type="hidden" id="invoice_count" name="invoice_count" value="<?php echo $invoice_count; ?>" />

    </form>
<div id="payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="payments-modal-title">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="payments-modal-title">Payments</h4> </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
  </div>

    </div>
  </section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
     comp = document.getElementById("companyname").value;
	 compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;

    jQuery('#search_invoice_terms').autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#search_invoice_terms').val(ui.item.name);
		jQuery('#customer_identification').val(ui.item.identification);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });	
  
  // -- Email Quotation one at a time BOC //              
function setupLinks(href) 
{ 
    if (href) 
    { 
	emailFlag = 0;
	id = 0;
		username = document.getElementById('username').value;
	    str = href;

	    Startpostion = href.indexOf("id");			
	    urlString = href.substr(Startpostion); 
	    var res = urlString.split("&");
			
	    id=res[0].substr(res[0].indexOf("=") + 1);
	    emailFlag= res[1].substr(res[1].indexOf("=") + 1);

//alert(id);
//alert(emailFlag);
	    jQuery(document).ready(function()
	     {
		jQuery.ajax({	
					type: "GET",
  					url:"/ims/pdf/pdf_quotation.php",
					data:{id:id,username:username,emailFlag:emailFlag},
					success: function(data)
					{
						var Message = '';
					  Message = Message+"\n<p>Mail Succesfull sent to for quotation#:"+id+".</p>";
					  //alert(data);
					 jQuery("#divSuccess").append(Message);
					 			move();

					 
				    }
				});
			});
	//}
	// -- loop EOC
    }
}
</script>	