<?php
Helper::menu('','','','','','active');
$menu = ROOT.'views/Layouts/menu.php';
include_once($menu);?>

<section id="bd">
    
    <div class="container">
                  
      
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
      <h2>Product <?php echo $product["reference"];?></h2>
    </header>

  <form name="product" method="post" action="#" class="form-stacked">
      <div class="row">
        <div class="col-md-6 clearfix">
          <div class="form-group"><label class="control-label required" for="product_reference">Product code</label><input type="text" id="product_reference" name="product[reference]" required="required" maxlength="100" class="form-control" value ="<?php if (isset($product["reference"])) echo $product["reference"];?>" /></div>
        </div>
        <div class="col-md-6 clearfix">
          <div class="form-group"><label class="control-label required" for="product_price">Price</label><input type="text" id="product_price" name="product[price]" required="required" class="form-control" value ="<?php if (isset($product["price"])) echo $product["price"];?>" /></div>
        </div>
        <div class="col-md-12 clearfix">
          <div class="form-group"><label class="control-label" for="product_description">Description</label><textarea id="product_description" name="product[description]" class="form-control"><?php if (isset($product["description"])) echo $product["description"];?></textarea></div>
        </div>
      </div>

      <input type="hidden" id="product__token" name="product[_token]" value="mR9PiAbv_0cYx6WCg_kBTgHVMz20AZ7na8_CxxMYHIU" />
    

      <div class="form-actions">
        <input type="submit" class="btn btn-default btn-primary" name="save" value="Save">
        <div class="float-right">
                    <a class="btn btn-default btn-danger" href="/ims2/web/product/1/delete" data-confirm="Are you sure you want to delete the product?">Delete</a>
                  </div>
      </div>
    
    </form>

  </article>

    </div>
  </section>
  
  <section id="bd">
    
    <div class="container">
                  
      
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
      <h2>Product </h2>
    </header>

    <form name="product" method="post" action="/ims2/web/product/add" class="form-stacked">

      

      <div class="row">
        <div class="col-md-6 clearfix">
          <div class="form-group"><label class="control-label required" for="product_reference">Product code</label><input type="text" id="product_reference" name="product[reference]" required="required" maxlength="100" class="form-control" /></div>
        </div>
        <div class="col-md-6 clearfix">
          <div class="form-group"><label class="control-label required" for="product_price">Price</label><input type="text" id="product_price" name="product[price]" required="required" class="form-control" /></div>
        </div>
        <div class="col-md-12 clearfix">
          <div class="form-group"><label class="control-label" for="product_description">Description</label><textarea id="product_description" name="product[description]" class="form-control"></textarea></div>
        </div>
      </div>

      <input type="hidden" id="product__token" name="product[_token]" value="mR9PiAbv_0cYx6WCg_kBTgHVMz20AZ7na8_CxxMYHIU" />
    

      <div class="form-actions">
        <input type="submit" class="btn btn-default btn-primary" name="save" value="Save">
        <div class="float-right">
                  </div>
      </div>
    
    </form>

  </article>

    </div>
  </section>
