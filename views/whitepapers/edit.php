<?php
Helper::menu2('','','','','','','','active');
$menu = ROOT.'views/Layouts/menu.php';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

include_once($menu);
$current_pic = 'default.png';
$logoTemp    = ''; 

$new_doc = '';

if(isset($_FILES['new_doc']))
{
	$new_doc = $_FILES['new_doc']['name'];
}

if(isset($_FILES['new_pic']))
{
	$current_pic = $current_pic = '/'.DIR.'/uploads/'.$_FILES['new_pic']['name'];
}
else
{
  $current_pic = '/'.DIR.'/images/'.$current_pic; 
}
?>  
  <section id="bd">
    
<div class="container">
<?php if(isset($_POST['id']))
{
	if(($_POST['id']) > 0)
	{?>	
			
		<div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            White Paper <? echo $_POST['id'];?> updated. 
		</div>
<?php 
	}
}?>
<?php
if(isset($_POST['error']))
{
	if(!empty($_POST['error']))
	{
	echo '<div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            '.$_POST['error'].'
	</div>';
	}
}
?>
  <article class="invoice-like">
    <header id="invoice-like-title" class="clearfix">
      <h2>White Paper <?php echo $wps["name"];?></h2>
    </header>

   <form name="whitepapers" id="whitepapers" method="post" action="#" enctype="multipart/form-data" class="form-stacked">
     <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
	
      <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="name">Name</label>
		  <input type="text" id="name" maxlength="35" name="name" required="required" maxlength="100" class="form-control" value="<?php if(isset($wps['name'])){echo $wps['name'];}else{ echo '';}?>"/></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="product_price">category</label>
		  <input type="text" id="category" name="category"  required="required" class="form-control" value="<?php if(isset($wps['category'])){echo $wps['category'];}else{ echo '';}?>"/></div>
        </div>
		</div>      
	   <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group">
		  <label class="control-label" for="description">Description</label>
			<textarea id="description" maxlength="35"  required="required" name="description" class="form-control"><?php if(isset($wps['description'])){echo $wps['description'];}else{echo '';}?></textarea>
		  </div>
        </div>
		<div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="title">IMS Organisation Name</label>
				<input type="text" class="form-control" id="organisation" placeholder="name of organisation" name="organisation" readonly value="<?php if(!empty($companyname)){echo $companyname;}else{ echo '';}?>">
			</div>
		</div>
		</div>
        <div class="row">
		   <div class="col-md-4 clearfix">
			<h3>Upload a White Paper Picture</h3>		   
			<div class="form-group">
				 <img id="current_pic" name="current_pic" src="<?php echo $wps['picture']; ?>" title="current_pic" height="100" /><br/>
				 <input type="file" id="new_pic" name="new_pic"  accept=".jpg,.png,.jpge" />
				 <input type="text" name="logoTemp" id="logoTemp" value="<?php echo $logoTemp;?>" style="display:none"/>
			</div>
		  </div>
		  <div class="col-md-4 clearfix">
			<h3>Upload a White Paper Document</h3>		   
			<div class="form-group">
			<input type="file" name="new_doc" id="new_doc" value="<?php echo $wps['document']; ?>" accept=".pdf" />
			<br/> 
			<!-- <input type="submit" value="Upload"> 
			<input type="submit" class="btn btn-primary" value="Upload">	-->
			 <input type="text" class="form-control" id="olddoc" name="olddoc" readonly value="<?php echo $wps['document']; ?>"  />
			</div>
		  </div>
		</div>
		
     
   	  <input type="hidden" id="error" name="error" value="<?php if(isset($_POST['error'])){echo $_POST['error'];}else{echo '';}?>" />
 
	  <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}else{echo 0;}?>" />

      <div class="form-actions">
      <!--  <input type="submit" class="btn btn-default btn-primary" name="save" value="Save"> -->
		
		    <button type="submit" name="Save" id="Save" class="btn btn-default btn-primary">Save</button>

        <div class="float-right">
  		   <a class="btn btn-default btn-primary" href = "/ims/whitepapers/index" name="back">Back</a>		
        </div>
      </div>
    
    </form>

  </article>

    </div>
  </section>
<script>
	function clear()
	{
		jQuery('#invoice_customer_id').val("");
		jQuery('#invoice_customer_name').val("");
		jQuery('#invoice_customer_email').val("");
		jQuery('#invoice_customer_identification').val("");
	}
  jQuery(document).ready(function () 
  {
	comp = document.getElementById("companyname").value;
	compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;
	 
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#invoice_customer_id').val(ui.item.id);
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
	
});	

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#current_pic').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#new_pic").change(function() {
  readURL(this);
});
</script>	