<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
	
Helper::menu2('','','','','','','','active');

$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}
$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}
include_once($menu);
?>
<style>
.Webinars-itemPic {
    position: relative;
    width: 100%;
    margin: 0 0 6px;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    border-radius: 3px;
}
figure {
    display: block;
}
</style>
<section id="bd">
          <div class="searchform">
        <div class="container">
                          <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />

<form name="search_product" method="post" action="#" class="search form-inline">

  <fieldset>	

    <div class="form-group"><label class="control-label" for="search_product_terms">Terms</label>
	<input type="text" id="search_product_terms" name="search_product_terms" class="form-control" value="<?php if(isset($_POST['search_product_terms'])){echo $_POST['search_product_terms'];}else{ echo '';}?>"/></div>

    <div class="form-group float-right btn-toolbar">
      <div class="btn-group">
        <button id="searchsubmit" name="searchsubmit" type="submit" class="btn btn-default btn-primary">Search</button>
      </div>
      <div class="btn-group">
        <a id="search-reset" name="search-reset" href="/<?php echo DIR; ?>/product/index" class="btn btn-default btn-warning">Reset</a>
      </div>
    </div>

  </fieldset>


    </form>
        </div>
      </div>
    
    <div class="container">
                  
        <div id="invoices-latest-recurring-invoices">
    
<form name="product_list" method="post" action="#">

<table class="table table-condensed table-striped align-middle" data-type="products">
<thead>
    <tr>

      <th class="cell-size-medium">
        <a class="sortable" href="#" title="Product code">Name</a>
      </th>
      <th>
        <a class="sortable" href="#" title="Description">Description</a>
      </th>
      <th>
        <a class="sortable" href="#" title="Category">Category</a>
      </th>	  
      <th class="cell-size-medium cell-align-right">
        <a class="sortable" href="#" title="Date">Date</a>
      </th>
      <th class="cell-size-medium cell-align-right">
        <a class="sortable" href="#" title="Picture">Picture</a>
      </th>
	   <th class="cell-size-medium cell-align-right">
        <a class="sortable" href="#" title="Picture">eBook</a>
      </th>
    </tr>
  </thead>
  <tbody>
  
	 <?php
	 $product_count = 0;
	 $i = 0;
        foreach ($wps as $wp)
        {	
			echo "<tr data-link='/".DIR."/whitepapers/edit/".$wp["id"]. "'>";
			echo 
		   "<td>".$wp["name"]."</td>
			<td>".$wp["description"]."</td>
			<td>".$wp["category"]."</td>			
			<td>".$wp["date"]."</td>
			<td><figure class='Webinars-itemPic'><img id='".$wp['id']."' name='".$wp['id']."' src='".$wp['picture']."' title='".$wp['id']."' /></figure></td>		
			<td> <a href='".$wp["document"]."' class='btn btn-default btn-info payments' title='Payments' target='_blank'><span class='glyphicon glyphicon-print'></span> View Document</a></td>						
		  </tr>";
		  $i = $i + 1;
		}
		$product_count = $i;
	?>
	
      </tbody>
</table>
<input type="hidden" id="product_list__token" name="product_list[_token]" value="6rGXjYbrXN-xRgXhxOoJKt264Rtd6eFY1giWVEOPwHg" />
<input type="hidden" id="product_count" name="product_count" value="<?php echo $product_count;?>" />
    </form>
  </div>
    </div>
  </section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
         comp = document.getElementById("companyname").value;
	 compUrl = '/ims/autocomplete/auto_products.php?companyname='+comp;

    jQuery('#search_product_terms').autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#search_product_terms').val(ui.item.reference);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
  });	
</script>