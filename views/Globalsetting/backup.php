<?php 
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>

<section id="bd">
<div class="searchform">
        <div class="container">
<?php
if(isset($settings))
{
?>				
<div class="alert alert-success fade in">
 <a href="#" class="close" data-dismiss="alert">×</a>
            Settings updated.
</div>
<?php }?>		  
<ul class="nav nav-tabs">
    <li class=""><a href="/<?php echo DIR; ?>/globalsetting/index">Global settings</a></li>
    <li class=""><a href="/<?php echo DIR; ?>/globalsetting/smtpsettings">SMTP settings</a></li>
	<li class="active"><a href="/<?php echo DIR; ?>/globalsetting/backup">Database Backup</a></li> 
    </ul>
        </div>
      </div>
             
<div class="container">
                  
        <div id="invoices-latest-recurring-invoices">
		<!-- action="#" -->
<form name="product_list" method="post"  action="/<?php echo DIR; ?>/globalsetting/backup">
<fieldset class="col-md-12">
            <h3><label class="control-label required">Download backup</label></h3>
            <div class="form-inline">
              <div>
				  <div class="form-group">
					  <div>
					    <table id="invoice-taxes" >
							<tbody id="taxes" name="taxes">
							<tr><td>	
								<div class='form-group'>
							    <label class='control-label'>BackUp File Directory</label>
								</div>
								</td>
								<td>
								<div class='form-group'>
								<input type='text' value="/<?php echo DIR; ?>/backup" id='path' name='path'  readonly />
								</div>
								</td>
								<td>
								<div class='form-group'>
								<button id='btnRunBackup' name='btnRunBackup' align="right" type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="BackUpDatabase()">Download Backup</button>
								</div>
							</td></tr>
							<tr><td>	
								<div class='form-group'>
							    <label class='control-label required' for='series_name_0'>Email Database BackUp </label>
								</div>
								</td>
								<td>
								<div class='form-group'>
								<input type='text' value='tumelomodise4@gmail.com' id='email' name='email' />
								</div>
								</td>
								<td>
								<div class='form-group'>
								<button id='btnEmailBackup' name='btnEmailBackup' align="right" type="submit" class="btn btn-info" data-popup-open="popup-1"  onclick="BackUpDatabase()">Email Backup</button>
								</div>
							</td></tr>
							</tbody>
					    </table>							
					</div>
				  </div>
			  </div>
            </div>
  </fieldset>
<table class="table table-condensed table-striped align-middle" data-type="tables">
<thead>
    <tr>
      <!--<th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th> -->
      <th class="cell-size-medium">
        <a class="sortable" href="#" title="Table Name">Table Name</a>

      </th>
      <th>
	    <input type="checkbox" name="all" />
        <a class="sortable" href="#" title="Backup">Backup</a>
      </th>
    </tr>
  </thead>
  <tbody>
	 <?php
        foreach ($backup as $table)
        {			  
		  	foreach($table AS $key => $value) { $table[$key] = stripslashes($value); } 
							echo "<tr>";  
							echo "<td valign='top'>".nl2br( $value ). "</td>";  
							echo "<td valign='top'><input type='checkbox' name='".$value."' checked></td>"; 
							echo "</tr>";
		}
	?>
	
      </tbody>
</table>
 
    </form>
  </div>
 
<!-- PopUp - script -->
<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
    <div class="popup-inner">
        <h2>Database Backup - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
			
		</div>
		</br>
					<div id="divPaymentSchedules">
					
					</div>
	<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>				
<!-- PopUp - Script End-->	 
    </div>

  </section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery('#search_product_terms').autocomplete({
      source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {
		jQuery('#search_product_terms').val(ui.item.reference);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
  });	
</script>