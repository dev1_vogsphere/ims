<?php 
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>
<section id="bd">
<div class="searchform">
        <div class="container">
<?php
if(isset($settings))
{
?>				
<div class="alert alert-success fade in">
 <a href="#" class="close" data-dismiss="alert">×</a>
            Settings updated.
</div>
<?php }?>		  
<ul class="nav nav-tabs">
    <li class=""><a href="/<?php echo DIR; ?>/globalsetting/index">Global settings</a></li>
    <li class="active"><a href="/<?php echo DIR; ?>/globalsetting/smtpsettings">SMTP settings</a></li>
	<li class=""><a href="/<?php echo DIR; ?>/globalsetting/backup">Database Backup</a></li> 
    </ul>
        </div>
      </div>
    
    <div class="container">
<?php
$host = '';
$port		   = '';
$secure		   = '';
$emailUsername = '';
$emailPassword = '';
$prod	= '';
$dev = '';
$captcha= '';
$checked = '';
$Prodchecked = '';
$Devchecked = '';
$globalsetting = $globalsettings;
if(!empty($globalsettings))
{	  
	if(!empty($globalsetting['host'])) {$host 	 = $globalsetting['host'];}
	if(isset($globalsetting['port']))  {$port = $globalsetting['port'];}
	if(isset($globalsetting['secure'])){$secure = $globalsetting['secure'];}
	if(isset($globalsetting['emailusername'])){$emailUsername = $globalsetting['emailusername'];}
	if(isset($globalsetting['emailpassword'])){$emailPassword = $globalsetting['emailpassword'];}
	if(isset($globalsetting['prod'])){$prod = $globalsetting['prod'];if(!empty($prod)){$Prodchecked = 'checked';}}
	if(isset($globalsetting['dev'])){$dev = $globalsetting['dev'];if(!empty($dev)){$Devchecked = 'checked';}}
	if(isset($globalsetting['captcha'])){$captcha = $globalsetting['captcha'];if(!empty($captcha)){$checked = 'checked';}}
}
?>
<form name="global_settings" id="global_settings" method="post" action="#" enctype="multipart/form-data">
        <fieldset class="col-md-6">
            <h3>SMTP Settings</h3>
            <div class="form-group">
			<label class="control-label" for="host">Email Host Server</label>
			<input type="text" id="host" name="host" class="form-control" value="<?php echo $host; ?>" /></div>
            <div class="form-group"><label class="control-label" for="port">Email Port Number</label>
			<input type="text" id="port" name="port" class="form-control" value="<?php echo $port; ?>" /></div>
            <div class="form-group"><label class="control-label" for="secure">Email SMTP Secure</label>
			<input type="text" id="secure" name="secure" class="form-control" value="<?php echo $secure; ?>" /></div>
            <div class="form-group"><label class="control-label" for="emailUsername">Email User Name</label><input type="text" id="emailUsername" name="emailUsername" class="form-control" value="<?php echo $emailUsername; ?>" /></div>
            <div class="form-group"><label class="control-label" for="emailPassword">Email Password</label><input type="password" id="emailPassword" name="emailPassword" class="form-control" value="<?php echo $emailPassword; ?>" /></div>
            <div class="form-group"><label class="control-label required" for="prod">In Production</label><input type="radio" name="radio" class="form-control" value="p" <?php  echo $Prodchecked; ?>/></div>
            <div class="form-group"><label class="control-label" for="dev">In Development</label><input type="radio" name="radio" class="form-control" value="d" <?php  echo $Devchecked;?>/></div>
        	<div class='form-group'><div ><label>Disable Captcha <input type='checkbox' id='captcha' name='captcha' value="d" <?php  echo $checked;?>/></label></div></div>
		</fieldset>
        <div class="form-actions col-md-12"><input name="Save" id="Save" type="submit" value="Save" class="btn btn-default btn-primary"></div>
</form>
</div>
</section>

<footer id="ft">
</footer>