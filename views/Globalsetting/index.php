<?php 
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
	  $logoTemp = '';

include_once($menu);?>
<section id="bd">
          <div class="searchform">
        <div class="container">
<?php
if(isset($settings))
{
?>				
<div class="alert alert-success fade in">
 <a href="#" class="close" data-dismiss="alert">×</a>
            Settings updated.
</div>
<?php }?>		  
<ul class="nav nav-tabs">
    <li class="active"><a href="/<?php echo DIR; ?>/globalsetting/index">Global settings</a></li>
    <li class=""><a href="/<?php echo DIR; ?>/globalsetting/smtpsettings">SMTP settings</a></li>
	<li class=""><a href="/<?php echo DIR; ?>/globalsetting/backup">Database Backup</a></li> 
    </ul>
        </div>
      </div>
    
    <div class="container">
                  
      <?php 
	  $company_name = '';
	  $company_address = '';
	  $company_email = '';
	  $company_fax = '';
	  $company_reg = '';
	  $company_logo = '';
	  $company_phone = '';
	  $company_url = '';
	  $currency = '';
	  $currency_decimals = '';
	  $default_template = '';
	  $last_calculation_date = '';
	  $legal_terms = '';
	  $pdf_orientation = '';
	  $pdf_size = '';
	  $sample_data_load = '';
	  $siwapp_modules = '';
	  $vat = '';
	  $logoTemp = '';
	  $account = '';
	  $accounttype = '';
	  $bankname = ''; 
	  $branchcode = '';
	  $branchname = '';
	  $company_reg = '';
	  $globalsetting = $globalsettings;
	if(!empty($globalsettings))
	{	  
		  if(!empty($globalsetting['company_name'])) {$company_name 	 = $globalsetting['company_name'];}
		  if(isset($globalsetting['company_address'])){$company_address = $globalsetting['company_address'];}
		  if(isset($globalsetting['company_email']))  {$company_email   = $globalsetting['company_email'];}
		  if(isset($globalsetting['company_fax']))    {$company_fax 	 = $globalsetting['company_fax'];}
		  if(isset($globalsetting['company_logo']))   {$company_logo 	 = $globalsetting['company_logo']; $logoTemp = $company_logo;}
		  if(isset($globalsetting['company_phone']))  {$company_phone 	 = $globalsetting['company_phone'];}
		  if(isset($globalsetting['company_url']))    {$company_url 	 = $globalsetting['company_url'];}
		  if(isset($globalsetting['currency']))       {$currency 		 = $globalsetting['currency'];}
		  if(isset($globalsetting['currency_decimals'])){$currency_decimals = $globalsetting['currency_decimals'];}
		  if(isset($globalsetting['default_template'])){$default_template 	 = $globalsetting['default_template'];}
		  if(isset($globalsetting['last_calculation_date'])){$last_calculation_date = $globalsetting['last_calculation_date'];}
		  if(isset($globalsetting['legal_terms'])){$legal_terms 		= $globalsetting['legal_terms'];}
		  if(isset($globalsetting['pdf_orientation'])) {$pdf_orientation 	= $globalsetting['pdf_orientation'];}
		  if(isset($globalsetting['pdf_size'])){$pdf_size 			= $globalsetting['pdf_size'];}
		  if(isset($globalsetting['sample_data_load'])){$sample_data_load 	= $globalsetting['sample_data_load'];}
		  if(isset($globalsetting['siwapp_modules'])){$siwapp_modules 	= $globalsetting['siwapp_modules'];}
		  if(isset($globalsetting['company_reg'])){$company_reg 	= $globalsetting['company_reg'];}
		  if(isset($globalsetting['VAT']))  {$vat 	= $globalsetting['VAT'];}

		  if(isset($globalsetting['account']))  {$account 	= $globalsetting['account'];}
		  if(isset($globalsetting['accounttype']))  {$accounttype 	= $globalsetting['accounttype'];}
		  if(isset($globalsetting['bankname']))  {$bankname 	= $globalsetting['bankname'];}
		  if(isset($globalsetting['branchcode']))  {$branchcode 	= $globalsetting['branchcode'];}
		  if(isset($globalsetting['branchname']))  {$branchname 	= $globalsetting['branchname'];}
		  
	}  
	  ?>
   <form name="global_settings" id="global_settings" method="post" action="#" enctype="multipart/form-data">
        <fieldset class="col-md-6">
            <h3>Company</h3>
            <div class="form-group">
			<label class="control-label" for="global_settings_company_name">Company name</label>
			<input type="text" id="global_settings_company_name" name="global_settings_company_name" required="required" class="form-control" value="<?php echo $company_name; ?>" /></div>
            <!--<div class="form-group"><label class="control-label" required="required" for="global_settings_company_identification">Company identification</label>
			<input type="text" id="global_settings_company_identification" name="global_settings_company_identification" class="form-control" value="" /></div>-->
            <div class="form-group"><label class="control-label" for="global_settings_company_address">Company address</label><textarea id="global_settings_company_address" name="global_settings_company_address" class="form-control"><?php echo $company_address; ?></textarea></div>
            <div class="form-group"><label class="control-label" for="global_settings_company_phone">Company phone</label><input type="text" id="global_settings_company_phone" name="global_settings_company_phone" class="form-control" value="<?php echo $company_phone; ?>" /></div>
            <div class="form-group"><label class="control-label" for="global_settings_company_fax">Company fax</label><input type="text" id="global_settings_company_fax" name="global_settings_company_fax" class="form-control" value="<?php echo $company_fax; ?>" /></div>
            <div class="form-group"><label class="control-label required" for="global_settings_company_email">Company e-mail</label><input type="email" id="global_settings_company_email" name="global_settings_company_email" required="required" class="form-control" value="<?php echo $company_email; ?>" /></div>
            <div class="form-group"><label class="control-label" for="global_settings_company_url">Company website</label><input type="url" id="global_settings_company_url" name="global_settings_company_url" class="form-control" value="<?php echo $company_url; ?>" /></div>
            <div class="form-group"><label class="control-label required" for="global_settings_currency">Currency</label><select id="global_settings_currency" name="global_settings_currency" class="form-control"><option value="EUR">Euro</option><option value="LVL">Latvian Lats</option><option value="LTL">Lithuanian Litas</option><option value="RUB">Russian Ruble</option><option value="USD">US Dollar</option><option disabled="disabled">-------------------</option><option value="AFN">Afghan Afghani</option><option value="AFA">Afghan Afghani (1927–2002)</option><option value="ALL">Albanian Lek</option><option value="ALK">Albanian Lek (1946–1965)</option><option value="DZD">Algerian Dinar</option><option value="ADP">Andorran Peseta</option><option value="AOA">Angolan Kwanza</option><option value="AOK">Angolan Kwanza (1977–1991)</option><option value="AON">Angolan New Kwanza (1990–2000)</option><option value="AOR">Angolan Readjusted Kwanza (1995–1999)</option><option value="ARA">Argentine Austral</option><option value="ARS">Argentine Peso</option><option value="ARM">Argentine Peso (1881–1970)</option><option value="ARP">Argentine Peso (1983–1985)</option><option value="ARL">Argentine Peso Ley (1970–1983)</option><option value="AMD">Armenian Dram</option><option value="AWG">Aruban Florin</option><option value="AUD">Australian Dollar</option><option value="ATS">Austrian Schilling</option><option value="AZN">Azerbaijani Manat</option><option value="AZM">Azerbaijani Manat (1993–2006)</option><option value="BSD">Bahamian Dollar</option><option value="BHD">Bahraini Dinar</option><option value="BDT">Bangladeshi Taka</option><option value="BBD">Barbadian Dollar</option><option value="BYN">Belarusian Ruble</option><option value="BYB">Belarusian Ruble (1994–1999)</option><option value="BYR">Belarusian Ruble (2000–2016)</option><option value="BEF">Belgian Franc</option><option value="BEC">Belgian Franc (convertible)</option><option value="BEL">Belgian Franc (financial)</option><option value="BZD">Belize Dollar</option><option value="BMD">Bermudan Dollar</option><option value="BTN">Bhutanese Ngultrum</option><option value="BOB">Bolivian Boliviano</option><option value="BOL">Bolivian Boliviano (1863–1963)</option><option value="BOV">Bolivian Mvdol</option><option value="BOP">Bolivian Peso</option><option value="BAM">Bosnia-Herzegovina Convertible Mark</option><option value="BAD">Bosnia-Herzegovina Dinar (1992–1994)</option><option value="BAN">Bosnia-Herzegovina New Dinar (1994–1997)</option><option value="BWP">Botswanan Pula</option><option value="BRC">Brazilian Cruzado (1986–1989)</option><option value="BRZ">Brazilian Cruzeiro (1942–1967)</option><option value="BRE">Brazilian Cruzeiro (1990–1993)</option><option value="BRR">Brazilian Cruzeiro (1993–1994)</option><option value="BRN">Brazilian New Cruzado (1989–1990)</option><option value="BRB">Brazilian New Cruzeiro (1967–1986)</option><option value="BRL">Brazilian Real</option><option value="GBP">British Pound</option><option value="BND">Brunei Dollar</option><option value="BGL">Bulgarian Hard Lev</option><option value="BGN">Bulgarian Lev</option><option value="BGO">Bulgarian Lev (1879–1952)</option><option value="BGM">Bulgarian Socialist Lev</option><option value="BUK">Burmese Kyat</option><option value="BIF">Burundian Franc</option><option value="XPF">CFP Franc</option><option value="KHR">Cambodian Riel</option><option value="CAD">Canadian Dollar</option><option value="CVE">Cape Verdean Escudo</option><option value="KYD">Cayman Islands Dollar</option><option value="XAF">Central African CFA Franc</option><option value="CLE">Chilean Escudo</option><option value="CLP">Chilean Peso</option><option value="CLF">Chilean Unit of Account (UF)</option><option value="CNX">Chinese People’s Bank Dollar</option><option value="CNY">Chinese Yuan</option><option value="COP">Colombian Peso</option><option value="COU">Colombian Real Value Unit</option><option value="KMF">Comorian Franc</option><option value="CDF">Congolese Franc</option><option value="CRC">Costa Rican Colón</option><option value="HRD">Croatian Dinar</option><option value="HRK">Croatian Kuna</option><option value="CUC">Cuban Convertible Peso</option><option value="CUP">Cuban Peso</option><option value="CYP">Cypriot Pound</option><option value="CZK">Czech Koruna</option><option value="CSK">Czechoslovak Hard Koruna</option><option value="DKK">Danish Krone</option><option value="DJF">Djiboutian Franc</option><option value="DOP">Dominican Peso</option><option value="NLG">Dutch Guilder</option><option value="XCD">East Caribbean Dollar</option><option value="DDM">East German Mark</option><option value="ECS">Ecuadorian Sucre</option><option value="ECV">Ecuadorian Unit of Constant Value</option><option value="EGP">Egyptian Pound</option><option value="GQE">Equatorial Guinean Ekwele</option><option value="ERN">Eritrean Nakfa</option><option value="EEK">Estonian Kroon</option><option value="ETB">Ethiopian Birr</option><option value="XEU">European Currency Unit</option><option value="FKP">Falkland Islands Pound</option><option value="FJD">Fijian Dollar</option><option value="FIM">Finnish Markka</option><option value="FRF">French Franc</option><option value="XFO">French Gold Franc</option><option value="XFU">French UIC-Franc</option><option value="GMD">Gambian Dalasi</option><option value="GEK">Georgian Kupon Larit</option><option value="GEL">Georgian Lari</option><option value="DEM">German Mark</option><option value="GHS">Ghanaian Cedi</option><option value="GHC">Ghanaian Cedi (1979–2007)</option><option value="GIP">Gibraltar Pound</option><option value="GRD">Greek Drachma</option><option value="GTQ">Guatemalan Quetzal</option><option value="GWP">Guinea-Bissau Peso</option><option value="GNF">Guinean Franc</option><option value="GNS">Guinean Syli</option><option value="GYD">Guyanaese Dollar</option><option value="HTG">Haitian Gourde</option><option value="HNL">Honduran Lempira</option><option value="HKD">Hong Kong Dollar</option><option value="HUF">Hungarian Forint</option><option value="ISK">Icelandic Króna</option><option value="ISJ">Icelandic Króna (1918–1981)</option><option value="INR">Indian Rupee</option><option value="IDR">Indonesian Rupiah</option><option value="IRR">Iranian Rial</option><option value="IQD">Iraqi Dinar</option><option value="IEP">Irish Pound</option><option value="ILS">Israeli New Shekel</option><option value="ILP">Israeli Pound</option><option value="ILR">Israeli Shekel (1980–1985)</option><option value="ITL">Italian Lira</option><option value="JMD">Jamaican Dollar</option><option value="JPY">Japanese Yen</option><option value="JOD">Jordanian Dinar</option><option value="KZT">Kazakhstani Tenge</option><option value="KES">Kenyan Shilling</option><option value="KWD">Kuwaiti Dinar</option><option value="KGS">Kyrgystani Som</option><option value="LAK">Laotian Kip</option><option value="LVR">Latvian Ruble</option><option value="LBP">Lebanese Pound</option><option value="LSL">Lesotho Loti</option><option value="LRD">Liberian Dollar</option><option value="LYD">Libyan Dinar</option><option value="LTT">Lithuanian Talonas</option><option value="LUL">Luxembourg Financial Franc</option><option value="LUC">Luxembourgian Convertible Franc</option><option value="LUF">Luxembourgian Franc</option><option value="MOP">Macanese Pataca</option><option value="MKD">Macedonian Denar</option><option value="MKN">Macedonian Denar (1992–1993)</option><option value="MGA">Malagasy Ariary</option><option value="MGF">Malagasy Franc</option><option value="MWK">Malawian Kwacha</option><option value="MYR">Malaysian Ringgit</option><option value="MVR">Maldivian Rufiyaa</option><option value="MVP">Maldivian Rupee (1947–1981)</option><option value="MLF">Malian Franc</option><option value="MTL">Maltese Lira</option><option value="MTP">Maltese Pound</option><option value="MRO">Mauritanian Ouguiya</option><option value="MUR">Mauritian Rupee</option><option value="MXV">Mexican Investment Unit</option><option value="MXN">Mexican Peso</option><option value="MXP">Mexican Silver Peso (1861–1992)</option><option value="MDC">Moldovan Cupon</option><option value="MDL">Moldovan Leu</option><option value="MCF">Monegasque Franc</option><option value="MNT">Mongolian Tugrik</option><option value="MAD">Moroccan Dirham</option><option value="MAF">Moroccan Franc</option><option value="MZE">Mozambican Escudo</option><option value="MZN">Mozambican Metical</option><option value="MZM">Mozambican Metical (1980–2006)</option><option value="MMK">Myanmar Kyat</option><option value="NAD">Namibian Dollar</option><option value="NPR">Nepalese Rupee</option><option value="ANG">Netherlands Antillean Guilder</option><option value="TWD">New Taiwan Dollar</option><option value="NZD">New Zealand Dollar</option><option value="NIO">Nicaraguan Córdoba</option><option value="NIC">Nicaraguan Córdoba (1988–1991)</option><option value="NGN">Nigerian Naira</option><option value="KPW">North Korean Won</option><option value="NOK">Norwegian Krone</option><option value="OMR">Omani Rial</option><option value="PKR">Pakistani Rupee</option><option value="PAB">Panamanian Balboa</option><option value="PGK">Papua New Guinean Kina</option><option value="PYG">Paraguayan Guarani</option><option value="PEI">Peruvian Inti</option><option value="PEN">Peruvian Sol</option><option value="PES">Peruvian Sol (1863–1965)</option><option value="PHP">Philippine Peso</option><option value="PLN">Polish Zloty</option><option value="PLZ">Polish Zloty (1950–1995)</option><option value="PTE">Portuguese Escudo</option><option value="GWE">Portuguese Guinea Escudo</option><option value="QAR">Qatari Rial</option><option value="XRE">RINET Funds</option><option value="RHD">Rhodesian Dollar</option><option value="RON">Romanian Leu</option><option value="ROL">Romanian Leu (1952–2006)</option><option value="RUR">Russian Ruble (1991–1998)</option><option value="RWF">Rwandan Franc</option><option value="SVC">Salvadoran Colón</option><option value="WST">Samoan Tala</option><option value="SAR">Saudi Riyal</option><option value="RSD">Serbian Dinar</option><option value="CSD">Serbian Dinar (2002–2006)</option><option value="SCR">Seychellois Rupee</option><option value="SLL">Sierra Leonean Leone</option><option value="SGD">Singapore Dollar</option><option value="SKK">Slovak Koruna</option><option value="SIT">Slovenian Tolar</option><option value="SBD">Solomon Islands Dollar</option><option value="SOS">Somali Shilling</option><option value="R" selected="selected">South African Rand</option><option value="ZAL">South African Rand (financial)</option><option value="KRH">South Korean Hwan (1953–1962)</option><option value="KRW">South Korean Won</option><option value="KRO">South Korean Won (1945–1953)</option><option value="SSP">South Sudanese Pound</option><option value="SUR">Soviet Rouble</option><option value="ESP">Spanish Peseta</option><option value="ESA">Spanish Peseta (A account)</option><option value="ESB">Spanish Peseta (convertible account)</option><option value="LKR">Sri Lankan Rupee</option><option value="SHP">St. Helena Pound</option><option value="SDD">Sudanese Dinar (1992–2007)</option><option value="SDG">Sudanese Pound</option><option value="SDP">Sudanese Pound (1957–1998)</option><option value="SRD">Surinamese Dollar</option><option value="SRG">Surinamese Guilder</option><option value="SZL">Swazi Lilangeni</option><option value="SEK">Swedish Krona</option><option value="CHF">Swiss Franc</option><option value="SYP">Syrian Pound</option><option value="STD">São Tomé &amp; Príncipe Dobra</option><option value="TJR">Tajikistani Ruble</option><option value="TJS">Tajikistani Somoni</option><option value="TZS">Tanzanian Shilling</option><option value="THB">Thai Baht</option><option value="TPE">Timorese Escudo</option><option value="TOP">Tongan Paʻanga</option><option value="TTD">Trinidad &amp; Tobago Dollar</option><option value="TND">Tunisian Dinar</option><option value="TRY">Turkish Lira</option><option value="TRL">Turkish Lira (1922–2005)</option><option value="TMT">Turkmenistani Manat</option><option value="TMM">Turkmenistani Manat (1993–2009)</option><option value="USN">US Dollar (Next day)</option><option value="USS">US Dollar (Same day)</option><option value="UGX">Ugandan Shilling</option><option value="UGS">Ugandan Shilling (1966–1987)</option><option value="UAH">Ukrainian Hryvnia</option><option value="UAK">Ukrainian Karbovanets</option><option value="AED">United Arab Emirates Dirham</option><option value="UYU">Uruguayan Peso</option><option value="UYP">Uruguayan Peso (1975–1993)</option><option value="UYI">Uruguayan Peso (Indexed Units)</option><option value="UZS">Uzbekistani Som</option><option value="VUV">Vanuatu Vatu</option><option value="VEF">Venezuelan Bolívar</option><option value="VEB">Venezuelan Bolívar (1871–2008)</option><option value="VND">Vietnamese Dong</option><option value="VNN">Vietnamese Dong (1978–1985)</option><option value="CHE">WIR Euro</option><option value="CHW">WIR Franc</option><option value="XOF">West African CFA Franc</option><option value="YDD">Yemeni Dinar</option><option value="YER">Yemeni Rial</option><option value="YUN">Yugoslavian Convertible Dinar (1990–1992)</option><option value="YUD">Yugoslavian Hard Dinar (1966–1990)</option><option value="YUM">Yugoslavian New Dinar (1994–2002)</option><option value="YUR">Yugoslavian Reformed Dinar (1992–1993)</option><option value="ZRN">Zairean New Zaire (1993–1998)</option><option value="ZRZ">Zairean Zaire (1971–1993)</option><option value="ZMW">Zambian Kwacha</option><option value="ZMK">Zambian Kwacha (1968–2012)</option><option value="ZWD">Zimbabwean Dollar (1980–2008)</option><option value="ZWR">Zimbabwean Dollar (2008)</option><option value="ZWL">Zimbabwean Dollar (2009)</option></select></div>
        </fieldset>
        <fieldset class="col-md-6" open>
            <h3>Legal texts</h3>
            <div class="form-group"><label class="control-label" for="global_settings_legal_terms">Legal terms</label><textarea id="global_settings_legal_terms" name="global_settings_legal_terms" class="form-control"><?php echo $legal_terms; ?></textarea></div>
        </fieldset>
        <fieldset class="col-md-6">
            <h3>PDF Settings</h3>
            <div class="form-group"><label class="control-label" for="global_settings_pdf_size">PDF size</label><select id="global_settings_pdf_size" name="global_settings_pdf_size" class="form-control"><option value=""></option><option value="4a0">4A0</option><option value="2a0">2A0</option><option value="a0">A0</option><option value="a1">A1</option><option value="a2">A2</option><option value="a3">A3</option><option value="a4" selected="selected">A4</option><option value="a5">A5</option><option value="a6">A6</option><option value="a7">A7</option><option value="a8">A8</option><option value="a9">A9</option><option value="a10">A10</option><option value="b0">B0</option><option value="b1">B1</option><option value="b2">B2</option><option value="b3">B3</option><option value="b4">B4</option><option value="b5">B5</option><option value="b6">B6</option><option value="b7">B7</option><option value="b8">B8</option><option value="b9">B9</option><option value="b10">B10</option><option value="c0">C0</option><option value="c1">C1</option><option value="c2">C2</option><option value="c3">C3</option><option value="c4">C4</option><option value="c5">C5</option><option value="c6">C6</option><option value="c7">C7</option><option value="c8">C8</option><option value="c9">C9</option><option value="c10">C10</option><option value="ra0">RA0</option><option value="ra1">RA1</option><option value="ra2">RA2</option><option value="ra3">RA3</option><option value="ra4">RA4</option><option value="sra0">SRA0</option><option value="sra1">SRA1</option><option value="sra2">SRA2</option><option value="sra3">SRA3</option><option value="sra4">SRA4</option><option value="letter">Letter</option><option value="legal">Legal</option><option value="ledger">Ledger</option><option value="tabloid">Tabloid</option><option value="executive">Executive</option><option value="folio">Folio</option><option value="commerical #10 envelope">Commercial #10 Envelope</option><option value="catalog #10 1/2 envelope">Catalog #10 1/2 Envelope</option><option value="8.5x11">8.5x11</option><option value="8.5x14">8.5x14</option><option value="11x17">11x17</option></select></div>
            <div class="form-group"><label class="control-label required" for="global_settings_pdf_orientation">PDF orientation</label><select id="global_settings_pdf_orientation" name="global_settings_pdf_orientation" class="form-control"><option value="Portrait" selected="selected">Portrait</option><option value="Landscape">Landscape</option></select></div>
		</fieldset>
		<fieldset class="col-md-6">
		<h3>Company Logo</h3>
		<div class="form-group"><!--<label class="control-label" for="global_settings_company_logo">Company logo</label>-->
			 <img id="global_settings_current_logo" name="global_settings_current_logo" src="<?php echo '/'.DIR.'/images/'.$company_logo; ?>" alt="Logo" title="Current logo" height="100" /><br/>
			 <input type="file" id="global_settings_company_logo" name="global_settings_company_logo" value="<?php echo $company_logo; ?>" />
			 <input type="text" name="logoTemp" id="logoTemp" value="<?php echo $logoTemp;?>" style="display:none"/>
		</div>
		</fieldset>
		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">VAT number</label><input type="text" id="vat" name="vat" class="form-control" value="<?php echo $vat; ?>" /></div>
        </fieldset>

		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">account</label><input type="text" id="account" name="account" class="form-control" value="<?php echo $account; ?>" /></div>
        </fieldset>
		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">accounttype</label><input type="text" id="accounttype" name="accounttype" class="form-control" value="<?php echo $accounttype; ?>" /></div>
        </fieldset>
		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">bankname</label><input type="text" id="bankname" name="bankname" class="form-control" value="<?php echo $bankname; ?>" /></div>
        </fieldset>
		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">branchcode</label><input type="text" id="branchcode" name="branchcode" class="form-control" value="<?php echo $branchcode; ?>" /></div>
        </fieldset>
		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="vat">branchname</label><input type="text" id="branchname" name="branchname" class="form-control" value="<?php echo $branchname; ?>" /></div>
        </fieldset>

		<fieldset class="col-md-6" open>
		  <div class="form-group"><label class="control-label" for="company_reg">company_reg</label><input type="text" id="company_reg" name="company_reg" class="form-control" value="<?php echo $company_reg; ?>" /></div>
        </fieldset>
		
        <fieldset class="col-md-12" hidden>
            <h3><label class="control-label required">Invoicing taxes</label></h3>
            <div class="form-inline">
              <div>
				  <div class="form-group">
					  <div>
					    <table id="invoice-taxes" >
							<tbody id="taxes" name="taxes">
							<?php 
							if(empty($taxes))
							{
							?>
							<tr>
								<td>
								<a href='#' class='btn' onclick='delete_tax(0)'><span class='glyphicon glyphicon-trash'></span></a>
								</td>
								<td><div class='form-group'>
									<label class='control-label' for='taxes_name'>Name</label>
									<input type='text' id='taxes_name_0' name='taxes_name_0' class='form-control' />
								</div>
								</td>
								<td>
								<div class='form-group'>
									<label class='control-label' for='taxes_name_value'>Value</label>
									<input type='text' id='taxes_value_0' name='taxes_value_0'  class='form-control' />
								</div>
								</td>
								<td>
								<div class='form-group'>
									<div class='checkbox'>
										<label>
										<input type='checkbox' id='taxes_active_0' name='taxes_active_0' value='1' />Active</label>
									</div>
								</div>
								</td>
								<td>
								<div class='form-group'>
									<div class='checkbox'><label><input type='checkbox' id='taxes_is_default_0' name='taxes_is_default_0' value='1' /> Is default</label></div>
								</div>
								</td>
								<td>
									<div class='form-group' style="display:none">                                                                           
									<label class='control-label' for='tax_id_0'>#id</label>                                  
									<input type='text' id='tax_id_0' name='tax_id_0'  class='form-control' />                
									</div>                                                                                             
								</td>
						   </tr>
						   	<?php 
							}
							else
							{
								  $i = 0;
								  $checked2 = '';
								  foreach($taxes as $t)
								  {
									if(!empty($t['active'])){  $checked = 'checked';}else{ $checked = '';}
									
									if(!empty($t['is_default'])){  $checked2 = 'checked';}else{ $checked2 = '';}

									echo "<tr>"
										."<td>"
										."<a href='#' class='btn' onclick='delete_tax(".$i.")'><span class='glyphicon glyphicon-trash'></span></a>"
										."</td>                                                                                              "
										."<td><div class='form-group'>                                                                       "
										."<label class='control-label' for='taxes_name'>Name</label>                                         "
										."<input type='text' id='taxes_name_".$i."' name='taxes_name_".$i."' class='form-control' value='".$t['name']."'/>                   "
										."</div>                                                                                             "
										."</td>                                                                                              "
										."<td>                                                                                               "
										."<div class='form-group'>                                                                           "
										."<label class='control-label' for='taxes_name_value'>Value</label>                                  "
										."<input type='text' id='taxes_value_".$i."' name='taxes_value_".$i."'  class='form-control' value='".$t['value']."'/>                "
										."</div>                                                                                             "
										."</td>                                                                                              "
										."<td>                                                                                               "
										."<div class='form-group'>                                                                           "
										."<div class='checkbox'>                                                                             "
										."<label>                                                                                                     "
										."<input type='checkbox' id='taxes_active_".$i."' name='taxes_active_".$i."' value='".$t['active']."' ".$checked."/>Active</label>        "
										."</div>                                                                                             "
										."</div>                                                                                             "
										."</td>                                                                                              "
										."<td>                                                                                               "
										."<div class='form-group'>"
										."<div class='checkbox'><label><input type='checkbox' id='taxes_is_default_".$i."' name='taxes_is_default_".$i."' value='".$t['is_default']."' ".$checked2."/> Is default</label></div>"
										."</div>"
										."</td> "
										."<td>"
										."<div class='form-group'>                                                                           "
										."<label class='control-label' for='taxes_id_".$i."'>#id</label>                                  "
										."<input type='text' id='taxes_id_".$i."' name='taxes_id_".$i."'  class='form-control' value='".$t['id']."'/>                "
										."</div>                                                                                             "
										."</td>"
										."</tr> ";
										$i = $i + 1;
								  }
							}
							?>
						</tbody>
					  </table>	
					  <a id="add-tax" name="add-tax" href="#" ><span class="glyphicon glyphicon-plus"></span> Add</a>
					  <input type="text" id="taxCount" name="taxCount" value="<?php echo $taxCount;?>" style="display:none"/>
					</div>
				 </div>
			</div>
            </div>
        </fieldset>
        <fieldset class="col-md-12" hidden>
            <h3><label class="control-label required">Invoicing series</label></h3>
            <div class="form-inline">
              <div>
			  <div class="form-group">
			  <div>
				<table id="invoice-series" >
				 <tbody id="Items" name="Items">
				  	<?php 
					if(empty($series))
					{
					?>
					<tr>
				  	<td>
						<a href='#' class='btn' onclick='delete_series(0)'><span class='glyphicon glyphicon-trash'></span></a>
					</td>
					<td>	
					<div class='form-group'>
						<label class='control-label required' for='series_name_0'>Name</label>
						<input type='text' id='series_name_0' name='series_name_0' required='required' class='form-control' value='default' />
					</div>
					</td>
					<td>	
					<div class='form-group'><label class='control-label required' for='series_value_0'>Value</label>
						<input type='text' id='series_value_0' name='series_value_0' required='required' class='form-control' value='1200' />
					</div>
					</td>	
					<td>	
					<div class='form-group'><label class='control-label required' for='series_first_number_0'>First number</label>
						<input type='text' id='series_first_number_0' name='series_first_number_0' required='required' class='form-control' value='1' />
					</div>
					</td>	
					<td>	
					<div class='form-group'>
					   <div class='checkbox'>
					     <label class='required'>
						   <input type='checkbox' id='series_enabled_0' name='series_enabled_0' required='required' value='1' checked='checked' /> Enabled</label>
					   </div>
				    </div>
					</td>	
					<td>
					<div class='form-group' style="display:none">                                                                           
					<label class='control-label' for='series_id_0'>#id</label>                                  
					<input type='text' id='series_id_0' name='series_id_0'  class='form-control' />                
					</div>                                                                                             
					</td>
				  </tr>
				  <?php }
				  else
				  {
					  $i = 0;
					  $checked = '';
					  foreach($series as $s)
					  {
						  if(!empty($s['enabled'])){  $checked = 'checked';}else{ $checked = '';}
						  echo "<tr>                                                                                                                            "
							."<td>                                                                                                                            "
							."<a href='#' class='btn' onclick='delete_series(".$i.")'><span class='glyphicon glyphicon-trash'></span></a>                          "
							."</td>                                                                                                                           "
							."<td>	                                                                                                                          "
							."<div class='form-group'>                                                                                                        "
							."<label class='control-label required' for='series_name_".$i."'>Name</label>                                                          "
							."<input type='text' id='series_name_".$i."' name='series_name_".$i."' required='required' class='form-control' value='".$s['name']."'/>          "
							."</div>                                                                                                                          "
							."</td>                                                                                                                           "
							."<td>	                                                                                                                          "
							."<div class='form-group'><label class='control-label required' for='series_value_".$i."'>Value</label>                                "
							."<input type='text' id='series_value_".$i."' name='series_value_".$i."' required='required' class='form-control' value='".$s['value']."'/>           "
							."</div>                                                                                                                          "
							."</td>	                                                                                                                          "
							."<td>	                                                                                                                          "
							."<div class='form-group'><label class='control-label required' for='series_first_number_".$i."'>First number</label>                  "
							."<input type='text' id='series_first_number_".$i."' name='series_first_number_".$i."' required='required' class='form-control' value='".$s['first_number']."' />"
							."</div>                                                                                                                          "
							."</td>	                                                                                                                          "
							."<td>	                                                                                                                          "
							."<div class='form-group'>                                                                                                        "
							."<div class='checkbox'>                                                                                                          "
							."<label class='required'>                                                                                                        "
							."<input type='checkbox' id='series_enabled_".$i."' name='series_enabled_".$i."' required='required' value='".$s['enabled']."' $checked /> Enabled</label>"
							."</div>"
							."</div>"
							."</td>	"
							."<td>"
							."<div class='form-group'>                                                                           "
							."<label class='control-label' for='series_id_".$i."'>#id</label>                                  "
							."<input type='text' id='series_id_".$i."' name='series_id_".$i."'  class='form-control' value='".$s['id']."'/>                "
							."</div>                                                                                             "
							."</td>"
							."</tr> ";
							$i = $i + 1;
					}
				  }?>
				</tbody>
				</table>	
				 <a id="add-series" name="add-series" href="#"><span class="glyphicon glyphicon-plus"></span> Add</a>
				 <input type="text" id="seriesCount" name="seriesCount" value="<?php echo $seriesCount;?>" style="display:none"/>
			  </div>
			 </div>
			</div>
			
            </div>
        </fieldset>
        <div id="global_settings"><input type="hidden" id="global_settings__token" name="global_settings[_token]" value="ep91KE3Vx39PGiuMkTL4E4LXQOAB-jInpiET0OkvnYQ" />
    </div>
        <div class="form-actions col-md-12"><input name="Save" id="Save" type="submit" value="Save" class="btn btn-default btn-primary"></div>
    
    </form>

    <script type="text/javascript">
	  jQuery('#add-tax').on('click', function(event)
			{
                  event.preventDefault();
					var row = add_taxes();
					jQuery("#invoice-taxes > tbody").append(row);      // Append the new elements 
			});
			
	  jQuery('#add-series').on('click', function(event)
		{
              event.preventDefault();
				var row = add_series();
				jQuery("#invoice-series > tbody").append(row);      // Append the new elements 
		});		
    </script>

    </div>
  </section>

  <footer id="ft">
  </footer>
