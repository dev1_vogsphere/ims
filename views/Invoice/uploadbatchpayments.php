<?php
if(!function_exists('getbaseurl'))
{
	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if (isset($_SERVER['HTTPS']))
			{
				if($_SERVER['HTTPS'] == "on")
				{
					$url = "https://";
			    }
				else
				{
					$url = "http://";
				}
			}
			else
			{
					$url = "http://";
			}
				
			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			//echo $_SERVER['HTTPS'];
			//echo $url.$dir;
			return $url.$dir;
	}
}
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);
$url = getbaseurl();
$url = str_replace('/invoice/uploadbatchpayments/','',$url);
//echo $url;

$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

?>
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->

  <script src="/<?php echo DIR; ?>/libs/js/jquery.min.js"></script>
  <script src="/<?php echo DIR; ?>/libs/js/bootstrap.min.js"></script>

  <style>
  .box
  {
   max-width:600px;
   width:100%;
   margin: 0 auto;;
  }
  </style>
  <section id="bd">
<div class="searchform">
        <div class="container">
                  </div>
      </div>
<div class="container">
    <div class="content">
	 <h1>Upload Batch Payments via csv File</h1>
	 <div class="col-md-12">
			    <div class="messages">
				  <p id="message_infoBatchPayments" name="message_infoBatchPayments" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
    </div>
	<form id="uploadbatchpayments" method="post" enctype="multipart/form-data" style="display:block" role="form">
		<p>Download a Template of Invoices to pay file <a style="color:red" href="/<?php echo DIR; ?>/invoice/index"><strong>here</strong></a> and Instructions <a href="/<?php echo DIR; ?>/Files/Documentation_BatchInstructions.docx" target='_blank' style="color:red"><strong>here</strong></a></p>
		<p>Max file size 10Mb, Valid formats csv</p>
	<div class="col-md-3">
     <br />
     <label>Select CSV File</label>
    </div>  
		<div class="col-md-4">  
             <input type="file" name="csv_file" id="csv_file" accept=".csv" style="margin-top:15px;" />
         </div> 
		 <br />
		<table class ="table table-user-information">
		  <tr>
			<td>
				<div >							
				  <input id="Upload" name="Upload" type="submit" class="btn btn-primary" value="Upload" />
				</div>
			</td>
		   </tr>
		 </table>		 
		<input id="userid" name="userid" style="display:none" type="text"  value="<?php echo $username;?>" />
   </form>
   <br />
   <div id="csv_file_data"></div>
   
</div></div>
</section>
<script charset="UTF-8">
jq162 = jQuery.noConflict( true );

jq162(document).ready(function(){
 jq162('#uploadbatchpayments').on('submit', function(event){
	 var fileInput     = document.getElementById('csv_file').value;
	 var message_info  = document.getElementById('message_infoBatchPayments');								

	 message_info.innerHTML = '';
     message_info.className  = '';	
     event.preventDefault();
	var filesize = 0;

  if(validFileExt(fileInput))
  {
	 filesize = document.getElementById('csv_file').files[0].size;
var dir = "<?php echo $url; ?>";
dir = dir + "/script_uploadbatchpayments.php";
	//alert(dir);
	 if(validFileSize(filesize)){ 
  jq162.ajax({
   url:dir,
   method:"POST",
   data:new FormData(this),
   dataType:'json',
   contentType:false,
   cache:false,
   processData:false,
   success:function(data)
   {

	   obj = jQuery.parseJSON(JSON.stringify(data));
	   	var message_info = document.getElementById('message_infoBatchPayments');								
	   if(data !== null)
	   {
		   message_info.innerHTML = 'file added successfully';
		   message_info.className  = 'status';	
		   	   	   //alert(data);

		   report_uploadedbatchpayments(data);
	   }
	   else
	   {
		   message_info.innerHTML = 'no file was uploaded';
		   message_info.className  = 'alert alert-error';
	   }
   },
    beforeSend:function() {
    jq162('#Upload').button('loading');
  },
  complete:function() {
    jq162('#Upload').button('reset');
  }

  })
  }
  else
  {
	 message_info.innerHTML = 'file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';	  
  }
  }
  else
  {
	  	message_info.innerHTML = 'Invalid file format please upload .csv';
		message_info.className  = 'alert alert-error';
  }
 });
 
});

</script>

<script>
function validFileExt(file) 
{
    var extension = file.substr( (file.lastIndexOf('.') +1) );
	switch(extension) {
        case 'csv':
		   return true;
		default:
           return false;
	}
}
function validFileSize(filesize) 
{
	var max_file_size = 1024*10000; //10MB
	//alert(filesize);
	if (filesize > max_file_size) 
	{
		return false;
	}else{return true;}
}
function report_uploadedbatchpayments(data)
{
	var html = '<div class="row"><div class="table-responsive"><table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
    var totalColumn = 0;
	if(data.column)
    {
		totalColumn  = data.column.length;
     html += '<tr>';
     for(var count = 0; count < data.column.length; count++)
     {
      html += '<th>'+data.column[count]+'</th>';
     }
     html += '</tr>';
    }

    if(data.row_data)
    {
     for(var count = 0; count < data.row_data.length; count++)
     {
	    
		     html += '<tr>';
			 for(var col = 0; col < data.column.length; col++)
			 {
			  html += '<td style="font-size: 10px;width:100%">'+data.row_data[count][col]+'</td>';
			 }
			 html += '</tr>';
		
     }
    }
    html += '</table></div></div>';
	//alert(html);
    $('#csv_file_data').html(html);
}
</script>