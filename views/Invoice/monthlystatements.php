<?php
if(!function_exists('getbaseurl'))
{
	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if (isset($_SERVER['HTTPS']))
			{
				if($_SERVER['HTTPS'] == "on")
				{
					$url = "https://";
			    }
				else
				{
					$url = "http://";
				}
			}
			else
			{
					$url = "http://";
			}
				
			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			//echo $_SERVER['HTTPS'];
			//echo $url.$dir;
			return $url.$dir;
	}
}
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);
$url = getbaseurl();
$url = str_replace('/invoice/uploadbatchpayments/','',$url);
//echo $url;

$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

$id   = $customer['id'];
$from = $daterange['fromdate'];
$to   = $daterange['todate'];

$customername = $customer['name'];
$file = '/ims/pdf/pdf_statement.php?id='.$id.'&username='.$username.'&type=Details&from='.$from.'&to='.$to ;
$dt = date('Y-m-d');
//echo date("Y-m-01", strtotime($dt));
//echo $file;
?>
<section id="bd">
<div class="searchform">
        <div class="container">
        <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
		<input type="text" id="username" name="username" value ="<?php echo $username; ?>" hidden />
        <form name="search_customer" method="post" action="#" class="search form-inline">
		  <fieldset>
			<div class="form-group"><label class="control-label" for="search_customer_terms" >Terms</label>
				<input type="text" id="search_customer_terms" name="search_customer_terms" class="form-control" 
				value="<?php if(isset($_POST['search_customer_terms'])){echo $_POST['search_customer_terms'];}else{ echo $customername;}?>"/>
			</div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_from">Date from</label>
	<input type="date" id="search_invoice_date_from_mon" name="search_invoice_date_from_mon" class="form-control" value="<?php if(isset($_POST['search_invoice_date_from_mon'])){ echo $_POST['search_invoice_date_from_mon'];}else{ echo date("Y-m-01", strtotime($dt)); }?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_to">to</label>
	<input type="date" id="search_invoice_date_to_mon" name="search_invoice_date_to_mon" class="form-control" value="<?php if(isset($_POST['search_invoice_date_to_mon'])){ echo $_POST['search_invoice_date_to_mon'];}else{ echo date("Y-m-t", strtotime($dt)); }?>"/></div>

			<div class="form-group float-right btn-toolbar">
			  <div class="btn-group">
				<button id="searchsubmit" name="searchsubmit" type="submit" class="btn btn-default btn-primary">Search</button>
			  </div>
			  <div class="btn-group">
				<a id="searchreset" name="searchreset" href="/<?php echo DIR; ?>/customer/index" class="btn btn-default btn-warning">Reset</a>
			  </div>
			</div>
		  </fieldset>
		</form>
		
         </div>
</div>
<div class="container">
		<div class="content">
		  <h3>Monthly Statement of Account</h3>
		  	<div id="pdffileDIV">	<!-- // -- Actual PDF Page Size : [w] => 612 [h] => 792 -->		
				<iframe style="z-Index:0" id="pdffile" name="pdffile" src="<?php echo $file;?>#zoom=FitH&toolbar=1&navpanes=0&scrollbar=0&statusbar=0&messages=0&scrollbar=0"  frameborder="0" width="100%" height="595px" style="z-Index:0">
				</iframe>
			</div>
		 </div>
</div>
</section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
       comp = document.getElementById("companyname").value;
	 compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;

    jQuery('#search_customer_terms').autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#search_customer_terms').val(ui.item.name);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });
</script>   