<?php
Helper::menu2('','','','','','','active','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>

<section id="bd">
          <div class="searchform">
        <div class="container">
                  </div>
      </div>
    
    <div class="container">
                  

<div class="content">
  <form name="invoice_payment_list" method="post" action="/<?php echo DIR.'/invoice/payments/'.$invoice['id']; ?>">
	<ul id="toolbar" class="table-actions list-inline list-unstyled">
	  <li>
		 <p><a class="btn btn-primary" href="/<?php echo DIR.'/invoice/uploadbatchpayments/'; ?>">Import Payments from .csv</a></p>
	  </li>
	</ul>
  <h4 class="title"><b>Payments</b></h4>
  <table class="table condensed-table">
    <thead>
      <tr>
        <th class="col-md-1">
          <input type="checkbox" name="all" />
        </th>
        <th class="col-md-2 date">Date</th>
        <th class="col-md-2">Payment Reference</th>
        <th class="col-md-2">Invoice</th>		  
        <th class="col-md-2 currency">Amount</th>
      </tr>
    </thead>
    <tbody>
	<?php
	   $i = 0;
	   $payment_count = 0;
    
	foreach ($payments as $payment)
	{	
		echo
		"<tr class='cell-size-medium'>
        <td class='payment-remove cell-size-medium'>
          <div><label>
		  <input type='checkbox' id='invoice_payment_".$i."' name='invoice_payment_".$i."' value='".$payment['invoice_id']."' />  
		  <input type='hidden' id='payment_".$i."' name='payment_".$i."' value='".$payment['id']."' />  
		  </label>
		  </div>
        </td>
        <td class='date cell-size-medium'>".$payment['date']."</td>
        <td class='cell-size-medium'>".$payment['notes']."</td>
        <td class='cell-size-medium'>".$payment['invoice_id']."</td>		
        <td class='currency cell-size-medium'>$currency".number_format($payment['amount'],2)."</td>
      </tr>";
	  	   $i = $i + 1;

	  }
	  $payment_count = $i;
	  ?>
          </tbody>
  </table>
  <div class="form-actions buttons">
    <button type="submit" class="btn btn-default btn-danger float-left" name="remove" id="remove" data-confirm="Are you sure you want to remove selected payments?"><span class="glyphicon glyphicon-trash"></span> Remove selected</button>
    <a href="#payment-add-form" class="btn btn-default btn-info" data-toggle="collapse"><span class="glyphicon glyphicon-plus"></span> Add payment</a>
  </div>
  <input type="hidden" id="invoice_payment_list__token" name="invoice_payment_list[_token]" value="1KqJUXHITXqlpaau648HznlqVgUrJXJ7x_T6uI3R0GU" />
  	  <input type="hidden" id="invoice_id2" name="invoice_id2" value="<?php echo $invoice['id'];?>" />
	  <input type="hidden" id="payment_count" name="payment_count" value="<?php echo $payment_count;?>" />
    </form>

    <form name="payment" method="post" action="/<?php echo DIR.'/invoice/payments/'.$invoice['id']; ?>" id="payment-add-form" class="collapse form-inline">
    <div class="form-group"><label class="control-label required" for="payment_date">Date</label><input type="date" id="payment_date" name="payment_date" required="required" class="form-control" value="<?php echo date('Y-m-d');?>" /></div>
    <div class="form-group"><label class="control-label required" for="payment_amount">Amount</label><div class="input-group">
	<span class="input-group-addon">R</span>
    <input type="text" id="payment_amount" name="payment_amount" required="required" class="form-control" />    </div></div>
    <div class="form-group"><label class="control-label" for="payment_notes">Notes</label><textarea id="payment_notes" name="payment_notes" class="form-control"></textarea></div>
	<div class="form-group"><label class="control-label" for="payment_notes">Invoice</label>
	 <div class="input-group"><input class="form-control" type="text" id="invoice_id" name="invoice_id" value="<?php echo $invoice['id'];?>" required="required" /></div>
	</div>
	    <button type="submit" class="btn btn-default btn-primary success" name="add" id="add">Save</button>

  <input type="hidden" id="payment__token" name="payment[_token]" value="1dLd_r4x4H_J3uohPIHpVDXO39gULrE7tqGPoLPJ81o" />
    </form>
</div>
    </div>
  </section>

  <footer id="ft">
  </footer>

    <script type="text/javascript" charset="utf-8" src="/ims2/web/js/frameworks.js"></script>
