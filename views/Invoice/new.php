<?php
$typeText = '';
$backLink = '';
if($type == 'Invoice')
{
	$backLink  = "/ims/invoice/index";
	$typeText = $type;	
}
elseif($type == 'RecurringInvoice') 
{
	$backLink  = "/ims/recurring/index";	
	$typeText = 'RecurringInvoice';
}
else
{
	$backLink  = "/ims/quotation/index";	
	$typeText = 'Quotation';
}
 
Helper::menu('','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
$legal_terms  = '';
if(isset($_SESSION['legal_terms'])){$legal_terms  = $_SESSION['legal_terms'];}

$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

include_once($menu);
$rowscount = 0;
if(!empty($_POST))
{
	$rowscount = $_POST['rowscount'];
}
?>
    
    <div class="container">
<?php
if(isset($idInvoiceError))
{
	echo "<div class='alert alert-danger fade in'>";
	echo  "<a href='#' class='close' data-dismiss='alert'>�</a>";
	foreach($idInvoiceError as $x => $x_value) {
		echo "Key=" . $x . ",";
		echo "Value=" . $x_value;
	}
	echo "</div>";
}
elseif(isset($idInvoiceItemsError))
{
	echo "<div class='alert alert-danger fade in'>";
	echo  "<a href='#' class='close' data-dismiss='alert'>�</a>";
	foreach($idInvoiceItemsError as $x => $x_value) {
		echo "Key=" . $x . ",";
		echo "Value=" . $x_value;
	}
	echo "</div>";
}

elseif(isset($_POST['invoiceNumber']))
{
	if(($_POST['invoiceNumber']) > 0)
	{
		echo "<div class='alert alert-success fade in'>
            <a href='#' class='close' data-dismiss='alert'>�</a>
            $typeText # ".$_POST['invoiceNumber']." created successfully. 
		</div>";
	}
}
?>                    

      
  <article class="invoice-like">
    <header id="invoice-like-title" class="clearfix">
      <h2><div class="col-md-2 clearfix"><input readonly type="text" value="<?php echo 'New '.$typeText; ?>" class="form-control-plaintext" /></div></h2>
	  <br/>
	  	  <br/>

  <ul class="list-inline list-unstyled">
    <li>
      <span class="label draft">Draft</span>
    </li>
    <li>
			<?php if(isset($_POST['invoice_sent_by_email']) && isset($_POST['invoiceNumber'])){?>
			<span class="label closed">sent by e-mail</span>
            <?php }else{?>
			<span class="label">Not sent by e-mail</span>
			<?php }?>
          </li>
  </ul>
    </header>

    <form name="invoice" method="post" action="#" class="form-stacked">
     <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
	 <input type="hidden" id="username" name="username" value ="<?php echo $username; ?>"  />

      <div class="row">
        <div id="invoice-like-customer-data" class="col-md-8">

          <h3>Customer data</h3>

        <div class="row">
		 <div class="col-md-4 clearfix" hidden>
              <div class="form-group"><label class="control-label required" for="invoice_customer_id">Customer ID</label>
			</div>
         </div>
		
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label required" for="invoice_customer_name">Name</label><input type="text" id="invoice_customer_name" name="invoice_customer_name" required="required" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_name'])){echo $_POST['invoice_customer_name'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_customer_identification">Customer Code</label><input readonly type="text" id="invoice_customer_identification" name="invoice_customer_identification" maxlength="128" class="form-control" value="<?php if(isset($_POST['invoice_customer_identification'])){echo $_POST['invoice_customer_identification'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_contact_person">Contact Name</label><input readonly type="text" id="invoice_contact_person" name="invoice_contact_person" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_contact_person'])){echo $_POST['invoice_contact_person'];}else{ echo '';}?>"/></div>
            </div>
		</div>
		<div class="row">
            <div class="col-md-4 clearfix">
              <div class="form-group">
                <label class="control-label required" for="invoice_customer_email">Email Address</label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                  <input readonly type="email" id="invoice_customer_email" name="invoice_customer_email" required="required" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_email'])){echo $_POST['invoice_customer_email'];}else{ echo '';}?>"/>
                </div>
              </div>
            </div>
			<div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="officenumber">Office Number</label><input readonly type="text" id="officenumber" name="officenumber" maxlength="255" class="form-control" value="<?php if(isset($_POST['officenumber'])){echo $_POST['officenumber'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="cellnumber">Cell Number</label><input readonly type="text" id="cellnumber" name="cellnumber" maxlength="255" class="form-control" value="<?php if(isset($_POST['cellnumber'])){echo $_POST['cellnumber'];}else{ echo '';}?>"/></div>
            </div>
			</div>
		<div class="row">
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_invoicing_address">Physical Address</label>
				<textarea readonly id="invoice_invoicing_address" name="invoice_invoicing_address" rows="3" class="form-control"><?php if(isset($_POST['invoice_invoicing_address'])){echo $_POST['invoice_invoicing_address'];}else{ echo '';}?></textarea></div>
            </div>
            <div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_shipping_address">Postal Address</label><textarea readonly id="invoice_shipping_address" name="invoice_shipping_address" rows="3" class="form-control"><?php if(isset($_POST['invoice_shipping_address'])){echo $_POST['invoice_shipping_address'];}else{ echo '';}?></textarea></div>
            </div>
			<div class="col-md-4 clearfix">
              <div class="form-group"><label class="control-label" for="vatnumber">Vat Number</label><input readonly type="text" id="vatnumber" name="vatnumber" maxlength="255" class="form-control" value="<?php if(isset($_POST['vatnumber'])){echo $_POST['vatnumber'];}else{ echo '';}?>"/></div>
            </div>

              <input type="hidden" id="invoice_customerid" name="invoice_customerid" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customerid'])){echo $_POST['invoice_customerid'];}else{ echo '';}?>"/>

          </div>
        </div>
      <div id="invoice-like-properties" class="col-md-4"><div class="panel panel-default">
          <h3 class="panel-heading">Properties</h3>
          <div class="panel-body form-inline">
			<div class="form-group"><label class="control-label required" for="invoice_series">Series</label><select id="invoice_series" name="invoice_series" class="form-control"><option value="1">default</option></select></div>
			<?php if($type != 'RecurringInvoice'){?>	
			<div class="form-group"><label class="control-label required" for="invoice_issue_date">Issued on</label><input type="date" id="invoice_issue_date" name="invoice_issue_date" required="required" class="form-control" value="<?php if(isset($_POST['invoice_issue_date'])){echo $_POST['invoice_issue_date'];}else{ echo date('Y-m-d');}?>" /></div>
			<div class="form-group"><label class="control-label required" for="invoice_due_date">Due date</label><input type="date" id="invoice_due_date" name="invoice_due_date" required="required" class="form-control" value="<?php if(isset($_POST['invoice_due_date'])){echo $_POST['invoice_due_date'];}else{ echo date('Y-m-d');}?>" /></div>
			<div class="form-group"><div class="checkbox"><label><input type="checkbox" id="invoice_forcefully_closed" name="invoice_forcefully_closed" <?php if(isset($_POST['invoice_forcefully_closed'])){echo 'checked';}?> /> Force to be closed</label>
			</div>
			</div>
		    <div class="form-group">
			  <div class="checkbox">
				<label><input type="checkbox" id="invoice_sent_by_email" name="invoice_sent_by_email" <?php if(isset($_POST['invoice_sent_by_email'])){echo 'checked';}?> /> Mark as sent by e-mail</label>
			  </div>
		    </div>
			<?php }else{?>	
		    <div class="form-group">
			  <div class="checkbox">
				<label><input type="checkbox" id="invoice_sent_by_email" name="invoice_sent_by_email" <?php if(isset($_POST['invoice_sent_by_email'])){echo 'checked';}?> /> Automatically send generated invoices by email</label>
			  </div>
		    </div>			
			<div class="form-group">
				<div class="checkbox">
				  <label><input type="checkbox" id="recurring_invoice_enabled" name="recurring_invoice_enabled" <?php if(isset($_POST['recurring_invoice_enabled'])){echo 'checked';}?>> Enabled</label>
				</div>
			</div>
			<?php }?>
          </div>
        </div>
	  </div>
	  
    </div>
<?php if($type == 'RecurringInvoice'){?>	
<!-- Execution Data 
<option value="day">Days</option>
<option value="week">Weeks</option>
<option value="year">Years</option>
--> 
<div id="invoice-like-execution-data" class="col-md-8">
    <h3>Execution Time</h3>
    <div class="row">
      <div class="col-md-3 clearfix">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_starting_date">Start date</label><input type="date" id="recurring_invoice_starting_date" name="recurring_invoice_starting_date" required="required" class="form-control" value="<?php if(isset($_POST['recurring_invoice_starting_date'])){echo $_POST['recurring_invoice_starting_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
      <div class="col-md-3 clearfix">
        <div class="form-group"><label class="control-label" for="recurring_invoice_finishing_date">Finish date</label><input type="date" id="recurring_invoice_finishing_date" name="recurring_invoice_finishing_date" class="form-control" value="<?php if(isset($_POST['recurring_invoice_finishing_date'])){echo $_POST['recurring_invoice_finishing_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_period">Create every</label><input readonly type="number" id="recurring_invoice_period" name="recurring_invoice_period" required="required" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($_POST['recurring_invoice_period'])){echo $_POST['recurring_invoice_period'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-2">
        <label>&nbsp;</label>
        <select id="recurring_invoice_period_type" name="recurring_invoice_period_type" class="form-control"><option value="month">Months</option></select>
      </div>
      <div class="col-md-3">
        <div class="form-group"><label class="control-label" for="recurring_invoice_max_occurrences">Max occurrences</label><input type="number" id="recurring_invoice_max_occurrences" name="recurring_invoice_max_occurrences" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($_POST['recurring_invoice_max_occurrences'])){echo $_POST['recurring_invoice_max_occurrences'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-2">
        <div class="form-group"><label class="control-label" for="recurring_invoice_days_to_due">Days to due</label><input type="number" id="recurring_invoice_days_to_due" name="recurring_invoice_days_to_due" pattern=".{0,}" class="form-control" value="<?php if(isset($_POST['recurring_invoice_days_to_due'])){echo $_POST['recurring_invoice_days_to_due'];}else{ echo '1';}?>"></div>
      </div>
    </div>
  </div>
<!-- EOC Execution Data -->	
<?php }?>				  
      <input type="hidden" id="rowscount" name="rowscount"  class="form-control" value="<?php if(isset($_POST['rowscount'])){echo $_POST['rowscount'];}else{ echo '1';}?>"/>
      <div class="row">
        <div class="col-md-12">
          <h3>Line items</h3>
          <table id="invoice-like-items" class="table table-condensed table-striped align-middle">
            <thead>
              <tr>
                <th></th>
                <th class="col-md-1 cell-align-center">Product</th>
                <th class="">Description</th>
                <th class="col-md-1 cell-align-center">Qty/Hours</th>
                <th class="col-md-2 cell-align-center">Price(<?php echo $currency; ?>)</th>
                <th class="col-md-1 cell-align-center">Discount(%)</th>
                <th class="col-md-2 cell-align-left">Taxes</th>
                <th class="cell-align-right">Line total</th>
				<th class="cell-align-right">Item Tax#Id</th>
              </tr>
            </thead>
            <tbody id="Items" name="Items">
<?php if(isset($_POST['Items'])){echo $_POST['Items'];}else{ echo '';}?>
 <tr class="edit-item-row" id='row0'>
  <td class="btn-group-xs">
		  <button class="btn btn-default btn-xs" type="button" onclick="delete_row2(0)"><span class="glyphicon glyphicon-trash"></span></button>
  </td>
    <td class="col-md-xs">
    <input required="required" type="text" id="invoice_items_0_product" name="invoice_items_0_product" class="product-autocomplete-name form-control" value="<?php if(isset($_POST['invoice_items_0_product'])){echo $_POST['invoice_items_0_product'];}else{echo '';}?>"/>
        <script>
    $(function() {
	       addProductNameAutocomplete(0,'invoice_items_0_product','invoice_items_0_description','invoice_items_0_unitary_cost');
    });
    </script>
      </td>
    <td>
    <input type="text" id="invoice_items_0_description" name="invoice_items_0_description" required="required" class="product-autocomplete-description form-control" value="<?php if(isset($_POST['invoice_items_0_description'])){echo $_POST['invoice_items_0_description'];}else{echo '';}?>"/>
  </td>
  <td class="cell-align-right">
    <input type="number" id="invoice_items_0_quantity" name="invoice_items_0_quantity" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_quantity'])){echo $_POST['invoice_items_0_quantity'];}else{echo '1';}?>" 
	onchange="line_total(0)" />
    
  </td>
  <td class="cell-align-right">
    <div class="input-group">
        <input type="number" id="invoice_items_0_unitary_cost" step="0.01" name="invoice_items_0_unitary_cost" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_unitary_cost'])){echo $_POST['invoice_items_0_unitary_cost'];}else{echo '0.00';}?>"
		onchange="line_total(0)"/>    </div>
  </td>
  <td class="cell-align-right">
    <div class="input-group"><input type="number" step="0.01" id="invoice_items_0_discount_percent" name="invoice_items_0_discount_percent" required="required" class="form-control"  value="<?php if(isset($_POST['invoice_items_0_discount_percent'])){echo $_POST['invoice_items_0_discount_percent'];}else{echo '0.00';}?>" 
	onchange="line_total(0)"/>
    </div>
    
  </td>
  <td class="taxes form-inline">
    <select id="invoice_items_0_taxes" name="invoice_items_0_taxes" class="form-control" size="1" onChange="line_total(0)">
		<option value="0.00">Choose</option>
		<?php  foreach ($taxes as $tax)
        {
			$taxId = '';
			if(isset($_POST['invoice_items_0_tax_id'])){$taxId = $_POST['invoice_items_0_tax_id'];}
				if($tax['id'] == $taxId)
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value']." selected>".$tax['name']."</option>";
				}
				else
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";
				}
		}
		?>
	</select>
    
    
  </td>
  <td class="cell-align-right item-gross-amount">
          <div class='input-group'>
                            <span class='input-group-addon'><?php echo $currency; ?></span>
    <input type="text" id="invoice_items_0_totals" name="invoice_items_0_totals" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_totals'])){echo $_POST['invoice_items_0_totals'];}else{echo '0.00';}?>" readonly /> 	
  </div>
  </td>
  
  	<td class='col-md-xs'>
    <input type='text' id='invoice_items_0_tax_id' name='invoice_items_0_tax_id' class='product-autocomplete-name form-control' value="
	 <?php if(isset($_POST["invoice_items_0_tax_id"])){echo $_POST["invoice_items_0_tax_id"];}else{echo '';}?>" readonly />
	</td>
</tr>
<?php
 			// -- Saved Item Tax.
			$saveItemTaxId = '';

if(isset($_POST['invoiceItemsCount']))
{
	$count      = 0;
	$totalIndex = 0;
	
			$arraryofindexes = array();
			$invoiceItemsCount = $_POST['invoiceItemsCount'];
			// While havent found all indexes.
			while($totalIndex != $invoiceItemsCount)
			{
				$value = "invoice_items_".$count."_description";
				if(isset($_POST[$value]))
				{
					// -- count number of items
				   $arraryofindexes[] = $count;
				   $totalIndex = $totalIndex + 1;
				}
				$count = $count + 1;
			}
			
   for($j=1;$j<$_POST['invoiceItemsCount'];$j++)
   {
	    $i = $arraryofindexes[$j];
		
	   echo "
	   <tr class='edit-item-row' id='row".$i."'>
  <td class='btn-group-xs'>
		  <button class='btn btn-default btn-xs' type='button' onclick='delete_row(".$i.")'><span class='glyphicon glyphicon-trash'></span></button>
  </td>
    <td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_product' name='invoice_items_".$i."_product' class='product-autocomplete-name form-control' value='";
	 if(isset($_POST['invoice_items_'.$i.'_product'])){echo $_POST['invoice_items_'.$i.'_product'];}else{echo '';}
	echo "'/>
        <script>
    $(function() {
	       addProductNameAutocomplete(".$i.",'invoice_items_".$i."_product','invoice_items_".$i."_description','invoice_items_".$i."_unitary_cost');
    });
    </script>
      </td>
    <td>
    <input type='text' id='invoice_items_".$i."_description' name='invoice_items_".$i."_description' required='required' class='product-autocomplete-description form-control' value='";
	if(isset($_POST['invoice_items_'.$i.'_description'])){echo $_POST['invoice_items_'.$i.'_description'];}else{echo '';}
	echo "'/>
  </td>
  <td class='cell-align-right'>
    <input type='text' id='invoice_items_".$i."_quantity' name='invoice_items_".$i."_quantity' required='required' class='form-control' value='";
	if(isset($_POST['invoice_items_'.$i.'_quantity'])){echo $_POST['invoice_items_'.$i.'_quantity'];}else{echo '1';}
	
	echo "' onchange='line_total(".$i.")' />
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'>
                            
        <input type='text' id='invoice_items_".$i."_unitary_cost' name='invoice_items_".$i."_unitary_cost' required='required' class='form-control' value='";
		if(isset($_POST['invoice_items_'.$i.'_unitary_cost'])){echo $_POST['invoice_items_'.$i.'_unitary_cost'];}else{echo '';}
		echo "' onchange='line_total(".$i.")'/>    </div>
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'><input type='text' id='invoice_items_".$i."_discount_percent' name='invoice_items_".$i."_discount_percent' required='required' class='form-control'  value='";
	if(isset($_POST['invoice_items_'.$i.'_discount_percent'])){echo $_POST['invoice_items_'.$i.'_discount_percent'];}else{echo '0.00';}
	echo "' onchange='line_total(".$i.")' />
    </div>
    
  </td>
  <td class='taxes form-inline'>
    <select id='invoice_items_".$i."_taxes' name='invoice_items_".$i."_taxes' class='form-control'  size='1' onChange='line_total(".$i.")'>";	
		echo "<option data-taxid='0' value='0.00'>Choose</option>";
		foreach ($taxes as $tax)
        {
			if(isset($_POST['invoice_items_'.$i.'_taxes']))
			{

				if($tax['id'] == $_POST['invoice_items_'.$i.'_tax_id'])
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value']." selected>".$tax['name']."</option>";
				}
				else
				{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		    }
		    else
			{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		}
	echo "</select>
    
  </td>

  <td class='cell-align-right item-gross-amount'>
        <div class='input-group'>
                            <span class='input-group-addon'>".$currency."</span>
	<input type='text' id='invoice_items_".$i."_totals' name='invoice_items_".$i."_totals' required='required' class='form-control' value='";
	
	if(isset($_POST['invoice_items_'.$i.'_totals'])){echo $_POST['invoice_items_'.$i.'_totals'];}else{echo '0.00';}
	
	echo "' readonly />    </div>	</td>
	
	<td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_tax_id' name='invoice_items_".$i."_tax_id' class='product-autocomplete-name form-control' value='";
	 if(isset($_POST["invoice_items_".$i."_tax_id"])){echo $_POST["invoice_items_".$i."_tax_id"];}else{echo '';}
	echo "' readonly />
	</td>
	</tr>
	   ";
	   
   }
}
?>
                                        </tbody>
          </table>

        </div>
      </div>

      <div class="row totals">
        <div class="col-md-6">

          <!--<a id="invoice-like-add-item" href="#" onclick="add_row();" class="btn btn-default btn-info"><span class="glyphicon glyphicon-plus glyphicon-white"></span> Add item</a>-->
          <a id="invoice-like-add-item" name="invoice-like-add-item" href="#"  class="btn btn-default btn-info"><span class="glyphicon glyphicon-plus glyphicon-white"></span> Add item</a>
         
        </div>
        <div class="col-md-4 col-md-offset-2">
          <table id="invoice-like-totals" class="table table-condensed table-striped">
            <tbody>
              <tr>
                <th class="cell-size-large">Subtotal</th>
                <td class="cell-align-right base-amount">
				   <input type="text"  class="form-control-plaintext" name='Subtotal' id="Subtotal" value="<?php if(isset($_POST['Subtotal'])){echo $_POST['Subtotal'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Taxes</th>
                <td class="cell-align-right tax-amount">
				   <input type="text"  class="form-control-plaintext" name='taxamount' id="taxamount" value="<?php if(isset($_POST['taxamount'])){echo $_POST['taxamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Total (ZAR)</th>
                <td class="cell-align-right gross-amount">
					<input type="text"  class="form-control-plaintext" name='grossamount' id="grossamount" value="<?php if(isset($_POST['grossamount'])){echo $_POST['grossamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div id="invoice-like-notes" class="row">
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_terms">Terms</label><textarea id="invoice_terms" name="invoice_terms" rows="5" class="form-control" readonly><?php if(empty($legal_terms )){echo '';}else{echo trim($legal_terms );}?></textarea></div>
        </div>
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_notes">Notes</label><textarea id="invoice_notes" name="invoice_notes" rows="5" class="form-control"><?php if(isset($_POST['invoice_notes'])){echo $_POST['invoice_notes'];}else{echo '';}?></textarea></div>
        </div>
      </div>

      <input type="hidden" id="invoice__token" name="invoice[_token]" value="tLnhcCnFuLBDcEQYEi5sbr_JrRWwZspTlNPJiiNIUXM" />
    
      <input type="hidden" id="invoiceItemsCount" name="invoiceItemsCount" value="<?php if(isset($_POST['invoiceItemsCount'])){echo $_POST['invoiceItemsCount'];}else{echo '1';}?>" />

	  <input type="hidden" id="invoiceNumber" name="invoiceNumber" value="<?php if(isset($_POST['invoiceNumber'])){echo $_POST['invoiceNumber'];}else{echo 0;}?>" />

	  <input type="hidden" id="invoice_customer_id" name="invoice_customer_id" required="required" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_id'])){echo $_POST['invoice_customer_id'];}else{ echo '';}?>"/>

      <div class="form-actions">
		<input type="submit"  class="btn btn-default btn-primary" name="save" value="Save">
		<input type="hidden" class="btn btn-default" name="save_draft" value="Save as draft">
		<input type="hidden" class="btn btn-default btn-warning" name="save_email" value="Save and send by e-mail">

       <div class="float-right">
  		<a class="btn btn-default btn-primary" href = "<?php echo $backLink; ?>" name="back">Back</a>
      </div>
      </div>
    
    </form>

  </article>

      <script>
  jQuery(document).ready(function () 
  {
	comp = document.getElementById("companyname").value;
	compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;
	 
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#invoice_customer_id').val(ui.item.id);
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        jQuery('#invoice_contact_person').val(ui.item.contact_person);
        jQuery('#invoice_invoicing_address').val(ui.item.invoicing_address);
        jQuery('#invoice_shipping_address').val(ui.item.shipping_address);
		jQuery('#invoice_customerid').val(ui.item.id);
		jQuery('#officenumber').val(ui.item.officenumber);
		jQuery('#cellnumber').val(ui.item.cellnumber);
		jQuery('#vatnumber').val(ui.item.vatnumber);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
	
	 jQuery('#invoice-like-add-item').on('click', function(event)
			{
                  event.preventDefault();
					var row = add_row2();//add_row();
					var table=document.getElementById("invoice-like-items");
					var table_len=(table.rows.length)-1;
 
var id = table_len; 
var element_id = ''+'#'+'invoice_items_'+table_len+'_product'+'';
	  var element_idDesc = '#'+'invoice_items_'+table_len+'_description';
	  var element_idPrice = '#'+'invoice_items_'+table_len+'_unitary_cost';
	  jQuery("#invoice-like-items > tbody").append(row);      // Append the new elements 
	  var comp = document.getElementById("companyname").value;
	  productUrl = '/ims/autocomplete/auto_products.php?companyname='+comp;

 jQuery('#invoice_items_'+table_len+'_product').on('keydown.autocomplete',function(){	  
   jQuery(element_id).autocomplete({source: productUrl,
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);
		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 });
									
		  });
	
	
	$('#but_add').click(function(){})
	
  });
  
  
  
  function addProductNameAutocomplete(id,idReference,idDesc,idPrice)
  {
	  var element_id = ''+'#'+idReference+'';
	  var element_idDesc = '#'+idDesc;
	  var element_idPrice = '#'+idPrice;
	  var comp = document.getElementById("companyname").value;
	  productUrl = '/ims/autocomplete/auto_products.php?companyname='+comp;

    jQuery(element_id).autocomplete({
		source: productUrl,
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);

		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 // }); 
  // -- call line total calculate.
		line_total(id);
  }
  
  /*
  function arraryofindexes(invoiceItemsCount)
  {
			indexes = new Array();
			count 			= 0;
			totalIndex 		= 0;
			
			// While havent found all indexes.
			while(totalIndex != invoiceItemsCount)
			{
				value = "invoice_items_"+count+"_description";
				if(document.getElementById(value))
				{
					// -- count number of items
				   indexes[totalIndex] = count;
				   totalIndex = totalIndex + 1;
				}
				count = count + 1;
			}
	   return indexes;
  }
  
  function add_row2()
{
 //var new_name=document.getElementById("new_name").value;
 //var new_country=document.getElementById("new_country").value;
 //var new_age=document.getElementById("new_age").value;

 var currency = 'R';	
 var table=document.getElementById("invoice-like-items");
 var table_len=(table.rows.length)-1;
 if(table_len < 0)
 {
	 table_len = 0;
 }
 // alert(table_len);
  var rowSize = (table.rows.length);
  var taxhtml = '<option value="0.00">Choose</option><option data-taxid="1" value="15">SARS</option';
//alert(typeof document.getElementById("invoice_items_0_taxes").value);

	if((document.getElementById("invoice_items_0_taxes")))
	{
		//alert(typeof document.getElementById("invoice_items_0_taxes"));
		taxhtml = document.getElementById("invoice_items_0_taxes").innerHTML;
	}

 var row ="<tr class='edit-item-row' id='row"+table_len+"'>"
	  +"<td class='btn-group-xs'>"
	  +"<button class='btn btn-default btn-xs' type='button' onclick='delete_row2("+table_len+")'><span class='glyphicon glyphicon-trash'></span></button>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<input type='text' id='invoice_items_"+table_len+"_product' name='invoice_items_"+table_len+"_product' class='product-autocomplete-name form-control ui-autocomplete-input' autocomplete='on'/>"  
      +"</td>"
	  +"<td>"
      +"<input type='text' id='invoice_items_"+table_len+"_description' name='invoice_items_"+table_len+"_description' required='required' class='product-autocomplete-description form-control' />"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<input type='number' id='invoice_items_"+table_len+"_quantity' name='invoice_items_"+table_len+"_quantity' required='required' class='form-control' value='1' onchange='line_total("+table_len+")'/>"
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'>"
	  //+"<span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='number' id='invoice_items_"+table_len+"_unitary_cost' name='invoice_items_"+table_len+"_unitary_cost' required='required' class='form-control' step='0.01' value='0.00' onchange='line_total("+table_len+")'/></div>"  
	  +"</td>"
	  +"<td class='cell-align-right'>"
	  +"<div class='input-group'><input type='number' id='invoice_items_"+table_len+"_discount_percent' name='invoice_items_"+table_len+"_discount_percent' required='required' class='form-control' value='0.00' onchange='line_total("+table_len+")'/>"
	  +"</div>" 
	  +"</td>"
	  +"<td class='taxes form-inline'>"
	  +"<select id='invoice_items_"+table_len+"_taxes' name='invoice_items_"+table_len+"_taxes' class='form-control' size='1' onChange='line_total("+table_len+")'>"
	  +taxhtml
	  +"</select>"
	  +"</td>"
	  +"<td class='cell-align-right item-gross-amount'>"
	  +"<div class='input-group'><span class='input-group-addon'>"+currency+"</span>"
	  +"<input type='text' id='invoice_items_"+table_len+"_totals' name='invoice_items_"+table_len+"_totals' required='required' class='form-control' value='0.00'"
	  +"readonly />    </div>"
	  +"</td>"
	  +"<td class='col-md-xs'>"
      +"<div class='input-group'><input type='text' id='invoice_items_"+table_len+"_tax_id' name='invoice_items_"+table_len+"_tax_id' class='product-autocomplete-name form-control' value='' readonly />"
	  +"</div></td>"
	  +"</tr>";
	  LineCount = parseInt(document.getElementById("invoiceItemsCount").value) + 1;
	  document.getElementById("invoiceItemsCount").value = LineCount;
	  rowCount = parseInt(document.getElementById("rowscount").value) + 1;
	  document.getElementById("rowscount").value = rowCount;
	 return row;
}

function delete_row2(no)
{
	//alert(no);
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value) - 1;
	document.getElementById("invoiceItemsCount").value = LineCount;
	//line_total(no);
	// -- Disable Item Remover for now
	// -- Need to rethink the logic.
	document.getElementById("row"+no+"").outerHTML="";
	rowCount = parseInt(document.getElementById("rowscount").value) - 1;
	if(rowCount < 0)
	{
		rowCount = 0;
	}
	document.getElementById("rowscount").value = rowCount;
	
	// -- Determined index if there deletione e.t.c sequence would have changed.
			totalIndex = 0;
			count      = 0;
			
			indexes = arraryofindexes(rowCount);
			
			// -- Loop through all the item & recalculate
			//alert(indexes);
			// -- Reinitialise Totals
			totalIndex = indexes.length;
			
			document.getElementById('Subtotal').value = '0.00';
			document.getElementById('taxamount').value = '0.00';
			document.getElementById('grossamount').value = '0.00';
			for(i=0;i<totalIndex;i++)
			{
				//alert(arraryofindexes[i]);
					line_total2(indexes[i],indexes);
			}
}

function line_total2(no,arraryofindexes)
{
	// - Declarations
	var Linetotal 			= 0.00;
	var LineCount			= 0;
	var subtotal 		    = 0.00;
	var Total				= 0.00;
	var tax 			    = 0.00;
	var Totaltax 			= 0.00;
	var LinetotalIter 		= 0.00;		

	//alert("Debug 1");
	var qty 	  		    = parseInt(document.getElementById("invoice_items_"+no+"_quantity").value);
	if(isNaN(qty)){qty = 1;}
	//alert("Debug qty");
	var unitprice 			= parseFloat(document.getElementById("invoice_items_"+no+"_unitary_cost").value);
	if(isNaN(unitprice)){unitprice = parseFloat(0.00).toFixed(2);}else{unitprice = parseFloat(unitprice).toFixed(2);}

	//alert("Debug unitprice");
	var discountPercentage  = parseFloat(document.getElementById("invoice_items_"+no+"_discount_percent").value);
	if(isNaN(discountPercentage)){discountPercentage = parseFloat(0.00).toFixed(2);}else{discountPercentage = parseFloat(discountPercentage).toFixed(2);}

	// Tax
	//tax = parseFloat(document.getElementById("invoice_items_"+no+"_taxes").value);
	
	//if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}
	//alert('tax : '+tax);	

	Linetotal = (qty * unitprice);

	// -- discount
	Linetotal = Linetotal - (discountPercentage / 100) * Linetotal;
	
	// -- Add tax on discounted amount.
	//	Linetotal = Linetotal - (tax / 100) * Linetotal;

			//alert('Linetotal = '+Linetotal+'+'+tax+'/ 100'+'*'+ Linetotal);

	if(isNaN(Linetotal)){Linetotal = parseFloat(0.00).toFixed(2);}else{Linetotal = parseFloat(Linetotal).toFixed(2);}
	
	document.getElementById("invoice_items_"+no+"_totals").value = Linetotal;
	
	// -- How many Item on the added add the subtotal for their line items.
	LineCount = parseInt(document.getElementById("invoiceItemsCount").value);
//	alert("Debug LineCount" + LineCount);

	if(LineCount > 0)
	{	var iter = 0;
		var iter2 = 0;
//	alert(LineCount);

		for (iter = 0; iter < LineCount; iter++)
		{
			// Tax
			iter2 = arraryofindexes[iter];
			//alert("iter : "+iter+" LineCount = "+LineCount);
			tax = parseFloat(document.getElementById("invoice_items_"+iter2 +"_taxes").value);
			if(isNaN(tax)){tax = parseFloat(0.00).toFixed(2);}else{tax = parseFloat(tax).toFixed(2);}

			// -- Linetotal
			LinetotalIter = parseFloat(document.getElementById("invoice_items_"+iter2 +"_totals").value);
			if(isNaN(LinetotalIter)){LinetotalIter = parseFloat(0.00).toFixed(2);}else{LinetotalIter = parseFloat(LinetotalIter).toFixed(2);}

			// -- Subtotal
			subtotal = parseFloat(subtotal) + parseFloat(LinetotalIter);
			if(isNaN(subtotal)){subtotal = parseFloat(0.00).toFixed(2);}else{subtotal = parseFloat(subtotal).toFixed(2);}
				//alert('subtotal : '+subtotal);
	//		alert('Totaltax = '+Totaltax+"+"+tax+"/ 100 * "+LinetotalIter);	

			// -- Total Tax
			Totaltax = parseFloat(Totaltax) + parseFloat((tax / 100)) * parseFloat(LinetotalIter);
			
			if(isNaN(Totaltax)){Totaltax = parseFloat(0.00).toFixed(2);}else{Totaltax = parseFloat(Totaltax).toFixed(2);}

			// -- Total inclusive of Tax
					Total = parseFloat(subtotal) + parseFloat(Totaltax);
					Total = parseFloat(Total).toFixed(2);
					
			// -- Get the Tax ID					
		}
		
		//if (isNaN(subtotal)) subtotal = 0.00;
		document.getElementById("Subtotal").value = subtotal;
		
		// -- Taxes
		//alert(tax);
		document.getElementById("taxamount").value = Totaltax;

		// -- Total inclusive of Tax
		document.getElementById("grossamount").value = Total;

jQuery("#invoice_items_"+no+"_taxes").on("change",function()
{
    var dataid = $("#invoice_items_"+no+"_taxes option:selected").attr('data-taxid');
	var element_tax_item = document.getElementById("invoice_items_"+no+"_tax_id");//tax_id
				//alert(dataid+' '+element_tax_item.toString());

	if (typeof(element_tax_item) != 'undefined' && element_tax_item != null)
	{

		element_tax_item.value = dataid;
	}
    
});

	}
}*/
  </script>

  
     <script>

  </script>