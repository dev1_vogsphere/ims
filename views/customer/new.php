<?php
Helper::menu('','','','','active','');
$menu = ROOT.'views/Layouts/menu.php';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

include_once($menu);?>

<section id="bd2">
    
    <div class="container">
<?php if(isset($_POST['customerid']))
{
	if(($_POST['customerid']) > 0)
	{?>	
			
		<div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            customer <? echo $_POST['description'];?> added. 
		</div>
<?php 
	}
}?>
<?php
if(isset($_POST['error']))
{
	if(!empty($_POST['error']))
	{
	echo '<div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            '.$_POST['error'].'
	</div>';
	}
}
?>

  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
      <h2>Customer </h2>
    </header>
	
<form method='post' action='#' class="form-stacked">
      <div class="row">
	    <div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="title">Customer/Registered Company Name</label>
				<input type="text" class="form-control" id="title" required="required" placeholder="Enter a name" name="title" value="<?php if(isset($_POST['title'])){echo $_POST['title'];}else{ echo '';}?>">
			</div>
		</div>

		<div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="description">Customer Code</label>
				<input type="text" class="form-control"  id="description" placeholder="System generated" name="description" value="<?php if(isset($_POST['description'])){echo $_POST['description'];}else{ echo '';}?>" readonly>
			</div>
		</div>
		
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="customer_contact_person">Contact Name</label>
		  <input type="text" required="required" id="customer_contact_person" name="customer_contact_person" class="form-control" value="<?php if(isset($_POST['customer_contact_person'])){echo $_POST['customer_contact_person'];}else{ echo '';}?>"/></div>
        </div>
		
        <div class="col-md-4 clearfix">
          <div class="form-group">
            <label class="control-label required" for="customer_email">Email Address</label>
            <div class="input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
              <input type="email" id="customer_email" name="customer_email" required="required" maxlength="191" class="form-control" value="<?php if(isset($_POST['customer_email'])){echo $_POST['customer_email'];}else{ echo '';}?>"/>
            </div>
          </div>
        </div>
	
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="customer_office_number">Office Number</label>
		  <input type="number" required="required" id="customer_office_number" name="customer_office_number" class="form-control" value="<?php if(isset($_POST['customer_office_number'])){echo $_POST['customer_office_number'];}else{ echo '';}?>"/></div>
        </div>

		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="customer_cell_number">Cell Number (Optional)</label>
		  <input type="number" id="customer_cell_number" name="customer_cell_number" class="form-control" value="<?php if(isset($_POST['customer_cell_number'])){echo $_POST['customer_cell_number'];}else{ echo '';}?>"/></div>
        </div>

	</div>
    <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_invoicing_address">Physical Address</label>
		  <textarea id="customer_invoicing_address" required="required" name="customer_invoicing_address" class="form-control"><?php if(isset($_POST['customer_invoicing_address'])){echo $_POST['customer_invoicing_address'];}else{ echo '';}?></textarea></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_shipping_address">Postal Address (Optional)</label><textarea id="customer_shipping_address" name="customer_shipping_address" class="form-control"><?php if(isset($_POST['customer_shipping_address'])){echo $_POST['customer_shipping_address'];}else{ echo '';}?></textarea></div>
        </div>
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="companyregistration">Company Registration Nr(Optional)</label>
		  <input type="text" id="companyregistration" name="companyregistration" class="form-control" value="<?php if(isset($_POST['companyregistration'])){echo $_POST['companyregistration'];}else{ echo '';}?>"/></div>
        </div>
    </div>
	    <div class="row">
		<div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="title">IMS Organisation Name</label>
				<input type="text" class="form-control" id="organisation" placeholder="name of organisation" name="organisation" readonly value="<?php if(!empty($companyname)){echo $companyname;}else{ echo '';}?>">
			</div>
		</div>
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_vat">VAT Number (Optional)</label>
		  <input type="number" id="customer_vat" name="customer_vat" class="form-control" value="<?php if(isset($_POST['customer_vat'])){echo $_POST['customer_vat'];}else{ echo '';}?>"/></div>
        </div>	
		
		</div>

	  <input type="hidden" id="customerid" name="customerid" value="<?php if(isset($_POST['customerid'])){echo $_POST['customerid'];}else{echo 0;}?>" />
	  <input type="hidden" id="error" name="error" value="<?php if(isset($_POST['error'])){echo $_POST['error'];}else{echo '';}?>" />

	      <div class="form-actions">
       <!-- <input type="submit" class="btn btn-default btn-primary" name="save" value="Save"> -->
	   
    <button type="submit" class="btn btn-primary">Save</button>

    <div class="float-right">
  	   <a class="btn btn-default btn-primary" href = "/ims/customer/index" name="back">Back</a>				
    </div>
</form>
</article>

</div>
</section> 


  <footer id="ft">
  </footer>
