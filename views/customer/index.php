<?php 
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
Helper::menu('','','','','active','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
$companyname = '';
$companyname = 'testing';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

$username = '';
if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

include_once($menu);?>
<?php 
$status 		 = ''; 
$statusInactive  = '';
$statusAll 		 = '';
if(isset($_POST['search_invoice_status']))
		{ 	
					switch ($_POST['search_invoice_status']) 
					{
					case "Inactive":
						$statusInactive  = 'selected';
						break;
					case "All":
						$statusAll  = 'selected';
						break;
					default:
						$status   = 'selected';
					}
		}else
		{ 	$status = '';
}

$dt = date('Y-m-d');
$from = date("Y-m-01", strtotime($dt));
$to   = date("Y-m-t", strtotime($dt));
?>
<section id="bd">
  <div class="searchform">
     <div class="container">
        <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
		<input type="text" id="username" name="username" value ="<?php echo $username; ?>" hidden />
        
		<input type="text" id="from" name="from" value ="<?php echo $from; ?>"  hidden />
		<input type="text" id="to" name="to" value ="<?php echo $to; ?>"  hidden />
		
		<form name="search_customer" method="post" action="#" class="search form-inline">
		  <fieldset>
			<div class="form-group"><label class="control-label" for="search_customer_terms" >Terms</label>
				<input type="text" id="search_customer_terms" name="search_customer_terms" class="form-control" 
				value="<?php if(isset($_POST['search_customer_terms'])){echo $_POST['search_customer_terms'];}else{ echo '';}?>"/>
			</div>
		  <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label>
			  <select id="search_invoice_status" name="search_invoice_status" class="form-control">
			  <option value=""  <?php echo $status;?>>Active</option>
			  <option value="Inactive" <?php echo $statusInactive;?>>Inactive</option>
			  <option value="All" <?php echo $statusAll;?>>All</option>
			  </select>
		  </div>
			<div class="form-group float-right btn-toolbar">
			  <div class="btn-group">
				<button id="searchsubmit" name="searchsubmit" type="submit" class="btn btn-default btn-primary">Search</button>
			  </div>
			  <div class="btn-group">
				<a id="searchreset" name="searchreset" href="/<?php echo DIR; ?>/customer/index" class="btn btn-default btn-warning">Reset</a>
			  </div>
			</div>
		  </fieldset>
		</form>
    </div>
  </div>
   <div class="container">
<!-- Email Invoices BOC -->              
		<div class="popup" data-popup="popup-1">
			<div class="popup-inner">
				<h2>eMail Statement - Feedback</h2>
				<div id="myProgress">
					<div id="myBar">10%</div>
				</div>
				<div id="divSuccess">
				
				</div>
				<p><a data-popup-close="popup-1" href="#">Close</a></p>
				<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
			</div>
		</div>
	    <div id="divWebsites">
			<?php 
			$count = count($eMailInvoices); 
			for($i=0;$i<$count;$i++)
				{
				  $href = $eMailInvoices[$i];
				  echo '<button type="button" data-popup-open="popup-1" class="btn btn-info" onclick="setupLinks('.$href.')">eMail Statement</button>';
				}
			?>	
		</div>		
<!-- Email Invoices EOC -->                      
        <div id="invoices-latest-recurring-invoices">
    <form name="customer_list" method="post" action="/<?php echo DIR; ?>customer/index">
<!--
<ul id="toolbar" class="table-actions list-inline list-unstyled">
  <li>
    <button type="submit" id="delete" name="delete" class="btn btn-default btn-danger" title="Remove selected" data-confirm="Are you sure you want to remove the selected customers?"><span class="glyphicon glyphicon-trash"></span></button>
  </li>
</ul>-->
<table class="table table-condensed table-striped align-middle" data-type="customers">
  <thead>
    <tr>
      <!--<th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th>-->
	  <th class="cell-size-medium">
        <a class="sortable" href="#" title="Name">#</a>
      </th>
      <th class="cell-size-medium">
        <a class="sortable" href="#" title="Name">Customer/Registered Company Name</a>
      </th>
      <th>
        <a class="sortable" href="#" title="Identification">Customer Code</a>
      </th>
      <th>
        <a class="sortable" href="#" title="Identification">Status</a>
      </th>
      <th class="cell-size-medium cell-align-right"></th>
    </tr>
  </thead>
  <tbody>
  <?php
	$customer_count = 1;
	$i = 0;
    $statusIn = '';

        foreach ($customers as $customer)
        {// /ims/pdf/pdf_statement.php?id=1&username=vogspesw&type=Details&from=2020-12-01&to=2020-12-31
		$href = htmlentities("href='/ims/pdf/pdf_statement.php?id=".$customer['id']."&username=".$username."'");
		$hrefEmail = htmlentities("href='/ims/pdf/pdf_statement.php?id=".$customer['id']."&emailFlag=Y"."&username=".$username."&type=Details&from=".$from."&to=".$to."'");
		$statusIn  = $customer['status'];
		$customercode = str_replace(" ","_",$customer["identification"]);	
		$customercode = htmlentities($customercode);
		$id = $customer["id"];
		$msoa = htmlentities("href='/ims/invoice/monthlystatements/$id'");
		//$msoa = $customer["id"];
		if(empty($statusIn)){$statusIn = 'Active'; }
		echo "<tr data-link='/".DIR."/customer/edit/".$customer["id"]."'>
				<!-- <td class='table-action cell-align-center no-link'>
					<div class='form-group'><div class='checkbox'>                                       
					<label><input type='checkbox' id='customer_".$i."' name='customer_".$i."' value='".$customer["id"]."' /> </label>
					</div></div>
				</td>--> 
				<td class='cell-size-medium'>".$customer_count."</td>				
				<td class='cell-size-medium'>".$customer['name']."</td>
				<td>".$customer['identification']."</td>
				<!-- HIDE FOR NOW <td class='cell-align-right'>$currency 4,700.00</td>
				<td class='cell-align-right'>$currency 13,101.44</td> -->
				<td>".$statusIn."</td>
				<td class='cell-align-right'><a href='/ims/invoice/search_customer_invoice/".$customercode."' class='btn btn-default btn-info'><span class='glyphicon glyphicon-book'></span> Invoices</a></td>
				<td class='cell-align-right payments'><a ".$href."' class='btn btn-default btn-info'><span class='glyphicon glyphicon-print'></span> Statement</a></td>".	
				'<td class="table-action cell-align-center no-link"><button type="button" data-popup-open="popup-1" class="btn btn-info" onclick="setupLinks('.$hrefEmail.')"><span class="glyphicon glyphicon-envelope"></span> Statement</button></td>'.
				"<td class='table-action cell-align-center no-link'><a ".$msoa."' class='btn btn-default btn-info'><span class='glyphicon glyphicon-print'></span> MON Statement</a></td>".
			'</tr>';
			$i = $i + 1;
		    $customer_count = $customer_count + 1;//$i;			
		}
	?>
</tbody>
</table>

	<input type="hidden" id="customer_count" name="customer_count" value="<?php echo $customer_count;?>" />

</form>
</div>
</div>
	  
</section>

    <footer id="ft">
  </footer>

  <!-- <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/frameworks.js"></script>
   <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/5e397c9.js"></script>
	-->
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
       comp = document.getElementById("companyname").value;
	 compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;

    jQuery('#search_customer_terms').autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#search_customer_terms').val(ui.item.name);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });	
  
<!-- Email PopUps -->  
// -- Email Invoices BOC //              
function setupLinks() 
{ 
    var webSites = document.getElementById("divWebsites"); 

    if (webSites) 
    { 
        //assign window.open event to divWebsites links 
        var links = webSites.getElementsByTagName("a"); 
        var i, href, title; 
	email = 0;
	id = 0;
	username = '';
	
	// -- loop BOC
	for (i=0; i<links.length; i++) 
        { 	
            href = links[i].getAttribute("href"); 
            title = links[i].getAttribute("title"); 
	    str  = links[i];

	    Startpostion = href.indexOf("id");			
	    urlString = href.substr(Startpostion); 
	    var res = urlString.split("&");
			
	    id=res[0].substr(res[0].indexOf("=") + 1);
		username = res[1].substr(res[1].indexOf("=") + 1);;
	    email= res[2].substr(res[2].indexOf("=") + 1);

	    jQuery(document).ready(function()
	     {
		jQuery.ajax({	
					type: "POST",
  					url:"/ims/pdf/pdf_statement.php",
					data:{id:id,username:username,email:email},
					success: function(data)
					{
					  Message = Message+"\n<p>Mail Succesfull sent to for statement#:"+id+" to email address :"+email+" by user :"+username+"</p>";
					 // alert(Message);
					 jQuery("#divSuccess").append(Message);
					 			move();

					 
				    }
				});
			});
	}
	// -- loop EOC
    }
}

// -- Email Invoices one at a time BOC //              
function setupLinks(href) 
{ 
    //var webSites = document.getElementById("divWebsites"); 

    if (href) 
    { 
        //assign window.open event to divWebsites links 
        //var links = webSites.getElementsByTagName("a"); 
	emailFlag = 0;
	id = 0;
		username = document.getElementById('username').value;
		typeMON  = 'Details';
		dateFromMON = document.getElementById('from').value;
		dateToMON   = document.getElementById('to').value;
		
	// -- loop BOC
	//for (i=0; i<links.length; i++) 
      //  { 	
           // href = links[i].getAttribute("href"); 
           // title = links[i].getAttribute("title"); 
	    str = href;

	    Startpostion = href.indexOf("id");			
	    urlString = href.substr(Startpostion); 
	    var res = urlString.split("&");
			
	    id=res[0].substr(res[0].indexOf("=") + 1);
	    emailFlag= res[1].substr(res[1].indexOf("=") + 1);

//alert(id);
//alert(emailFlag);
	    jQuery(document).ready(function()
	     {
		jQuery.ajax({	
					type: "GET",
  					url:"/ims/pdf/pdf_statement.php",
					data:{id:id,username:username,emailFlag:emailFlag,type:typeMON,
						 from:dateFromMON,to:dateToMON},
					success: function(data)
					{
						var Message = '';
					  Message = Message+"\n<p>Mail Succesfull sent to for statement#:"+id+".</p>";
					 // alert(data);
					 jQuery("#divSuccess").append(Message);
					 			move();

					 
				    }
				});
			});
	//}
	// -- loop EOC
    }
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
// -- Email Statement EOC // 

</script>	