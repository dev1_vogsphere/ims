<?php
Helper::menu('','','','','active','');
$menu = ROOT.'views/Layouts/menu.php';
include_once($menu);
?>

<section id="bd">
    
    <div class="container">
 <?php if(isset($_POST['customerid']))
{
	if(($_POST['customerid']) > 0)
	{?>	
			
		<div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            customer <? echo $_POST['customerid'];?> updated successfully. 
		</div>
<?php 
	}
}

$status 		 = ''; 
$statusInactive  = '';
$statusAll 		 = '';
$statusIn = '';
if(isset($customer['status'])){$statusIn = $customer['status'];}
	switch ($statusIn) 
	{
	case "Inactive":
		$statusInactive  = 'selected';
		break;
	case "All":
		$statusAll  = 'selected';
		break;
	default:
		$status   = 'selected';
	}                 
   ?>   
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
      <h2><?php if(isset($customer['name'])){echo $customer['name'];}else{ echo '';}?></h2>
    </header>

    <form name="customer" method="post" action="<?php $id = ''; if(isset($customer['id'])){$id = $customer['id'];}else{ echo '';} echo "/".DIR."/customer/edit/".$id;?>"  class="form-stacked">
      <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="name">Customer/Registered Company Name</label><input type="text" id="customer_name" name="customer_name" required="required" readonly maxlength="191" class="form-control" value="<?php if(isset($customer['name'])){echo $customer['name'];}else{ echo '';}?>" /></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_identification">Customer Code</label><input type="text" id="customer_identification" readonly name="customer_identification" maxlength="128" class="form-control" value="<?php if(isset($customer['identification'])){echo $customer['identification'];}else{ echo '';}?>" /></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_contact_person">Contact Name</label><input type="text" id="customer_contact_person" name="customer_contact_person" class="form-control" value="<?php if(isset($customer['contact_person'])){echo $customer['contact_person'];}else{ echo '';}?>" /></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group">
            <label class="control-label required" for="customer_email">E-mail</label>
            <div class="input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
              <input type="email" id="customer_email" name="customer_email" required="required" maxlength="191" class="form-control" value="<?php if(isset($customer['email'])){echo $customer['email'];}else{ echo '';}?>" />
            </div>
            
          </div>
        </div>
		
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="customer_office_number">Office Number</label>
		  <input type="number" required="required" id="customer_office_number" name="customer_office_number" class="form-control" value="<?php if(isset($customer['officenumber'])){echo $customer['officenumber'];}else{ echo '';}?>"/></div>
        </div>

		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="customer_cell_number">Cell Number (Optional)</label>
		  <input type="number" id="customer_cell_number" name="customer_cell_number" class="form-control" value="<?php if(isset($customer['cellnumber'])){echo $customer['cellnumber'];}else{ echo '';}?>"/></div>
        </div>
		
      </div>

      <div class="row">
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" required="required" for="customer_invoicing_address">Physical Address</label><textarea id="customer_invoicing_address" name="customer_invoicing_address" class="form-control">
<?php if(isset($customer['invoicing_address'])){echo $customer['invoicing_address'];}else{ echo '';}?> 
</textarea></div>
        </div>
        <div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_shipping_address">Postal Address (Optional)</label><textarea id="customer_shipping_address" name="customer_shipping_address" class="form-control">
<?php if(isset($customer['shipping_address'])){echo $customer['shipping_address'];}else{ echo '';}?>
</textarea></div>
        </div>
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label required" for="companyregistration">Company Registration Nr(Optional)</label>
		  <input type="text" id="companyregistration" name="companyregistration" class="form-control" value="<?php if(isset($customer['companyregistration'])){echo $customer['companyregistration'];}else{ echo '';}?>"/></div>
        </div>		
      </div>

		<div class="row">
		<div class="col-md-4 clearfix">
			<div class="form-group">
				<label class="control-label required" for="title">IMS Organisation Name</label>
				<input type="text" class="form-control" id="organisation" placeholder="name of organisation" name="organisation" readonly value="<?php if(isset($customer['organisation'])){echo $customer['organisation'];}else{ echo '';}?>" />
			</div>
		</div>
		<div class="col-md-4 clearfix">
          <div class="form-group"><label class="control-label" for="customer_vat">VAT Number (Optional)</label>
		  <input type="number" id="customer_vat" name="customer_vat" class="form-control" value="<?php if(isset($customer['vatnumber'])){echo $customer['vatnumber'];}else{ echo '';}?>"/></div>
        </div>
		<div class="col-md-4 clearfix">		
			<div class="form-group"><label class="control-label" for="status">Status</label>
			  <select id="status" name="status" class="form-control">
			  <option value=""  <?php echo $status;?>>Active</option>
			  <option value="Inactive" <?php echo $statusInactive;?>>Inactive</option>
			  </select>
		    </div>
		</div>
		</div>
      <input type="hidden" id="customer__token" name="customer[_token]" value="B_r1BvdowcvOvVqXk7yKZP4aE_3qBUEoI-rJf_Vrhn0" />
          <input type="hidden" id="customerid" name="customerid" value="<?php if(isset($_POST['customerid'])){echo $_POST['customerid'];}else{echo 0;}?>" />


      <div class="form-actions">
        <input type="submit" class="btn btn-default btn-primary" name="save" value="Save">
        <div class="float-right">
       <!-- <a class="btn btn-default btn-danger" href="/<?php echo DIR; ?>/customer/remove/<?php if(isset($customer['id'])){echo $customer['id'];}else{echo 0;}?>" data-confirm="Are you sure you want to delete the customer?">Delete</a> -->
	     <a class="btn btn-default btn-primary" href = "/ims/product/index" name="back">Back</a>		
        </div>
      </div>
    
    </form>

  </article>

    </div>
  </section>

  <footer id="ft">
  </footer>

   <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/frameworks.js"></script>
