<?php 
//	require(ROOT . 'controllers/loginController.php');
/* Menu option is selected */
$Dashboard	= '';
$Invoices	= '';
$RInvoices	= '';
$Quotations	= '';
$Customers	= '';
$Products	= '';
$Payments   = '';
$whitepapers = '';

/* New Button for each Entity*/
//$NewInvoicesPath	= '/invoice/create';
$NewInvoicesPath	= '/invoice/createinvoice';
//$NewRInvoicesPath	= '/recurring/createRecurringInvoice';
$NewRInvoicesPath	= '/invoice/createRecurringInvoice';
//$NewQuotationsPath	= '/quotation/create';
$NewQuotationsPath	= '/invoice/createquotation';
$NewCustomersPath	= '/customer/create';
$NewProductsPath	= '/product/create';

/* New Buttons for text*/
$NewInvoicesText	= 'New Invoice';
$NewRInvoicesText	= 'New recurring invoice';
$NewQuotationsText	= 'New Quotation';
$NewCustomersText	= 'New Customer';
$NewProductsText	= 'New Product';

/* New Buttons for whitepapers */
$NewwhitepapersPath	= '/whitepapers/create';
$NewwhitepapersText	= 'New White Paper';

/* New Buttons for whitepapers */
$NewPaymentsPath	= '/invoice/uploadbatchpayments';
$NewPaymentsText	= 'New Payment';

/*Generic variable*/
$NewbuttonPath = '';
$NewbuttonText = '';

// -- toggle create button.
$toggleCreate = false;

if(isset($_SESSION['Dashboard']))
{
	$Dashboard	= $_SESSION['Dashboard'];
	if(!empty($Dashboard))
	{    	
	 $NewbuttonPath = $NewInvoicesPath;
	 $NewbuttonText = $NewInvoicesText;
     // -- toggle create button.
     $toggleCreate = true;
	}
}
if(isset($_SESSION['Invoices']))
{
	$Invoices	= $_SESSION['Invoices'];
	if(!empty($Invoices))
	{    	
	 $NewbuttonPath = $NewInvoicesPath;
	 $NewbuttonText = $NewInvoicesText;
     // -- toggle create button.
     $toggleCreate = true;
	}
}
if(isset($_SESSION['RInvoices']))
{
	$RInvoices	= $_SESSION['RInvoices'];
	if(!empty($RInvoices))
	{    
		$NewbuttonPath = $NewRInvoicesPath;
		$NewbuttonText = $NewRInvoicesText;
		// -- toggle create button.
		$toggleCreate = true;
	}
}
if(isset($_SESSION['Quotations']))
{
	$Quotations	= $_SESSION['Quotations'];
	if(!empty($Quotations))
	{    
		$NewbuttonPath = $NewQuotationsPath;
		$NewbuttonText = $NewQuotationsText;
	   // -- toggle create button.
	   $toggleCreate = true;
	}
}	
if(isset($_SESSION['Customers']))
{	
	$Customers	= $_SESSION['Customers'];
		if(!empty($Customers))
		{
			$NewbuttonPath = $NewCustomersPath;
			$NewbuttonText = $NewCustomersText;
			// -- toggle create button.
			$toggleCreate = true;
		}
}

if(isset($_SESSION['Products']))
{
	$Products	= $_SESSION['Products'];
	if(!empty($Products))
	{    
		$NewbuttonPath = $NewProductsPath;
		$NewbuttonText = $NewProductsText;
		// -- toggle create button.
		$toggleCreate = true;
	}
}
if(isset($_SESSION['whitepapers']))
{
	$whitepapers	= $_SESSION['whitepapers'];
	if(!empty($whitepapers))
	{    
		$NewbuttonPath = $NewwhitepapersPath;
		$NewbuttonText = $NewwhitepapersText;
		// -- toggle create button.
		$toggleCreate = true;
	}
}

if(isset($_SESSION['Payments']))
{
	$Payments	= $_SESSION['Payments'];
	if(!empty($Payments))
	{    
		$NewbuttonPath = $NewPaymentsPath;
		$NewbuttonText = $NewPaymentsText;
		// -- toggle create button.
		$toggleCreate = true;
	}
}


?>
<header id="hd">
    <div class="container">
    <h1>
      <a href="/<?php echo DIR; ?>/invoice/index">
        <!-- <img src="/<?php echo DIR; ?>/images/Fintergrate 71x19px.jpeg" alt="Invoice Software" />
        <span class="version">v1.0.3-dev</span> -->
      </a>
    </h1>

    <nav class="user">
      <ul class="list-unstyled">
        <li><span class="glyphicon glyphicon-user"></span>
          Logged in as 
		  <?php if(!isset($_SESSION['username_IMS']))
				{
					$_SESSION['username_IMS'] = Controller::$username; 
					echo Controller::$username;
				}
				else
				{ 
					echo $_SESSION['username_IMS'];
				} ?>
        </li>
        <li>
          <a href="/<?php echo DIR; ?>/globalsetting/index" data-shortcut="alt+g, s">
            <span class="glyphicon glyphicon-cog"></span>
            Settings
          </a>
        </li>
        <!--<li><a href="https://github.com/siwapp/siwapp-sf3/wiki" target="_blank">
            <span class="glyphicon glyphicon-question-sign"></span>
            Help</a>
        </li>-->
        <li>
          <a href="/<?php echo DIR; ?>/login/logout" data-shortcut="alt+g, q">
            <span class="glyphicon glyphicon-log-out"></span>
            Log out</a>
        </li>
      </ul>
    </nav>

    <nav class="main">
      <label for="drop" class="toggle"><span class="glyphicon glyphicon-menu-hamburger"></span>MENU</label>
      <input type="checkbox" id="drop" />

      <ul class="nav nav-tabs menu">
        <li class="<?php echo $Dashboard; ?>">
          <a href="/<?php echo DIR; ?>/dashboard/index">Dashboard</a>
        </li>
        <li class="<?php echo $Invoices; ?>">
          <a href="/<?php echo DIR; ?>/invoice/index"><?php if(empty($typeText)){ echo 'Invoices';} else { echo $typeText;}?></a>
        </li>
		<li class="<?php echo $RInvoices;?>">
          <a href="/<?php echo DIR; ?>/recurring/index">Recurring invoices</a>
        </li>
        <li class="<?php echo $Quotations;?>">
          <a href="/<?php echo DIR; ?>/quotation/index">Quotations</a>
        </li>
        <li class="<?php echo $Customers;?>">
          <a href="/<?php echo DIR; ?>/customer/index">Customers</a>
        </li>
        <li class="<?php echo $Products; ?>">
          <a href="/<?php echo DIR; ?>/product/index">Products</a>
        </li>
		
		<li class="<?php echo $Payments; ?>">
          <a href="/<?php echo DIR; ?>/invoice/payments/">Payments</a>
        </li>
		
		<li class="<?php echo $whitepapers; ?>">
          <a href="/<?php echo DIR; ?>/whitepapers/index">White Papers</a>
        </li>
		
      </ul>

      <div id="create-button" class="btn-group">
                <a href="/<?php echo DIR.$NewbuttonPath; ?>" class="btn btn-default btn-primary">
          <span class="glyphicon glyphicon-plus"></span>
          <?php echo $NewbuttonText;?>
        </a>
              </div>

    </nav>

    </div>
  </header>