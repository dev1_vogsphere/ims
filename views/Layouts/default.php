<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Invoice Software | IMS</title>
     <link rel="stylesheet" href="/<?php echo DIR; ?>/libs/css/6da8889.css" />
     <link rel="stylesheet" href="/<?php echo DIR; ?>/libs/css/678669b.css" />
     <link rel="stylesheet" href="/<?php echo DIR; ?>/libs/css/4cc3b63.css" />
     <link rel="stylesheet" href="/<?php echo DIR; ?>/libs/css/2059864.css" />
     <link rel="stylesheet" href="/<?php echo DIR; ?>/libs/css/custom.css" rel="stylesheet">

    <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/0b48bb7.js"></script>
	<script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/frameworks.js"></script>
    <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/5e397c9.js"></script>
	<script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/core.js"></script>
	
	<!-- autocomplete 
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
-->
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
	<script src="/<?php echo DIR; ?>/libs/js/jquery.min.js"></script> -->
	

    <script src="/<?php echo DIR; ?>/libs/js/jquery-ui.min.js"></script>
	
	<script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/table_script.js"></script>
	
	<script src="/<?php echo DIR; ?>/libs/js/bootstrap.min.js"></script> 

    <link rel="shortcut icon" href="/<?php echo DIR; ?>/libs/favicon.ico" />
	<script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/jquery.tableToExcel.js"></script>
	
</head>
<body>
  <?php
        echo $content_for_layout;
  ?>
    <!-- <script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/7b44f5d.js"></script>-->
  
  </body>
</html>