<?php 
Helper::menu('active','','','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
$companyname = '';
if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

include_once($menu);?>

  <section id="bd">
<div class="searchform">
        <div class="container">  
            <input type="text" id="companyname" name="companyname" value ="<?php echo $companyname; ?>" hidden />
		
<!-- Search Form -->
<form name="search_invoice" method="post" action="/<?php echo DIR; ?>/invoice/index" class="search form-inline">
<!-- /<?php //echo DIR; ?>/invoice> -->
  <fieldset>

    <div class="form-group"><label class="control-label" for="search_invoice_terms">Customer Name</label>
	<input type="text" id="search_invoice_terms" name="search_invoice_terms" class="form-control" 
	value="<?php if(isset($_POST['search_invoice_terms'])){ echo $_POST['search_invoice_terms'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_from">Date from</label>
	<input type="date" id="search_invoice_date_from" name="search_invoice_date_from" class="form-control" value="<?php if(isset($_POST['search_invoice_date_from'])){ echo $_POST['search_invoice_date_from'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_to">to</label>
	<input type="date" id="search_invoice_date_to" name="search_invoice_date_to" class="form-control" value="<?php if(isset($_POST['search_invoice_date_to'])){ echo $_POST['search_invoice_date_to'];}?>"/></div>
	
    <div class="form-group float-right btn-toolbar">
      <div class="btn-group">
        <button id="searchsubmit" name="searchsubmit"  type="submit" class="btn btn-default btn-primary">Search</button>
        <a href="#search-secondary" data-toggle="collapse" class="btn btn-default"" aria-controls="search-secondary">
          Advanced        <span class="caret"></span>
        </a>
      </div>
      <div class="btn-group">
        <a id="search-reset" href="/<?php echo DIR; ?>/invoice/index" class="btn btn-default btn-warning">Reset</a>
      </div>
    </div>

  </fieldset>

<fieldset id="search-secondary" class="collapse">
<?php  
$statusDraft   = '';
$statusOpened  = '';
$statusOverdue = '';
$statusClosed  = '';
$status = '';

if(isset($_POST['search_invoice_status']))
		{ 	
					switch ($_POST['search_invoice_status']) 
					{
					case "0":
						$statusDraft   = 'selected';
						break;
					case "1":
						$statusClosed  = 'selected';
						break;
					case "2":
						$statusOpened  = 'selected';
						break;
					case "3":
						$statusOverdue = 'selected';
						break;
					default:
						$status = 'selected';
					}
		}else
		{ 	$status = '';
}
?>
      <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label>
	  <select id="search_invoice_status" name="search_invoice_status" class="form-control">
	  <option value=""  <?php echo $status;?>></option>
	  <option value="0" <?php echo  $statusDraft;?>>Draft</option>
	  <option value="2" <?php echo $statusOpened;?>>Opened</option>
	  <option value="3" <?php echo $statusOverdue;?>>Overdue</option>
	  <option value="1" <?php echo $statusClosed;?>>Closed</option></select></div>
      <div class="form-group"><label class="control-label" for="search_invoice_customer">Customer</label><input type="text" id="customer_identification" name="customer_identification" class="form-control" value="<?php if(isset($_POST['customer_identification'])){ echo $_POST['customer_identification'];}?>"/></div>
</fieldset>

  <!--HIDE FOR NOW <fieldset id="search-secondary" class="collapse">
      <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label><select id="search_invoice_status" name="search_invoice[status]" class="form-control"><option value=""></option><option value="0">Draft</option><option value="2">Opened</option><option value="3">Overdue</option><option value="1">Closed</option></select></div>
      <div class="form-group"><label class="control-label" for="search_invoice_customer">Customer</label><input type="text" id="customer_identification" name="customer_identification" class="form-control" value="<?php if(isset($_POST['customer_identification'])){ echo $_POST['customer_identification'];}?>"/></div>
      <div class="form-group"><label class="control-label" for="search_invoice_series">Series</label><select id="search_invoice_series" name="search_invoice[series]" class="form-control"><option value=""></option><option value="1">default</option></select></div>
  </fieldset>
-->
    </form></div></div>
<!-- EOC Search Form -->
	
	<div class="container">
<!-- BEGIN Modal -->
<div class="" >
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
        <!-- Modal content  -->
	<div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Automatic Updates Invoices</h4>
            </div>
        <div class="modal-body">
		</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>  
	</div>  
</div> 		
</div>
<!-- END Modal -->	
		<div id="dashboard-summary">

    <table class="table table-bordered sw-table-horizontal payments">
      <thead>
        <tr>
          <th colspan="2">Payments summary</th>
        </tr>
      </thead>
      <tbody>
        <tr class="receipts">
          <th>Total Paid</th>
          <td><?php if(isset($summary))
					{
						echo $currency.number_format($summary[0],2); 
					}
					else
					{
						echo $currency.'0.00'; 						
					}
		  ?></td>
        </tr>
        <tr class="due">
          <th>Total Invoiced</th>
			<td><!-- 7,689,834.54 -->
				<?php if(isset($summary))
					{
						echo $currency.number_format($summary[1],2); 
					}
					else
					{
						echo $currency.'0.00'; 						
					}
				?></td>
        </tr>
        <tr class="overdue">
          <th>Total Due</th><!--7,696,107.74-->
				<td>
				<?php
				$totalDue = 0.00;
				if(isset($summary))
					{
						$totalDue = $summary[1] - $summary[0];
						echo $currency.number_format($totalDue,2); 
					}
					else
					{
						echo $currency.'0.00'; 						
					}
				?></td>        </tr>
      </tbody>
    </table>
<!-- BOC Dashboard -->		
    <table class="table table-bordered sw-table-horizontal balance">
      <thead>
        <tr>
          <th colspan="2">Invoice Status</th>
        </tr>
      </thead>
      <tbody>
        <tr class="total">
          <th><span class='label closed'>CLOSED Invoices</span></th>
				<td>
					<?php if(isset($statuses))
						  {
							  echo $statuses[1];
						  }
						  else
						  {
							 echo '0'; 
						  }
					?>
				</td>
		  </tr>
        <tr class="net">
          <th><span class='label opened'>OPENED Invoices</span></th>
          <td>
		  			<?php if(isset($statuses))
						  {
							  echo $statuses[2];
						  }
						  else
						  {
							 echo '0'; 
						  }
					?>

		  </td>
        </tr>
        <tr class="taxes">
          <th><span class='label overdue'>OVERDUE Invoices</span></th>
          <td>
		  					<?php if(isset($statuses))
						  {
							  echo $statuses[3];
						  }
						  else
						  {
							 echo '0'; 
						  }
					?>

		  </td>
        </tr>
      </tbody>
    </table> 
    <!--<table class="table table-bordered sw-table-horizontal taxes">
      <thead>
        <tr>
          <th colspan="2">Withholding taxes</th>
        </tr>
      </thead>
      <tbody>
              </tbody>
    </table>  -->
		</div>
<!-- EOC Dashboard -->	
<br/>
<!--<table id="recurring-summary1" class="table table-bordered table-condensed col-md-2">
		      <thead>
				<tr>
					<th><a class='ls-modal btn btn-info' href='<?php echo "/".DIR."/webservices/auto_updateinvoices.php"; ?>' class='btn btn-default btn-info'><span class='glyphicon glyphicon-piggy-bank'></span> Auto Update Invoices Statuses</a></th>
				    <th><a class='ls-modal btn btn-info' href='<?php echo "/".DIR."/webservices/auto_followup_dueinvoices.php"; ?>' class='btn btn-default btn-info'><span class='glyphicon glyphicon-piggy-bank'></span> Followup Emails OVERDUE Invoices</a></th>				
				</tr>
			</thead>
</table>-->
<div class="dashboard-listing">
    <h2>Recent invoices</h2>
<table class="table table-condensed table-striped align-middle" data-type="invoices">
  <thead>
    <tr>
       <th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th>
            <th class="cell-size-medium">
                Number
              </th>
      <th>
               Customer

              </th>
      <th class="cell-size-medium">
              Date

              </th>
      <th class="cell-size-medium">
                Due Date

              </th>
      <th class="cell-size-small-medium cell-align-center">
                Status

              </th>
      <th class="cell-size-medium cell-align-right">
                Due

              </th>
      <th class="cell-size-medium cell-align-right">
                Total
              </th>
      <th class="cell-size-medium"></th>
    </tr>
  </thead>
  <tbody>
  <?php
  	   $i = 0;
	   $invoices_count = 0;

        foreach ($invoices as $invoice)
        {
			// -- statuses.
			/*overdueinvoices
			define("DRAFT", 0);
			define("CLOSED", 1);
			define("OPENED", 2);
			define("OVERDUE", 3);
			*/
			$Statusclass = ''; 
			if($invoice['status'] == DRAFT)
			{
				$Statusclass = 'draft'; 
			}

			if($invoice['status'] == CLOSED)
			{
				$Statusclass = 'closed'; 
			}

			if($invoice['status'] == OPENED)
			{
				$Statusclass = 'opened'; 
			}

			if($invoice['status'] == OVERDUE)
			{
				$Statusclass = 'overdue'; 
			}			
$tot = $invoice['base_amount']+$invoice['tax_amount'];
			echo"<tr data-link='/".DIR."/invoice/editinvoice/".$invoice['id']."'>
					<td class='table-action cell-align-center no-link'>
						<div class='form-group'><div class='checkbox'>                                        
						<label><input type='checkbox' id='invoice_".$i."' name='invoice_".$i."' value='".$invoice['id']."' /> </label>
						</div></div>
					</td>
					<td class='cell-size-medium'>".$invoice['id']."</td>
					<td>".$invoice['customer_name']."</td>
					<td>".$invoice['issue_date']."</td>
					<td>".$invoice['due_date']."</td>
					<td class='cell-align-center'>
					  <span class='label $Statusclass'>$Statusclass</span>
					</td>
					<td class='cell-align-right'>$currency".number_format(($invoice['base_amount']+$invoice['tax_amount']) - $invoice['paid_amount'],2)."</td>
					<td class='cell-align-right'>$currency".number_format($tot,2)."</td>
					<td class='cell-align-right payments'>";
					echo "<a href='/".DIR."/invoice/payments/".$invoice['id']."' class='btn btn-default btn-info payments' title='Payments'  data-toggle='modal' data-target='#payment-modal' data-remote='false'><span class='glyphicon glyphicon-piggy-bank'></span> Payments</a>
					</td>
				</tr>";
			  	   $i = $i + 1;

	  }
	  $invoice_count = $i;

	?>
 </tbody>
</table>	 

<div id="payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="payments-modal-title">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="payments-modal-title">Payments</h4> </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
  </div>

  <div class="dashboard-listing">
    <h2>Past due invoices</h2>
	
<table class="table table-condensed table-striped align-middle" data-type="invoices">
  <thead>
    <tr>
       <th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th>
            <th class="cell-size-medium">
                Number
              </th>
      <th>
               Customer

              </th>
      <th class="cell-size-medium">
              Date

              </th>
      <th class="cell-size-medium">
                Due Date

              </th>
      <th class="cell-size-small-medium cell-align-center">
                Status

              </th>
      <th class="cell-size-medium cell-align-right">
                Due

              </th>
      <th class="cell-size-medium cell-align-right">
                Total
              </th>
      <th class="cell-size-medium"></th>
    </tr>
  </thead>
  <tbody>
  <?php
  	   $i = 0;
	   $invoices_count = 0;

        foreach ($overdueinvoices as $invoice)
        {
			// -- statuses.
			/*
			define("DRAFT", 0);
			define("CLOSED", 1);
			define("OPENED", 2);
			define("OVERDUE", 3);
			*/
			$Statusclass = ''; 
			if($invoice['status'] == DRAFT)
			{
				$Statusclass = 'draft'; 
			}

			if($invoice['status'] == CLOSED)
			{
				$Statusclass = 'closed'; 
			}

			if($invoice['status'] == OPENED)
			{
				$Statusclass = 'opened'; 
			}

			if($invoice['status'] == OVERDUE)
			{
				$Statusclass = 'overdue'; 
			}			
$tot = $invoice['base_amount']+$invoice['tax_amount'];
			echo"<tr data-link='/".DIR."/invoice/editinvoice/".$invoice['id']."'>
					<td class='table-action cell-align-center no-link'>
						<div class='form-group'><div class='checkbox'>                                        
						<label><input type='checkbox' id='invoice_".$i."' name='invoice_".$i."' value='".$invoice['id']."' /> </label>
						</div></div>
					</td>
					<td class='cell-size-medium'>".$invoice['id']."</td>
					<td>".$invoice['customer_name']."</td>
					<td>".$invoice['issue_date']."</td>
					<td>".$invoice['due_date']."</td>
					<td class='cell-align-center'>
					  <span class='label $Statusclass'>$Statusclass</span>
					</td>
					<td class='cell-align-right'>$currency".number_format($invoice['base_amount'] - $invoice['paid_amount'],2)."</td>
					<td class='cell-align-right'>$currency".number_format($tot,2)."</td>
					<td class='cell-align-right payments'>";
					echo "<a href='/".DIR."/invoice/payments/".$invoice['id']."' class='btn btn-default btn-info payments' title='Payments'  data-toggle='modal' data-target='#payment-modal' data-remote='false'><span class='glyphicon glyphicon-piggy-bank'></span> Payments</a>
					</td>
				</tr>";
			  	   $i = $i + 1;

	  }
	  $invoice_count = $i;

	?>
 </tbody>
</table>	 	
<div id="payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="payments-modal-title">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="payments-modal-title">Payments</h4> </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
 
	</div> 
 
  </section>

    <footer id="ft">
  </footer>

<!-- Autocomplete -->  
<script>
  jQuery(document).ready(function () 
  {//alert("customer name");
	 comp = document.getElementById("companyname").value;
	 compUrl = '/ims/autocomplete/auto_customers.php?companyname='+comp;
    jQuery('#search_invoice_terms').autocomplete({
      source: compUrl,
      select: function (event, ui) {
		jQuery('#search_invoice_terms').val(ui.item.name);
		jQuery('#customer_identification').val(ui.item.identification);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });
  
$('.ls-modal').on('click', function(e){
  e.preventDefault();
  $('#myModal').modal('show').find('.modal-body').load($(this).attr('href'));
});  
</script>		