<?php 
Helper::menu('','','','','','active');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>

<section id="bd">
          <div class="searchform">
        <div class="container">
            
<form name="search_product" method="post" action="#" class="search form-inline">

  <fieldset>	

    <div class="form-group"><label class="control-label" for="search_product_terms">Terms</label>
	<input type="text" id="search_product_terms" name="search_product_terms" class="form-control" value="<?php if(isset($_POST['search_product_terms'])){echo $_POST['search_product_terms'];}else{ echo '';}?>"/></div>

    <div class="form-group float-right btn-toolbar">
      <div class="btn-group">
        <button id="searchsubmit" name="searchsubmit" type="submit" class="btn btn-default btn-primary">Search</button>
      </div>
      <div class="btn-group">
        <a id="search-reset" name="search-reset" href="/<?php echo DIR; ?>/product/index" class="btn btn-default btn-warning">Reset</a>
      </div>
    </div>

  </fieldset>


    </form>
        </div>
      </div>
    
    <div class="container">
                  
        <div id="invoices-latest-recurring-invoices">
    
<form name="product_list" method="post" action="#">
<ul id="toolbar" class="table-actions list-inline list-unstyled">
  <li>
    <button type="submit" name="delete" class="btn btn-default btn-danger" title="Remove selected" data-confirm="Are you sure you want to remove the selected products?"><span class="glyphicon glyphicon-trash"></span></button>
  </li>
</ul>
<table class="table table-condensed table-striped align-middle" data-type="tables">
<thead>
    <tr>
      <th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th> 
      <th class="cell-size-medium">
        <a class="sortable" href="#" title="Table Name">Table Name</a>

      </th>
      <th>
        <a class="sortable" href="#" title="Backup">Backup</a>

      </th>
    </tr>
  </thead>
  <tbody>
	 <?php
        foreach ($backup as $table)
        {	
			/*echo "<tr data-link='/".DIR."/product/edit/".$product["id"]. "'>";
			echo "<td class='table-action cell-align-center no-link'>
			  <div class='form-group'>
			  <div class='checkbox'>";                                        
			echo "<label><input type='checkbox' id='product_".$i."' name='product_".$i."' value='".$product["id"]."' /> </label>
			</div></div>
			</td>
			<td class='cell-size-medium'>".$product["reference"]."</td>
			<td>".$product["description"]."</td>
			<td class='cell-align-right'>$currency".number_format($product["price"],2)."</td>
			<!--<td class='cell-align-right'></td>
			<td class='cell-align-right'>0.00</td>-->
		  </tr>";*/
		  
		  	foreach($table AS $key => $value) { $table[$key] = stripslashes($value); } 
							echo "<tr>";  
							echo "<td valign='top'>".nl2br( $value ). "</td>";  
							echo "<td valign='top'><input type='checkbox' name='".$value."' checked></td>"; 
							echo "</tr>";
		}
	?>
	
      </tbody>
</table>
<label>BackUp File Directory :</label><input type='text' value='C:\\DOWNLOAD\\' id='path' readonly />
<div style="text-align:center"><button align="right" type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="BackUpDatabase()">Run a Database Backup</button></div><br /><br />
    </form>
  </div>
    </div>
<!-- PopUp - script -->
<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
    <div class="popup-inner">
        <h2>Database Backup - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
			
		</div>
		</br>
					<div id="divPaymentSchedules">
					
					</div>
	<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>				
<!-- PopUp - Script End-->	
  </section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery('#search_product_terms').autocomplete({
      source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {
		jQuery('#search_product_terms').val(ui.item.reference);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
  });	
</script>