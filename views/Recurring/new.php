<?php
Helper::menu('','','active','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';

include_once($menu);?>
    
    <div class="container">
                    

<?php if(isset($_POST['invoiceNumber']))
{
	if(($_POST['invoiceNumber']) > 0)
	{?>	
			
		<div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">×</a>
            Invoice # <? echo $_POST['invoiceNumber'];?> added. 
		</div>
<?php 
	}
}?>
      
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
              <h2>Recurring Invoice [draft]</h2>

  <ul class="list-inline list-unstyled">
    <li>
      <span class="label draft">Draft</span>
    </li>
    <li>
            <span class="label">Not sent by e-mail</span>
          </li>
  </ul>
    </header>

    <form name="invoice" method="post" action="#" class="form-stacked">

      <div class="row">
        <div id="invoice-like-customer-data" class="col-md-8">

          <h3>Customer data</h3>

          <div class="row">
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label required" for="invoice_customer_name">Name</label><input type="text" id="invoice_customer_name" name="invoice_customer_name" required="required" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_name'])){echo $_POST['invoice_customer_name'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_customer_identification">Identification</label><input type="text" id="invoice_customer_identification" name="invoice_customer_identification" maxlength="128" class="form-control" value="<?php if(isset($_POST['invoice_customer_identification'])){echo $_POST['invoice_customer_identification'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_contact_person">Contact person</label><input type="text" id="invoice_contact_person" name="invoice_contact_person" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_contact_person'])){echo $_POST['invoice_contact_person'];}else{ echo '';}?>"/></div>
            </div>

            <div class="col-md-6 clearfix">
              <div class="form-group">
                <label class="control-label required" for="invoice_customer_email">E-mail</label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                  <input type="email" id="invoice_customer_email" name="invoice_customer_email" required="required" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customer_email'])){echo $_POST['invoice_customer_email'];}else{ echo '';}?>"/>
                </div>
                
              </div>
            </div>

            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_invoicing_address">Invoicing address</label>
				<textarea id="invoice_invoicing_address" name="invoice_invoicing_address" rows="3" class="form-control"><?php if(isset($_POST['invoice_invoicing_address'])){echo $_POST['invoice_invoicing_address'];}else{ echo '';}?></textarea></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_shipping_address">Shipping address</label><textarea id="invoice_shipping_address" name="invoice_shipping_address" rows="3" class="form-control"><?php if(isset($_POST['invoice_shipping_address'])){echo $_POST['invoice_shipping_address'];}else{ echo '';}?></textarea></div>
            </div>
			
              <input type="hidden" id="invoice_customerid" name="invoice_customerid" maxlength="255" class="form-control" value="<?php if(isset($_POST['invoice_customerid'])){echo $_POST['invoice_customerid'];}else{ echo '';}?>"/>

          </div>
        </div>

        <div id="invoice-like-properties" class="col-md-4"><div class="panel panel-default">
          <h3 class="panel-heading">Properties</h3>
          <div class="panel-body form-inline">
            <div class="form-inline"><div class="form-group"><label class="control-label required" for="invoice_series">Series</label><select id="invoice_series" name="invoice_series" class="form-control"><option value="1">default</option></select></div></div>
  <div class="form-group" style="display:none"><label class="control-label required" for="invoice_issue_date">Issued on</label><input type="date" id="invoice_issue_date" name="invoice_issue_date" required="required" class="form-control" value="<?php if(isset($_POST['invoice_issue_date'])){echo $_POST['invoice_issue_date'];}else{ echo date('Y-m-d');}?>" /></div>
  <div class="form-group" style="display:none"><label class="control-label required" for="invoice_due_date">Due date</label><input type="date" id="invoice_due_date" name="invoice_due_date" required="required" class="form-control" value="<?php if(isset($_POST['invoice_due_date'])){echo $_POST['invoice_due_date'];}else{ echo date('Y-m-d');}?>" /></div>
  <div class="form-group" style="display:none"><div class="checkbox">                                        <label><input type="checkbox" id="invoice_forcefully_closed" name="invoice_forcefully_closed" value="<?php if(isset($_POST['invoice_forcefully_closed'])){echo $_POST['invoice_forcefully_closed'];}else{ echo '1';}?>" /> Force to be closed</label>
    </div></div>
  <div class="form-group" style="display:none"><div class="checkbox">                                        <label><input type="checkbox" id="invoice_sent_by_email" name="invoice_sent_by_email" value="<?php if(isset($_POST['invoice_sent_by_email'])){echo $_POST['invoice_sent_by_email'];}else{ echo '1';}?>" /> Mark as sent by e-mail</label>
    </div></div>
          </div>
		  <div class="form-inline">
    <div class="form-group"><div class="checkbox">                                        <label><input type="checkbox" id="recurring_invoice_enabled" name="recurring_invoice_enabled" value="<?php if(isset($_POST['recurring_invoice_enabled'])){echo $_POST['recurring_invoice_enabled'];}else{ echo '1';}?>"> Enabled</label>
    </div></div>
  </div>
        </div></div>
<!-- Execution Data --> 
<div id="invoice-like-execution-data" class="col-md-8">
    <h3>Execution Time</h3>
    <div class="row">
      <div class="col-md-4 clearfix">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_starting_date">Start date</label><input type="date" id="recurring_invoice_starting_date" name="recurring_invoice_starting_date" required="required" class="form-control" value="<?php if(isset($_POST['recurring_invoice_starting_date'])){echo $_POST['recurring_invoice_starting_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
      <div class="col-md-4 clearfix">
        <div class="form-group"><label class="control-label" for="recurring_invoice_finishing_date">Finish date</label><input type="date" id="recurring_invoice_finishing_date" name="recurring_invoice_finishing_date" class="form-control" value="<?php if(isset($_POST['recurring_invoice_finishing_date'])){echo $_POST['recurring_invoice_finishing_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_period">Create every</label><input type="number" id="recurring_invoice_period" name="recurring_invoice_period" required="required" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($_POST['recurring_invoice_period'])){echo $_POST['recurring_invoice_period'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-3">
        <label>&nbsp;</label>
        
        <select id="recurring_invoice_period_type" name="recurring_invoice_period_type" class="form-control"><option value="day">Days</option><option value="week">Weeks</option><option value="month">Months</option><option value="year">Years</option></select>
      </div>
      <div class="col-md-3">
        <div class="form-group"><label class="control-label" for="recurring_invoice_max_occurrences">Max occurrences</label><input type="number" id="recurring_invoice_max_occurrences" name="recurring_invoice_max_occurrences" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($_POST['recurring_invoice_max_occurrences'])){echo $_POST['recurring_invoice_max_occurrences'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-2">
        <div class="form-group"><label class="control-label" for="recurring_invoice_days_to_due">Days to due</label><input type="number" id="recurring_invoice_days_to_due" name="recurring_invoice_days_to_due" pattern=".{0,}" class="form-control" value="<?php if(isset($_POST['recurring_invoice_days_to_due'])){echo $_POST['recurring_invoice_days_to_due'];}else{ echo '1';}?>"></div>
      </div>
    </div>
  </div>
<!-- EOC Execution Data -->		
              </div>

      <div class="row">
        <div class="col-md-12">

          <h3>Line items</h3>

          <table id="invoice-like-items" class="table table-condensed table-striped align-middle">
            <thead>
              <tr>
                <th></th>
                <th class="col-md-1 cell-align-center">Product</th>
                <th class="">Description</th>
                <th class="col-md-1 cell-align-center">Qty/Hours</th>
                <th class="col-md-2 cell-align-center">Price</th>
                <th class="col-md-1 cell-align-center">Discount</th>
                <th class="col-md-2 cell-align-left">Taxes</th>
                <th class="cell-align-right">Line total</th>
				<th class="cell-align-right">Item Tax#Id</th>
              </tr>
            </thead>
            <tbody id="Items" name="Items">
<?php if(isset($_POST['Items'])){echo $_POST['Items'];}else{ echo '';}?>
 <tr class="edit-item-row" id='row0'>
  <td class="btn-group-xs">
		  <button class="btn btn-default btn-xs" type="button" onclick="delete_row(0)"><span class="glyphicon glyphicon-trash"></span></button>
  </td>
    <td class="col-md-xs">
    <input type="text" id="invoice_items_0_product" name="invoice_items_0_product" class="product-autocomplete-name form-control" value="<?php if(isset($_POST['invoice_items_0_product'])){echo $_POST['invoice_items_0_product'];}else{echo '';}?>"/>
        <script>
    $(function() {
	       addProductNameAutocomplete(0,'invoice_items_0_product','invoice_items_0_description','invoice_items_0_unitary_cost');
    });
    </script>
      </td>
    <td>
    <input type="text" id="invoice_items_0_description" name="invoice_items_0_description" required="required" class="product-autocomplete-description form-control" value="<?php if(isset($_POST['invoice_items_0_description'])){echo $_POST['invoice_items_0_description'];}else{echo '';}?>"/>
  </td>
  <td class="cell-align-right">
    <input type="text" id="invoice_items_0_quantity" name="invoice_items_0_quantity" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_quantity'])){echo $_POST['invoice_items_0_quantity'];}else{echo '1';}?>" 
	onchange="line_total(0)" />
    
  </td>
  <td class="cell-align-right">
    <div class="input-group">
                            <span class="input-group-addon"><?php echo $currency; ?> </span>
        <input type="text" id="invoice_items_0_unitary_cost" name="invoice_items_0_unitary_cost" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_unitary_cost'])){echo $_POST['invoice_items_0_unitary_cost'];}else{echo '';}?>"
		onchange="line_total(0)"/>    </div>
    
  </td>
  <td class="cell-align-right">
    <div class="input-group"><input type="text" id="invoice_items_0_discount_percent" name="invoice_items_0_discount_percent" required="required" class="form-control"  value="<?php if(isset($_POST['invoice_items_0_discount_percent'])){echo $_POST['invoice_items_0_discount_percent'];}else{echo '0.00';}?>" 
	onchange="line_total(0)"/><span class="input-group-addon">%</span>
    </div>
    
  </td>
  <td class="taxes form-inline">
    <select id="invoice_items_0_taxes" name="invoice_items_0_taxes" class="form-control" size="1" onChange="line_total(0)">
		<option value="0.00">Choose</option>
		<?php  foreach ($taxes as $tax)
        {
			$taxId = '';
			if(isset($_POST['invoice_items_0_tax_id'])){$taxId = $_POST['invoice_items_0_tax_id'];}
				if($tax['id'] == $taxId)
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value']." selected>".$tax['name']."</option>";
				}
				else
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";
				}
		}
		?>
	</select>
    
    
  </td>
  <td class="cell-align-right item-gross-amount">
          <div class='input-group'>
                            <span class='input-group-addon'><?php echo $currency; ?></span>
    <input type="text" id="invoice_items_0_totals" name="invoice_items_0_totals" required="required" class="form-control" value="<?php if(isset($_POST['invoice_items_0_totals'])){echo $_POST['invoice_items_0_totals'];}else{echo '0.00';}?>" readonly /> 	
  </div>
  </td>
  
  	<td class='col-md-xs'>
    <input type='text' id='invoice_items_0_tax_id' name='invoice_items_0_tax_id' class='product-autocomplete-name form-control' value="
	 <?php if(isset($_POST["invoice_items_0_tax_id"])){echo $_POST["invoice_items_0_tax_id"];}else{echo '';}?>" readonly />
	</td>
</tr>
<?php
 			// -- Saved Item Tax.
			$saveItemTaxId = '';

if(isset($_POST['invoiceItemsCount']))
{
   for($i=1;$i<$_POST['invoiceItemsCount'];$i++)
   {
	   echo "
	   <tr class='edit-item-row' id='row".$i."'>
  <td class='btn-group-xs'>
		  <button class='btn btn-default btn-xs' type='button' onclick='delete_row(".$i.")'><span class='glyphicon glyphicon-trash'></span></button>
  </td>
    <td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_product' name='invoice_items_".$i."_product' class='product-autocomplete-name form-control' value='";
	 if(isset($_POST['invoice_items_'.$i.'_product'])){echo $_POST['invoice_items_'.$i.'_product'];}else{echo '';}
	echo "'/>
        <script>
    $(function() {
	       addProductNameAutocomplete(".$i.",'invoice_items_".$i."_product','invoice_items_".$i."_description','invoice_items_".$i."_unitary_cost');
    });
    </script>
      </td>
    <td>
    <input type='text' id='invoice_items_".$i."_description' name='invoice_items_".$i."_description' required='required' class='product-autocomplete-description form-control' value='";
	if(isset($_POST['invoice_items_'.$i.'_description'])){echo $_POST['invoice_items_'.$i.'_description'];}else{echo '';}
	echo "'/>
  </td>
  <td class='cell-align-right'>
    <input type='text' id='invoice_items_".$i."_quantity' name='invoice_items_".$i."_quantity' required='required' class='form-control' value='";
	if(isset($_POST['invoice_items_'.$i.'_quantity'])){echo $_POST['invoice_items_'.$i.'_quantity'];}else{echo '1';}
	
	echo "' onchange='line_total(".$i.")' />
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'>
                            <span class='input-group-addon'>".$currency."</span>
        <input type='text' id='invoice_items_".$i."_unitary_cost' name='invoice_items_".$i."_unitary_cost' required='required' class='form-control' value='";
		if(isset($_POST['invoice_items_'.$i.'_unitary_cost'])){echo $_POST['invoice_items_'.$i.'_unitary_cost'];}else{echo '';}
		echo "' onchange='line_total(".$i.")'/>    </div>
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'><input type='text' id='invoice_items_".$i."_discount_percent' name='invoice_items_".$i."_discount_percent' required='required' class='form-control'  value='";
	if(isset($_POST['invoice_items_'.$i.'_discount_percent'])){echo $_POST['invoice_items_'.$i.'_discount_percent'];}else{echo '0.00';}
	echo "' onchange='line_total(".$i.")' /><span class='input-group-addon'>%</span>
    </div>
    
  </td>
  <td class='taxes form-inline'>
    <select id='invoice_items_".$i."_taxes' name='invoice_items_".$i."_taxes' class='form-control'  size='1' onChange='line_total(".$i.")'>";	
		echo "<option data-taxid='0' value='0.00'>Choose</option>";
		foreach ($taxes as $tax)
        {
			if(isset($_POST['invoice_items_'.$i.'_taxes']))
			{
						//echo "<option value=".$_POST['invoice_items_'.$i.'_taxes'].">".$_POST['invoice_items_'.$i.'_taxes']."</option>";

				if($tax['id'] == $_POST['invoice_items_'.$i.'_tax_id'])
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value']." selected>".$tax['name']."</option>";
				}
				else
				{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		    }
		    else
			{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		}
	echo "</select>
    
  </td>

  <td class='cell-align-right item-gross-amount'>
        <div class='input-group'>
                            <span class='input-group-addon'>".$currency."</span>
	<input type='text' id='invoice_items_".$i."_totals' name='invoice_items_".$i."_totals' required='required' class='form-control' value='";
	
	if(isset($_POST['invoice_items_'.$i.'_totals'])){echo $_POST['invoice_items_'.$i.'_totals'];}else{echo '0.00';}
	
	echo "' readonly />    </div>	</td>
	
		<td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_tax_id' name='invoice_items_".$i."_tax_id' class='product-autocomplete-name form-control' value='";
	 if(isset($_POST["invoice_items_".$i."_tax_id"])){echo $_POST["invoice_items_".$i."_tax_id"];}else{echo '';}
	echo "' readonly />
	</td>
	</tr>
	   ";
	   
   }
}
?>
                                        </tbody>
          </table>

        </div>
      </div>

      <div class="row totals">
        <div class="col-md-6">

          <!--<a id="invoice-like-add-item" href="#" onclick="add_row();" class="btn btn-default btn-info"><span class="glyphicon glyphicon-plus glyphicon-white"></span> Add item</a>-->
          <a id="invoice-like-add-item" name="invoice-like-add-item" href="#"  class="btn btn-default btn-info"><span class="glyphicon glyphicon-plus glyphicon-white"></span> Add item</a>
          
<!--<td><input type="button" class="btn btn-default btn-info" onclick="add_row();" value="Add Item"></td> -->

        </div>
        <div class="col-md-4 col-md-offset-2">
          <table id="invoice-like-totals" class="table table-condensed table-striped">
            <tbody>
              <tr>
                <th class="cell-size-large">Subtotal</th>
                <td class="cell-align-right base-amount">
				   <input type="text"  class="form-control-plaintext" name='Subtotal' id="Subtotal" value="<?php if(isset($_POST['Subtotal'])){echo $_POST['Subtotal'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Taxes</th>
                <td class="cell-align-right tax-amount">
				   <input type="text"  class="form-control-plaintext" name='taxamount' id="taxamount" value="<?php if(isset($_POST['taxamount'])){echo $_POST['taxamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Total (ZAR)</th>
                <td class="cell-align-right gross-amount">
					<input type="text"  class="form-control-plaintext" name='grossamount' id="grossamount" value="<?php if(isset($_POST['grossamount'])){echo $_POST['grossamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div id="invoice-like-notes" class="row">
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_terms">Terms</label><textarea id="invoice_terms" name="invoice_terms" rows="5" class="form-control"><?php if(isset($_POST['invoice_terms'])){echo $_POST['invoice_terms'];}else{echo trim($_SESSION['legal_terms']);}?></textarea></div>
        </div>
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_notes">Notes</label><textarea id="invoice_notes" name="invoice_notes" rows="5" class="form-control"><?php if(isset($_POST['invoice_notes'])){echo $_POST['invoice_notes'];}else{echo '';}?></textarea></div>
        </div>
      </div>

      <input type="hidden" id="invoice__token" name="invoice[_token]" value="tLnhcCnFuLBDcEQYEi5sbr_JrRWwZspTlNPJiiNIUXM" />
    
      <input type="hidden" id="invoiceItemsCount" name="invoiceItemsCount" value="<?php if(isset($_POST['invoiceItemsCount'])){echo $_POST['invoiceItemsCount'];}else{echo '1';}?>" />

	  <input type="hidden" id="invoiceNumber" name="invoiceNumber" value="<?php if(isset($_POST['invoiceNumber'])){echo $_POST['invoiceNumber'];}else{echo 0;}?>" />

      <div class="form-actions">
		<input type="submit" class="btn btn-default btn-primary" name="save" value="Save">
		<input type="submit" class="btn btn-default" name="save_draft" value="Save as draft">
		<input type="submit" class="btn btn-default btn-warning" name="save_email" value="Save and send by e-mail">

  <div class="float-right">
      </div>
      </div>
    
    </form>

  </article>

      <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: '/ims/autocomplete/auto_customers.php',
      select: function (event, ui) {
		jQuery('#invoice_customer_id').val(ui.item.id);
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        jQuery('#invoice_contact_person').val(ui.item.contact_person);
        jQuery('#invoice_invoicing_address').val(ui.item.invoicing_address);
        jQuery('#invoice_shipping_address').val(ui.item.shipping_address);
		jQuery('#invoice_customerid').val(ui.item.id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
	
	 jQuery('#invoice-like-add-item').on('click', function(event)
			{
                  event.preventDefault();
					var row = add_row();
					var table=document.getElementById("invoice-like-items");
					var table_len=(table.rows.length)-1;
 
var id = table_len; 
var element_id = ''+'#'+'invoice_items_'+table_len+'_product'+'';
	  var element_idDesc = '#'+'invoice_items_'+table_len+'_description';
	  var element_idPrice = '#'+'invoice_items_'+table_len+'_unitary_cost';
	  					jQuery("#invoice-like-items > tbody").append(row);      // Append the new elements 

 jQuery('#invoice_items_'+table_len+'_product').on('keydown.autocomplete',function(){	  
   jQuery(element_id).autocomplete({source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);

		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 });
									
		  });
	
	
	$('#but_add').click(function(){})
	
  });
  
  
  
  function addProductNameAutocomplete(id,idReference,idDesc,idPrice)
  {
	  var element_id = ''+'#'+idReference+'';
	  var element_idDesc = '#'+idDesc;
	  var element_idPrice = '#'+idPrice;
	 
    jQuery(element_id).autocomplete({
		
		
      source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);

		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 // }); 
  // -- call line total calculate.
		line_total(id);
  }
  </script>

  
     <script>
  /*jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: '/ims/autocomplete/auto_customers.php',
      select: function (event, ui) {
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        jQuery('#invoice_contact_person').val(ui.item.contact_person);
        jQuery('#invoice_invoicing_address').val(ui.item.invoicing_address);
        jQuery('#invoice_shipping_address').val(ui.item.shipping_address);
		jQuery('#invoice_customerid').val(ui.item.id);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });
  
  
  function addProductNameAutocomplete(id,idReference,idDesc,idPrice)
  {
	  var element_id = ''+'#'+idReference+'';
	  var element_idDesc = '#'+idDesc;
	  var element_idPrice = '#'+idPrice;
	 jQuery(document).ready(function () 
  {//alert(element_id);
    jQuery(element_id).autocomplete({
      source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.reference);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);
				// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
  }); 
  
    // -- call line total calculate.
		line_total(id);
  }
  </script>
      </div>
  </section>

  <footer id="ft">
  </footer>

    <script type="text/javascript" charset="utf-8" src="/ims2/web/js/frameworks.js"></script>
  
      

      <script type="text/javascript" charset="utf-8" src="/ims2/web/js/55990f2.js"></script>
    

  <script>
  jQuery(function($) {
    $(document).on('change', '.edit-item-row select, .edit-item-row input', function () {
      updateInvoiceTotals('/ims2/web/invoice/form-totals', $(this).parents('tr'));
    });
  });*/
  </script>