<?php
Helper::menu('','','active','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>

<section id="bd">
    
    <div class="container">
                    
<?php
if(isset($idInvoiceError))
{
	echo "<div class='alert alert-danger fade in'>";
	echo  "<a href='#' class='close' data-dismiss='alert'>×</a>";
	foreach($idInvoiceError as $x => $x_value) {
		echo "Key=" . $x . ",";
		echo "Value=" . $x_value;
	}
	echo "</div>";
}
elseif(isset($idInvoiceItemsError))
{
	echo "<div class='alert alert-danger fade in'>";
	echo  "<a href='#' class='close' data-dismiss='alert'>×</a>";
	foreach($idInvoiceItemsError as $x => $x_value) {
		echo "Key=" . $x . ",";
		echo "Value=" . $x_value;
	}
	echo "</div>";
}

elseif(isset($_POST['invoiceNumber']))
{
	if(($_POST['invoiceNumber']) > 0)
	{
		echo "<div class='alert alert-success fade in'>
            <a href='#' class='close' data-dismiss='alert'>×</a>
            Invoice # ".$_POST['invoiceNumber']." updated successfully. 
		</div>";
	}
}
?>
      
  <article class="invoice-like">

    <header id="invoice-like-title" class="clearfix">
              <h2>Invoice - <?php if(isset($invoice['id'])){echo $invoice['id'];}else{ echo '';}
			  $Statusclass = ''; 
			if($invoice['status'] == DRAFT)
			{
				$Statusclass = 'draft'; 
			}

			if($invoice['status'] == CLOSED)
			{
				$Statusclass = 'closed'; 
			}

			if($invoice['status'] == OPENED)
			{
				$Statusclass = 'opened'; 
			}

			if($invoice['status'] == OVERDUE)
			{
				$Statusclass = 'overdue'; 
			}			
			
			
			  ?></h2>

  <ul class="list-inline list-unstyled">
    <li>
	
      <span class="label <?php echo $Statusclass; ?>"><?php echo $Statusclass; ?></span>
    </li>
    <li>
            <span class="label"><?php if($invoice['sent_by_email'] == 0){ echo 'Not sent by e-mail';}else{echo 'sent by e-mail';}?></span>
          </li>
  </ul>
    </header>

    <form name="invoice" method="post" action="#" class="form-stacked">

      <div class="row">
        <div id="invoice-like-customer-data" class="col-md-8">

          <h3>Customer data</h3>

          <div class="row">
		  <div class="col-md-6 clearfix" hidden>
              <div class="form-group"><label class="control-label required" for="invoice_customer_id">Customer ID</label>
			  <input type="text" id="invoice_customer_id" name="invoice_customer_id" required="required" maxlength="255" class="form-control" value="<?php if(isset($invoice['customer_id'])){echo $invoice['customer_id'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label required" for="invoice_customer_name">Name</label><input type="text" id="invoice_customer_name" name="invoice_customer_name" required="required" maxlength="255" class="form-control" value="<?php if(isset($invoice['customer_name'])){echo $invoice['customer_name'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_customer_identification">Identification</label><input type="text" id="invoice_customer_identification" name="invoice_customer_identification" maxlength="128" class="form-control" value="<?php if(isset($invoice['customer_identification'])){echo $invoice['customer_identification'];}else{ echo '';}?>"/></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_contact_person">Contact person</label><input type="text" id="invoice_contact_person" name="invoice_contact_person" maxlength="255" class="form-control" value="<?php if(isset($invoice['contact_person'])){echo $invoice['contact_person'];}else{ echo '';}?>"/></div>
            </div>

            <div class="col-md-6 clearfix">
              <div class="form-group">
                <label class="control-label required" for="invoice_customer_email">E-mail</label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                  <input type="email" id="invoice_customer_email" name="invoice_customer_email" required="required" maxlength="255" class="form-control" value="<?php if(isset($invoice['customer_email'])){echo $invoice['customer_email'];}else{ echo '';}?>"/>
                </div>
                
              </div>
            </div>

            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_invoicing_address">Invoicing address</label>
				<textarea id="invoice_invoicing_address" name="invoice_invoicing_address" rows="3" class="form-control"><?php if(isset($invoice['invoicing_address'])){echo $invoice['invoicing_address'];}else{ echo '';}?></textarea></div>
            </div>
            <div class="col-md-6 clearfix">
              <div class="form-group"><label class="control-label" for="invoice_shipping_address">Shipping address</label><textarea id="invoice_shipping_address" name="invoice_shipping_address" rows="3" class="form-control"><?php if(isset($invoice['shipping_address'])){echo $invoice['shipping_address'];}else{ echo '';}?></textarea></div>
            </div>
          </div>
        </div>

        <div id="invoice-like-properties" class="col-md-4"><div class="panel panel-default">
          <h3 class="panel-heading">Properties</h3>
          <div class="panel-body form-inline">
              <div class="form-group"><label class="control-label required" for="invoice_series">Series</label><select id="invoice_series" name="invoice_series" class="form-control"><option value="1">default</option></select></div>
  <div class="form-group" style="display:none"><label class="control-label required" for="invoice_issue_date">Issued on</label><input type="date" id="invoice_issue_date" name="invoice_issue_date" required="required" class="form-control" value="<?php if(isset($invoice['issue_date'])){echo $invoice['issue_date'];}else{ echo date('Y-m-d');}?>" /></div>
  <div class="form-group" style="display:none"><label class="control-label required" for="invoice_due_date">Due date</label><input type="date" id="invoice_due_date" name="invoice_due_date" required="required" class="form-control" value="<?php if(isset($invoice['due_date'])){echo $invoice['due_date'];}else{ echo date('Y-m-d');}?>" /></div>
  <div class="form-group" style="display:none"><div class="checkbox">                                        <label><input type="checkbox" id="invoice_forcefully_closed" name="invoice_forcefully_closed" value="<?php if($invoice['forcefully_closed'] == 0){echo '';}else{ echo 'checked';}?>" /> Force to be closed</label>
    </div></div>
  <div class="form-group" style="display:none"><div class="checkbox">                                        <label><input type="checkbox" id="invoice_sent_by_email" name="invoice_sent_by_email" <?php if($invoice['sent_by_email'] == 0){echo '';}else{ echo 'checked';}?> /> Mark as sent by e-mail</label>
    </div></div>
          </div>
		  		  <div class="form-inline">
    <div class="form-group"><div class="checkbox">                                        <label><input type="checkbox" id="recurring_invoice_enabled" name="recurring_invoice_enabled" <?php if($invoice['enabled'] == 0){echo '';}else{echo 'checked';}?> /> Enabled</label>
    </div></div>
  </div>

        </div></div>
<!-- Execution Data --> 
<div id="invoice-like-execution-data" class="col-md-8">
    <h3>Execution Time</h3>
    <div class="row">
      <div class="col-md-4 clearfix">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_starting_date">Start date</label><input type="date" id="recurring_invoice_starting_date" name="recurring_invoice_starting_date" required="required" class="form-control" value="<?php if(isset($invoice['starting_date'])){echo $invoice['starting_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
      <div class="col-md-4 clearfix">
        <div class="form-group"><label class="control-label" for="recurring_invoice_finishing_date">Finish date</label><input type="date" id="recurring_invoice_finishing_date" name="recurring_invoice_finishing_date" class="form-control" value="<?php if(isset($invoice['finishing_date'])){echo $invoice['finishing_date'];}else{ echo date('Y-m-d');}?>"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2">
        <div class="form-group"><label class="control-label required" for="recurring_invoice_period">Create every</label><input type="number" id="recurring_invoice_period" name="recurring_invoice_period" required="required" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($invoice['period'])){echo $invoice['period'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-3">
        <label>&nbsp;</label>
        
        <select id="recurring_invoice_period_type" name="recurring_invoice_period_type" class="form-control"><option value="day">Days</option><option value="week">Weeks</option><option value="month">Months</option><option value="year">Years</option></select>
      </div>
      <div class="col-md-3">
        <div class="form-group"><label class="control-label" for="recurring_invoice_max_occurrences">Max occurrences</label><input type="number" id="recurring_invoice_max_occurrences" name="recurring_invoice_max_occurrences" pattern=".{1,}" min="1" class="form-control" value="<?php if(isset($invoice['max_occurrences'])){echo $invoice['max_occurrences'];}else{ echo '1';}?>"></div>
      </div>
      <div class="col-md-2">
        <div class="form-group"><label class="control-label" for="recurring_invoice_days_to_due">Days to due</label><input type="number" id="recurring_invoice_days_to_due" name="recurring_invoice_days_to_due" pattern=".{0,}" class="form-control" value="<?php if(isset($invoice['recurring_invoice_days_to_due'])){echo $invoice['days_to_due'];}else{ echo '1';}?>"></div>
      </div>
    </div>
  </div>
<!-- EOC Execution Data -->		

              </div>
      <div class="row">
        <div class="col-md-12">

          <h3>Line items</h3>

          <table id="invoice-like-items" class="table table-condensed table-striped align-middle">
            <thead>
              <tr>
                <th></th>
                <th class="col-md-1 cell-align-center">Product</th>
                <th class="">Description</th>
                <th class="col-md-1 cell-align-center">Qty/Hours</th>
                <th class="col-md-2 cell-align-center">Price</th>
                <th class="col-md-1 cell-align-center">Discount</th>
                <th class="col-md-2 cell-align-left">Taxes</th>
                <th class="cell-align-right">Line total</th>
				<th class="cell-align-right">Item #Id</th>
				<th class="cell-align-right">Item Tax#Id</th>
              </tr>
            </thead>
            <tbody id="Items" name="Items">
<?php
//if(isset($_POST['invoiceItemsCount']))
{
   //for($i=1;$i<$_POST['invoiceItemsCount'];$i++)
	   $i = 0;
 			// -- Saved Item Tax.
			$saveItemTaxId = '';
  
 foreach($invoiceItems as $invoiceItem)  
   {
	   echo "
	   <tr class='edit-item-row' id='row".$i."'>
  <td class='btn-group-xs'>
		  <button class='btn btn-default btn-xs' type='button' onclick='delete_row(".$i.")'><span class='glyphicon glyphicon-trash'></span></button>
  </td>
    <td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_product' name='invoice_items_".$i."_product' class='product-autocomplete-name form-control' value='";
	 if(isset($invoiceItem['product_id'])){echo $invoiceItem['product_id'];}else{echo '';}
	echo "'/>
        <script>
    $(function() {
     addProductNameAutocomplete(".$i.",'invoice_items_".$i."_product','invoice_items_".$i."_description','invoice_items_".$i."_unitary_cost');
    });
    </script>
      </td>
    <td>
    <input type='text' id='invoice_items_".$i."_description' name='invoice_items_".$i."_description' required='required' class='product-autocomplete-description form-control' value='";
	if(isset($invoiceItem['description'])){echo $invoiceItem['description'];}else{echo '';}
	echo "'/>
  </td>
  <td class='cell-align-right'>
    <input type='text' id='invoice_items_".$i."_quantity' name='invoice_items_".$i."_quantity' required='required' class='form-control' value='";
	if(isset($invoiceItem['quantity'])){echo $invoiceItem['quantity'];}else{echo '1';}
	
	echo "' onchange='line_total(".$i.")' />
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'>
                            <span class='input-group-addon'>".$currency."</span>
        <input type='text' id='invoice_items_".$i."_unitary_cost' name='invoice_items_".$i."_unitary_cost' required='required' class='form-control' value='";
		if(isset($invoiceItem['unitary_cost'])){echo $invoiceItem['unitary_cost'];}else{echo '';}
		echo "' onchange='line_total(".$i.")'/>    </div>
    
  </td>
  <td class='cell-align-right'>
    <div class='input-group'><input type='text' id='invoice_items_".$i."_discount_percent' name='invoice_items_".$i."_discount_percent' required='required' class='form-control'  value='";
	if(isset($invoiceItem['discount'])){echo $invoiceItem['discount'];}else{echo '0.00';}
	echo "' onchange='line_total(".$i.")' /><span class='input-group-addon'>%</span>
    </div>
    
  </td>
  <td class='taxes form-inline'>
    <select id='invoice_items_".$i."_taxes' name='invoice_items_".$i."_taxes' class='form-control'  size='1' onChange='line_total(".$i.")'>";	
		echo "<option data-taxid='0' value='0.00'>Choose</option>";
		foreach ($taxes as $tax)
        {
			// -- Saved Item Tax.
			//$saveItemTaxId = '';
			if(isset($invoiceItem['id']))
			{
				//echo "<option value=".$_POST['invoice_items_'.$i.'_taxes'].">".$_POST['invoice_items_'.$i.'_taxes']."</option>";
				// -- Determine the Item Taxes.
				foreach ($itemtaxes as $itemtax)
				{	
					// -- if $itemtax
					echo 'Modise :'. $itemtax['item_id'];
					if($itemtax['item_id'] == $invoiceItem['id'])
					{
						$saveItemTaxId = $itemtax['tax_id'];
					}
				}
	   //echo "saveItemTaxId 2: ".$saveItemTaxId;
						
				if($tax['id'] == $saveItemTaxId)
				{
					echo "<option data-taxid='".$tax['id']."' value=".$tax['value']." selected>".$tax['name']."</option>";
				}
				else
				{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		    }
		    else
			{echo "<option data-taxid='".$tax['id']."' value=".$tax['value'].">".$tax['name']."</option>";}
		}
	echo "</select>
    
    <script type='text/javascript'>
      $(function(){
        //$('td.taxes select').select2();
      });
    </script>
  </td>

  <td class='cell-align-right item-gross-amount'>
        <div class='input-group'>
                            <span class='input-group-addon'>".$currency."</span>
	<input type='text' id='invoice_items_".$i."_totals' name='invoice_items_".$i."_totals' required='required' class='form-control' value='";
	
	if(isset($_POST['invoice_items_'.$i.'_totals'])){echo $_POST['base_amount'];}else{echo '0.00';}
	
	echo "' readonly />    </div>	</td>";
	echo "
	<td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_id' name='invoice_items_".$i."_id' class='product-autocomplete-name form-control' value='";
	 if(isset($invoiceItem['id'])){echo $invoiceItem['id'];}else{echo '';}
	echo "' readonly />
	</td>

	<td class='col-md-xs'>
    <input type='text' id='invoice_items_".$i."_tax_id' name='invoice_items_".$i."_tax_id' class='product-autocomplete-name form-control' value='";
	 if(isset($_POST["invoice_items_".$i."_tax_id"])){echo $_POST["invoice_items_".$i."_tax_id"];}else{echo '';}
	echo "' readonly />
	</td>
	</tr>
	   ";
	   $i  = $i  + 1;
   }
}
?>
                                        </tbody>
          </table>

        </div>
      </div>

      <div class="row totals">
        <div class="col-md-6">

          <a id="invoice-like-add-item" name="invoice-like-add-item" href="#"  class="btn btn-default btn-info"><span class="glyphicon glyphicon-plus glyphicon-white"></span> Add item</a>
         
<!--<td><input type="button" class="btn btn-default btn-info" onclick="add_row();" value="Add Item"></td>-->

        </div>
        <div class="col-md-4 col-md-offset-2">
          <table id="invoice-like-totals" class="table table-condensed table-striped">
            <tbody>
              <tr>
                <th class="cell-size-large">Subtotal</th>
                <td class="cell-align-right base-amount">
				   <input type="text"  class="form-control-plaintext" name='Subtotal' id="Subtotal" value="<?php if(isset($_POST['Subtotal'])){echo $invoice['Subtotal'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Taxes</th>
                <td class="cell-align-right tax-amount">
				   <input type="text"  class="form-control-plaintext" name='taxamount' id="taxamount" value="<?php if(isset($invoice['taxamount'])){echo $invoice['taxamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
              <tr>
                <th>Total (<?php echo $currency;?>)</th>
                <td class="cell-align-right gross-amount">
					<input type="text"  class="form-control-plaintext" name='grossamount' id="grossamount" value="<?php if(isset($invoice['grossamount'])){echo $invoice['grossamount'];}else{echo '0.00';}?>" readonly />
				</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div id="invoice-like-notes" class="row">
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_terms">Terms</label><textarea id="invoice_terms" name="invoice_terms" rows="5" class="form-control"><?php if(isset($invoice['terms'])){echo $invoice['terms'];}else{echo '';}?></textarea></div>
        </div>
        <div class="col-md-6">
          <div class="form-group"><label class="control-label" for="invoice_notes">Notes</label><textarea id="invoice_notes" name="invoice_notes" rows="5" class="form-control"><?php if(isset($invoice['notes'])){echo $invoice['notes'];}else{echo '';}?></textarea></div>
        </div>
      </div>

      <input type="hidden" id="invoice__token" name="invoice[_token]" value="tLnhcCnFuLBDcEQYEi5sbr_JrRWwZspTlNPJiiNIUXM" />
    
      <input type="hidden" id="invoiceItemsCount" name="invoiceItemsCount" value="<?php echo $invoiceItemsCount;  /*if($invoiceItemsCount !== 0){echo $invoiceItemsCount;}else{echo '1';}*/?>" />

	  <input type="hidden" id="invoiceNumber" name="invoiceNumber" value="<?php if(isset($invoice['id'])){echo $invoice['id'];}else{ echo '';}?>" />
	  
	  <div class="form-actions">
          <input type="submit" class="btn btn-default btn-primary" name="save" value="Save">
      <input type="submit" class="btn btn-default" name="save_close" value="Save and close">
  
   <!-- <input type="submit" class="btn btn-default btn-info" name="save_pdf" value="Save and download PDF">-->
      <a class="btn btn-default btn-info" href="/ims/pdf/pdf_invoice.php?id=<?php if(isset($invoice['id'])){echo $invoice['id'];}else{ echo '';}?>" id="save_pdf" name="save_pdf">Save and download PDF</a>
  <input type="submit" class="btn btn-default btn-info" name="save_print" value="Save and print">
    <input type="submit" class="btn btn-default btn-warning" name="save_email" value="Save and send by e-mail">

  <div class="float-right">
        <a class="btn btn-default btn-danger" href="/ims2/web/invoice/375/delete" data-confirm="Are you sure you want to delete the invoice?">Delete</a>
      </div>
      </div>
    
    </form>

  </article>

    <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery( '#invoice_customer_name' ).autocomplete({
      source: '/ims/autocomplete/auto_customers.php',
      select: function (event, ui) {
		jQuery('#invoice_customer_id').val(ui.item.id);
        jQuery('#invoice_customer_name').val(ui.item.name);
		jQuery('#invoice_customer_email').val(ui.item.email);
        jQuery('#invoice_customer_identification').val(ui.item.identification);
        jQuery('#invoice_contact_person').val(ui.item.contact_person);
        jQuery('#invoice_invoicing_address').val(ui.item.invoicing_address);
        jQuery('#invoice_shipping_address').val(ui.item.shipping_address);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
	
	 jQuery('#invoice-like-add-item').on('click', function(event)
			{
                  event.preventDefault();
					var row = add_row();
					var table=document.getElementById("invoice-like-items");
					var table_len=(table.rows.length)-1;
 
var id = table_len; 
var element_id = ''+'#'+'invoice_items_'+table_len+'_product'+'';
	  var element_idDesc = '#'+'invoice_items_'+table_len+'_description';
	  var element_idPrice = '#'+'invoice_items_'+table_len+'_unitary_cost';
	  					jQuery("#invoice-like-items > tbody").append(row);      // Append the new elements 

 jQuery('#invoice_items_'+table_len+'_product').on('keydown.autocomplete',function(){	  
   jQuery(element_id).autocomplete({source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);

		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 });
									
		  });
	
	
	$('#but_add').click(function(){})
	
  });
  
  
  
  function addProductNameAutocomplete(id,idReference,idDesc,idPrice)
  {
	  var element_id = ''+'#'+idReference+'';
	  var element_idDesc = '#'+idDesc;
	  var element_idPrice = '#'+idPrice;
	 
    jQuery(element_id).autocomplete({
		
		
      source: '/ims/autocomplete/auto_products.php',
      select: function (event, ui) {	
        jQuery(element_id).val(ui.item.id);
		jQuery(element_idDesc).val(ui.item.description);
		jQuery(element_idPrice).val(ui.item.price);

		// -- call line total calculate.
		line_total(id);

        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.reference + "</a>" )
        .appendTo( ul );
    };
 // }); 
  // -- call line total calculate.
		line_total(id);
  }
  </script>
      </div>
  </section>

  <footer id="ft">
  </footer>

    <!--<script type="text/javascript" charset="utf-8" src="/<?php echo DIR; ?>/libs/js/frameworks.js"></script>-->
  
      

      <!-- <script type="text/javascript" charset="utf-8" src="/ims2/web/js/55990f2.js"></script> -->
    

  <script>
  jQuery(function($) {
    $(document).on('change', '.edit-item-row select, .edit-item-row input', function () {
     // updateInvoiceTotals('/ims2/web/invoice/form-totals', $(this).parents('tr'));
    });
  });
  </script>