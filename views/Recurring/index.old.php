<?php 
Helper::menu('','','active','','','','');
$menu = ROOT.'views/Layouts/menu.php';
$currency = 'R';
include_once($menu);?>
<section id="bd">
          <div class="searchform">
        <div class="container">
            
<form name="search_invoice" method="post" action="#" class="search form-inline">
<!-- /<?php //echo DIR; ?>/invoice> -->
  <fieldset>

    <div class="form-group"><label class="control-label" for="search_invoice_terms">Customer Name</label>
	<input type="text" id="search_invoice_terms" name="search_invoice_terms" class="form-control" 
	value="<?php if(isset($_POST['search_invoice_terms'])){ echo $_POST['search_invoice_terms'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_from">Date from</label>
	<input type="date" id="search_invoice_date_from" name="search_invoice_date_from" class="form-control" value="<?php if(isset($_POST['search_invoice_date_from'])){ echo $_POST['search_invoice_date_from'];}?>"/></div>
    <div class="form-group"><label class="control-label" for="search_invoice_date_to">to</label>
	<input type="date" id="search_invoice_date_to" name="search_invoice_date_to" class="form-control" value="<?php if(isset($_POST['search_invoice_date_to'])){ echo $_POST['search_invoice_date_to'];}?>"/></div>
	
    <div class="form-group float-right btn-toolbar">
      <div class="btn-group">
        <button id="searchsubmit" name="searchsubmit"  type="submit" class="btn btn-default btn-primary">Search</button>
        <a href="#search-secondary" data-toggle="collapse" class="btn btn-default"" aria-controls="search-secondary">
          Advanced        <span class="caret"></span>
        </a>
      </div>
      <div class="btn-group">
        <a id="search-reset" href="/<?php echo DIR; ?>/invoice/index" class="btn btn-default btn-warning">Reset</a>
      </div>
    </div>

  </fieldset>

<fieldset id="search-secondary" class="collapse">
<?php  
$statusDraft   = '';
$statusOpened  = '';
$statusOverdue = '';
$statusClosed  = '';
$status = '';

if(isset($_POST['search_invoice_status']))
		{ 	
					switch ($_POST['search_invoice_status']) 
					{
					case "0":
						$statusDraft   = 'selected';
						break;
					case "1":
						$statusClosed  = 'selected';
						break;
					case "2":
						$statusOpened  = 'selected';
						break;
					case "3":
						$statusOverdue = 'selected';
						break;
					default:
						$status = 'selected';
					}
		}else
		{ 	$status = '';
}
?>
      <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label>
	  <select id="search_invoice_status" name="search_invoice_status" class="form-control">
	  <option value=""  <?php echo $status;?>></option>
	  <option value="0" <?php echo  $statusDraft;?>>Draft</option>
	  <option value="2" <?php echo $statusOpened;?>>Opened</option>
	  <option value="3" <?php echo $statusOverdue;?>>Overdue</option>
	  <option value="1" <?php echo $statusClosed;?>>Closed</option></select></div>
      <div class="form-group"><label class="control-label" for="search_invoice_customer">Customer</label><input type="text" id="customer_identification" name="customer_identification" class="form-control" value="<?php if(isset($_POST['customer_identification'])){ echo $_POST['customer_identification'];}?>"/></div>
</fieldset>

  <!--HIDE FOR NOW <fieldset id="search-secondary" class="collapse">
      <div class="form-group"><label class="control-label" for="search_invoice_status">Status</label><select id="search_invoice_status" name="search_invoice[status]" class="form-control"><option value=""></option><option value="0">Draft</option><option value="2">Opened</option><option value="3">Overdue</option><option value="1">Closed</option></select></div>
      <div class="form-group"><label class="control-label" for="search_invoice_customer">Customer</label><input type="text" id="customer_identification" name="customer_identification" class="form-control" value="<?php if(isset($_POST['customer_identification'])){ echo $_POST['customer_identification'];}?>"/></div>
      <div class="form-group"><label class="control-label" for="search_invoice_series">Series</label><select id="search_invoice_series" name="search_invoice[series]" class="form-control"><option value=""></option><option value="1">default</option></select></div>
  </fieldset>
-->

    </form>
        </div>
      </div>
    
    <div class="container">
<div class="row"><div class="col-md-6">
    <table id="recurring-summary" class="table table-bordered table-condensed col-md-2">
      <thead>
        <tr>
          <th colspan="4">Average turnover</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><strong>ZAR0.00</strong> /day</td>
          <td><strong>ZAR0.00</strong> /week</td>
          <td><strong>ZAR0.00</strong> /month</td>
          <td><strong>ZAR0.00</strong> /year</td>
        </tr>
      </tbody>
    </table>
  </div></div>
  
        <div id="invoices-latest-invoices">
    
              
<form name="invoice_list" method="post" action="/<?php echo DIR; ?>/recurring/index">
<ul id="toolbar" class="table-actions list-inline list-unstyled">
  <li>
    <button type="submit" name="delete" id="delete" class="btn btn-default btn-danger" title="Remove selected" data-confirm="Are you sure you want to remove selected invoices?"><span class="glyphicon glyphicon-trash"></span></button>
  </li>
  <!--<li>
    <button type="submit" name="print" id="print" class="btn btn-default" title="Print selected"><span class="glyphicon glyphicon-print"></span></button>
  </li>
  <li>
    <button type="submit" name="pdf" id="pdf" class="btn btn-default" title="Download selected as PDF"><span class="glyphicon glyphicon-download-alt"></span></button>
  </li>
  <li>
    <button type="submit" name="email" id="email" class="btn btn-default" title="E-mail selected"><span class="glyphicon glyphicon-envelope"></span></button>
  </li>-->
</ul>

<table class="table table-condensed table-striped align-middle" data-type="invoices">
  <thead>
    <tr>
            <th class="cell-size-tiny cell-align-center">
        <input type="checkbox" name="all" />
      </th>
            <th class="cell-size-medium">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.number&amp;direction=desc&amp;page=1" title="Number">Number</a>

              </th>
      <th>
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.customer_name&amp;direction=desc&amp;page=1" title="Customer">Customer</a>

              </th>
      <th class="cell-size-medium">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.issue_date&amp;direction=desc&amp;page=1" title="Date">Period</a>

              </th>
      <th class="cell-size-medium">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.due_date&amp;direction=desc&amp;page=1" title="Due Date">Frequency</a>

              </th>
      <th class="cell-size-small-medium cell-align-center">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.status&amp;direction=desc&amp;page=1" title="Status">Status</a>

              </th>
      <!--<th class="cell-size-medium cell-align-right">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=due_amount&amp;direction=desc&amp;page=1" title="Due">Due</a>

              </th>-->
      <th class="cell-size-medium cell-align-right">
                <a class="sortable" href="/<?php echo DIR; ?>/invoice?sort=i.gross_amount&amp;direction=desc&amp;page=1" title="Total">Total</a>

              </th>
      <!--<th class="cell-size-medium"></th>-->
    </tr>
  </thead>
  <tbody>
  <?php
  	   $i = 0;
	   $invoices_count = 0;

        foreach ($invoices as $invoice)
        {
			// -- statuses.
			/*
			define("DRAFT", 0);
			define("CLOSED", 1);
			define("OPENED", 2);
			define("OVERDUE", 3);
			*/
			$Statusclass = ''; 
			if($invoice['status'] == DRAFT)
			{
				$Statusclass = 'draft'; 
			}

			if($invoice['status'] == CLOSED)
			{
				$Statusclass = 'closed'; 
			}

			if($invoice['status'] == OPENED)
			{
				$Statusclass = 'opened'; 
			}

			if($invoice['status'] == OVERDUE)
			{
				$Statusclass = 'overdue'; 
			}			
$tot = $invoice['base_amount']+$invoice['tax_amount'];
			echo"<tr data-link='/".DIR."/recurring/edit/".$invoice['id']."'>
					<td class='table-action cell-align-center no-link'>
						<div class='form-group'><div class='checkbox'>                                        
						<label><input type='checkbox' id='invoice_".$i."' name='invoice_".$i."' value='".$invoice['id']."' /> </label>
						</div></div>
					</td>
					<td class='cell-size-medium'>".$invoice['id']."</td>
					<td>".$invoice['customer_name']."</td>
					<td>".$invoice['period_type']."</td>
					<td>".$invoice['period']."</td>
					<td class='cell-align-center'>
					  <span class='label $Statusclass'>$Statusclass</span>
					</td>
					<!--<td class='cell-align-right'>$currency".number_format($invoice['base_amount'] - $invoice['paid_amount'],2)."</td>-->
					<td class='cell-align-right'>$currency".number_format($tot,2)."</td>
					<!--<td class='cell-align-right payments'>";
					echo "<a href='/".DIR."/invoice/payments/".$invoice['id']."' class='btn btn-default btn-info payments' title='Payments'  data-toggle='modal' data-target='#payment-modal' data-remote='false'><span class='glyphicon glyphicon-piggy-bank'></span> Payments</a>
					</td>-->
				</tr>";
			  	   $i = $i + 1;

	  }
	  $invoice_count = $i;

	?>
 </tbody>
</table>	 
  <!--<input type="hidden" id="invoice_list__token" name="invoice_list[_token]" value="J3x1pXDJMqyI-u1bZGRFK2lqLdqlNSVzLlkJYK86TqM" />-->
    <input type="hidden" id="invoice_count" name="invoice_count" value="<?php echo $invoice_count; ?>" />

    </form>

<!--<div class="navigation">
  
    <ul class="pagination">

            <li class="disabled">
            <span>&laquo;&nbsp;Previous</span>
        </li>
    
    
                        <li class="active">
                <span>1</span>
            </li>
        
                        <li>
                <a href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=2">2</a>
            </li>
        
                        <li>
                <a href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=3">3</a>
            </li>
        
                        <li>
                <a href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=4">4</a>
            </li>
        
                        <li>
                <a href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=5">5</a>
            </li>
        
    
                                        <li class="disabled">
                    <span>&hellip;</span>
                </li>
                            <li>
            <a href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=8">8</a>
        </li>
    
            <li>
            <a rel="next" href="/<?php echo DIR; ?>/invoice?sort=i.id&amp;direction=desc&amp;page=2">Next&nbsp;&raquo;</a>
        </li>
        </ul>

</div>-->
<div id="payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="payments-modal-title">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="payments-modal-title">Payments</h4> </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
  </div>

    </div>
  </section>
<!-- Autocomplete -->  
  <script>
  jQuery(document).ready(function () 
  {//alert("customer name");
    jQuery('#search_invoice_terms').autocomplete({
      source: '/ims/autocomplete/auto_customers.php',
      select: function (event, ui) {
		jQuery('#search_invoice_terms').val(ui.item.name);
		jQuery('#customer_identification').val(ui.item.identification);
        return false;
      },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return jQuery( "<li>" )
        .append( "<a>" + item.name + "</a>" )
        .appendTo( ul );
    };
  });	
</script>	