<div class="row">
  <div class="col-sm-6 col-md-4 col-md-offset-4">

    <!-- <h1 class="text-center login-title"><a href="/<?php echo DIR; ?>/login/index">Sign in</a> to continue to Siwapp</h1> -->

    
    <div class="account-wall">
      <div class="logo text-center">
        <img src="/<?php echo DIR; ?>/images/Fintergrate 316x80px.png" alt="Invoice Software" />
      </div>
		<?php
		if(!empty($error))
		{
		?>				
			  <div class="alert alert-danger fade in">Invalid credentials.</div>
		<?php }
		?>	  
      <form action="#" method="post" class="form-signin">
  <input type="hidden" name="_csrf_token" value="X0769Q3Zwa5_6zhhAbO582W9CaaHAymdV2nqo_Esk40" />
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" id="username" name="username" value="<?php if(isset($_POST['username'])){echo $_POST['username'];}else{ echo '';}?>" required="required" class="form-control" />
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" id="password" name="password" required="required" class="form-control" />
  </div>
  <!--<div class="form-group">
    <div class="checkbox pull-left">
      <label for="remember_me"><input type="checkbox" id="remember_me" name="_remember_me" value="on" />
      Remember me</label>
    </div>
    <div class="forgot-pass-link pull-right text-right">
      <a href="/<?php echo DIR; ?>/resetting/request">Forgot password ?</a>
    </div>
  </div>-->
  <input type="submit" id="_submit" name="_submit" value="Log in" class="btn btn-lg btn-primary btn-block" />
</form>

    </div>
  </div>
</div>