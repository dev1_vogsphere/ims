<?php
    class Controller
    {
        var $vars = [];
        var $layout = "default";

		public static $logged_in = false;
		public static $username = '';
		
        function set($d)
        {
            $this->vars = array_merge($this->vars, $d);
        }

        function render($filename)
        {
			if($this->check_login())
			{	
				extract($this->vars);
				ob_start();
				require(ROOT . "views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php');
				$content_for_layout = ob_get_clean();

				//echo 'Modise'.get_class($this);

				if ($this->layout == false)
				{
					$content_for_layout;
				}
				else
				{
					require(ROOT . "views/Layouts/" . $this->layout . '.php');
				}
			}
			elseif(get_class($this) == "loginController")
			{
				extract($this->vars);
				ob_start();
				require(ROOT . "views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php');
				$content_for_layout = ob_get_clean();

				//echo 'Modise'.get_class($this);

				if ($this->layout == false)
				{
					$content_for_layout;
				}
				else
				{
					require(ROOT . "views/Layouts/" . $this->layout . '.php');
				}
			}
			else
			{
			  $this->Notloggin();	
			}
        }
		
        private function secure_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        protected function secure_form($form)
        {
            foreach ($form as $key => $value)
            {
                $form[$key] = $this->secure_input($value);
            }
        }
		
		function Notloggin() 
		{
			require(ROOT . 'Controllers/loginController.php');	
			$login_controller = new loginController();
			$login_controller->index(); 
		}

function check_login()
{
	//require(ROOT . 'controllers/loginController.php');	

	$valid = false;
	 //= false;
	 //e = '';
	// -- Check if LoggedIn
	
		if(Controller::$logged_in == false )
		{
			// -- Not LoggedIn.

		}
		else
		{
			if(!empty(Controller::$username))
			{
				// -- LoggedIn
				$valid = true;
			}
			else
			{
				// -- Not LoggedIn.
			}
		}
		
		if(isset($_SESSION['username_IMS']))
		{
				// -- LoggedIn
				$valid = true;
		}

	return $valid;
}

    }
?>