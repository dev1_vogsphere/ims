<?php
//define('ROOT', str_replace("core/Helper.php", "", $_SERVER["SCRIPT_FILENAME"]));
if (!class_exists('Helper')) 
{  
  class Helper
    {
			public static function menu($Dashboard,	
			$Invoices,	
			$RInvoices,	
			$Quotations,	
			$Customers,	
			$Products)	
			{
					$_SESSION['Dashboard'] = $Dashboard;
					$_SESSION['Invoices']  = $Invoices;
					$_SESSION['RInvoices'] = $RInvoices;
					$_SESSION['Quotations'] = $Quotations;
					$_SESSION['Customers'] = $Customers;
					$_SESSION['Products']  = $Products;
			}
			
public static function SendEmailWithAttachment($att,$email,$environment,$path)
{
		require_once(ROOT . '/libs/class/class.phpmailer.php');
		try
		{
						 // -- SMTP Settings
						$Globalmailhost = $_SESSION['host'];
						$Globalport = $_SESSION['port'];
						$Globalmailusername = $_SESSION['emailusername'];
						$Globalmailpassword = $_SESSION['emailpassword'];
						$GlobalSMTPSecure = $_SESSION['secure'];
						$Globaldev = $_SESSION['dev'];
						$Globalprod = $_SESSION['prod'];
						$companyname =	$_SESSION['company_name'];				
						$adminemail = $email;										
						// -- Email Server Hosting -- //
					 
					/*	Production - Live System */
						if($Globalprod == 'p')
						{
							$mail=new PHPMailer();
							$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
							$mail->SetFrom($adminemail,$companyname);
							$mail->AddReplyTo($adminemail,$companyname);
						}
					/*	Development - Testing System 	*/
						else
						{
								$mail=new PHPMailer();
								$mail->IsSMTP();
								$mail->SMTPDebug=1;
								$mail->SMTPAuth=true;
								$mail->SMTPSecure= $GlobalSMTPSecure;//"tls";//
								$mail->Host=$Globalmailhost;//"smtp.vogsphere.co.za";//
								$mail->Port= $Globalport;//587;//
								$mail->Username=$Globalmailusername;//"vogspesw";//
								$mail->Password=$Globalmailpassword;//"fCAnzmz9";//
						}	
								$dateForm = date('F')."  ".date('Y');
								
								$mail->SetFrom($adminemail,$companyname);

								//$mail->AddStringAttachment($att, $filename);
								$mail->AddAttachment($path);

								//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
								$mail->Subject=  $companyname." database backup on ".$dateForm;//$_SESSION['invAccountNumber'];
								// Enable output buffering
											
								ob_start();
								//include $att;//'content\text_emailInvoice.php';//execute the file as php
								$body = 'Database Backup on '.$dateForm;		

								// print $body;
								$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
								//$mail->AddAddress($email, $fullname);
								//$mail->AddBCC($adminemail, $companyname);
								$mail->AddAddress($adminemail, $companyname);

								if($mail->Send()) 
								{
								$successMessage = "An e-mail is sent your Inbox!";
								}
								else 
								{
								$successMessage = "Your invalid details!";
								}
			}		
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}	
return $successMessage;
}
	
// -- Generic SendEmail Template.	
public static function SendEmailWithAttachmentGeneric($att,$fileName,$email,$emailaddr,$subject,$body)
{
		require_once(ROOT . '/libs/class/class.phpmailer.php');
		try
		{
						 // -- SMTP Settings
						$Globalmailhost = $_SESSION['host'];
						$Globalport = $_SESSION['port'];
						$Globalmailusername = $_SESSION['emailusername'];
						$Globalmailpassword = $_SESSION['emailpassword'];
						$GlobalSMTPSecure = $_SESSION['secure'];
						$Globaldev = $_SESSION['dev'];
						$Globalprod = $_SESSION['prod'];
						$companyname =	$_SESSION['company_name'];				
						$adminemail = $email;										
						// -- Email Server Hosting -- //
					 
					/*	Production - Live System */
						if($Globalprod == 'p')
						{
							$mail=new PHPMailer();
							$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
							$mail->SetFrom($adminemail,$companyname);
							$mail->AddReplyTo($adminemail,$companyname);
						}
					/*	Development - Testing System 	*/
						else
						{
								$mail=new PHPMailer();
								$mail->IsSMTP();
								$mail->SMTPDebug=1;
								$mail->SMTPAuth=true;
								$mail->SMTPSecure= $GlobalSMTPSecure;//"tls";//
								$mail->Host=$Globalmailhost;//"smtp.vogsphere.co.za";//
								$mail->Port= $Globalport;//587;//
								$mail->Username=$Globalmailusername;//"vogspesw";//
								$mail->Password=$Globalmailpassword;//"fCAnzmz9";//
						}	
								$dateForm = date('F')."  ".date('Y');
								
								$mail->SetFrom($adminemail,$companyname);

								$mail->AddStringAttachment($att, $fileName);
								//$mail->AddAttachment($att);

								//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
								$mail->Subject =  $subject;//$companyname." database backup on ".$dateForm;//$_SESSION['invAccountNumber'];
								// Enable output buffering
								ob_start();
								include $body;
								$body = ob_get_clean();		
								$mail->MsgHTML($body );
								//$mail->AddAddress($email, $fullname);
								//$mail->AddBCC($adminemail, $companyname);
								$mail->AddAddress($emailaddr, $companyname);

								if($mail->Send()) 
								{
								$successMessage = "An e-mail is sent your Inbox!";
								}
								else 
								{
								$successMessage = "Your invalid details!";
								}
			}		
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}	
return $successMessage;
			}

    }
}	
?>