<?php 
 error_reporting( E_ALL ); 
 ini_set('display_errors', 1);
 ?>

<?php

if (!class_exists('loginController')) 
{
require(ROOT . 'request.php');

class loginController extends Controller 
{
	
public $request = null;

function __construct() 
{
  //parent::__construct();

}
function index() 
{ 
	require(ROOT . 'models/sf_guard_user.php');
	require(ROOT . 'models/globalsetting.php');

    $sf_guard_user= new sf_guard_user();
	$logindata = null;
	$valid = false;
	$error = array();

	//-- if post
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		// -- Get Login data.
		$logindata = $sf_guard_user->logindata($_POST['username']);

		// -- Validate againts Input data.
		if(empty($logindata))
		{
			$error['error'] = 'Incorrect login details';
		}
		else
		{
			if (password_verify(trim($_POST['password']), trim($logindata['password']))) 
			{
				    $_SESSION['loggedin_IMS'] = true;
					$_SESSION['username_IMS'] = $_POST['username'];
					/* -- BOC - Global Settings -- */
					// -- Global Setting Controller.
					$globalsetting  = new globalsetting();
					$globalsettings = $globalsetting->show($_SESSION['username_IMS']);	
					
					// -- Bind Global Data into Session.
					$globalsetting = $globalsettings;
					//foreach($globalsettings as $globalsetting)
					  //{
					 // -- Global Settings	  
					  if(isset($globalsetting['id']))   {$_SESSION['id'] 	 = $globalsetting['id'];}
					  if(isset($globalsetting['company_name']))   {$_SESSION['company_name'] 	 = $globalsetting['company_name'];}
					  if(isset($globalsetting['company_address'])){$_SESSION['company_address'] = $globalsetting['company_address'];}
					  if(isset($globalsetting['company_email']))  {$_SESSION['company_email']   = $globalsetting['company_email'];}
					  if(isset($globalsetting['company_fax']))    {$_SESSION['company_fax'] 	 = $globalsetting['company_fax'];}
					  if(isset($globalsetting['company_logo']))   {$_SESSION['company_logo'] 	 = $globalsetting['company_logo'];}
					  if(isset($globalsetting['company_phone']))  {$_SESSION['company_phone'] 	 = $globalsetting['company_phone'];}
					  if(isset($globalsetting['company_url']))    {$_SESSION['company_url'] 	 = $globalsetting['company_url'];}
					  if(isset($globalsetting['currency']))       {$_SESSION['currency'] 		 = $globalsetting['currency'];}
					  if(isset($globalsetting['currency_decimals'])){$_SESSION['currency_decimals'] = $globalsetting['currency_decimals'];}
					  if(isset($globalsetting['default_template'])){$_SESSION['default_template'] 	 = $globalsetting['default_template'];}
					  if(isset($globalsetting['last_calculation_date'])){$_SESSION['last_calculation_date'] = $globalsetting['last_calculation_date'];}
					  if(isset($globalsetting['legal_terms']))     {$_SESSION['legal_terms']		= $globalsetting['legal_terms'];}
					  if(isset($globalsetting['pdf_orientation'])) {$_SESSION['pdf_orientation'] 	= $globalsetting['pdf_orientation'];}
					  if(isset($globalsetting['pdf_size']))        {$_SESSION['pdf_size']			= $globalsetting['pdf_size'];}
					  if(isset($globalsetting['sample_data_load'])){$_SESSION['sample_data_load'] 	= $globalsetting['sample_data_load'];}
					  if(isset($globalsetting['siwapp_modules']))  {$_SESSION['siwapp_modules'] 	= $globalsetting['siwapp_modules'];}
					  if(isset($globalsetting['company_reg']))  {$_SESSION['company_reg'] 	= $globalsetting['company_reg'];}
					  if(isset($globalsetting['VAT']))  {$_SESSION['VAT'] 	= $globalsetting['VAT'];}
					  
					  // -- SMTP Settings
					  if(isset($globalsetting['host'])) {$_SESSION['host'] 	 = $globalsetting['host'];}
					  if(isset($globalsetting['port'])) {$_SESSION['port']	 = $globalsetting['port'];}
					  if(isset($globalsetting['secure'])) {$_SESSION['secure']	 = $globalsetting['secure'];}
					  if(isset($globalsetting['emailusername'])) {$_SESSION['emailusername'] 	 = $globalsetting['emailusername'];}
					  if(isset($globalsetting['emailpassword'])) {$_SESSION['emailpassword'] 	 = $globalsetting['emailpassword'];}
					  if(isset($globalsetting['prod'])) {$_SESSION['prod']	 = $globalsetting['prod'];}
					  if(isset($globalsetting['dev'])) {$_SESSION['dev']	 = $globalsetting['dev'];}
					  if(isset($globalsetting['captcha'])) {$_SESSION['captcha'] 	 = $globalsetting['captcha'];}
	  
					  //}
					
					/* -- EOC - Global Settings -- */		
				    Controller::$logged_in = true;
					Controller::$username = $_POST['username'];
					$valid = true;
			}
			else
			{
				$error['error'] = 'Incorrect login details';
			}
		}
	}
	if($valid)
	{
		$this->login_check();
	}
	else
	{
		$this->set($error);					
		$this->render("index");
	}

}

function login_check() 
{
		/*require(ROOT . 'Controllers/invoiceController.php');

	$invoice_controller = new invoiceController();
	$invoice_controller->index(); */
	require(ROOT . 'Controllers/dashboardController.php');
	$dashboard_controller = new dashboardController();
	$dashboard_controller->index(); 
}

function logout() 
{
	$_SESSION['loggedin_IMS'] = false;
	$_SESSION['username_IMS'] = '';
	/* -- BOC - Global Settings -- */	 
	$_SESSION['captcha'] = '';
	$_SESSION['company_address'] = '';
	$_SESSION['company_email'] = '';
	$_SESSION['company_fax'] = '';
	$_SESSION['company_logo'] = '';
	$_SESSION['company_name'] = '';
	$_SESSION['company_phone'] = '';
	$_SESSION['company_reg'] = '';
	$_SESSION['company_url'] = '';
	$_SESSION['currency'] = '';
	$_SESSION['currency_decimals'] = '';
	$_SESSION['default_template'] = '';
	$_SESSION['dev'] = '';
	$_SESSION['emailpassword'] = '';
	$_SESSION['emailusername'] = '';
	$_SESSION['host'] = '';
	$_SESSION['last_calculation_date'] = '';
	$_SESSION['legal_terms'] = '';
	$_SESSION['pdf_orientation'] = '';
	$_SESSION['pdf_size'] = '';
	$_SESSION['port'] = '';
	$_SESSION['prod'] = '';
	$_SESSION['sample_data_load'] = '';
	$_SESSION['secure'] = '';
	$_SESSION['siwapp_modules'] = '';
	$_SESSION['VAT'] = '';
	/* -- EOC - Global Settings -- */	 
	 loginController::$logged_in = false;
	 loginController::$username = '';

	  $_SESSION = array(); 
      session_destroy();
	 
	 $this->render("logout");
}		   

// -- Hash Every Page.
function redirect($url) 
{

    ob_start();
	$hash="";//?guid=".md5(date("h:i:sa"));
	//echo $request->url; //$url;
	
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}

}}
?>