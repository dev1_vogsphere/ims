<?php
if (!class_exists('recurringController')) 
{

class recurringController extends Controller 
{

public $request = null;

function __construct() 
{
  //parent::__construct();

}
function index() 
{
	
	require(ROOT . 'models/invoice.php');

        $invoice = new invoice();
// -- Count number of Invoices
		$invoice_count = 0;		
		$d['invoices'] = array();
		$whereArr = array();
		$customer_identification = '';
		$datefrom 	= null;
		$dateto 	= null;
		$status 	= '';
		$type = 'RecurringInvoice';
		
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

		
// -- Search button
        if (isset($_POST["searchsubmit"]))
		{
		//1. -- search_invoice_terms.
			if(isset($_POST["search_invoice_terms"]))
			{
				if(!empty($_POST["search_invoice_terms"]))
				{
					$customer_identification 	= $_POST["customer_identification"];
					$whereArr[] = "customer_identification = '{$customer_identification}'";
					//$d['invoices'] = $invoice->search_customer_invoice($_POST["customer_identification"]);
				}
				
			}
		//2. -- search_invoice_status.search_invoice_status
			if(isset($_POST["search_invoice_status"]))
			{
					$status 	= $_POST["search_invoice_status"];
					if($status != '')
					{$whereArr[] = "status = {$status}";}
			}
		// 3. search_invoice_date_from.
			if(isset($_POST["search_invoice_date_from"]))
			{
				if(!empty($_POST["search_invoice_date_from"]))
				{
					$datefrom 	= $_POST["search_invoice_date_from"];
					$whereArr[] = "issue_date >= {$datefrom}";
				}
			}

		// 4. search_invoice_date_to
			if(isset($_POST["search_invoice_date_to"]))
			{
				if(!empty($_POST["search_invoice_date_to"]))
				{
					$dateto 	= $_POST["search_invoice_date_to"];
					$whereArr[] = "issue_date <= {$dateto}";
				}
			}
			//print_r($whereArr);
		 // -- No dynamic query.
			if(empty($whereArr))
			{
		//		$d['invoices'] = $invoice->showAllRecurring();	
				$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = 'Invoice')";
				$whereArr[] = "id in {$value}";
				$whereArr[] = "type = {$type}";
				$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);								
		
			}
		// -- Dynamic Query search.
			else
			{
				$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = '".$type."')";
				$whereArr[] = "id in {$value}";
				$whereArr[] = "type = '{$type}'";
				$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);	
				//print_r($d['invoices']);				
			}
		}
		else
		{
			//$d['invoices'] = $invoice->showAllRecurring();
				$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
				$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);											
		}		
		
// -- Delete Selected Invoice(s)
// -- Determine if Remove button is clicked.
		if (isset($_POST["invoice_count"]))
        {
		//	echo $_POST["invoice_count"];
			
			// -- determined how many Items are selected to be removed.
			$invoice_count = $_POST["invoice_count"];

			 if(isset($_POST["delete"]))
			 {		

				for($i=0;$i<$invoice_count;$i++)
				{
					if(isset($_POST["invoice_".$i.""]))
					{
						// -- Delete the selected invoice(s).
						if(!empty($_POST["invoice_".$i.""]))
						{
							$idinvoice = $invoice->remove($_POST["invoice_".$i.""]);
							// -- Reload invoices.
							//$d['invoices'] = $invoice->showAllRecurring();
				$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
							$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);																		
						}
					}
				}
				
			 }	
// -- Print Selected Invoice(s)					 
			 if(isset($_POST["print"]))
			 {		
				for($i=0;$i<$invoice_count;$i++)
				{
					if(isset($_POST["invoice_".$i.""]))
					{
						// -- Print the selected invoice(s).
						if(!empty($_POST["invoice_".$i.""]))
						{
							header("Location: /ims/pdf/pdf_invoice.php?id=".$_POST["invoice_".$i.""]);
							//$d['invoices'] = $invoice->showAllRecurring();	
				$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
							$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);																		
							exit;
						}
					}
				}
				
			 }	
// -- Download Selected Invoice(s) to pdf.			 
			 if(isset($_POST["pdf"]))
			 {		
				for($i=0;$i<$invoice_count;$i++)
				{
					if(isset($_POST["invoice_".$i.""]))
					{
						// -- Print the selected invoice(s).
						if(!empty($_POST["invoice_".$i.""]))
						{
							header("Location: /ims/pdf/pdf_invoice.php?id=".$_POST["invoice_".$i.""]."&download=Y",TRUE);
							// -- Reload invoices.
							//$d['invoices'] = $invoice->showAll();	
				$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
							$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);																		
							exit;
						}
					}
				}
				
			 }	
// -- Email Selected Invoices.			 
			 if(isset($_POST["email"]))
			 {		
				for($i=0;$i<$invoice_count;$i++)
				{
					if(isset($_POST["invoice_".$i.""]))
					{
						// -- Email the selected payment(s).
						if(!empty($_POST["invoice_".$i.""]))
						{
							//$idinvoice = $invoice->delete($_POST["invoice_".$i.""]);
							// -- Reload invoices.
							//$d['invoices'] = $invoice->showAll();		
				$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
							$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);											
						}
					}
				}
				
			 }	
		}
// -- Update paid Amounts
// -- Get the Invoice Payments details.
		$invoices = $d['invoices'];
		$data = array();
		if(!empty($invoices))
		{	
        foreach ($invoices as $rowInvoice)
        {
			$payments_data = $invoice->payments($rowInvoice['id']);
			$totalPayment = 0;
			$balance = 0.00;
		
			foreach ($payments_data as $row) 
			{
				$amount = $row['amount'];
				$totalPayment = $totalPayment+$amount;
			}
			
			//echo $rowInvoice['id'].";amount = ".$totalPayment." \n";
			$rowInvoice['paid_amount'] = $totalPayment;
			
			array_push($data,$rowInvoice);
			
		}
	  }
		
		$d['invoices'] = $data;

		$this->set($d);
        $this->render("index");
		
}
// -- Search by indentification.
function search_customer_invoice($id)
{
		require(ROOT . 'models/invoice.php');
        $invoice = new invoice();
		$d['invoices'] = array();
		$d['invoices'] = $invoice->search_customer_invoice($id);	
$data = array();

// -- Update paid Amounts
// -- Get the Invoice Payments details.
		$invoices = $d['invoices'];
        foreach ($invoices as $rowInvoice)
        {
			$payments_data = $invoice->payments($rowInvoice['id']);
			$totalPayment = 0;
			$balance = 0.00;
		
			foreach ($payments_data as $row) 
			{
				$amount = $row['amount'];
				$totalPayment = $totalPayment+$amount;
			}
		//	echo $rowInvoice['id'].";amount = ".$totalPayment." \n";
			$rowInvoice['paid_amount'] = $totalPayment;
		array_push($data,$rowInvoice);
			
		}
		
		$d['invoices'] = $data;
		
		$this->set($d);
        $this->render("index");
}
function invoice() 
{
	require(ROOT . 'models/invoice.php');

        $invoice = new invoice();

		if(isset($_POST["search_invoice_customer"]))
		{
			$terms = $_POST["search_invoice_customer"];
			$d['invoices'] = $invoice->search_byname($terms);
		}
		else
		{
        //$d['invoices'] = $invoice->showAll();	
		$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
		$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);											
		}
		
		$this->set($d);
        $this->render("index");
}

function indexSearch($term,$da) 
{
	require(ROOT . 'models/invoice.php');

        $invoice = new invoice();

		if(!empty($term))
		{
			//echo 'term : '.$_POST["search_invoice_customer"];
			$d['invoices'] = $invoice->search_byname($term);
		}
		else
		{
        //$d['invoices'] = $invoice->showAll();
		$whereArr = array_merge($whereArr, $invoice->defaultSearch($companyname,$type));
		$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);											
		
		}
		
		        $this->set($d);
        $this->render("index");

}

function create()
{
	$idInvoice 		= null;
	$idInvoiceItems = null;
	
	// -- Default status to DRAFT until is changed.
	$status = DRAFT;
	$type = 'RecurringInvoice';
	/* Status counter if status = 0, then draft should be 1 */
	/* status = 1, the closed should be 1 */
	$draft  = 0;
	$closed = 0;
	/* if send_by_email button was clicked, should be equal to 1*/
	$sent_by_email = 0;
	
	/*number of Invoice Items */
	$invoiceItemsCount = 0;
	$customer_phone = '';
	
	/* Amounts*/
	$base_amount 	 = DECIMAL;
	$discount_amount = DECIMAL;
	$net_amount 	 = DECIMAL;
	$gross_amount 	 = DECIMAL;
	$paid_amount 	 = DECIMAL;
	$tax_amount 	 = DECIMAL;
	$recurring_invoice_id = 0;
	
	/* default values */
	$days_to_due 		 = 0;
	$enabled			 = 0;
	$max_occurrences	 = 0;
	$must_occurrences 	 = 0;
	$period				 = 0;
	$period_type		 = '';
	$starting_date		 = null;
	$finishing_date		 = null;
	$last_execution_date = null;
	
	require(ROOT . 'models/invoice.php');
	require(ROOT . 'models/tax.php');

    $invoice= new invoice();
	
	// -- Get Tax Master Data.
	$tax = new tax();
	$d['taxes'] = $tax->showAll();		
	$this->set($d);
	
    if (isset($_POST["invoice_customer_name"]))
    {
		
		/* Invoice Header */
/*	-- Determine status of the Invoice -- */
		if(isset($_POST["save_email"]))
		{
			$status = OPENED;
			$sent_by_email = 1;
		}
		
		if(isset($_POST["save_draft"]))
		{
			$status = DRAFT;
			$draft = 1;
		}

		if(isset($_POST["save"]))
		{
			$status = OPENED;
		}

		if(isset($_POST["invoice_forcefully_closed"]))
		{
			$status = CLOSED;
			$closed = 1;
		}	
		
		// -- count number of invoice items added.
		$key = true;
		$value  = '';
		while($key)
		{
			$value = "invoice_items_".$invoiceItemsCount."_description";
		//	$value = '"'.$value.'"';
			
			//$value = "invoice[items][".$invoiceItemsCount."][description]";
			if(isset($_POST[$value]))
			{
				// -- count number of items
				$invoiceItemsCount = $invoiceItemsCount + 1;
			}
			else
			{
				// -- No more Invoice Items
				$key = false;
			}
		}
		
		// -- base_amount
		if(isset($_POST["Subtotal"]))
		{
			$base_amount = $_POST["Subtotal"];
		}
		
		// -- discount_amount
		if(isset($_POST[""]))
		{
			$discount_amount = $_POST[""];
		}
	// -- net_amount
		if(isset($_POST["Subtotal"]))
		{
			$net_amount = $_POST["Subtotal"];
		}
	// -- gross_amount
		if(isset($_POST["grossamount"]))
		{
			$net_amount = $_POST["grossamount"];
		}
	// -- paid_amount
	// -- U wont have at creating of invoice 0.00
	
	// -- tax_amount
		if(isset($_POST["taxamount"]))
		{
			$tax_amount = $_POST["taxamount"];
		}

// -- 7 recurring values.
if(isset($_POST["recurring_invoice_enabled"])){$enabled=1;}

if(isset($_POST["recurring_invoice_starting_date"])){$starting_date=$_POST["recurring_invoice_starting_date"];}
if(isset($_POST["recurring_invoice_finishing_date"])){$finishing_date=$_POST["recurring_invoice_finishing_date"];}
if(isset($_POST["recurring_invoice_period"])){$period=$_POST["recurring_invoice_period"];}
if(isset($_POST["recurring_invoice_period_type"])){$period_type=$_POST["recurring_invoice_period_type"];}
if(isset($_POST["recurring_invoice_max_occurrences"])){$max_occurrences=$_POST["recurring_invoice_max_occurrences"];}
if(isset($_POST["recurring_invoice_days_to_due"])){$days_to_due=$_POST["recurring_invoice_days_to_due"];}		
// -- recurring values.
	// -- Create Invoice. 	
		$idInvoice = $invoice->create($_POST["invoice_series"], $_POST["invoice_customerid"],$_POST["invoice_customer_name"],
							 $_POST["invoice_customer_identification"],$_POST["invoice_customer_email"],
							 $_POST["invoice_invoicing_address"],$_POST["invoice_shipping_address"],$customer_phone,$_POST["invoice_contact_person"],
							 $_POST["invoice_terms"],$_POST["invoice_notes"],$base_amount,$discount_amount,$net_amount,$gross_amount,$paid_amount,$tax_amount,
							 
							 $status,$type,$draft,$closed,$sent_by_email,$invoiceItemsCount,$recurring_invoice_id,
							 $_POST["invoice_issue_date"],$_POST["invoice_due_date"],$days_to_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,'','');
						 
        if ($idInvoice > 0)
        {			
			// -- Create Invoice Items.
			$_POST['invoiceNumber'] = $idInvoice;
			for($i=0;$i<$invoiceItemsCount;$i++)
			{
				 $idInvoiceItems = $invoice->createItems($_POST["invoice_items_".$i."_quantity"],$_POST["invoice_items_".$i."_discount_percent"],$idInvoice,
				 $_POST["invoice_items_".$i."_product"],$_POST["invoice_items_".$i."_description"],$_POST["invoice_items_".$i."_unitary_cost"]);
				 
				 // -- Add Item tax if it was selected.
					if(!empty($_POST["invoice_items_".$i."_tax_id"]))
					{
					   //id="invoice_items_0_tax_id"
					   $idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$_POST["invoice_items_".$i."_tax_id"]);
					}
			}
			
        }
		
		echo 'Invoice #: '.$idInvoice;

					$this->render("new");			
    }
	else
	{
		$this->render("new");
	}
}

function edit($id)
{
// -- BOC Declarations.
	$idInvoice = array();
	$idInvoiceError = array();
	
	//$customerData = array();

	$idInvoiceItems = array();
	$idInvoiceItemsError = array();

	// -- Default status to DRAFT until is changed.
	$status = DRAFT;
	$type = 'RecurringInvoice';
	/* Status counter if status = 0, then draft should be 1 */
	/* status = 1, the closed should be 1 */
	$draft  = 0;
	$closed = 0;
	/* if send_by_email button was clicked, should be equal to 1*/
	$sent_by_email = 0;
	
	/*number of Invoice Items */
	$invoiceItemsCount = 0;
	$customer_phone = '';
	
	/* Amounts*/
	$base_amount 	 = DECIMAL;
	$discount_amount = DECIMAL;
	$net_amount 	 = DECIMAL;
	$gross_amount 	 = DECIMAL;
	$paid_amount 	 = DECIMAL;
	$tax_amount 	 = DECIMAL;
	$recurring_invoice_id = 0;
	
	/* default values */
	$days_to_due 		 = 0;
	$enabled			 = 0;
	$max_occurrences	 = 0;
	$must_occurrences 	 = 0;
	$period				 = 0;
	$period_type		 = '';
	$starting_date		 = null;
	$finishing_date		 = null;
	$last_execution_date = null;
// -- EOC Declarations.	
	require(ROOT . 'models/invoice.php');
	require(ROOT . 'models/tax.php');

// -- Get Tax Master Data.
	$tax = new tax();
	$dTax['taxes'] = $tax->showAll();	

	// -- Get Items Tax Data.
	$tax = new tax();
	$dItemTax['itemtaxes'] = $tax->showAllItemTax();	

// -- Invoice Data.	
    $invoice= new invoice();
    $dInvoice["invoice"] = $invoice->showInvoice($id);
    $dItems["invoiceItems"] = $invoice->show($id);
	$dItemsCount['invoiceItemsCount'] = count($invoice->show($id));

    if (isset($_POST["invoice_customer_name"]))
    {
// -- Statuses.		
/* Invoice Header */
/*	-- Determine status of the Invoice -- */
		if(isset($_POST["save_email"]))
		{
			$status = OPENED;
			$sent_by_email = 1;
		}
		
		if(isset($_POST["save_draft"]))
		{
			$status = DRAFT;
			$draft = 1;
		}

		if(isset($_POST["save"]))
		{
			$status = OPENED;
		}

		if(isset($_POST["invoice_forcefully_closed"]))
		{
			$status = CLOSED;
			$closed = 1;
		}	
// -- Statuses.
		// -- count number of invoice items added.
		if(isset($_POST["invoiceItemsCount"]))
		{
			$invoiceItemsCount = $_POST["invoiceItemsCount"];
		}	
// -- base_amount
		if(isset($_POST["Subtotal"]))
		{
			$base_amount = $_POST["Subtotal"];
		}
		// -- discount_amount
		if(isset($_POST[""]))
		{
			$discount_amount = $_POST[""];
		}
	// -- net_amount
		if(isset($_POST["Subtotal"]))
		{
			$net_amount = $_POST["Subtotal"];
		}
	// -- gross_amount
		if(isset($_POST["grossamount"]))
		{
			$net_amount = $_POST["grossamount"];
		}
	// -- tax_amount
		if(isset($_POST["taxamount"]))
		{
			$tax_amount = $_POST["taxamount"];
		}		
		
		// -- 7 recurring values.
if(isset($_POST["recurring_invoice_enabled"])){$enabled=1;}

if(isset($_POST["recurring_invoice_starting_date"])){$starting_date=$_POST["recurring_invoice_starting_date"];}
if(isset($_POST["recurring_invoice_finishing_date"])){$finishing_date=$_POST["recurring_invoice_finishing_date"];}
if(isset($_POST["recurring_invoice_period"])){$period=$_POST["recurring_invoice_period"];}
if(isset($_POST["recurring_invoice_period_type"])){$period_type=$_POST["recurring_invoice_period_type"];}
if(isset($_POST["recurring_invoice_max_occurrences"])){$max_occurrences=$_POST["recurring_invoice_max_occurrences"];}
if(isset($_POST["recurring_invoice_days_to_due"])){$days_to_due=$_POST["recurring_invoice_days_to_due"];}		
        // -- Update an Invoice. 	
		$idInvoice = $invoice->edit($_POST['invoiceNumber'],$_POST["invoice_series"], $_POST["invoice_customer_id"],$_POST["invoice_customer_name"],
							 $_POST["invoice_customer_identification"],$_POST["invoice_customer_email"],
							 $_POST["invoice_invoicing_address"],$_POST["invoice_shipping_address"],$customer_phone,$_POST["invoice_contact_person"],
							 $_POST["invoice_terms"],$_POST["invoice_notes"],$base_amount,$discount_amount,$net_amount,$gross_amount,$paid_amount,$tax_amount,
							 
							 $status,$type,$draft,$closed,$sent_by_email,$invoiceItemsCount,$recurring_invoice_id,
							 $_POST["invoice_issue_date"],$_POST["invoice_due_date"],$days_to_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,'');
						 
		
		// -- success.
		if (isset($idInvoice['id']) > 0)
        {			
			// -- Create or update Invoice Items.
			$_POST['invoiceNumber'] = $idInvoice['id'];
			for($i=0;$i<$invoiceItemsCount;$i++)
			{
				$product_id = $_POST["invoice_items_".$i."_product"];
				if(empty($product_id)){$product_id = null;}
				
				 $idInvoiceItems = $invoice->editItems($_POST["invoice_items_".$i."_id"],$_POST["invoice_items_".$i."_quantity"],$_POST["invoice_items_".$i."_discount_percent"],$idInvoice['id'],
				 $product_id,$_POST["invoice_items_".$i."_description"],$_POST["invoice_items_".$i."_unitary_cost"]);
				 
				 if(isset($idInvoiceItems['id']))
				 {
					 						   //print($_POST["invoice_items_".$i."_tax_id"]);

					$_POST["invoice_items_".$i."_id"] = $idInvoiceItems['id']; 
					
					// -- Add Item tax if it was selected.
					if(!empty($_POST["invoice_items_".$i."_tax_id"]))
					{
					   //id="invoice_items_0_tax_id"
					   $idItemTaxid = $tax->createItemTax($_POST["invoice_items_".$i."_id"],$_POST["invoice_items_".$i."_tax_id"]);
					}
				 }
				 else
				 {
					 if(isset($idInvoiceItemsError['idInvoiceItemsError']))
					 {
						//$idInvoiceItemsError['idInvoiceItemsError'] = $idInvoiceItemsError['idInvoiceItemsError'] + $idInvoiceItems;
						$idInvoiceItemsError['idInvoiceItemsError'] = array_merge($idInvoiceItems, $idInvoiceItemsError['idInvoiceItemsError']);
					 }
					 else
					 {
						 $idInvoiceItemsError['idInvoiceItemsError'] = $idInvoiceItems;
					 }
				 }
			}
			
        }
		else
		{
			$idInvoiceError['idInvoiceError'] = $idInvoice;
			$this->set($idInvoiceError);
		}
		// -- Re-load -Invoice Data.	
		$invoice= new invoice();
		$this->set($idInvoiceItemsError);
		$dInvoice["invoice"] = $invoice->showInvoice($id);
		$dItems["invoiceItems"] = $invoice->show($id);
		$dItemsCount['invoiceItemsCount'] = count($invoice->show($id));
		
        }
		
		// -- Data fo display.
		$this->set($dItemTax);
		$this->set($dTax);
		$this->set($dItems);
		$this->set($dItemsCount);
        $this->set($dInvoice);
		$this->render("edit");
		
}
	
	function payments($id)
    {
        require(ROOT . 'models/invoice.php');
		require(ROOT . 'models/payment.php');
// -- Paymets & any error.
		$idPayment = array();
		$idPaymentError = array();

// -- Invoice.
        $invoice= new invoice();
        $d["payments"] = $invoice->payments($id);
		$dinvoice_id["invoice"] = $invoice->showInvoice($id);
        
					//echo 'payment_count :0';

// -- Add Payments.
        $payment = new payment();
		
		if (isset($_POST["invoice_id"]))
        {
// -- Determine if Save button is clicked.
		 if(isset($_POST["add"]))
		 {			
			// -- Payments.
			$idPayment = $payment->create($_POST["invoice_id"], $_POST["payment_date"],$_POST["payment_amount"],$_POST["payment_notes"]);

			// -- success.
			if (isset($idPayment['id']) > 0)
			{
				// -- Reload payments.
				 $d["payments"] = $invoice->payments($id);
			}
			else
			{
				$idPaymentError['idPaymentError'] = $idPayment;
				$this->set($idPaymentError);
			}
		  }			 
        }
// -- Remove Payments.
// -- Determine if Remove button is clicked.
		if (isset($_POST["invoice_id2"]))
        {
			 if(isset($_POST["remove"]))
			 {		
					//echo 'payment_count :3';

				// -- determined how many Items are selected to be removed.
				$payment_count = $_POST["payment_count"];
				//echo 'payment_count :'.$payment_count;
				for($i=0;$i<$payment_count;$i++)
				{
					if(isset($_POST["invoice_payment_".$i.""]))
					{
						// -- Delete the selected payment(s).
						if(!empty($_POST["invoice_payment_".$i.""]))
						{
							$idPayment = $payment->remove($_POST["payment_".$i.""]);
							// -- Reload payments.
							$d["payments"] = $invoice->payments($_POST["invoice_id2"]);
							//print($_POST["payment_".$i.""]);
						}
					}
				}
				
			 }	
		}
		$this->set($dinvoice_id);
        $this->set($d);
        $this->render("payments");
    }
	
	function remove($id)
    {
        require(ROOT . 'models/invoice.php');

        $invoice = new invoice();
        if ($invoice->remove($id))
        {
            header("Location: " . WEBROOT . "invoice/index");
        }
    }


}}
?>