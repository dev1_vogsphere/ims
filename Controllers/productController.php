<?php
if (!class_exists('productController')) 
{
class productController extends Controller 
{

function __construct() 
{
  //parent::__construct();

}
function index() 
{
		$this->requireFile(ROOT . 'models/product.php');

		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

		// -- Count number of products
		$product_count = 0;		
        $product = new product();
        $d['products'] = $product->dynamic_showAll($companyname);
		// -- $product->showAll();
// -- Delete Selected product(s)
// -- Determine if Remove button is clicked.
		if (isset($_POST["product_count"]))
        {			
			// -- determined how many Items are selected to be removed.
			$product_count = $_POST["product_count"];

			 if(isset($_POST["delete"]))
			 {		

				for($i=0;$i<$product_count;$i++)
				{
					if(isset($_POST["product_".$i.""]))
					{
						// -- Delete the selected product(s).
						if(!empty($_POST["product_".$i.""]))
						{
							$idproduct = $product->remove($_POST["product_".$i.""]);
							// -- Reload products.
							$d['products'] = $product->dynamic_showAll($companyname);
						}
					}
				}
				
			 }
		}

// -- Search button
        if (isset($_POST["searchsubmit"]))
		{
			// -- Search term.
			if(isset($_POST["search_product_terms"]))
			{
				if(empty($_POST["search_product_terms"]))
				{
					// -- Reload products.
					$d['products'] = $product->dynamic_showAll($companyname);
				}
				else
				{
					$d['products'] = $product->search_product($_POST["search_product_terms"],$companyname);
				}
			}
		}
		
	    $this->set($d);
        $this->render("index");		
}

function search_product($terms,$companyname) 
{
		$this->requireFile(ROOT . 'models/product.php');

        $product = new product();
        $d["product"] = $product->search_product($terms,$companyname);
		
        $this->set($d);
        $this->render("index");
}

function create()
    {
		$this->requireFile(ROOT . 'models/product.php');

        $product= new product();
		$companyname = '';
		
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}


        if (isset($_POST["product_reference"]))
        {
			
			// -- Check if Exist First
				$prodObj = $product->getproductby_name($_POST["product_reference"],$companyname);

				if(isset($prodObj[0]['reference']))
				{
						// -- Customer exist already.
						$_POST['error'] = 'product already exists.';
						$_POST['productid'] = '0';
				}
				else
				{
					$id = $product->createSponsor($_POST["product_reference"], $_POST["product_description"],$_POST["product_price"],$companyname,
					$_POST['invoice_customer_id'],$_POST['invoice_customer_name'],$_POST['invoice_customer_email'],$_POST['invoice_customer_identification']);
					$_POST['productid'] = $id;
					$_POST['error'] = '';					
				}
			$this->render("create");

		}
		else
		{
			   $this->render("create");
		}
    }


    function edit($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        
		$product= new product();

        $d["product"] = $product->show($id);
			//echo 'id ='.$id;

        if (isset($_POST["product_reference"]))
        {
            if($product->editSponsor($id, $_POST["product_reference"], $_POST["product_description"],$_POST["product_price"],
				$_POST['invoice_customer_id'],$_POST['invoice_customer_name'],$_POST['invoice_customer_email'],$_POST['invoice_customer_identification']))
            {
				$d["product"] = $product->show($id);
				$_POST['productid'] = $id;
            }
        }
        $this->set($d);
        $this->render("edit");
    }
	
	function remove($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        $product = new product();
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}


        if ($product->remove($id))
        {
			//header("Location: " . WEBROOT . "product/index");
        }
		// -- Reload products.
        $d['products'] = $product->dynamic_showAll($companyname);		
		$this->set($d);
        $this->render("index");
    }
	
	function requireFile($filename)
	{
		$checkFile = $filename;
		$DoNotIncludeFile = '';
		$included_files = get_included_files();

		foreach ($included_files as $include_filename) 
		{

			if($include_filename == $checkFile)
			{
			  $DoNotIncludeFile = 'yes';
			}
		}

		if(empty($DoNotIncludeFile))
		{
			require $filename;
		}
	}
}
}
?>