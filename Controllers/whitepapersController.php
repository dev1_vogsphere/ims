<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if (!class_exists('whitepapersController')) 
{
class whitepapersController extends Controller 
{

function __construct() 
{
  //parent::__construct();

}
function index() 
{
		$this->requireFile(ROOT . 'models/whitepapers.php');
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}
		$username = '';
		if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}
	
        $whitepapersObj   = new whitepapers();
        $d['wps'] = $whitepapersObj->showAll($companyname);	
		
	    $this->set($d);
		$this->render("index");		
}

	function tmp_file($file)
	{
		// -- Upload directory
		$path = "uploads/"; // Upload director
		$name = '';
		$f    = $name;
		$filenameEncryp  = '';
		$url = '';
		// -- Upload a file -------------------------------------------- //
		$count = 0;
		if(isset($file["tmp_name"]))
		{
			$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
		// -- Encrypt The filename.
			$name = $file["name"];//md5($name).'.'.$ext;

			if(move_uploaded_file($file["tmp_name"], $path.$name)) 
			{
				$count++; // Number of successfully uploaded files
			}
			if($count > 0)
			{
			// -- BOC Encrypt the uploaded PDF with ID as password.
			$file_parts = pathinfo($name);
			$filenameEncryp = $path.$name;
			switch($file_parts['extension'])
			{
				case "pdf":
				{
					

				break;
				}
				case "PDF":
				{
					break;
				}
			}
			// -- File not success..
			$url = $this->getbaseurl();
			$url = str_replace('/whitepapers','',$url);
			$url = str_replace('/edit','',$url);		
			$filenameEncryp = $url.$filenameEncryp;
		  }
		}	
		

		return $filenameEncryp;
	}

	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if (isset($_SERVER['HTTPS']))
			{
				if($_SERVER['HTTPS'] == "on")
				{
					$url = "https://";
			    }
				else
				{
					$url = "http://";
				}
			}
			else
			{
					$url = "http://";
			}
				
			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			//echo $_SERVER['HTTPS'];
			//echo $url.$dir;
			return $url.$dir;
	}
function search_product($terms,$companyname) 
{
		$this->requireFile(ROOT . 'models/product.php');

        $product = new product();
        $d["product"] = $product->search_product($terms,$companyname);
		
        $this->set($d);
        $this->render("index");
}

function create()
    {
		$this->requireFile(ROOT . 'models/whitepapers.php');
        $whitepapersObj   = new whitepapers();
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

		$username = '';
		if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}


        if (isset($_POST["name"]))
        {
			// -- Save button clicked.
			if(isset($_POST["Save"]))
			{   
			
					// -- Check if Exist First -- //
						$wpsObj = $whitepapersObj->showbyname($_POST["name"],$companyname);

						if(isset($wpsObj[0]['name']))
						{
								// -- Customer exist already.
								$_POST['error'] = 'white paper already exists.';
								$_POST['id'] = '0';
						}
						else
						{
							// -- Move Document & also Picture -- isset
							//print_r($_FILES);
							$doc 	 = $this->tmp_file($_FILES['new_doc']);
							$picture = $this->tmp_file($_FILES['new_pic']);
							$date = date("Y-m-d H:m:s");
							
							$id = $whitepapersObj->create($_POST["name"],$_POST["description"],$date,$_POST["category"],$picture,$username,$doc,$companyname);
							$_POST['id']    = $id;					
							$_POST['error'] = '';	
						}
					$this->render("create");
			}
		}
		else
		{
			   $this->render("create");
		}
    }


    function edit($id)
    {        

		$this->requireFile(ROOT . 'models/whitepapers.php');
        $whitepapersObj   = new whitepapers();
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

		$username = '';
		if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}

		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$id = $_POST['id'];
		}
		
        $d["wps"] = $whitepapersObj->show($id);

		// -- Move Document & also Picture -- isset
		$doc 	 = $d["wps"]['document'];
		$picture = $d["wps"]['picture'];
		
		//echo $doc;
		
		if(isset($_FILES['new_doc']['name']))
		{
			$doc_tmp = $this->tmp_file($_FILES['new_doc']);
			if(!empty($doc_tmp))
			 {
				$doc = $doc_tmp;
			 }
		}
		if(isset($_FILES['new_pic']['name']))
		{
			$picture_tmp = $this->tmp_file($_FILES['new_pic']);
			if(!empty($picture_tmp))
			{
		      $picture = $picture_tmp;
			}
		}				

		//echo $id;
		//echo $doc;
		
        if (isset($_POST["name"]))
        {			
            if($whitepapersObj->edit($id, $_POST["name"], $_POST["description"],$_POST["category"],$picture,$username,$doc))
            {
				$d["wps"] = $whitepapersObj->show($id);
				$_POST['id'] = $id;
            }
        }
        $this->set($d);
        $this->render("edit");
    }
	
	function remove($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        $product = new product();
		$companyname = '';
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}


        if ($product->remove($id))
        {
			//header("Location: " . WEBROOT . "product/index");
        }
		// -- Reload products.
        $d['products'] = $product->dynamic_showAll($companyname);		
		$this->set($d);
        $this->render("index");
    }
	
	function requireFile($filename)
	{
		$checkFile = $filename;
		$DoNotIncludeFile = '';
		$included_files = get_included_files();

		foreach ($included_files as $include_filename) 
		{

			if($include_filename == $checkFile)
			{
			  $DoNotIncludeFile = 'yes';
			}
		}

		if(empty($DoNotIncludeFile))
		{
			require $filename;
		}
	}
}
}
?>