<?php
//require(ROOT . 'request.php');

class productController extends Controller 
{

public $request = null;

function __construct() 
{
  //parent::__construct();

}
function index() 
{
		$this->requireFile(ROOT . 'models/backup.php');

		// -- Count number of products
		$product_count = 0;		
        $backup = new backup();
        $d['backup'] = $backup->showtables();		
	    $this->set($d);
        $this->render("index");		
}

function search_product($terms) 
{
		$this->requireFile(ROOT . 'models/product.php');

        $product = new product();
        $d["product"] = $product->search_product($terms);
		
        $this->set($d);
        $this->render("index");
}

function create()
    {
		$this->requireFile(ROOT . 'models/product.php');

            $product= new product();

        if (isset($_POST["product_reference"]))
        {
			$id = $product->create($_POST["product_reference"], $_POST["product_description"],$_POST["product_price"]);

			$_POST['productid'] = $id;

			$this->render("create");

		}
		else
		{
			        $this->render("create");

		}

    }


    function edit($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        
		$product= new product();

        $d["product"] = $product->show($id);
			//echo 'id ='.$id;

        if (isset($_POST["product_reference"]))
        {
            if ($product->edit($id, $_POST["product_reference"], $_POST["product_description"],$_POST["product_price"]))
            {
				$d["product"] = $product->show($id);
				$_POST['productid'] = $id;
            }
        }
        $this->set($d);
        $this->render("edit");
    }
	
	function remove($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        $product = new product();
        if ($product->delete($id))
        {
			
        }
		// -- Reload products.
		$d['products'] = $product->showAll();		
		$this->set($d);
        $this->render("index");
    }
	
	function requireFile($filename)
	{
		$checkFile = $filename;
		$DoNotIncludeFile = '';
		$included_files = get_included_files();

		foreach ($included_files as $include_filename) 
		{

			if($include_filename == $checkFile)
			{
			  $DoNotIncludeFile = 'yes';
			}
		}

		if(empty($$DoNotIncludeFile))
		{
			require $filename;
		}
	}
}
?>