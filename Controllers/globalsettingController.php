<?php
if (!class_exists('globalsettingController')) 
{

class globalsettingController extends Controller 
{

public $request = null;

function __construct() 
{
  //parent::__construct();

}
function index() 
{
	$this->requireFile(ROOT . 'models/globalsetting.php');
	$this->requireFile(ROOT . 'models/tax.php');
	$this->requireFile(ROOT . 'models/series.php');
	$target_dir  = "images/"; // Upload directory

	/*number of taxes Items */
	$taxCount = 0;
	/*number of series Items */
	$seriesCount = 0;
	$settings = array();
	// -- Global Setting Controller.
	$globalsetting = new globalsetting();
	$globalsettings = $globalsetting->show($_SESSION['username_IMS']);		
	$d['globalsettings'] = $globalsettings;//$globalsetting->showAll();	
	$id = 0;
	
	// --  Taxes	
	$taxes = new tax();
	$dtaxes['taxes'] = $taxes->showAll2();	
	$dtaxCount['taxCount'] = count($taxes->showAll2());

	// -- Series.
	$series = new series();
	$dseries['series'] = $series->showAll();	
	$dseriesCount['seriesCount'] = count($series->showAll());

	// -- Logo
	$logo = "";
	$logoTemp = "";

	// -- count number of tax items added.
		if(isset($_POST["taxCount"]))
		{
			$taxCount = $_POST["taxCount"];
		}	
	
	// -- count number of series items added.
		if(isset($_POST["seriesCount"]))
		{
			$seriesCount = $_POST["seriesCount"];
		}		
	
	
	// -- Save button clicked.
	if(isset($_POST["Save"]))
	{   
	
// -- Array.
	$arrayGlobalsetting = array();
	if(isset($_SESSION['id']))
	{
		$arrayGlobalsetting[] = $_SESSION['id'];//0
	}else{$arrayGlobalsetting[] = '';}
// -- BOC Global Settings. 11.08.2018.
	if(isset($_POST["global_settings_company_name"])){          $arrayGlobalsetting[] = $_POST["global_settings_company_name"];}else{$arrayGlobalsetting[] = '';}//1
	//if(isset($_POST["global_settings_company_identification"])){$arrayGlobalsetting[] = $_POST["global_settings_company_identification"];}else{$arrayGlobalsetting[] = '';}//2
	if(isset($_POST["global_settings_company_address"])){		$arrayGlobalsetting[] = $_POST["global_settings_company_address"];}else{$arrayGlobalsetting[] = '';}//3
	if(isset($_POST["global_settings_company_phone"])){			$arrayGlobalsetting[] = $_POST["global_settings_company_phone"];}else{$arrayGlobalsetting[] = '';}//4
	if(isset($_POST["global_settings_company_fax"])){           $arrayGlobalsetting[] = $_POST["global_settings_company_fax"];}else{$arrayGlobalsetting[] = '';}//5
	if(isset($_POST["global_settings_company_email"])){         $arrayGlobalsetting[] = $_POST["global_settings_company_email"];}else{$arrayGlobalsetting[] = '';}//6
	if(isset($_POST["global_settings_company_url"])){			$arrayGlobalsetting[] = $_POST["global_settings_company_url"];}else{$arrayGlobalsetting[] = '';}//7
	if(isset($_POST["global_settings_currency"])){				$arrayGlobalsetting[] = $_POST["global_settings_currency"];}else{$arrayGlobalsetting[] = '';}//8	
	if(isset($_POST["global_settings_legal_terms"])){			$arrayGlobalsetting[] = $_POST["global_settings_legal_terms"];}else{$arrayGlobalsetting[] = '';}//9
	if(isset($_POST["global_settings_pdf_size"])){				$arrayGlobalsetting[] = $_POST["global_settings_pdf_size"];}else{$arrayGlobalsetting[] = '';}//10
	if(isset($_POST["global_settings_pdf_orientation"])){		$arrayGlobalsetting[] = $_POST["global_settings_pdf_orientation"];}else{$arrayGlobalsetting[] = '';}//11
	if(isset($_POST["logoTemp"]))
	{
		   if($_FILES["global_settings_company_logo"]["name"] != "")
			{
				$logo = $_FILES["global_settings_company_logo"]["name"];
			}
			else
			{
				$logo = $_POST["logoTemp"]; 
			}
		$arrayGlobalsetting[] = $logo;//12	
		// -- Move uploaded file to our directory.
		$target_file = $target_dir . basename($logo);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES["global_settings_company_logo"]["tmp_name"], $target_file)) 
	     {
        //echo "The file ". basename( $_FILES["logo"]["name"]). " has been uploaded.";
	     } 
	    else 
	     {
          //echo "Sorry, there was an error uploading your file.";$_SESSION['username_IMS']
         }		
    }
	if(isset($_POST["vat"])){		  $arrayGlobalsetting[] = $_POST["vat"];}else{$arrayGlobalsetting[] = '';}//13
	if(isset($_SESSION['username_IMS'])){ $arrayGlobalsetting[] = $_SESSION['username_IMS'];}else{$arrayGlobalsetting[] = '';}//14
	
	if(isset($_POST["account"])){		  $arrayGlobalsetting[] = $_POST["account"];}else{$arrayGlobalsetting[] = '';}//15
	if(isset($_POST["accounttype"])){		  $arrayGlobalsetting[] = $_POST["accounttype"];}else{$arrayGlobalsetting[] = '';}//16
	if(isset($_POST["bankname"])){		  $arrayGlobalsetting[] = $_POST["bankname"];}else{$arrayGlobalsetting[] = '';}//17
	if(isset($_POST["branchcode"])){		  $arrayGlobalsetting[] = $_POST["branchcode"];}else{$arrayGlobalsetting[] = '';}//18
	if(isset($_POST["branchname"])){		  $arrayGlobalsetting[] = $_POST["branchname"];}else{$arrayGlobalsetting[] = '';}//20
	if(isset($_POST["company_reg"])){		  $arrayGlobalsetting[] = $_POST["company_reg"];}else{$arrayGlobalsetting[] = '';}//21

/*	if(isset($_POST["global_settings_default_template"])){		$arrayGlobalsetting[] = $_POST["global_settings_default_template"];}//12
	if(isset($_POST["global_settings_last_calculation_date"])){ $arrayGlobalsetting[] = $_POST["global_settings_last_calculation_date"];}//13
	if(isset($_POST["global_settings_current_logo"])){ /*$id = $$arrayGlobalsetting[] = bal_settings_current_logo"]);}
	if(isset($_POST["vat"])){									$arrayGlobalsetting[] = $_POST["vat"];}//16
	*/
	
	// -- Save Global Settings.
	/* Its either we create or edit and existing*/
	if(isset($arrayGlobalsetting[0]))
	{ 
		if($arrayGlobalsetting[0] == 0)
		{
			// -- Call create
			$globalsetting->create($arrayGlobalsetting);
		}
		else
		{
			// -- Call updated/Edit  
			$globalsetting->edit($arrayGlobalsetting);			
		}
	}
	//print_r($arrayGlobalsetting);
	
	// -- EOC Global Settings. 11.08.2018.
		// -- Taxes.
		$active = '';
		$is_default = 0;

		for($i=0;$i<$taxCount;$i++)
		{
			if(isset($_POST["taxes_active_".$i.""])){ $active = $_POST["taxes_active_".$i.""];}
			if(isset($_POST["taxes_is_default_".$i.""])){ $is_default = $_POST["taxes_is_default_".$i.""];}

				$tax_id = $_POST["taxes_id_".$i.""];
				if(empty($tax_id))
				{
					// -- Create new.
					$tax_id = $taxes->create($_POST["taxes_name_".$i],$_POST["taxes_value_".$i], $active,$is_default);
				   $settings['settings'] = $tax_id;
				   $this->set($settings);					

 			    }
				else
				{
				   // -- Updated existing.
				   $tax_id = $taxes->edit($tax_id,$_POST["taxes_name_".$i],$_POST["taxes_value_".$i],$active,$is_default);
				   $settings['settings'] = $tax_id;
				   $this->set($settings);					
 			    }
		}
		// -- Series.
		$series_id = '';
		//echo "Modise Count :".$seriesCount;
		$enabled = '1';
		
		for($j=0;$j<$seriesCount;$j++)
		{
			
				if(isset($_POST["series_id_".$j.""]))
				{
					$series_id = $_POST["series_id_".$j.""];
				}
				if(isset($_POST["series_enabled_".$j.""])){ $enabled = '1';}

				if(empty($series_id))
				{
				    // -- Create new.
					$series_id = $series->create($_POST["series_name_".$j],$_POST["series_value_".$j],$_POST["series_first_number_".$j],$enabled);
					$settings['settings'] = $series_id;
				    $this->set($settings);					

				}
				else
				{
					// -- Updated existing.
					$series_id = $series->edit($series_id,$_POST["series_name_".$j],$_POST["series_value_".$j],$_POST["series_first_number_".$j],$enabled);
					$settings['settings'] = $series_id;
				    $this->set($settings);					
 			    }
		}
		// -- Global Settings.
		$gsetting = $globalsetting->show($_SESSION['username_IMS']);		
		$d['globalsettings'] = $gsetting;//$globalsetting->showAll();	

		// --  Taxes	
		$dtaxes['taxes'] = $taxes->showAll2();	
		$dtaxCount['taxCount'] = count($taxes->showAll2());

		// -- Series.
		$dseries['series'] = $series->showAll();	
		$dseriesCount['seriesCount'] = count($series->showAll());
		
		
	}
	
	//print_r($d['globalsettings']);
	// -- globalsetting.
	//print_r($d);
	$this->set($d);
	// Taxes and Series.
	$this->set($dtaxes);
	$this->set($dseries);
	// -- Count.
	$this->set($dtaxCount);
	$this->set($dseriesCount);
	$this->render("index");		
}

function smtpsettings() 
{
	$this->requireFile(ROOT . 'models/globalsetting.php');
	// -- Global Setting Controller.
	$globalsetting = new globalsetting();
	// -- Global Settings.
	$gsetting = $globalsetting->show($_SESSION['username_IMS']);		
	$d['globalsettings'] = $gsetting;	

	$id = 0;
	$radio   = '';
	$captcha = '';
	$dev = '';
	$prod = '';

	// -- Save button clicked.
	if(isset($_POST["Save"]))
	{   

	$arrayGlobalsetting = array();
	if(isset($_SESSION['id']))
	{
		$arrayGlobalsetting[] = $_SESSION['id'];//0
	}else{$arrayGlobalsetting[] = '';}

		// -- BOC Global Settings. 11.08.2018.
		if(isset($_POST["host"])){$arrayGlobalsetting[] = $_POST["host"];}else{$arrayGlobalsetting[] = '';}	
		if(isset($_POST["port"])){$arrayGlobalsetting[] = $_POST["port"];}else{$arrayGlobalsetting[] = '';}	
		if(isset($_POST["secure"])){$arrayGlobalsetting[] = $_POST["secure"];}else{$arrayGlobalsetting[] = '';}	
		if(isset($_POST["emailUsername"])){$arrayGlobalsetting[] = $_POST["emailUsername"];}else{$arrayGlobalsetting[] = '';}	
		if(isset($_POST["emailPassword"])){$arrayGlobalsetting[] = $_POST["emailPassword"];}else{$arrayGlobalsetting[] = '';}	
		
		if(!empty($_POST['radio'])){$radio = $_POST['radio'];}
		if(!empty($radio))
		{
			if($radio == 'd')
			{
				$dev = 'd';
				$prod = '';
			}
			if($radio == 'p')
			{
				$prod = 'p';
				$dev = '';
			}
		}
			$arrayGlobalsetting[] = $dev;	
			$arrayGlobalsetting[] = $prod;				

		// -- Disable Captcha -- //
		if(isset($_POST['captcha']))
		{
			$captcha = $_POST['captcha'];
		}
		else
		{
			$captcha = '';
		}
		if(isset($_POST["captcha"])){$arrayGlobalsetting[] = $captcha;}else{$arrayGlobalsetting[] = '';}	

		if(isset($_SESSION['username_IMS'])){ $arrayGlobalsetting[] = $_SESSION['username_IMS'];}else{$arrayGlobalsetting[] = '';}//14

		if(isset($arrayGlobalsetting[0]))
		{ 
			if($arrayGlobalsetting[0] != 0)
			{	
				$globalsetting->edit_smtp($arrayGlobalsetting);
			}
		}
				
		// -- Global Settings.
		$gsetting = $globalsetting->show($_SESSION['username_IMS']);		
		$d['globalsettings'] = $gsetting;	
		
	}
// -- globalsetting.
	$this->set($d);        
	$this->render("smtpsettings");
}

function backup() 
{
		$this->requireFile(ROOT . 'models/backup.php');

		// -- Count number of products
		$product_count = 0;		
        $backup = new backup();
        $d['backup'] = $backup->showtables();		
	    $this->set($d);
        $this->render("backup");		
}

function create()
    {
		$this->requireFile(ROOT . 'models/product.php');

            $product= new product();

        if (isset($_POST["product_reference"]))
        {
			$id = $product->create($_POST["product_reference"], $_POST["product_description"],$_POST["product_price"]);

			$_POST['productid'] = $id;

			$this->render("create");

		}
		else
		{
			        $this->render("create");

		}

    }


    function edit($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        
		$product= new product();

        $d["product"] = $product->show($id);
			//echo 'id ='.$id;

        if (isset($_POST["product_reference"]))
        {
            if ($product->edit($id, $_POST["product_reference"], $_POST["product_description"],$_POST["product_price"]))
            {
				$d["product"] = $product->show($id);
				$_POST['productid'] = $id;
            }
        }
        $this->set($d);
        $this->render("edit");
    }
	
	function remove($id)
    {
		$this->requireFile(ROOT . 'models/product.php');
        $product = new product();
        if ($product->delete($id))
        {
			
        }
		// -- Reload products.
		$d['products'] = $product->showAll();		
		$this->set($d);
        $this->render("index");
    }
	
	function requireFile($filename)
	{
		$checkFile = $filename;
		$DoNotIncludeFile = '';
		$included_files = get_included_files();

		foreach ($included_files as $include_filename) 
		{

			if($include_filename == $checkFile)
			{
			  $DoNotIncludeFile = 'yes';
			}
		}

		if(empty($DoNotIncludeFile))
		{
			require $filename;
		}
	}
}
}
?>