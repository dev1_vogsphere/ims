<?php
if (!class_exists('dashboardController')) 
{
class dashboardController extends Controller 
{
function __construct() 
{
  //parent::__construct();
}
function index() 
{
	require(ROOT . 'models/payment.php');	
	require(ROOT . 'models/invoice.php');	
	$companyname = '';
	if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}
//echo 	'com :'. $companyname;
// -- Get Totals
	$totalPayment = 0.00;	
	$totalInvoiceDue = 0.00;	

	$summary = array();
	$dOverDue = null;
    $payment = new payment();
	$totalPayment = $payment->organisation_totalpayments($companyname);
// -- Get Total Invoiced Amount.
	$invoice = new invoice();
	$totalInvoiceDue = $invoice->organisation_totalinvoiceamount($companyname);
	
	// Bind Totals into Array.
	$summary[0] = $totalPayment;
	$summary[1] = $totalInvoiceDue;

	$dSummary['summary'] = $summary;	
// -- BOC Count Total Status - Invoice.
// -- statuses.
/*
define("DRAFT", 0);
define("CLOSED", 1);
define("OPENED", 2);
define("OVERDUE", 3);
*/
$statuses = array();
// -- 1. Invoices in CLOSED  = 1
$CLOSED  = $invoice->organisation_tot_statusCount(1,'Invoice',$companyname);
// -- 2. Invoices in OPENED  = 2
$OPENED  = $invoice->organisation_tot_statusCount(2,'Invoice',$companyname);
// -- 3. Invoices in OVERDUE = 3
$OVERDUE = $invoice->organisation_tot_statusCount(3,'Invoice',$companyname);
// -- combine into an array.
$statuses[1] = $CLOSED;
$statuses[2] = $OPENED;
$statuses[3] = $OVERDUE;
$dstatuses['statuses'] = $statuses;	
// -- EOC Count Total Status - Invoice.

	// -- BOC Invoice -- //
// -- Count number of Invoices
		$invoice_count = 0;		
		$d['invoices'] = array();
		$whereArr = array();
		$customer_identification = '';
		$datefrom 	= null;
		$dateto 	= null;
		$status 	= '';
		
// -- Search button
        if (isset($_POST["searchsubmit"]))
		{
			$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = 'Invoice')";
			$whereArr[] = "id in {$value}";

		//1. -- search_invoice_terms.
			if(isset($_POST["search_invoice_terms"]))
			{
				if(!empty($_POST["search_invoice_terms"]))
				{
					$customer_identification 	= $_POST["customer_identification"];
					$whereArr[] = "customer_identification = '{$customer_identification}'";
					//$d['invoices'] = $invoice->search_customer_invoice($_POST["customer_identification"]);
				}
				
			}
		//2. -- search_invoice_status.search_invoice_status
			if(isset($_POST["search_invoice_status"]))
			{
					$status 	= $_POST["search_invoice_status"];
					if($status != '')
					{$whereArr[] = "status = {$status}";}
			}
		// 3. search_invoice_date_from.
			if(isset($_POST["search_invoice_date_from"]))
			{
				if(!empty($_POST["search_invoice_date_from"]))
				{
					$datefrom 	= $_POST["search_invoice_date_from"];
					$whereArr[] = "issue_date >= {$datefrom}";
				}
			}

		// 4. search_invoice_date_to
			if(isset($_POST["search_invoice_date_to"]))
			{
				if(!empty($_POST["search_invoice_date_to"]))
				{
					$dateto 	= $_POST["search_invoice_date_to"];
					$whereArr[] = "issue_date <= {$dateto}";
				}
			}
			//print_r($whereArr);
		 // -- No dynamic query.
			if(empty($whereArr))
			{
				$d['invoices'] = $invoice->showAll();		
			}
		// -- Dynamic Query search.
			else
			{
				$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);	
				//print_r($d['invoices']);				
			}
		}
		else
		{
			//$d['invoices'] = $invoice->showAll();
			$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = 'Invoice')";
			$whereArr[] = "id in {$value}";
			$d['invoices'] = $invoice->search_dynamic_invoice($whereArr);	

		// -- Past Due Invoices.
		$whereArr = null;
		$value = "(select i.id from customer as c join common as i on c.id = i.customer_id where c.organisation = '".$companyname."' and i.type = 'Invoice' and i.status = '3')";		
		$whereArr[] = "id in {$value}";
		$dOverDue['overdueinvoices'] = $invoice->search_dynamic_invoice($whereArr);			
		}		

	   // -- Get the Invoice Payments details. ONLY 5 Recent Invoices
		$invoices = $d['invoices'];
		$data = array();
		$count = 0;
        foreach ($invoices as $rowInvoice)
        {
			$payments_data = $invoice->payments($rowInvoice['id']);
			$totalPayment = 0;
			$balance = 0.00;
		
			foreach ($payments_data as $row) 
			{
				$amount = $row['amount'];
				$totalPayment = $totalPayment+$amount;
			}
			
			//echo $rowInvoice['id'].";amount = ".$totalPayment." \n";
			$rowInvoice['paid_amount'] = $totalPayment;
			
			array_push($data,$rowInvoice);
			$count = $count + 1;	
			
			// -- Only needs five recent paid invoices.
			if($count >= 5)
			{
				break;
			}
		}
		$d['invoices'] = $data;
	   // -- Get the Invoice Payments details. ONLY 5 Past Due Invoices.
		$invoices = $dOverDue['overdueinvoices'];
		$data = array();
		$count = 0;
		// -- FIX : php Warning: Invalid argument supplied for foreach()
		if (is_array($invoices) || is_object($invoices))
		{
			foreach ($invoices as $rowInvoice)
			{
				$payments_data = $invoice->payments($rowInvoice['id']);
				$totalPayment = 0;
				$balance = 0.00;
			
				foreach ($payments_data as $row) 
				{
					$amount = $row['amount'];
					$totalPayment = $totalPayment+$amount;
				}
				
				//echo $rowInvoice['id'].";amount = ".$totalPayment." \n";
				$rowInvoice['paid_amount'] = $totalPayment;
				
				array_push($data,$rowInvoice);
				$count = $count + 1;	
				
				// -- Only needs five recent paid invoices.
				if($count >= 5)
				{
					break;
				}
			}
		}	
		$dOverDue['overdueinvoices'] = $data;
		
	// -- EOC Invoice -- // 
	$this->set($dstatuses);
	$this->set($dSummary);
	$this->set($d);
	$this->set($dOverDue);		
	$this->render("index");
}
}
}
?>