<?php
if (!class_exists('customerController')) 
{

class customerController extends Controller 
{

public $request = null;

function __construct() 
{
  //parent::__construct();

}
function index() 
{
     //require(ROOT . 'models/customer.php');
	$this->requireFile(ROOT . 'models/customer.php');
	 
	 $d['customers'] = array();
     $customer = new customer();
	 $status   = '';
	 $companyname = '';
	if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}
	$username = '';
	if(isset($_SESSION['username_IMS'])){$username = $_SESSION['username_IMS'];}
	$eMailInvoices 					 = array();
	$indexInvoiceSent				 = 0;
	$deMailInvoices['eMailInvoices'] = array();

	   //   $d['customers'] = $customer->search_customer_by_organisation($companyname);
	      $d['customers'] = $customer->search_customer_by_status($companyname,$status);
//print_r($d['customers']);

// -- Search button
        if (isset($_POST["searchsubmit"]))
		{
			if(isset($_POST['search_invoice_status']))
			{
				$status   = $_POST['search_invoice_status'];	
			}
			// -- Search term.
			if(isset($_POST["search_customer_terms"]))
			{
				if(empty($_POST["search_customer_terms"]))
				{
					// -- Reload customers.
					//$d['customers'] = $customer->showAll();
					//$d['customers'] = $customer->search_customer_by_organisation($companyname);
					$d['customers'] = $customer->search_customer_by_status($companyname,$status);					
				}
				else
				{
					$d['customers'] = $customer->search_customer($_POST["search_customer_terms"],"'".$companyname."'");
				}
			}
		}
	 
	 // -- Reset button
        if (isset($_POST["search-reset"]))
		{
			     //$d['customers'] = $customer->showAll();
						$d['customers'] = $customer->search_customer_by_organisation($companyname);
			 
		}
	 
	 // -- Delete Selected customer(s)
// -- Determine if Remove button is clicked.
		if (isset($_POST["customer_count"]))
        {			
			// -- determined how many Items are selected to be removed.
			$customer_count = $_POST["customer_count"];

			 if(isset($_POST["delete"]))
			 {		

				for($i=0;$i<$customer_count;$i++)
				{
					if(isset($_POST["customer_".$i.""]))
					{
						// -- Delete the selected customer(s).
						if(!empty($_POST["customer_".$i.""]))
						{
							$idcustomer = $customer->remove($_POST["customer_".$i.""]);
							// -- Reload customers.
							//$d['customers'] = $customer->showAll();	
					$d['customers'] = $customer->search_customer_by_organisation($companyname);
							
						}
					}
				}
				
			 }
		}
		$deMailInvoices['eMailInvoices']=$eMailInvoices;		
		$this->set($d);
		$this->set($deMailInvoices);	 
		$this->render("index");
}

function edit($id)
{
        require(ROOT . 'models/customer.php');
        $customer= new customer();

        $d["customer"] = $customer->show($id);

        if (isset($_POST["customer_name"]))
        {
if($customer->edit($id,
$_POST['customer_name'],
$_POST['customer_name'],
$_POST['customer_identification'],
$_POST['customer_contact_person'],
$_POST['customer_email'],
$_POST['customer_invoicing_address'],
$_POST['customer_shipping_address'],
$_POST['customer_office_number'],
$_POST['customer_cell_number'],
$_POST['customer_vat'],
$_POST['companyregistration'],
$_POST['status']))
            {
              //  header("Location:index");
                //header("Location: " . WEBROOT . "customer/index");
        $d["customer"] = $customer->show($id);
				$_POST['customerid'] = $id;

            }
        }
        $this->set($d);
        $this->render("edit");
}

function remove($id)
    {
        require(ROOT . 'models/customer.php');
        $customer = new customer();
				$companyname = '';
		
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

        if ($customer->remove($id))
        {
			
        }
		// -- Reload customers.
		//$d['customers'] = $customer->showAll();	
		$d['customers'] = $customer->search_customer_by_organisation($companyname);
		
		$this->set($d);
        $this->render("index");
    }

		function create()
		{
		$companyname = '';
		
		if(isset($_SESSION['company_name'])){$companyname = $_SESSION['company_name'];}

			if (isset($_POST["title"]))
				{
					require(ROOT . 'models/customer.php');
					$customer = new customer();
					// -- Check if Exist First
					$cusObj = $customer->getcustomerby_name($_POST["title"],$companyname);
					//echo $cusObj[0]['name'];
					//print_r($cusObj);
					if(isset($cusObj[0]['name']))
					{
						// -- Customer exist already.
						$_POST['error'] = 'Customer already exists.';
						$_POST['customerid'] = '0';
					}
					else
					{
						$id = $customer->create($_POST["title"],$_POST["title"], $_POST["description"],$_POST['customer_email'],$_POST['customer_contact_person'], $_POST['customer_invoicing_address'], $_POST['customer_shipping_address'],$companyname,
						$_POST["customer_office_number"],$_POST["customer_cell_number"],$_POST["customer_vat"],$_POST['companyregistration']);
						//echo 'id ='.$id;
						
						if($id > 0)
						{			
							$_POST['customerid'] = 	$id['id'];
							$_POST['description'] = $id['identification'];
							$_POST['error'] = '';
						}
					}
				
					$this->render("new");
				}
				else
				{
					$this->render("new");
				}
		}
	function requireFile($filename)
	{
		$checkFile = $filename;
		$DoNotIncludeFile = '';
		$included_files = get_included_files();

		foreach ($included_files as $include_filename) 
		{

			if($include_filename == $checkFile)
			{
			  $DoNotIncludeFile = 'yes';
			}
		}

		if(empty($DoNotIncludeFile))
		{
			require $filename;
		}
	}	
}


}	
?>