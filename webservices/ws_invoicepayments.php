<?php
header('Content-Type: text/html; charset=utf-8');
define('ROOT', str_replace("webservices/ws_invoicepayments.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

if(!function_exists('xmlEntities'))
{
	function xmlEntities($string) 
	{
		$terms = htmlentities($string);					
		$terms =str_replace('&nbsp;', '',$terms);
		$terms=str_replace('&bull;', '',$terms);
		$terms=str_replace('&ndash;', '',$terms);
		
		$terms=str_replace('&iuml;', '',$terms);
		$terms=str_replace('&iquest;', '',$terms);
		$terms=str_replace('&frac12;', '',$terms);
		$terms=str_replace('&Acirc;', '',$terms);
		$terms=str_replace('&Atilde;', '',$terms);
		$terms=str_replace('&macr;', '',$terms);
		return $terms;
	}
}
/* require the user as the parameter 
customerid
ApplicationId
guid
repayment

PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json
*/
// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;

$customerid = null;
$ApplicationId = null;
$dataUserEmail = '';
$role = '';
$repaymentAmount = null;
$found = false;
$RemainingTerm = 0;
$paymentid = 0;		
$credit = 0.00;
$xml = "";
$found = true;
$dataInvoice = null;

/* soak in the passed variable or set our own */					
$format = '';
if(isset($_GET['format']))				
{
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
}

// -- Get Parameters
$customerid 	= '';
$ApplicationId  = '';
$repaymentAmount = 0;

if(isset($_GET['customerid']))				
{
	$customerid = $_GET['customerid'];
}
if(isset($_GET['ApplicationId']))				
{
	$ApplicationId = $_GET['ApplicationId'];
}
if(isset($_GET['amount']))				
{
	$repaymentAmount = $_GET['amount'];
}
// -- If all Parameters are populated. 
if($found) 
{
	/* soak in the passed variable or set our own */
	//$format = 'xml';//strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
	try
	{
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 		 			 
// --- Invoice Data ---
			$invoice = new invoice();
			if(!empty($customerid))
			{
				$dataInput 			        = null;
				$dataInput['customerid']    = $customerid;
				$dataInput['ApplicationId'] = $ApplicationId;
				$dataInput['amount'] 	    = $repaymentAmount;				
				//print_r($dataInput);
				$dataInvoice = $invoice->GET_IMS_INVOICE_PAYMENTS($dataInput);
			}
			if(empty($dataInvoice[0]['paymentid']))
			{
				 
				$xml = '<?xml version="1.0" encoding="utf-8"?><invoices>';
  				$xml = $xml.'<invoice>';
					$xml = $xml.'<paymentid></paymentid>';
					$xml = $xml.'<iddocument></iddocument>'; 
					$xml = $xml.'<path></path>';
					$xml = $xml.'<tot_paid></tot_paid>';
					$xml = $xml.'<tot_outstanding></tot_outstanding>';		
					$xml = $xml. '</invoice>';					
			    $xml = $xml. '</invoices>';
			}
			else{
// ----------------------------------------------------------------------------------- //				 		 			 
			$xml = '<?xml version="1.0" encoding="utf-8"?><invoices>';
			foreach($dataInvoice as $rowCommon)
			{
					//print_r($rowCommon);
					//$invoice_id = $rowCommon['id'];	
					//echo $rowCommon['paymentid'];					
					$xml = $xml.'<invoice>';					  
					$xml = $xml.'<paymentid>'.$rowCommon['paymentid'].'</paymentid>';
					$xml = $xml.'<iddocument>'.$rowCommon['iddocument'].'</iddocument>'; 
					$xml = $xml.'<path>'.$rowCommon['path'].'</path>';
					$xml = $xml.'<tot_paid>'.$rowCommon['tot_paid'].'</tot_paid>';
					$xml = $xml.'<tot_outstanding>'.$rowCommon['tot_outstanding'].'</tot_outstanding>';
					$xml = $xml. '</invoice>';
			}	// End all Customer Invoices
				$xml = $xml. '</invoices>';
			}	
			// -- Determine the format.
			
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
	}	
	// -- EOC 31.10.2017 -------- //
?>
