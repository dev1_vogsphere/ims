<?php
header('Content-Type: text/html; charset=utf-8');

define('ROOT', str_replace("webservices/ws_invoices.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

if(!function_exists('xmlEntities'))
{
	function xmlEntities($string) 
	{
		$terms = htmlentities($string);					
		$terms =str_replace('&nbsp;', '',$terms);
		$terms=str_replace('&bull;', '',$terms);
		$terms=str_replace('&ndash;', '',$terms);
		
		$terms=str_replace('&iuml;', '',$terms);
		$terms=str_replace('&iquest;', '',$terms);
		$terms=str_replace('&frac12;', '',$terms);
		$terms=str_replace('&Acirc;', '',$terms);
		$terms=str_replace('&Atilde;', '',$terms);
		$terms=str_replace('&macr;', '',$terms);
		return $terms;
	}
}
/* require the user as the parameter 
customerid
ApplicationId
guid
repayment

PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json
*/
// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;

$customerid = null;
$ApplicationId = null;
$dataUserEmail = '';
$role = '';
$repaymentAmount = null;
$found = false;
$RemainingTerm = 0;
$paymentid = 0;		
$credit = 0.00;
$xml = "";
$found = true;
$dataCommon = null;

/* soak in the passed variable or set our own */					
$format = '';
if(isset($_GET['format']))				
{
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
}

// -- Get Parameters
$invoiceid = '';
if(isset($_GET['invoiceid']))				
{
	$invoiceid = $_GET['invoiceid'];
}

// -- If all Parameters are populated. 
if($found) 
{
	/* soak in the passed variable or set our own */
	//$format = 'xml';//strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
	try
	{
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 		 			 
		// -- Invoice Data.	
			$invoice = new invoice();
			if(empty($invoiceid))
			{
				$dataCommon = $invoice->showAll();
			}
			else
			{
				$dataCommonItem = $invoice->showInvoice($invoiceid);
				$dataCommon[] = $dataCommonItem;	
			}
			//print_r($dataCommon);
// ----------------------------------------------------------------------------------- //				 		 			 
			$dataInvoice = null;
			$xml = '<?xml version="1.0" encoding="utf-8"?><invoices>';
			foreach($dataCommon as $rowCommon)
			{
				//print_r($rowCommon);
					//$invoice_id = $rowCommon['id'];					  					  
					$xml = $xml.'<invoice>';					  
					$xml = $xml.'<id>'.$rowCommon['id'].'</id>';
					$xml = $xml.'<series_id>'.$rowCommon['series_id'].'</series_id>'; 
					$xml = $xml.'<customer_id>'.$rowCommon['customer_id'].'</customer_id>';
					$customer_name=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['customer_name']);
					$xml = $xml.'<customer_name>'.$customer_name.'</customer_name>';
					$customer_identification=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['customer_identification']);
					$xml = $xml.'<customer_identification>'.$customer_identification.'</customer_identification>';
					$customer_email=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['customer_email']);					
					$xml = $xml.'<customer_email>'.$customer_email.'</customer_email>';					
					$invoicing_address = xmlEntities($rowCommon['invoicing_address']); 
					$xml = $xml.'<invoicing_address>'.$invoicing_address.'</invoicing_address>';
					$shipping_address = xmlEntities($rowCommon['shipping_address']); 					
					$xml = $xml.'<shipping_address>'.$shipping_address.'</shipping_address>'; 
					$contact_person=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['contact_person']);															
					$xml = $xml.'<contact_person>'.$contact_person.'</contact_person>'; 
					$terms = xmlEntities($rowCommon['terms']); 
					$xml = $xml.'<terms>'.$terms.'</terms>';
					$notes = xmlEntities($rowCommon['notes']); 
					$xml = $xml.'<notes>'.$notes.'</notes>'; 
					$xml = $xml.'<base_amount>'.$rowCommon['base_amount'].'</base_amount>'; 
					$xml = $xml.'<discount_amount>'.$rowCommon['discount_amount'].'</discount_amount>'; 
					$xml = $xml.'<net_amount>'.$rowCommon['net_amount'].'</net_amount>';
					$xml = $xml.'<gross_amount>'.$rowCommon['gross_amount'].'</gross_amount>';
					$xml = $xml.'<paid_amount>'.$rowCommon['paid_amount'].'</paid_amount>';
					$xml = $xml.'<outstanding_amount>'.$rowCommon['outstanding_amount'].'</outstanding_amount>';
					$xml = $xml.'<tax_amount>'.$rowCommon['tax_amount'].'</tax_amount>';
					$xml = $xml.'<status>'.$rowCommon['status'].'</status>';
					$type=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['type']);					
					$xml = $xml.'<type>'.$type.'</type>';
					$xml = $xml.'<draft>'.$rowCommon['draft'].'</draft>';
					$xml = $xml.'<closed>'.$rowCommon['closed'].'</closed>'; 
					$xml = $xml.'<sent_by_email>'.$rowCommon['sent_by_email'].'</sent_by_email>';
					$xml = $xml.'<number>'.$rowCommon['number'].'</number>';
					$xml = $xml.'<recurring_invoice_id>'.$rowCommon['recurring_invoice_id'].'</recurring_invoice_id>'; 
					$xml = $xml.'<issue_date>'.$rowCommon['issue_date'].'</issue_date>'; 
					$xml = $xml.'<due_date>'.$rowCommon['due_date'].'</due_date>';
					$xml = $xml.'<days_to_due>'.$rowCommon['days_to_due'].'</days_to_due>'; 
					$xml = $xml.'<enabled>'.$rowCommon['enabled'].'</enabled>'; 
					$xml = $xml.'<max_occurrences>'.$rowCommon['max_occurrences'].'</max_occurrences>'; 
					$xml = $xml.'<must_occurrences>'.$rowCommon['must_occurrences'].'</must_occurrences>'; 
					$xml = $xml.'<period>'.$rowCommon['period'].'</period>';
					$xml = $xml.'<period_type>'.$rowCommon['period_type'].'</period_type>'; 
					$xml = $xml.'<starting_date>'.$rowCommon['starting_date'].'</starting_date>'; 
					//$xml = $xml.'<finishing_date>'.$rowCommon['finishing_date'].'</finishing_date>'; 
					$xml = $xml.'<last_execution_date>'.$rowCommon['last_execution_date'].'</last_execution_date>';
					$xml = $xml.'<created_at>'.$rowCommon['created_at'].'</created_at>'; 
					$xml = $xml.'<updated_at>'.$rowCommon['updated_at'].'</updated_at>'; 
					$customer_phone=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommon['customer_phone']);					
					$xml = $xml.'<customer_phone>'.$customer_phone.'</customer_phone>';
					
					// -- Invoice Items Data.	
					$dataCommonItems = $invoice->show($rowCommon['id']);
					$xml = $xml.'<items>';
					foreach($dataCommonItems as $rowCommonItem)
					{	$xml = $xml. '<item>';
							$xml = $xml.'<id>'.$rowCommonItem['id'].'</id>';
							$xml = $xml.'<quantity>'.$rowCommonItem['quantity'].'</quantity>';
							$xml = $xml.'<discount>'.$rowCommonItem['discount'].'</discount>';
							$xml = $xml.'<common_id>'.$rowCommonItem['common_id'].'</common_id>';
							$xml = $xml.'<product_id>'.$rowCommonItem['product_id'].'</product_id>';
							$description=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;',$rowCommonItem['description']);
							//$xml = $xml.'<description>'.$description.'</description>';
							//$xml = $xml.'<description></description>';
							$xml = $xml.'<unitary_cost>'.$rowCommonItem['unitary_cost'].'</unitary_cost>';
							$xml = $xml.'<tax_id>'.$rowCommonItem['tax_id'].'</tax_id>';							
						$xml = $xml.'</item>';
					}
					$xml = $xml. '</items>';
					$xml = $xml. '</invoice>';
			}	// End all Customer Invoices
				$xml = $xml. '</invoices>';
				
			// -- Determine the format.
			
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
	}	
	// -- EOC 31.10.2017 -------- //
?>
