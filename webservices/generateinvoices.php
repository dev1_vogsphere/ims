<?php
date_default_timezone_set('Africa/Johannesburg');

/* switch-on error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

if(!function_exists('getbaseurl'))
{
	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if (isset($_SERVER['HTTPS']) != "on")
			{
				$url = "http://";
			}
			else
			{
				$url = "http://";
			}
			
			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			return $url.$dir;
	}
}
	
try 
{
// -- libxml_use_internal_errors(TRUE);
/* Scheduled to run daily for daily/montlhy/yearly.*/
define('ROOT', str_replace("webservices/generateinvoices.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

// -- Invoice Object
$invoiceobj = new invoice();

// -- Get Tax Master Data.
$tax = new tax();

// -- Web Service.
$WebServiceURL = getbaseurl()."ws_recurringinvoice.php";

//echo $WebServiceURL;

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// -- Frequency type.
$frequencyType  = '';

// -- Frequency.
$frequency  = '';

// -- Max occurance.
$max_occurance = 0;

// -- days in due.
$days_due = 0;

// -- Issue date.
$InvoiceDueDate = date('Y-m-d');

// -- Store new Invoice IDs
$idInvoice 		= null;
$idInvoiceItems = null;

// -- Constant.
$status = 2;
$type   = 'Invoice';

/* default values */
$days_to_due 		 = 0;
$enabled			 = 0;
$max_occurrences	 = 0;
$must_occurrences 	 = 0;
$period				 = 0;
$period_type		 = '';
$starting_date		 = null;
$finishing_date		 = null;
$last_execution_date = null;
$draft  = 0;
$closed = 0;
$sent_by_email = 0;
$invoiceid = 0;
/*
1. Current date is within the date range
-------------------------------------
2. Check whether is day or month,yearly.
----------------------------------------
3. Create invoice every 1,2,3.....
add [every] from [start date] compare to [current date] 
-------------------------------------------------------
4.Max occurance 
count [recurring_id] < [Max occurance]
-----------------------------------------------
5. Create [days to due] add [days to due] to invoice date
$days_due
--------------------------------------------------------- 
,"<style> table, th, td {border: 1px solid black;border-collapse: collapse;}</style>"
*/
$rootUrl = str_replace("/webservices","",getbaseurl());

$html = array();
array_push($html, "<!DOCTYPE html><html><head>");
array_push($html,"<link rel='stylesheet' href='".$rootUrl."libs/css/6da8889.css' />",
     "<link rel='stylesheet' href='".$rootUrl."libs/css/678669b.css' />");
array_push($html,"<link rel='stylesheet' href='".$rootUrl."libs/css/4cc3b63.css'/>"
     ,"<link rel='stylesheet' href='".$rootUrl."/libs/css/2059864.css' />");
array_push($html,"<link rel='stylesheet' href='".$rootUrl."libs/css/custom.css' rel='stylesheet'>");
array_push($html, "</head><body><section id='bd'><div class=container'>","<div class=content'>");
array_push($html, "<form name='invoice_payment_list'><table class='table condensed-table'>","<thead>");
array_push($html, "<tr>","<th class='col-md-2 date'>Recurring Invoice#</th>");
array_push($html,"<th class='col-md-7'>Generated Invoice#</th>","<th class='col-md-7'>Feedback</th>");
array_push($html,"</tr>","</thead>");
array_push($html,"<tbody>");
// And if you want to fetch multiple paymentschedule IDs:
foreach($sxml->invoice as $invoice)
{	
// -- current date.
	$today 	   = date("Y-m-d");
// -- 1. Current date is within the date range.
	$fromUser  = new DateTime($today);
	// -- starting_date
	$startDate = new DateTime($invoice->starting_date);
	// -- finishing_date
	$endDate   = new DateTime($invoice->finishing_date);
	
	$invoiceid 	  = $invoice->id;
	array_push($html,"<tr>","<td>".$invoiceid."</td>");
	
	if($invoiceobj->isDateBetweenDates($fromUser , $startDate, $endDate))
	{
// -- 2. Check whether is day or month,yearly.
	 $frequencyType  = $invoice->period_type;	
	 
// -- 3. Check the frequency.
	 $frequency = $invoice->period;

// -- 4. Max Occurance.
	 $max_occurance = $invoice->max_occurrences;

// -- 5. Create [days to due] add [days to due] to invoice date
// -- Invoice Due date after certain date.
	 $days_due = $invoice->days_to_due;
	 
	 $max_occurrences = $invoice->max_occurrences;
	 $invoiceid 	  = $invoice->id;
	 $sent_by_email   = $invoice->sent_by_email;
	 $createdby 	  = $invoice->createdby;;

// -- check if max occurance has not been reached.	 
	 if($invoiceobj->isWithinMaxOccurance($invoiceid,$max_occurrences))
	 {
		 // -- Invoice Due date added the config.
		 $InvoiceDueDate = date('Y-m-d', strtotime(' + '.$days_due.' days'));

		 switch($frequencyType)
		 {
				 case 'day':
				 // -- check if this day an invoice referecing [RecurringInvoiceId] for that day has already been created.
				 /*if(!$invoiceobj->checkInvoiceExist($invoiceid,$InvoiceDueDate,$frequencyType))
				 {
				   // -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,$createdby);

				   // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						foreach($invoice->items as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						} 
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						echo 'Invoice #: '.$idInvoice;
						$invoiceobj->updatelast_execution_date($invoiceid);
					 }
				 }		*/		 
				 break;				 
				 case 'month':
				// ONLY Active Recurring Invoices  
				if(!empty($invoice->enabled))
				{
				 // -- check if this month an invoice referecing [RecurringInvoiceId] for that month has already been created.
				 $invoiceData = $invoiceobj->checkInvoiceExist($invoice->id,$InvoiceDueDate,$frequencyType);
				 if(!$invoiceData)
				 {
				  // -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,$createdby);

				   // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						 //print_r($invoice->items->item);
						foreach($invoice->items->item as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							//print_r($invoiceItem);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						} 
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						$invoiceobj->updatelast_execution_date($invoiceid);
						array_push($html,"<td>".$idInvoice."</td>","<td>Created Invoice #: ".$idInvoice.' for Recurring Invoice# '.$invoiceid."</td></tr>");
					 }	
					else
					{
						array_push($html,"<td></td>",'<td>Something went wrong, No Invoice created #: '.$idInvoice.' for Recurring Invoice# '.$invoiceid.'</td></tr>');
					}	
				  }	
			      else
				   {
					   	array_push($html,"<td></td>",'<td>No Invoice created #: '.$idInvoice.' for Recurring Invoice# '.$invoiceData['id'].' already invoice created this month.</td></tr>');
				   }	
		         }else{ 	array_push($html,"<td></td>",'<td>No Invoice created #: '.$idInvoice.' for Recurring Invoice# '.$invoiceid.' is not active.</td></tr>');}				  
				 break;
				 case 'year':
				 // -- check if this year an invoice referecing [RecurringInvoiceId] for that year has already been created.
				 if(!$invoiceobj->checkInvoiceExist($invoice->id,$InvoiceDueDate,$frequencyType))
				 {
					// -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,$createdby);

				    // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						foreach($invoice->invoices->invoice->items as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						}
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						echo 'Invoice #: '.$idInvoice;
					 }
				 }
				 break;
		 }
	 }
	 else
	 {
		 	array_push($html,"<td></td>",'<td>isWithinMaxOccurance() - No Invoice created for Recurring Invoice #: '.$invoice->id."</td></tr>");
	 }
  }	
  else
  {
	 		array_push($html,"<td></td>",'<td>Not within date range, No Invoice created for Recurring Invoice #: '.$invoice->id."</td></tr>");
 
  }
}

				 // -- Display content
				 array_push($html,"</table></form></div></div></section></body></html>");;
					//print_r($html);
					$count = count($html);
					for($i=0;$i<$count;$i++)
					{
						echo $html[$i];
					}

} 
catch (Exception $e) 
{
    echo 'Caught exception: ' . $e->getMessage() . chr(10);
    /*echo 'Failed loading XML: ' . chr(10);
    foreach(libxml_get_errors() as $error) {
        echo '- ' . $error->message;*/
}

// -- PDF Invoice 
function pdf_invoice($sent_by_email,$idInvoice)
{
		$_SESSION['email_template'] = "email_newinvoice.php";
		$_SESSION['id'] 	   = $idInvoice;
		$_SESSION['emailFlag'] = 'Y';
		require(ROOT . 'pdf/pdf_invoice.php');
}
?>