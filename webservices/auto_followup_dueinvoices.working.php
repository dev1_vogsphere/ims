<?php
/* switch-on error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
try 
{
// -- libxml_use_internal_errors(TRUE);
/* Scheduled to run daily for daily/montlhy/yearly.*/
define('ROOT', str_replace("webservices/auto_followup_dueinvoices.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

// -- Invoice Object
$invoiceobj = new invoice();

// -- Get Tax Master Data.
$tax = new tax();

// -- Web Service.
$WebServiceURL = "https://www.vogsphere.co.za/ims/webservices/ws_invoices.php";

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// -- Frequency type.
$frequencyType  = '';

// -- Frequency.
$frequency  = '';

// -- Max occurance.
$max_occurance = 0;

// -- days in due.
$days_due = 0;

// -- Issue date.
$InvoiceDueDate = date('Y-m-d');

// -- closed Invoice = 1.
$closedInvoice = 0;

// -- Store new Invoice IDs
$idInvoice 		= 0;
$idInvoiceItems = null;

// -- Constant.
$status = 2;
$type   = 'Invoice';

/* default values */
$days_to_due 		 = 0;
$enabled			 = 0;
$max_occurrences	 = 0;
$must_occurrences 	 = 0;
$period				 = 0;
$period_type		 = '';
$starting_date		 = null;
$finishing_date		 = null;
$last_execution_date = null;
$draft  = 0;
$closed = 0;
$sent_by_email = 0;
$invoiceid = 0;
/*
1. Current date is within the date range
-------------------------------------
2. Check whether is day or month,yearly.
----------------------------------------
3. Create invoice every 1,2,3.....
add [every] from [start date] compare to [current date] 
-------------------------------------------------------
4.Max occurance 
count [recurring_id] < [Max occurance]
-----------------------------------------------
5. Create [days to due] add [days to due] to invoice date
$days_due
--------------------------------------------------------- 
*/

// And if you want to fetch multiple paymentschedule IDs:
foreach($sxml->invoice as $invoice)
{	
// -- current date.
	$today 	   = date("Y-m-d");
// -- 1. Current date is within the date range.
	$fromUser  = new DateTime($today);
	// -- starting_date
	$startDate = new DateTime($invoice->starting_date);
	// -- finishing_date
	$endDate   = new DateTime($invoice->finishing_date);
	
	// -- Invoice close Status.
	$closedInvoice = $invoice->closed;
	
	// -- InvoiceDueDate.
	$InvoiceDueDate = $invoice->due_date;

	// -- Sent by email.
	$sent_by_email = $invoice->sent_by_email;

	// - InvoiceId
	$idInvoice = $invoice->id;
	
	// -- Status 
	$status = $invoice->status;

    // -- InvoiceDueDate.
	$dueDate = new DateTime($invoice->due_date);

	if($status == 3 && $sent_by_email == 1)
	{
		// -- Check if Invoice is Due.
		if($invoiceobj->isPastDueDate($fromUser,$dueDate))
		{
			// -- Send follow up email of an Invoice to a client.
			pdf_invoice($sent_by_email,$idInvoice);
			echo 'Follow up email sent for Invoice #: '.$invoice->id.' due date in past to '.$invoice->customer_email."<br/>";
		}
	}
 }
} 
catch (Exception $e) 
{
    echo 'Caught exception: ' . $e->getMessage() . chr(10);
    /*echo 'Failed loading XML: ' . chr(10);
    foreach(libxml_get_errors() as $error) {
        echo '- ' . $error->message;*/
}

// -- PDF Invoice 
function pdf_invoice($sent_by_email,$idInvoice)
{
		$_SESSION['id'] 	   = $idInvoice;
		$_SESSION['emailFlag'] = 'Y';
		require(ROOT . 'pdf/pdf_invoice.php');
}
?>