<?php
/* switch-on error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
try 
{
// -- libxml_use_internal_errors(TRUE);
/* Scheduled to run daily for daily/montlhy/yearly.*/
define('ROOT', str_replace("webservices/generateinvoices.php", "", $_SERVER["SCRIPT_FILENAME"]));
require(ROOT . '/core/Model.php');
require(ROOT . '/config/db.php');
require(ROOT . '/models/invoice.php');
require(ROOT . '/models/tax.php');

// -- Invoice Object
$invoiceobj = new invoice();

// -- Get Tax Master Data.
$tax = new tax();

// -- Web Service.
$WebServiceURL = "https://www.vogsphere.co.za/ims/webservices/ws_recurringinvoice.php";

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// -- Frequency type.
$frequencyType  = '';

// -- Frequency.
$frequency  = '';

// -- Max occurance.
$max_occurance = 0;

// -- days in due.
$days_due = 0;

// -- Issue date.
$InvoiceDueDate = date('Y-m-d');

// -- Store new Invoice IDs
$idInvoice 		= null;
$idInvoiceItems = null;

// -- Constant.
$status = 2;
$type   = 'Invoice';

/* default values */
$days_to_due 		 = 0;
$enabled			 = 0;
$max_occurrences	 = 0;
$must_occurrences 	 = 0;
$period				 = 0;
$period_type		 = '';
$starting_date		 = null;
$finishing_date		 = null;
$last_execution_date = null;
$draft  = 0;
$closed = 0;
$sent_by_email = 0;
$invoiceid = 0;
/*
1. Current date is within the date range
-------------------------------------
2. Check whether is day or month,yearly.
----------------------------------------
3. Create invoice every 1,2,3.....
add [every] from [start date] compare to [current date] 
-------------------------------------------------------
4.Max occurance 
count [recurring_id] < [Max occurance]
-----------------------------------------------
5. Create [days to due] add [days to due] to invoice date
$days_due
--------------------------------------------------------- 
*/

// And if you want to fetch multiple paymentschedule IDs:
foreach($sxml->invoice as $invoice)
{	
// -- current date.
	$today 	   = date("Y-m-d");
// -- 1. Current date is within the date range.
	$fromUser  = new DateTime($today);
	// -- starting_date
	$startDate = new DateTime($invoice->starting_date);
	// -- finishing_date
	$endDate   = new DateTime($invoice->finishing_date);
	
	if($invoiceobj->isDateBetweenDates($fromUser , $startDate, $endDate))
	{
// -- 2. Check whether is day or month,yearly.
	 $frequencyType  = $invoice->period_type;	
	 
// -- 3. Check the frequency.
	 $frequency = $invoice->period;

// -- 4. Max Occurance.
	 $max_occurance = $invoice->max_occurrences;

// -- 5. Create [days to due] add [days to due] to invoice date
// -- Invoice Due date after certain date.
	 $days_due = $invoice->days_to_due;
	 
	 $max_occurrences = $invoice->max_occurrences;
	 $invoiceid 	  = $invoice->id;
	 $sent_by_email   = $invoice->sent_by_email;
	 
// -- check if max occurance has not been reached.	 
	 if($invoiceobj->isWithinMaxOccurance($invoiceid,$max_occurrences))
	 {
		 // -- Invoice Due date added the config.
		 $InvoiceDueDate = date('Y-m-d', strtotime(' + '.$days_due.' days'));

		 switch($frequencyType)
		 {
				 case 'day':
				 // -- check if this day an invoice referecing [RecurringInvoiceId] for that day has already been created.
				 if(!$invoiceobj->checkInvoiceExist($invoiceid,$InvoiceDueDate,$frequencyType))
				 {
				   // -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,'','');

				   // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						foreach($invoice->items->item as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						} 
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						echo 'Invoice #: '.$idInvoice;
					 }
				 }				 
				 break;				 
				 case 'month':
				 // -- check if this month an invoice referecing [RecurringInvoiceId] for that month has already been created.
				 if(!$invoiceobj->checkInvoiceExist($invoice->id,$InvoiceDueDate,$frequencyType))
				 {
				  // -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,'','');

				   // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						foreach($invoice->invoices->invoice->items as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						} 
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						echo 'Invoice #: '.$idInvoice;
					 }				 
				  }				 
				 break;
				 case 'year':
				 // -- check if this year an invoice referecing [RecurringInvoiceId] for that year has already been created.
				 if(!$invoiceobj->checkInvoiceExist($invoice->id,$InvoiceDueDate,$frequencyType))
				 {
					// -- create an invoice.
					$idInvoice = $invoiceobj->create($invoice->series_id, $invoice->customer_id,$invoice->customer_name,
							 $invoice->customer_identification,$invoice->customer_email,
							 $invoice->invoicing_address,$invoice->shipping_address,$invoice->customer_phone,$invoice->contact_person,
							 $invoice->terms,$invoice->notes,$invoice->base_amount,$invoice->discount_amount,$invoice->net_amount,$invoice->gross_amount,$invoice->paid_amount,$invoice->tax_amount,
							 $status,$type,$draft,$closed,$sent_by_email,$invoice->invoiceItemsCount,$invoice->id,
							 date('Y-m-d'),$InvoiceDueDate,$days_due,$enabled,$max_occurrences,$must_occurrences,$period,
							 $period_type,$starting_date,$finishing_date,$last_execution_date,'','');

				    // -- Create Invoice Items.
					 if ($idInvoice > 0)
					 {
						foreach($invoice->invoices->invoice->items as $invoiceItem)
						{	
							$idInvoiceItems = $invoiceobj->createItems($invoiceItem->quantity,$invoiceItem->discount,$idInvoice,
							$invoiceItem->product_id,$invoiceItem->description,$invoiceItem->unitary_cost);
							// -- Create Invoice Tax Items.	
							if($invoiceItem->tax_id != 0)
							{
								$idItemTaxid = $tax->createItemTax($idInvoiceItems['id'],$invoiceItem->tax_id);
							}
						}
						// -- BOC email Invoice for a client, if sent_by_email is selected.
						if($sent_by_email != 0)
						{
							 pdf_invoice($sent_by_email,$idInvoice);
						}
						// -- EOC email Invoice for a client, if sent_by_email is selected.
						echo 'Invoice #: '.$idInvoice;
					 }
				 }
				 break;
		 }
	 }
	 else
	 {
			echo 'isWithinMaxOccurance() - No Invoice created for Recurring Invoice #: '.$invoice->id;
	 }
  }	
  else
  {
	 		echo 'isDateBetweenDates() - No Invoice created for Recurring Invoice #: '.$invoice->id;
 
  }
}

} 
catch (Exception $e) 
{
    echo 'Caught exception: ' . $e->getMessage() . chr(10);
    /*echo 'Failed loading XML: ' . chr(10);
    foreach(libxml_get_errors() as $error) {
        echo '- ' . $error->message;*/
}

// -- PDF Invoice 
function pdf_invoice($sent_by_email,$idInvoice)
{
		$_SESSION['id'] 	   = $idInvoice;
		$_SESSION['emailFlag'] = 'Y';
		require(ROOT . 'pdf/pdf_invoice.php');
}
?>