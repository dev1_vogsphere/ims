<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php 							
//------------------------------------------------------------------- 
//           				BOC - GLOBAL DATA						-
//-------------------------------------------------------------------
$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$password 		= '';
$idnumber 		= '';
$website = '';
$logo = '';
$phone = '';
$fax = '';
$hashUrl = '';
$adminemail = '';
$companyname = '';

// ====================== Global Settings =========================== //
$GlobalCompany  = '';
$Globalphone    = '';
$Globalfax     = '';
$Globalemail    = '';
$Globalwebsite  = '';
$Globallogo     = '';
$GloballogoTemp = '';
$Globalcurrency = ''; 
$Globallegalterms = '';

$Globalstreet = '';
$Globalsuburb = '';
$Globalcity = '';
$Globalstate = '';
$Globalpostcode = '';
$Globalregistrationnumber = '';
$Globaladdress = '';
$ApplicationId = '';
// -- BOC Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = "";
$AppIdEncoded = "";
$uploadURL = "";
$rejectreason = "";
$rejectreasonHTML = "";

// -- EOC Encrypt 16.09.2017 - Parameter Data.
// ======================= EOC Global Settings ====================== //

if(isset($_SESSION))
{	
$Title 			= $_SESSION['Title'];
$FirstName 		= $_SESSION['FirstName'];
$LastName 		= $_SESSION['LastName'];
$password 		= $_SESSION['password'];
$idnumber 		= $_SESSION['idnumber'];
$adminemail = $_SESSION['adminemail'];
$companyname = $_SESSION['companyname'];
$phone = $_SESSION['phone'];
$fax = $_SESSION['fax'];
$website = $_SESSION['website'];
$idEncoded = $idnumber;
$hashUrl = $website.'/resetpassword?userid='.$idEncoded.'&hash='.$_SESSION['hashUrl'];

// ====================== Global Settings =========================== //
$Globalphone    = 		$_SESSION['Globalphone'];
$Globalfax     = 		$_SESSION['Globalfax'];
$Globalemail    = 		$_SESSION['Globalemail'];
$Globalwebsite  = 		$_SESSION['Globalwebsite']; 
$GloballogoTemp = 		$_SESSION['GloballogoTemp'];
$Globalcurrency = 		$_SESSION['Globalcurrency']; 
$Globallegalterms = 	$_SESSION['Globallegalterms'];

$Globalstreet = 		$_SESSION['Globalstreet'];
$Globalsuburb = 		$_SESSION['Globalsuburb'];
$Globalcity =			$_SESSION['Globalcity'];
$Globalstate = 			$_SESSION['Globalstate'];
$Globalpostcode = 		$_SESSION['Globalpostcode'];	
$Globalregistrationnumber = 		$_SESSION['Globalregistrationnumber'];
$Globalphone = $Globalphone.'/'.$Globalfax;
$Globaladdress = $companyname.'<br/>Phone :'.$Globalphone.'| Mail :'.$Globalemail.'| Web :'.$website;
$logo = $_SESSION['logo'];
$ApplicationId = $_SESSION['ApplicationId'];

$CustomerIdEncoded =  urlencode(base64_encode($idnumber));
$AppIdEncoded = urlencode(base64_encode($ApplicationId));

$uploadURL = $website.'/upload?customerid='.$CustomerIdEncoded.'&ApplicationId='.$AppIdEncoded;

$rejectreason = $_SESSION['rejectreason'];

if(!empty($rejectreason))
{
	$rejectreason = "<ul><li>".str_replace(".","</li><li>", $rejectreason)."</li></ul>";
}

/* --- BOC Production or Development */
// -- Email Server Hosting -- //
$Globalmailhost = $_SESSION['Globalmailhost'];
$Globalport = $_SESSION['Globalport'];
$Globalmailusername = $_SESSION['Globalmailusername'];
$Globalmailpassword = $_SESSION['Globalmailpassword'];
$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
$Globaldev = $_SESSION['Globaldev'];
$Globalprod = $_SESSION['Globalprod'];		
// -- Email Server Hosting -- //
	
/*	Production - Live System */
if($Globalprod == 'p')
{
	
}
/*	Development - Testing System */	
else
{
	$website = "http://www.vogsphere.co.za/eloan";	
	$logo 	 = "ecashmeup.png";
}
/* --- ECO Production or Development */


// ======================= EOC Global Settings ====================== //
}

  /*echo "<h3> PHP List All Session Variables</h3>";
    foreach ($_SESSION as $key=>$val)
    echo $key." ".$val."<br/>";*/

//------------------------------------------------------------------- 
//           				EOC - GLOBAL DATA						-		-
//-------------------------------------------------------------------
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Favicon -->
	<link href="<?php if(!empty($website)){echo $website."/assets/img/Icon_vogs.ico";}else{}?>" rel="shortcut icon">	
	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>
<body bgcolor="#f0f0f0" width="100%" style="margin: 0;">
    <center style="width: 100%; background: ##f0f0f0;">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
        </table>
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            <tr>
				<td bgcolor="#f0f0f0">
					<?php echo "<img src='".$website.'/images/'.$logo."' width='600' height='' alt='".$website.'/images/'.$logo."' border='0' align='center' style='width: 100%; max-width: 600px;'/>";?>
				</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
<?php 					
//-------------------------------------------------------------------// 
//           				CUSTOMER DATA							-//
//-------------------------------------------------------------------//
$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$password 		= '';
									
// Id number
$idnumber 		= '';
$clientname 	= $Title.''.$FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= '';	
// Email
$email 			= '';	
// Street
$Street 		= '';	
// Suburb
$Suburb 		= '';	
// City
$City 			= '';	
// State
$State 			= '';	
// PostCode
$PostCode 		= '';	
// Dob
$Dob 			= '';	
// phone
$phone 			= '';	

$ApplicationId = '';
	
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
$loandate 		= '';	
$loanamount 	= '0.00';	
$loanpaymentamount = '0.00';	
$loanpaymentInterest = "";
$FirstPaymentDate = '';	
$LastPaymentDate  = '';	
$applicationdate  = '';	
$status 		  = '';	
$monthlypayment = '0.00';// Monthly Payment Amount 
$LoanDuration = '0';

if(isset($_SESSION))
{	
// -- BOC 27.01.2017
$personalloan = $_SESSION['personalloan'];
$loantype = $_SESSION['loantypedesc'];
$loanpaymentInterest = $personalloan."%";
// -- EOC 27.01.2017

$Title 			= $_SESSION['Title'];
$FirstName 		= $_SESSION['FirstName'];
$LastName 		= $_SESSION['LastName'];
$password 		= $_SESSION['password'];
									
// Id number
$idnumber 		= $_SESSION['idnumber'];
$clientname 	= $Title.''.$FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= $_SESSION['phone'];	
// Email
$email 			= $_SESSION['email'];	
// Street
$Street 		= $_SESSION['Street'];	
// Suburb
$Suburb 		= $_SESSION['Suburb'];	
// City
$City 			= $_SESSION['City'];	
// State
$State 			= $_SESSION['State'];	
// PostCode
$PostCode 		= $_SESSION['PostCode'];	
// Dob
$Dob 			= $_SESSION['Dob'];	
// phone
$phone 			= $_SESSION['phone'];		
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
$loandate 		= $_SESSION['loandate'];	
$loanamount 	= $_SESSION['loanamount'];	
$loanpaymentamount = $_SESSION['loanpaymentamount'];	
$loanpaymentInterest = "20%";
$FirstPaymentDate = $_SESSION['FirstPaymentDate'];	
$LastPaymentDate  = $_SESSION['LastPaymentDate'];	
$applicationdate  = $_SESSION['applicationdate'];	
$status 		  = $_SESSION['status'];	
$ApplicationId    =  $_SESSION['ApplicationId'];

if($status == 'REJ')
// Rejected
{
	$status  = 'Rejected';
}
// Approved
if($status == 'APPR')
{
	$status  = 'Approved';
}

$monthlypayment = $_SESSION['monthlypayment'];// Monthly Payment Amount 
$LoanDuration = $_SESSION['LoanDuration'];// Loan Duration

}

//-------------------------------------------------------------------// 
//           				COMPANY DATA							-//
//-------------------------------------------------------------------//
	echo "<h3>Dear $Title $FirstName $LastName </h3>
		  <p>Your $companyname loan request <b>$ApplicationId </b> rejected due rejection reasons below.</p>
		  <p>$rejectreason</p>
		  <p>We advise you rectify above before submitting  another loan request with us.</p>	
		  <p>
		  Should you have any queries or require any further information, please contact us on $phone/$fax.</br>or email us on $adminemail.
		  </p>";															;
?>
						<p>Thank  you</p>
						<p><b>Regards</b></p>
						<p><?php  echo $Globaladdress; ?></p>
                </td>
            </tr>
        </table>
    </center>
</body>
</html>

